# Gunk (Work in progress)
A realtime strategy game with day-night cycle, resources gathering and 
large-scale battles. Project done in Unity Engine, written in C# and Hlsl(for Shaders).

# Units
There are three types of units: battle units, welding bots that can repair other
units and workers that can mine Crystals. Crystals are used for construction of units
and buildings. 
![alt text](Pictures/Gunk1.png)
Workers mining crystals around crystal Field.
# Buildings
Player can place buildings that are connected in energy networks. Every building
can only be placed near connector to connect to existing network.From graph theory
point of view energy networks are trees. They repair themselves is possible if 
for example a connector is destroyed, but there is another connector that 
can reconnect the network There are two types of connectors.
![alt text](Pictures/Gunk3.png)
The one on the left is something like the root of the tree, player gets one at the 
beginning of the game and another one cannot be constructed. It is also the main 
building of the player and it is Game Over when it id destroyed. It also produces
light at night, precisely this large white shiny ball on its top. The building
on the right is light tower, Its main purpose is to emit light at night. 
Other types of buildings are for instance Energy generators, defensive towers 
or regular connectors that can be constructed by player to expand his network.
All 3D models of units and buildings were designed and modelled by me

# Day-Night cycle and Gunk 
The game has it's pace, and after some time it always gets dark. Player has to 
construct light towers in order to see anything. Player main nemesis is a purple 
substance called simply gunk. It spreads by night and also produced hostile 
creatures that attack player. 
![alt text](Pictures/Gunk6.png)

One of the creatures created by gunk, during day and night. It really gets dark 
and without light towers player is definitely lost. Hordes of such small beast will
try to destroy every source of light during every night. There is also purple gunk
that swallows land piece by piece and that spreads rapidly as soon as it gets dark
![alt text](Pictures/Gunk2.png)
That purple goo is updated realtime and spreads really fast and the only way to defend
against it during night is to build special defensive towers.
# How to survive?
Expand your base! Game allows you to place hundreds of buildings and construct 
huge defense systems. Expansion and exploration also allows you to get to more distant 
sources of crystals
![alt text](Pictures/Gunk7.png)