using System;

namespace Gunk_Scripts.Asset_Importing
{
    [Serializable]
    public enum AssetSource
    {
        GunkNative,
        LowPolyPack,
        LowPolyLandscape
    }
}