﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using Gunk_Scripts.Shader_Constants;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Gunk_Scripts.Asset_Importing
{
    public class DefaultModelProcessor:IModelProcessor
    {
        private const string ASSET_SAVE_PATH = "Assets/Prefabs";

        private string generatePrefabSavePath(string objectName)
        {
            return ASSET_SAVE_PATH + "/"+generatePrefabName(objectName)+".prefab";
        }

        private string generateMaterialSavePath(string objectName)
        {
            return ASSET_SAVE_PATH + "/" + "Prefab Materials" + "/" + generateMaterialName(objectName)+".mat";
        }

        private string generatePrefabName(string objectName)
        {
            return objectName;
        }

        private string generateMaterialName(string objectName)
        {
            return objectName + "_material";
        }

        /// <summary>
        /// prcesses provides resources and creates prefabs based on them
        /// </summary>
        /// <param name="models">meshes available fro processor</param>
        /// <param name="textures">textures available for processor</param>
        /// <returns></returns>
        public List<PrefabAssets> process(List<Mesh> models, List<Texture2D> textures)
        {
            var result = new List<PrefabAssets>();
            StopWatch meshParamsStopWatch = new StopWatch(true);

            var time = DateTime.Now;
            List<string> modelsSuffixes = new List<string> { "skeleton", "lod", "lod0", "lod1", "lod2", "lod3" };
            Dictionary<string, List<Mesh>> meshesDict =
                PrefabImporterUtils.divideMeshesBySuffixes(models, modelsSuffixes);

            foreach (var modelsMeshes in meshesDict)
            {
                string objectName = modelsMeshes.Key;
                Debug.Log("Started importing "+objectName);
                foreach (var myMesh in meshesDict[objectName])
                {
                    meshParamsStopWatch.resume();
                    PrefabImporterUtils.applyMeshParameters((Mesh)myMesh);
                    meshParamsStopWatch.stop();
                }

                GameObject prefabCopy = new GameObject();

                //Setting prefab and GameObject names
                prefabCopy.name = objectName;

                //Creating material
                var myMat = PrefabImporterUtils.createMaterial(generateMaterialName(objectName), ToonStandardConstants.shaderName, new List<string> { "FBX_ENABLED" });
                AssetDatabase.CreateAsset(myMat,generateMaterialSavePath(objectName));

                //Finding textures
                string mainTexFileName = String.Concat(objectName, "_alb");
                string oclTexFileName = String.Concat(objectName, "_ocl");

                Texture2D mainTex = PrefabImporterUtils.findObjectWithName(mainTexFileName, textures);
                Texture2D oclTex = PrefabImporterUtils.findObjectWithName(oclTexFileName, textures);

                Debug.Log(mainTex != null ? "found main texture" : "no main texture found");
                Debug.Log(oclTex != null ? "found occlusion texture" : "no occlusion texture found");

                string mainTexProperty = "_MainTex";
                string oclTexProperty = "_Occlusion";

                //Adding potentially missing material textures
                if (mainTex != null)
                {
                    PrefabImporterUtils.applyTextureParameters(mainTex, false, FilterMode.Point);
                    myMat.SetTexture(mainTexProperty, mainTex);
                }
                if (oclTex != null)
                {
                    PrefabImporterUtils.applyTextureParameters(oclTex, true, FilterMode.Trilinear);
                    myMat.SetTexture(oclTexProperty, oclTex);
                }

                //Creating LOD Children and updating LODGroup
                var lodDatas = new List<PrefabImporterUtils.LodData>();
                var lod0Mesh = PrefabImporterUtils.findObjectWithoutSuffix(meshesDict[objectName], modelsSuffixes);
                lodDatas.Add(new PrefabImporterUtils.LodData (lod0Mesh, myMat));
                int maxLod = 4;
                for (int i = 1; i < maxLod; i++)
                {
                    string lodExtension = "lod" + (char)('0' + i);
                    var mesh = PrefabImporterUtils.findObjectWithSuffix(meshesDict[objectName], lodExtension);
                    if (mesh != null)
                    {
                        lodDatas.Add(new PrefabImporterUtils.LodData(mesh,myMat));
                    }
                }

                Debug.Log("added " + lodDatas.Count + " LOD objects");

                PrefabImporterUtils.installRenderersWithLod(lodDatas, prefabCopy);
                var prefab = new PrefabAssets();
                //TODO Add miniature generation

                //Setting skeleton mesh
                Mesh skeleton = PrefabImporterUtils.findObjectWithSuffix(meshesDict[objectName], " skeleton");
                prefab.meshSkeleton = skeleton;
                Debug.Log(skeleton != null ? "found skeleton mesh" : "no skeleton mesh found");
#if UNITY_EDITOR
                prefab.go = PrefabUtility.CreatePrefab(generatePrefabSavePath(objectName), prefabCopy);
#endif
                Object.DestroyImmediate(prefabCopy);
                result.Add(prefab);
            }

            float timeElapsedSec = (float)(DateTime.Now - time).TotalSeconds;
            Debug.Log("Reimporting " + meshesDict.Count + " prefabs took " + PrettyPrinter.reformatFloat(timeElapsedSec) + " seconds, on average " + PrettyPrinter.reformatFloat(timeElapsedSec / meshesDict.Count) + " seconds per prefab");
            return result;
        }
    }
}
#endif