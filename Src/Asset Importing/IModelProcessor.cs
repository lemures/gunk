﻿using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.Asset_Importing
{
    public interface IModelProcessor
    {
        List<PrefabAssets> process(List<Mesh> meshes, List<Texture2D> textures);
    }
}
