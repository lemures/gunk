﻿using System.Collections.Generic;
using Gunk_Scripts.SkeletonAnimations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Gunk_Scripts.Asset_Importing
{
    public interface IPrefabManager
    {
        IEnumerable<Object> getAllPrefabsUnityReferences();

        /// <summary>
        /// Creates prefab based on prefab name
        /// </summary>
        void addPrefab(GameObject prefab);

        void setSkeletonMesh(string prefabName, Mesh meshSkeleton);

        void deletePrefab(string prefabName);
        Prefab findPrefab(string prefabName);
        List<string> getAllPrefabNames();
    }
}