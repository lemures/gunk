#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using Gunk_Scripts.Shader_Constants;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Gunk_Scripts.Asset_Importing
{
    public class LowPolyModelProcessor : IModelProcessor
    {
        private const string ASSET_SAVE_PATH = "Assets/Prefabs/Low Poly Prefabs";

        private string generatePrefabSavePath(string objectName)
        {
            return ASSET_SAVE_PATH + "/"+generatePrefabName(objectName)+".prefab";
        }

        private string generateMaterialSavePath(string objectName)
        {
            return ASSET_SAVE_PATH + "/" + "Low Poly Prefab Materials" + "/" + generateMaterialName(objectName)+".mat";
        }

        private string generatePrefabName(string objectName)
        {
            return objectName;
        }

        private string generateMaterialName(string objectName)
        {
            return objectName + "_material";
        }

        public List<PrefabAssets> process(List<Mesh> models, List<Texture2D> textures)
        {
            var result = new List<PrefabAssets>();
            var modelsSuffixes = new List<string>() {"lod0", "lod1", "lod2", "collider"};

            var time = DateTime.Now;
            var meshParamsStopWatch = new StopWatch(true);
            var meshesDict = PrefabImporterUtils.divideMeshesBySuffixes(models, modelsSuffixes);

            var myMat = PrefabImporterUtils.createMaterial(generateMaterialName(textures[0].name), ToonStandardConstants.shaderName, new List<string> { "FBX_ENABLED" });
            myMat.mainTexture = textures[0];
            AssetDatabase.CreateAsset(myMat,generateMaterialSavePath(textures[0].name));
            foreach (var modelsMeshes in meshesDict)
            {
                var objectName = modelsMeshes.Key;
                Debug.Log("Started importing "+objectName);
                foreach (var myMesh in meshesDict[objectName])
                {
                    meshParamsStopWatch.resume();
                    PrefabImporterUtils.applyMeshParameters((Mesh)myMesh);
                    meshParamsStopWatch.stop();
                }

                GameObject prefabCopy = new GameObject();

                //Setting prefab and GameObject names
                prefabCopy.name = objectName;


                //Creating LOD Children and updating LODGroup
                var lodDatas = new List<PrefabImporterUtils.LodData>();
                var lod0Mesh = PrefabImporterUtils.findObjectWithoutSuffix(meshesDict[objectName], modelsSuffixes);
                lodDatas.Add(new PrefabImporterUtils.LodData (lod0Mesh, myMat));
                int maxLod = 4;
                for (int i = 1; i < maxLod; i++)
                {
                    string lodExtension = "lod" + (char)('0' + i);
                    var mesh = PrefabImporterUtils.findObjectWithSuffix(meshesDict[objectName], lodExtension);
                    if (mesh != null)
                    {
                        lodDatas.Add(new PrefabImporterUtils.LodData(mesh,myMat));
                    }
                }

                Debug.Log("added " + lodDatas.Count + " LOD objects");

                PrefabImporterUtils.installRenderersWithLod(lodDatas, prefabCopy);
                var prefab = new PrefabAssets();
                prefab.assetSource = AssetSource.LowPolyPack;

#if UNITY_EDITOR
                prefab.go = PrefabUtility.CreatePrefab(generatePrefabSavePath(objectName), prefabCopy);
#endif
                Object.DestroyImmediate(prefabCopy);
                result.Add(prefab);
            }

            float timeElapsedSec = (float)(DateTime.Now - time).TotalSeconds;
            Debug.Log("Reimporting " + meshesDict.Count + " prefabs took " + PrettyPrinter.reformatFloat(timeElapsedSec) + " seconds, on average " + PrettyPrinter.reformatFloat(timeElapsedSec / meshesDict.Count) + " seconds per prefab");
            return result;
        }
    }
}
#endif