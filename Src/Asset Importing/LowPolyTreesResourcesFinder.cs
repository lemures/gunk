using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Gunk_Scripts.Asset_Importing
{
    public class LowPolyTreesResourcesFinder
    {
        private static string meshesFolder = "Assets\\Used Assets\\Low Poly Trees Pack\\Tree Assets\\Meshes";
        private static string texturesFolder = "Assets\\Used Assets\\Low Poly Trees Pack\\Tree Assets\\Texture";

        public static List<Mesh> findAllMeshes()
        {
            return (from asset in AssetUtils.findAllAssetsOfType(typeof(Mesh), meshesFolder) select (asset as Mesh)).ToList();
        }

        public static List<Texture2D> findAllTextures()
        {
            return (from asset in AssetUtils.findAllAssetsOfType(typeof(Texture2D), texturesFolder) select (asset as Texture2D)).ToList();
        }
    }
}