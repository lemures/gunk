﻿using Gunk_Scripts.SkeletonAnimations;
using UnityEngine;

namespace Gunk_Scripts.Asset_Importing
{
    [System.Serializable]
    public class Prefab
    {
        public string name;
        public GameObject go;
        public Mesh meshSkeleton;
        public Texture2D miniature;
        public Prefab(GameObject myGo)
        {
            go = myGo;
            name = go.name;
        }
    }
}