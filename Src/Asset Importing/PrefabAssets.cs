﻿using UnityEngine;

namespace Gunk_Scripts.Asset_Importing
{
    [System.Serializable]
    public class PrefabAssets
    {
        public GameObject go;
        public Mesh meshSkeleton;
        public Texture2D miniature;
        public AssetSource assetSource;
    }
}