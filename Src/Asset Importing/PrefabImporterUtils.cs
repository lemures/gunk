﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Gunk_Scripts.Asset_Importing
{
    public class PrefabImporterUtils
    {
        public class LodData
        {
            public Mesh mesh;
            public Material material;

            public LodData(Mesh mesh, Material material)
            {
                this.mesh = mesh;
                this.material = material;
            }
        }

        /// <summary>
        /// returns string with all capital letters replaced with lower counterparts and replacing '_' with ' '
        /// </summary>
        public static string normalizeName(string name)
        {
            name = name?.ToLower();
            name = name?.Replace('_', ' ');
            return name;
        }

        public static bool hasNameSuffix(string name, string suffix) => (suffix.Length <= name.Length && (normalizeName(name).Contains(normalizeName(suffix))));

        public static string removeNameSuffix(string name, List<string> suffixes)
        {
            name = name.Replace(MeshFbxUtils.suffix,"");
            string suffix = suffixes.Find(x => hasNameSuffix(name, x));
            var result= suffix != null ? name.Substring(0, -1 + name.Length - suffix.Length) : name;
            if (result[result.Length - 1] == '_' || result[result.Length - 1] == ' ')
            {
                result = result.Substring(0, result.Length - 1);
            }

            return result;
        }

        public static T findObjectWithSuffix<T>(List<T> objects, string suffix) where T : Object
            => objects.Find(myObject => hasNameSuffix(myObject.name, suffix));

        public static T findObjectWithoutSuffix<T>(List<T> objects, List<string> suffixes) where T : Object
            => objects.Find(x => findObjectNameSuffix(x, suffixes) == "");

        public static string findObjectNameSuffix(Object myObject, List<string> suffixes)
            => myObject.name.Substring(removeNameSuffix(myObject.name, suffixes).Length);

        /// <summary>
        /// Creates new Material with a given name and appropriate settings
        /// </summary>
        /// <param name="materialName">name to assign to new material</param>
        /// <param name="shaderName">name of shader to use</param>
        /// <param name="keywords">list of keyword to enable in material's shader</param>
        /// <returns></returns>
        public static Material createMaterial(string materialName, string shaderName, List<string> keywords)
        {
            var myMat = new Material(Shader.Find(shaderName)) {enableInstancing = true};
            keywords.ForEach(myMat.EnableKeyword);
            return myMat;
        }

        /// <summary>
        /// Finds all objects with a certain name, treating '_' as ' '. The search is not case sensitive
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="objectsToSearch"></param>
        /// <returns></returns>
        public static T findObjectWithName<T>(string name, List<T> objectsToSearch) where T : Object
            => objectsToSearch.Find(x => normalizeName(x.name) == normalizeName(name));


        /// <summary>
        /// Creates LODGroup in gameObject and adds number of children to that object with MeshRenderers and MeshFilters
        /// </summary>
        /// <param name="meshes"></param>
        /// <param name="materials"></param>
        /// <param name="gameObject"></param>
        public static void installRenderersWithLod(List<LodData> lodDatas, GameObject gameObject)
        {
            if (gameObject.GetComponent<LODGroup>() == null)
                gameObject.AddComponent<LODGroup>();
            gameObject.GetComponent<LODGroup>().animateCrossFading = true;

            var lods = new LOD[lodDatas.Count];

            for (int i = 0; i < lodDatas.Count; i++)
            {
                var lodObject = new GameObject("LOD " + i);
                var meshFilter = lodObject.AddComponent<MeshFilter>();
                meshFilter.mesh = lodDatas[i].mesh;
                var meshRenderer = lodObject.AddComponent<MeshRenderer>();
                meshRenderer.material = lodDatas[i].material;
                lodObject.transform.parent = gameObject.transform;
                lods[i].renderers = new Renderer[] { gameObject.transform.GetChild(i).GetComponent<MeshRenderer>() };
                lods[i].screenRelativeTransitionHeight = (lods.Length - i - 1) / ((float)lods.Length) * 0.99f + 0.01f;
                lods[i].fadeTransitionWidth = 0.1f;
            }

            gameObject.GetComponent<LODGroup>().SetLODs(lods);
        }

        /// <summary>
        /// Crunches texture, sets filtering mode and enables/disables mipMaps if needed, costs much nut only if it really changes anything
        /// </summary>
        /// <param name="tex"></param>
        /// <param name="needsMipmap"></param>
        /// <param name="filterMode"></param>
        public static void applyTextureParameters([NotNull] Texture2D tex, bool needsMipmap, FilterMode filterMode)
        {
#if UNITY_EDITOR
            var importer = (TextureImporter)AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(tex));

            //reimporting resources is very expensive
            if (importer.mipmapEnabled != needsMipmap ||
                importer.crunchedCompression == false ||
                importer.filterMode != filterMode)
            {
                importer.mipmapEnabled = needsMipmap;
                importer.crunchedCompression = true;
                importer.compressionQuality = 50;
                importer.filterMode = filterMode;
                importer.SaveAndReimport();
            }
#endif
        }

        /// <summary>
        /// Loads optimal mesh import values
        /// </summary>
        /// <param name="myMesh"></param>
        public static void applyMeshParameters([NotNull] Mesh myMesh)
        {
#if UNITY_EDITOR
            ModelImporter myImporter = (ModelImporter)AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(myMesh));
            myImporter.importMaterials = false;
            myImporter.importNormals = ModelImporterNormals.Calculate;
            myImporter.normalSmoothingAngle = 90.0f;
            myImporter.weldVertices = true;
            myImporter.materialLocation = ModelImporterMaterialLocation.InPrefab;
            //TODO enable scale Slider of a mesh
            //my_importer.globalScale = object_scale;
            try
            {
                myImporter.SaveAndReimport();
            }
            catch (Exception e)
            {
                Debug.Log(myMesh.name + " mesh was unsuccessfully re-imported");
            }
#endif
        }

        public static Dictionary<string, List<Mesh>> divideMeshesBySuffixes(IEnumerable<Mesh>models, List<String>modelsSuffixes)
        {
            Dictionary<string, List<Mesh>> meshesDict = new Dictionary<string, List<Mesh>>();
            foreach (var model in models)
            {
                string name = model.name;
                name = PrefabImporterUtils.normalizeName(name);
                name = PrefabImporterUtils.removeNameSuffix(name, modelsSuffixes);
                List<Mesh> modelMeshes;
                meshesDict.TryGetValue(name, out modelMeshes);
                if (modelMeshes == null)
                {
                    modelMeshes = new List<Mesh>();
                }
                modelMeshes.Add(model);
                meshesDict[name] = modelMeshes;
            }

            return meshesDict;
        }
    }
}
#endif