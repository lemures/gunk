﻿using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.Asset_Importing
{
    public class PrefabManagerHelper : ScriptableObject
    {
        public List<Prefab> allPrefabs;
    }
}

