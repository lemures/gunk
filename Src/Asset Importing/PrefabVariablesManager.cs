﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Gunk_Scripts.Asset_Importing
{
    public static class PrefabVariablesManager
    {
        private static string prefabVariablesName = "prefabVariables";

        public static bool hasVariable(string prefabName, string variableName) => getVariable(prefabName, variableName) != null;

        public static object getVariable(string prefabName, string variableName)
        {
            string myName = getPropertyName(prefabName, variableName);
            return prefabVariables.ContainsKey(myName) ? prefabVariables[myName] : null;
        }

        private static bool isSerializable(object obj)
        {
            return ((obj is ISerializable) || (Attribute.IsDefined(obj.GetType(), typeof(SerializableAttribute))));
        }

        public static void saveVariable(string prefabName, string variableName, object value)
        {
            if (!isSerializable(value))
            {
                throw new System.NotSupportedException("you cannot save object that is not serializable");
            }
            prefabVariables.Remove(getPropertyName(prefabName, variableName));
            prefabVariables.Add(getPropertyName(prefabName, variableName), value);
            DatabasesManager.saveObject(prefabVariables, prefabVariablesName);
        }

        private static Dictionary<string, object> prefabVariablesInternal;

        private static Dictionary<string,object> prefabVariables
        {
            get
            {
                if (prefabVariablesInternal == null)
                {
                    prefabVariablesInternal =
                        DatabasesManager.getObjectLatestVersion(prefabVariablesName) as Dictionary<string, object>;
                    if (prefabVariablesInternal == null)
                    {
                        prefabVariablesInternal = new Dictionary<string, object>();
                        DatabasesManager.saveObject(prefabVariablesInternal, prefabVariablesName);
                    }
                }

                return prefabVariablesInternal;
            }
        }

        private static string getPropertyName(string prefabName, string variableName)
        {
            return prefabName + "_______" + variableName;
        }

    }
}
