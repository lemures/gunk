﻿using UnityEngine;

namespace Gunk_Scripts.Camera_Behavoiur
{
    [System.Serializable]
    public class CameraController
    {
        protected Vector3 currentPosition;
        protected Vector3 currentRotation;
        protected Transform transform;
        public string name;

        public virtual void setAsActive()
        {
            transform.position = currentPosition;
            transform.rotation = Quaternion.Euler(currentRotation);
        }

        private void setParams(Vector3 position, Vector3 rotation)
        {
            currentPosition = position;
            currentRotation = rotation;
        }

        public virtual void update()
        { }

        public virtual void setObservedTransform(Transform cameraObject)
        {
            transform = cameraObject;
        }

        public CameraController(string name, Vector3 position, Vector3 rotation)
        {
            this.name = name;
            setParams(position, rotation);
        }
    }
}
