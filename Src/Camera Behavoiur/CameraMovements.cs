﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.Camera_Behavoiur
{
    [System.Serializable]
    public partial class CameraMovements
    {
        private readonly Transform _transform;
        private CameraController _currentCamera;
        public List<CameraController> cameraBehaviours = new List<CameraController>();

        public class WrongCameraNameException : Exception
        {
            public WrongCameraNameException(string message) : base(message)
            {
            }
        }

        public void updateCamera()
        {
            UnityEngine.Profiling.Profiler.BeginSample("Updating Camera Position");
            if (_currentCamera != null)
            {
                _currentCamera.update();
            }
            else
            {
                Debug.Log("no camera in camera movements class was set!");
            }
            UnityEngine.Profiling.Profiler.EndSample();
        }

        public void addNewCameraController(CameraController camera)
        {
            camera.setObservedTransform(_transform);
            cameraBehaviours.Add(camera);
        }

        public void changeCameraState(string name)
        {
            _currentCamera=cameraBehaviours.Find(x => x.name == name);
            if (_currentCamera == null)
            {
                throw new WrongCameraNameException(name);
            }
            _currentCamera.setAsActive();
        }

        public CameraMovements(Transform cameraObject)
        {
            _transform = cameraObject;
        }
    }
}
