﻿using UnityEngine;

namespace Gunk_Scripts.Camera_Behavoiur
{
    [System.Serializable]
    public class FreeMovingCamera : CameraController
    {
        public float speedH = 2.0f;
        public float speedV = 2.0f;
        public float rotV = 0.0f;
        public float rotH = 0.0f;

        public override void setAsActive()
        {
            currentPosition = transform.position;
            currentRotation = transform.rotation.eulerAngles;
            base.setAsActive();
        }

        public override void update()
        {
            base.update();
            rotH = currentRotation.x;
            rotV = currentRotation.y;
            if (Input.GetMouseButton(1))
            {
                rotV += speedH * Input.GetAxis("Mouse X");
                rotH -= speedV * Input.GetAxis("Mouse Y");
            }
            currentRotation = new Vector3(rotH, rotV, 0.0f);
            var leftVector = transform.rotation * new Vector3(1, 0, 0);
            var upVector = transform.rotation * new Vector3(0, 1, 0);
            var frontVector = transform.rotation * new Vector3(0, 0, 1);
            upVector.y = 0;
            leftVector.y = 0;

            upVector.Normalize();
            leftVector.Normalize();
            currentPosition += 8.0f * frontVector * Input.mouseScrollDelta.y;

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                currentPosition -= 64.0f * leftVector * Time.unscaledDeltaTime;
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                currentPosition += 64.0f * leftVector * Time.unscaledDeltaTime;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                currentPosition -= 64.0f * upVector * Time.unscaledDeltaTime;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                currentPosition += 64.0f * upVector * Time.unscaledDeltaTime;
            }
            transform.position = currentPosition;
            transform.rotation = Quaternion.Euler(currentRotation);
        }

        public FreeMovingCamera(string name, Vector3 position, Vector3 rotation) : base(name, position, rotation)
        {
        }
    }
}
