﻿using Gunk_Scripts.Camera_Behavoiur;
using UnityEngine;

namespace Src.Camera_Behavoiur
{
    [System.Serializable]
    public class OrbitAroundCamera : CameraController
    {
        private Vector3 lookAtSphereCenter;

        public override void update()
        {
            base.update();
            transform.SetPositionAndRotation(currentPosition,Quaternion.Euler(currentRotation));

            //rotating
            if (Input.GetMouseButton(1))
            {
                transform.RotateAround(lookAtSphereCenter, new Vector3(0.0f, 1.0f, 0.0f), 4.0f * Input.GetAxis("Mouse X"));
            }

            //zooming or changing camera height
            if (Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl))
            {
                var deltaY = 0.5f * Time.deltaTime
                                  * Vector3.Distance(lookAtSphereCenter, transform.position)
                                  * Input.mouseScrollDelta.y;
                lookAtSphereCenter.y += deltaY;
                transform.position += new Vector3(0, deltaY, 0);
            }
            else
            {
                var lookDirection = (lookAtSphereCenter - transform.position);
                transform.position += (lookDirection.normalized) * Input.mouseScrollDelta.y;
            }

            currentPosition = transform.position;
            currentRotation = transform.rotation.eulerAngles;
        }

        public void setLookAtSphereCenter(Vector3 position)
        {
            lookAtSphereCenter = position;
            currentPosition = position + new Vector3(0, 5, -5);
            currentRotation = new Vector3(30, 0, 0);
        }

        public OrbitAroundCamera(string name, Vector3 orbitSphereCenter) : base(name, Vector3.zero,Vector3.zero)
        {
            setLookAtSphereCenter(orbitSphereCenter);
        }
    }
}
