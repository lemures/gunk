﻿using UnityEngine;

namespace Gunk_Scripts.Camera_Behavoiur
{
    [System.Serializable]
    public class StaticCamera : CameraController
    {
        public StaticCamera(string name, Vector3 position, Vector3 rotation) : base(name, position, rotation)
        {

        }
    }
}
