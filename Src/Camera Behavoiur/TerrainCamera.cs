﻿using Gunk_Scripts.Maps;
using UnityEngine;

namespace Gunk_Scripts.Camera_Behavoiur
{
    /// <summary>
    /// RTS like camera that nicely behaves over terrain
    /// works well only with main camera
    /// </summary>
    [System.Serializable]
    public class TerrainCamera : CameraController
    {
        public float cameraHeight = 0.05f;
        RaycastHit _hit = new RaycastHit();
        Ray _ray = new Ray();

        public float accumulatedScroll = 0.0f;
        public float currentScroll = 0.0f;
        public float accelerationX = 0.0f;
        public float accelerationY = 0.0f;
#if !UNITY_EDITOR
    float movingBorderSize = 5.0f;
#endif

        readonly Vector3 _microCameraRotation = new Vector3(50, 0, 0);
        readonly Vector3 _macroCameraRotation = new Vector3(90, 0, 0);

        void Recenter_camera(Vector3 desiredPos)
        {
            var step = 4000.0f;
            var stepsDone = 0;
            while (stepsDone < 15)
            {
                currentPosition.x += step;
                transform.position = currentPosition;
                _ray = transform.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                TerrainPlaneColliderManager.getPlaneCollider().Raycast(_ray, out _hit, 10000.0f);
                if (_hit.point.x > desiredPos.x)
                { currentPosition.x -= step; }
                step /= 2.0f;
                stepsDone++;
            }
            transform.position = currentPosition;
            step = 4000.0f;
            stepsDone = 0;
            while (stepsDone < 15)
            {
                currentPosition.z += step;
                transform.position = currentPosition;
                _ray = transform.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
                TerrainPlaneColliderManager.getPlaneCollider().Raycast(_ray, out _hit, 10000.0f);
                if (_hit.point.z > desiredPos.z)
                { currentPosition.z -= step; }
                step /= 2.0f;
                stepsDone++;
            }
            transform.position = currentPosition;
        }

        void Move_inside_bounds_camera()
        {
            var aktPosition = transform.position;
            aktPosition.x = Mathf.Max(cameraHeight * 0.5f, aktPosition.x);
            aktPosition.x = Mathf.Min(NewMapManager.getCurrentMapWidth() - cameraHeight * 0.5f, aktPosition.x);
            _ray = transform.GetComponent<Camera>().ScreenPointToRay(new Vector3(Screen.height, 0, Screen.width / 2));
            TerrainPlaneColliderManager.getPlaneCollider().Raycast(_ray, out _hit, 10000.0f);
            aktPosition.z = Mathf.Min(NewMapManager.getCurrentMapHeight() - cameraHeight * 0.5f, aktPosition.z);
            var step = 4000.0f;
            var stepsDone = 0;
            while (stepsDone < 20)
            {
                aktPosition.z += step;
                transform.position = aktPosition;
                _ray = transform.GetComponent<Camera>().ScreenPointToRay(new Vector3(Screen.height, 0, Screen.width / 2));
                TerrainPlaneColliderManager.getPlaneCollider().Raycast(_ray, out _hit, 10000.0f);
                if (_hit.point.z > -15.0f)
                { aktPosition.z -= step; }
                step /= 2.0f;
                stepsDone++;
            }

            transform.position = aktPosition;
            currentPosition = transform.position;

        }

        public override void update()
        {
            base.update();
            //TODO repair CameraMode
            /*
            if (transform.position.y - Terrain.activeTerrain.SampleHeight(transform.position) > 350)
            {
                myMode = CameraMode.Macro;
            }
            else
            {
                myMode = transform.position.y - Terrain.activeTerrain.SampleHeight(transform.position) > 125 ? CameraMode.RegionMicro : CameraMode.Micro;
            }
            */
            accumulatedScroll += Input.mouseScrollDelta.y;
            var mousePos = Input.mousePosition;
#if UNITY_EDITOR
#else

    if (mousePos.x < movingBorderSize) { accelerationX -= 6.0f * Time.unscaledDeltaTime; }
    if (mousePos.x > Screen.width-movingBorderSize) { accelerationX += 6.0f * Time.unscaledDeltaTime; }
    if (mousePos.y < movingBorderSize) { accelerationY -= 6.0f * Time.unscaledDeltaTime; }
    if (mousePos.y > Screen.height - movingBorderSize) { accelerationY += 6.0f * Time.unscaledDeltaTime; }
#endif
            currentScroll = Mathf.Clamp(accumulatedScroll, -7.0f * Time.unscaledDeltaTime, 7.0f * Time.unscaledDeltaTime);
            accumulatedScroll -= currentScroll;

            var moveDist = Time.unscaledDeltaTime * (Mathf.Sqrt(cameraHeight)) * 30.0f;
            var hasMoved = 0;
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                accelerationX -= 10.0f * Time.unscaledDeltaTime;
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                accelerationX += 10.0f * Time.unscaledDeltaTime;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                accelerationY -= 10.0f * Time.unscaledDeltaTime;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                accelerationY += 10.0f * Time.unscaledDeltaTime;
            }

            accelerationY = Mathf.Clamp(accelerationY, -1.0f, 1.0f);
            accelerationX = Mathf.Clamp(accelerationX, -1.0f, 1.0f);
            accelerationX -= Mathf.Sign(accelerationX) * Mathf.Min(Mathf.Abs(accelerationX), Time.unscaledDeltaTime * 5.0f);
            accelerationY -= Mathf.Sign(accelerationY) * Mathf.Min(Mathf.Abs(accelerationY), Time.unscaledDeltaTime * 5.0f);
            currentPosition.x += accelerationX * moveDist;
            currentPosition.z += accelerationY * moveDist;
            if (accelerationX != 0.0f || accelerationY != 0.0f)
            {
                hasMoved = 1;
            }
            currentPosition.y = Mathf.Max(currentPosition.y, Terrain.activeTerrain.SampleHeight(transform.position) + 20.0f);
            transform.position = currentPosition;
            cameraHeight = transform.position.y;
            if (accumulatedScroll == 0.0f || (transform.position.y == Terrain.activeTerrain.SampleHeight(transform.position) + 20.0f && accumulatedScroll > 0.0f))
            {
                if (hasMoved == 1)
                {
                    Move_inside_bounds_camera();
                }
                return;
            }
            _ray = transform.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            TerrainPlaneColliderManager.getPlaneCollider().Raycast(_ray, out _hit, 10000.0f);
            var zoomScale = 0.25f;
            currentPosition.y *= (1 - zoomScale * currentScroll);
            currentPosition.y = Mathf.Min(NewMapManager.getCurrentMapWidth(), currentPosition.y);
            cameraHeight = transform.position.y;
            var contribution = (cameraHeight - Terrain.activeTerrain.SampleHeight(transform.position)) / 1000.0f;
            transform.rotation = Quaternion.Euler((1.0f - contribution) * _microCameraRotation + contribution * _macroCameraRotation);
            currentPosition.x = -1000;
            currentPosition.z = -1000;

            var desiredPos = _hit.point;
            Recenter_camera(_hit.point);

            cameraHeight = transform.position.y;
            transform.position = currentPosition;
            currentPosition.y = Mathf.Max(currentPosition.y, Terrain.activeTerrain.SampleHeight(transform.position) + 20.0f);
            Move_inside_bounds_camera();

            currentPosition = transform.position;
            currentRotation = transform.rotation.eulerAngles;
        }

        public TerrainCamera(string name, Vector3 position, Vector3 rotation) : base(name, position, rotation)
        {

        }
    }
}
