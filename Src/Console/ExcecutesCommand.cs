using System;

namespace Src.Console
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class ExecutesCommand : Attribute
    {
        public string commandName;

        /// <summary>
        /// add this attribute to define new command in console, you also need to add method with the
        /// same name that is public and static, and has signature string[] -> string in the class declaring
        /// this attribute
        /// </summary>
        public ExecutesCommand(string text)
        {
            commandName = text;
        }
    }
}