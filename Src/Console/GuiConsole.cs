using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.GUI;
using Src.Console.Internal;
using UnityEngine;

namespace Src.Console
{
    public class GuiConsole : GuiElement
    {
        private GuiInputfield inputfield;
        private GuiTextfield history;
        private GuiImage historyBackground;

        private static int getScreenGuiWidth() => (int)GuiManager.layersManager.getScreenGuiDimensions().x;

        private static GuiConsole console = new GuiConsole();

        private GuiConsole() : base(0, 0, 12, "Console", null)
        {
            historyBackground = new GuiImage(0, 0, getScreenGuiWidth(), 300, null, 0);
            historyBackground.setImageColor(new Color(0.12f, 0.12f, 0.12f, 0.98f));
            historyBackground.attachToObject(this);

            history = new GuiTextfield(0, 0, Screen.width, 300, "", 0);
            history.attachToObject(historyBackground);
            history.setTextAlignmentToBottom();
            history.setTextFontSize(20);
            history.enableScrolling();

            inputfield = new GuiInputfield(0, 300, getScreenGuiWidth(), 30, "", 0);
            inputfield.attachToObject(this);
            inputfield.addOnValueSubmittedListener(interpretCommand);
            inputfield.enableMultiLineEditing();

            GuiManager.updateCaller.addFunction(this, () =>
            {
                var size = GuiManager.layersManager.getScreenGuiDimensions();

                historyBackground.registerSize((int)size.x, 300);
                inputfield.registerSize((int)size.x, 35);
            });

            setPosition(0, -1000);
        }

        private static void interpretCommand(string command)
        {
            printCommand(command, CommandInterpreter.executeLine(command));
            commandsHistory.Add(command);
            lastCommandIndex = commandsHistory.Count;
        }

        private static void printCommand(string command, string result)
        {
            console.history.setText(console.history.getText() + ("<color=\"green\">" + "Gunk RTS"+
                                                                 "<color=\"white\">" + ":" +
                                                                 "<color=\"blue\">" + "~" +
                                                                 "<color=\"white\">" + "$ " + command + "\n"));

            if (result?.Length > 0)
            console.history.setText(console.history.getText() + (result + "\n"));
        }

        private static bool isHidden = true;

        private static void handleKeyboard()
        {
            removeUnnecessaryCharacters();
            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                if (isHidden)
                {
                    show();
                }
                else
                {
                    hide();
                }
            }

            if (console.inputfield.isBeingEdited())
            {
                handleTabs();
                handleArrows();
                console.history.enableScrolling();
            }
            else
            {
                console.history.disableScrolling();
            }
        }

        private static List<string>commandsHistory = new List<string>();
        private static int lastCommandIndex = 0;

        private static void handleArrows()
        {
            if (Input.GetKeyDown(KeyCode.DownArrow) && lastCommandIndex < commandsHistory.Count - 1)
            {
                lastCommandIndex++;
                console.inputfield.setValue(commandsHistory[lastCommandIndex]);
            }

            if (Input.GetKeyDown(KeyCode.UpArrow) && lastCommandIndex != 0)
            {
                lastCommandIndex--;
                console.inputfield.setValue(commandsHistory[lastCommandIndex]);
            }
        }

        private static void handleTabs()
        {
            var currentCommand = console.inputfield.text;
            if (!Input.GetKeyDown(KeyCode.Tab) || currentCommand.Length == 0) return;

            if (Time.unscaledTime - lastTimeTabPressed >= 0.5f)
            {
                console.inputfield.setValue(CommandInterpreter.tryAutoComplete(currentCommand));
            }
            else
            {
                var options = CommandInterpreter.getAutoCompleteOptions(currentCommand);
                if (options.Length > 0)
                printCommand(currentCommand, options);
            }

            lastTimeTabPressed = Time.unscaledTime;
        }

        private static float lastTimeTabPressed = -1;

        private static void removeUnnecessaryCharacters()
        {
            console.inputfield.setValue(console.inputfield.text.Replace("\t", ""));
            console.inputfield.setValue(console.inputfield.text.Replace("`", ""));
        }

        private static void show()
        {
            isHidden = false;
            console.setPosition(0, 0);
            console.inputfield.enable();
            console.inputfield.setValue("");
            console.history.enableScrolling();
        }

        private static void hide()
        {
            isHidden = true;
            console.setPosition(0, -1000);
            console.inputfield.disable();
            console.inputfield.setValue("");
            console.history.disableScrolling();
        }

        public static void init()
        {
            GuiManager.updateCaller.addFunction(console, handleKeyboard);
        }
    }
}