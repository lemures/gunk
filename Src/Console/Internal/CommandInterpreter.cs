using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Src.Console.Internal
{
    public static class CommandInterpreter
    {
        public static string executeLine(string command)
        {
            var arguments = command.Split(' ');
            var result = new List<string>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var types =
                    from type in assembly.GetTypes()
                    where type.IsDefined(typeof(ExecutesCommand), false)
                    select type;
                foreach (var rightType in types)
                {
                    if (rightType.GetMethod(arguments[0]) != null)
                    {
                        try
                        {
                            return rightType.GetMethod(arguments[0])?.Invoke(null, new object[] {arguments}) as string;
                        }
                        catch (Exception e)
                        {
                            return e.ToString();
                        }
                    }

                }
            }

            if (arguments.Length > 0 && getAllAvailableCommands().Contains(arguments[0]))
            {
                return arguments[0] + ": command was found, but was either not static or not public";
            }

            return arguments[0] + ": command not found";
        }

        public static List<string> getAllAvailableCommands()
        {
            var result = new List<string>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                var attributes =
                    from type in assembly.GetTypes()
                    where type.IsDefined(typeof(ExecutesCommand), false)
                    select type.GetCustomAttributes<ExecutesCommand>();
                foreach (var attribute in attributes)
                {
                    attribute.ToList().ForEach(x => result.Add(x.commandName));
                }
            }

            //command that exists only for testing purposes
            result.Remove("testCommand");
            return result;
        }

        private static bool isPrefix(string prefix, string str)
        {
            return str.StartsWith(prefix);
        }

        private static List<string> getAllMatchingCommands(string command)
        {
            return getAllAvailableCommands().FindAll(x => isPrefix(command, x));
        }

        public static string tryAutoComplete(string command)
        {
            var matching = getAllMatchingCommands(command);
            return matching.Count == 1 ? matching[0] : command;
        }

        public static string getAutoCompleteOptions(string command)
        {
            var result = "";
            var matching = getAllMatchingCommands(command);
            for (var i = 0; i < matching.Count; i++)
            {
                var positioningRichTextTag = "<pos=" + ((i % 3) * 20) + "%>";
                result += positioningRichTextTag + matching[i];
                if (i % 3 == 2)
                {
                    result += "\n";
                }
            }

            //remove last '\n'
            if (result.EndsWith("\n"))
            {
                result = result.Substring(0, result.Length - 1);
            }

            //nothing to auto complete really
            if (matching.Count == 1 && command == matching[0])
            {
                return "";
            }

            return result;
        }
    }
}