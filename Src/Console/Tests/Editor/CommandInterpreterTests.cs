using System;
using System.ComponentModel;
using System.Reflection.Emit;
using NUnit.Framework;
using Src.Console.Internal;
using UnityEngine;

namespace Src.Console.Tests.Editor
{
    public class CommandInterpreterTests
    {
        [ExecutesCommand("testCommand")]
        public class TestCommandClass
        {
            public static int counter = 0;

            public static string testCommand(string[] arguments)
            {
                counter = int.Parse(arguments[1]);
                return arguments[1];
            }
        }

        [Test]
        public void testCommandExecutes()
        {
            CommandInterpreter.executeLine("testCommand " + 42);
            Assert.AreEqual(42, TestCommandClass.counter);

            CommandInterpreter.executeLine("testCommand " + 17);
            Assert.AreEqual(17, TestCommandClass.counter);
        }
    }
}