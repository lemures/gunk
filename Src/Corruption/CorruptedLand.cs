using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.Maps;
using UnityEngine;
using Random = System.Random;

namespace Src.Libraries
{
    public class CorruptedLand
    {
        private static readonly Random generator = new Random();
        private readonly float[,] fields;

        private float maxCorruptionLevel = 10.0f;
        //should be less than growthFactor
        private float growthFactor = 1.0f;

        private readonly int iterationsPerSecond = 30;
        private readonly int mapHeight;
        private readonly int mapWidth;
        private readonly List<Vector2Int> shuffledPositions = new List<Vector2Int>();

        //infectors are fields that can infect someone(that are infected and have a neighbour that is not infected)
        private List<Vector2Int> infectors = new List<Vector2Int>();
        //given by a formula (isInfected * 8 + notInfectedNeighboursCount)
        private readonly int[,] infectorState;

        //for performance reasons infectors can be added all at once after tick()
        private List<Vector2Int>newInfectors = new List<Vector2Int>();
        private List<Vector2Int>toRemoveInfectors = new List<Vector2Int>();

        //growers are fields that are infected and that did not yet reach maximum corruption level
        private const int bucketsCount = 256;
        private List<Vector2Int>[]growers = new List<Vector2Int>[bucketsCount];

        //for performance reasons growers can be added all at once after tick()
        private List<Vector2Int>[]newGrowers = new List<Vector2Int>[bucketsCount];
        private List<Vector2Int>[]toRemoveGrowers = new List<Vector2Int>[bucketsCount];

        private bool[,] isInfectable;

        //int limited range should not be a concern here
        private int iterationsSoFar;
        private float timePassed;

        private List<FieldChange> changes = new List<FieldChange>();

        public CorruptedLand(int mapHeight, int mapWidth)
        {
            this.mapHeight = mapHeight;
            this.mapWidth = mapWidth;
            fields = new float[mapWidth, mapHeight];
            foreach (var xPos in Enumerable.Range(0, mapHeight))
            foreach (var yPos in Enumerable.Range(0, mapWidth))
                shuffledPositions.Add(new Vector2Int(xPos, yPos));
            shuffle(shuffledPositions);

            infectorState = new int[mapWidth, mapHeight];

            foreach (var xPos in Enumerable.Range(0, mapHeight))
            foreach (var yPos in Enumerable.Range(0, mapWidth))
            {
                infectorState[xPos, yPos] = 4;
                if (xPos == 0) infectorState[xPos, yPos]--;
                if (xPos == mapWidth - 1) infectorState[xPos, yPos]--;
                if (yPos == 0) infectorState[xPos, yPos]--;
                if (yPos == mapHeight - 1) infectorState[xPos, yPos]--;
            }

            foreach (var i in Enumerable.Range(0, bucketsCount))
            {
               growers[i] = new List<Vector2Int>();
               newGrowers[i] = new List<Vector2Int>();
               toRemoveGrowers[i] = new List<Vector2Int>();
            }

            isInfectable = new bool[mapHeight, mapWidth];
            foreach (var xPos in Enumerable.Range(0, mapHeight))
            foreach (var yPos in Enumerable.Range(0, mapWidth))
                isInfectable[xPos, yPos] = true;
        }

        private static int getPositionHash(Vector2Int position)
        {
            return (Math.Abs(position.x * 37 + position.y * 79)) % bucketsCount;
        }

        private static void shuffle<T>(IList<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = generator.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public bool isCorrupted(Vector2Int position)
        {
            return isValid(position) && fields[position.x, position.y] > 0.0f;
        }

        /// <summary>
        ///     returns true if any field under object is corrupted
        /// </summary>
        public bool isCorruptionUnder(Vector2Int center, ObjectPattern pattern)
        {
            foreach (var field in pattern.fieldsLocations)
                if (isValid(center) && isCorrupted(center + field.position))
                    return true;

            return false;
        }

        private void updateInfectorState(int x, int y, int infectedNeighboursChange, bool isImmediate)
        {
            if (!isValid(x, y)) {return;}

            //we had no not infectable neighbours so far, now it changed
            if (infectorState[x, y] == 8)
            {
                if (isImmediate)
                {
                    infectors.Add(new Vector2Int(x, y));
                }
                else
                {
                    newInfectors.Add(new Vector2Int(x, y));
                }
            }

            infectorState[x, y] += infectedNeighboursChange;

            //we now do not have any not infected neighbours
            if (infectorState[x, y] == 8)
            {
                if (isImmediate)
                {
                    infectors.Remove(new Vector2Int(x, y));
                }
                else
                {
                    toRemoveInfectors.Add(new Vector2Int(x, y));
                }
            }
        }

        private void changeFieldLevel(Vector2Int location, float amount, bool isImmediate)
        {
            if (!isValid(location) || !isInfectable[location.x, location.y]) { return; }

            changes.Add(new FieldChange(amount, location));

            var x = location.x;
            var y = location.y;

            var changedToZero = fields[x, y] + amount <= 0.01f;
            var wasZero = fields[x, y] < 0.01f;
            var wasMaxLevel = fields[x, y] >= maxCorruptionLevel - 0.01f;
            var growerHash = getPositionHash(location);

            var change = changedToZero ? 1 : (wasZero ? -1 : 0);

            if (changedToZero)
            {
                if (!wasMaxLevel)
                {
                    if (isImmediate)
                    {
                        growers[growerHash].Remove(location);
                    }
                    else
                    {
                        toRemoveGrowers[growerHash].Add(location);
                    }
                }
                fields[x, y] = 0;
                //we have been an infector up to this moment
                if (infectorState[x, y] != 8 && isValid(location))
                {
                    if (isImmediate)
                    {
                        infectors.Remove(location);
                    }
                    else
                    {
                        toRemoveInfectors.Add(location);
                    }
                }
                infectorState[x, y] ^= 8;
            }
            else
            {
                if (fields[x, y] + amount >= maxCorruptionLevel && !wasMaxLevel)
                {
                    if (isImmediate)
                    {
                        growers[growerHash].Remove(location);
                    }
                    else
                    {
                        toRemoveGrowers[growerHash].Add(location);
                    }
                }

                fields[x, y] = Math.Min(fields[x, y] + amount, maxCorruptionLevel);
                if (wasZero)
                {
                    //we can infect neighbours and we are infected
                    if (infectorState[x, y] > 0 && isValid(location))
                    {
                        if (isImmediate)
                        {
                            infectors.Add(location);
                        }
                        else
                        {
                            newInfectors.Add(location);
                        }
                    }
                    infectorState[x, y] ^= 8;

                    if (fields[x, y] < maxCorruptionLevel - 0.01f)
                    {
                        if (isImmediate)
                        {
                            growers[growerHash].Add(location);
                        }
                        else
                        {
                            newGrowers[growerHash].Add(location);
                        }
                    }
                }
            }

            if (change != 0)
            {
                updateInfectorState(x - 1, y, change, isImmediate);
                updateInfectorState(x + 1, y, change, isImmediate);
                updateInfectorState(x, y + 1, change, isImmediate);
                updateInfectorState(x, y - 1, change, isImmediate);
            }
        }

        /// <summary>
        ///     decreases corruption level in field, corruption levels in fields are in range [0,10]
        /// </summary>
        /// <param name="position"></param>
        /// <param name="amountOfDamage"></param>
        public void damageField(Vector2Int position, float amountOfDamage)
        {
            changeFieldLevel(position, -amountOfDamage, true);
        }

        private bool isValid(Vector2Int position)
        {
            return isValid(position.x, position.y);
        }

        private bool isValid(int x, int y)
        {
            return x >= 0 && x < mapWidth && y >= 0 && y < mapHeight;
        }

        public Vector2Int? getClosestCorruptedField(Vector2Int position, int maxDistance = 15)
        {
            var closest = new Vector2Int(-1, -1);
            var bestDistance = 2 * (maxDistance + 1) * (maxDistance + 1);

            foreach (var offsetX in Enumerable.Range(-maxDistance, maxDistance * 2))
            foreach (var offsetY in Enumerable.Range(-maxDistance, maxDistance * 2))
            {
                var currentPosition = position + new Vector2Int(offsetX, offsetY);
                if (!isValid(currentPosition)) continue;

                var distance = offsetX * offsetX + offsetY * offsetY;
                if (isCorrupted(currentPosition) && distance < bestDistance)
                {
                    bestDistance = distance;
                    closest = currentPosition;
                }
            }

            return isCorrupted(closest) ? closest : (Vector2Int?) null;
        }

        public void infectField(Vector2Int position)
        {
            if (isCorrupted(position)) return;

            changeFieldLevel(position, 1.0f, true);
        }

        private void addDelayedInfectors()
        {
            infectors.AddRange(newInfectors);
            newInfectors.Clear();

            if (toRemoveInfectors.Count != 0)
            {
                var toRemoveSet = new HashSet<Vector2Int>(toRemoveInfectors);
                infectors = infectors.FindAll(x => !toRemoveSet.Contains(x));
                toRemoveInfectors.Clear();

            }
        }

        private void addDelayedGrowers()
        {
            foreach (var i in Enumerable.Range(0, bucketsCount))
            {
                growers[i].AddRange(newGrowers[i]);
                newGrowers[i].Clear();

                if (toRemoveGrowers[i].Count != 0)
                {
                    var toRemoveSet = new HashSet<Vector2Int>(toRemoveGrowers[i]);
                    growers[i] = growers[i].FindAll(x => !toRemoveSet.Contains(x));
                    toRemoveGrowers[i].Clear();
                }
            }
        }

        private void spread(float aggressiveness)
        {
            var offsetsX = new List<int> {1, -1, 0, 0};
            var offsetsY = new List<int> {0, 0, 1, -1};

            foreach (var position in infectors)
            {
                //between 0 and 1
                var random = generator.NextDouble();
                var spreads = random / aggressiveness < 0.2f;

                if (spreads)
                {
                    var spreadDirection = generator.Next() % 4;
                    var location = position + new Vector2Int(offsetsX[spreadDirection], offsetsY[spreadDirection]);
                    if (!isCorrupted(location)) changeFieldLevel(location, growthFactor, false);
                }
            }

            //we use a fact that when we call spread(), for a given field it first can become infector, then stop being
            //it, not the other way round.
            addDelayedInfectors();

            //practically we will only add new growers here
            addDelayedGrowers();
        }

        private int currentBucket = 0;
        private void grow(float aggressiveness)
        {
            foreach (var grower in growers[currentBucket])
            {
                var random = generator.NextDouble();
                changeFieldLevel(grower, growthFactor, false);
            }

            currentBucket++;
            currentBucket %= bucketsCount;

            //we will only remove growers here
            addDelayedGrowers();
        }

        /// <summary>
        ///     simulates single tick of corruption spreading
        /// </summary>
        private void tick(float aggressiveness)
        {
            spread(aggressiveness);
            grow(aggressiveness);
        }

        /// <summary>
        ///     updates corruption with given aggressiveness that can be from range (0, 10) where 0 is no spreading at all,
        ///     1 is normal level of spreading and 10 is death wish
        /// </summary>
        public void update(float deltaTime, float aggressiveness = 1)
        {
            if (aggressiveness < 0.0001f) return;

            timePassed += deltaTime;
            while (iterationsSoFar < timePassed * iterationsPerSecond)
            {
                tick(aggressiveness);
                iterationsSoFar++;
            }
        }

        public struct FieldChange
        {
            public float delta;
            public Vector2Int field;

            public FieldChange(float delta, Vector2Int field)
            {
                this.delta = delta;
                this.field = field;
            }
        }

        public List<FieldChange> getChanges()
        {
            var result = changes;
            changes = new List<FieldChange>();
            return result;
        }

        /// <summary>
        /// cleanses corruption from given field, it will never be corrupted again
        /// </summary>
        public void markFieldAsNonInfectable(Vector2Int location)
        {
            if (isCorrupted(location))
            {
                if (Math.Abs(fields[location.x, location.y] - maxCorruptionLevel) > 0.01f)
                {
                    growers[getPositionHash(location)].Remove(location);
                }
                changeFieldLevel(location, -10.0f, true);
            }

            var x = location.x;
            var y = location.y;

            var offsetsX = new List<int> {1, -1, 0, 0};
            var offsetsY = new List<int> {0, 0, 1, -1};

            for (var i = 0; i < 4; i++)
            {
                if (isValid(location + new Vector2Int(offsetsX[i], offsetsY[i])))
                {
                    updateInfectorState(x + offsetsX[i], y + offsetsY[i], -1, true);
                }
            }

            isInfectable[location.x, location.y] = false;
        }
    }
}