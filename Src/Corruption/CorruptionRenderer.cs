using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts;
using Src.Libraries;
using UnityEngine;

namespace Src.Renderers
{
    public class CorruptionRenderer
    {
        private static RenderTexture corruptionTexture;
        private static CorruptedLand.FieldChange[] changesBuffer = new CorruptedLand.FieldChange[1024 * 128];

        public static void addChanges(List<CorruptedLand.FieldChange> changes)
        {
            KernelUtils.setInt("changesCount", changes.Count);
            for (int i = 0; i < changes.Count; i++)
            {
                changesBuffer[i] = changes[i];
            }
            KernelUtils.setBufferData("changes", changesBuffer);
            KernelUtils.dispatch("CorruptionKernel", 1 + (changes.Count/1024));
            Debug.Log(changes.Count);
        }

        public static void init(int mapHeight, int mapWidth)
        {
            corruptionTexture = new RenderTexture(mapWidth, mapHeight, 0);
            corruptionTexture.enableRandomWrite = true;
            corruptionTexture.Create();
            KernelUtils.setGlobalTexture("corruptionTexture", corruptionTexture);
            KernelUtils.initializeEmptyBuffer("changes", 1024 * 16, 12, true);
        }
    }
}