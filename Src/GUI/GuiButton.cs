﻿using System;
using TMPro;
using UnityEngine.UI;

namespace Gunk_Scripts.GUI
{
    public class GuiButton : GuiElement
    {

        public enum ButtonStyle
        {
            Simple,
            Decorated
        }

        protected Button button;
        protected TextMeshProUGUI propertyField;

        public GuiButton(ButtonStyle myStyle, int x, int y, int width, int height,
            string title, int layer) :
            base(x, y, layer, "Button: " + title,
                myStyle == ButtonStyle.Simple
                    ? GuiManager.GuiPrimitives.getPrimitive(GuiManager.GuiPrimitives.PrimitiveType.Button)
                    : GuiManager.GuiPrimitives.getPrimitive(GuiManager.GuiPrimitives.PrimitiveType.DecoratedButton))
        {
            button = myObject.GetComponent<Button>();
            var myTexts = myObject.GetComponentsInChildren<TextMeshProUGUI>();
            foreach (var text in myTexts)
                if (text.transform.gameObject.name == "Text")
                {
                    propertyField = text;
                    propertyField.text = "";
                }

            registerSize(width, height);
            setPropertyName(title);
        }

        public void setPropertyName(string title)
        {
            propertyField.text = title;
            myObject.name = "Button: " + title;
        }

        /// <summary>
        ///     Add function that will be called every time the Dropdown value is changed
        /// </summary>
        public void addOnClickListener(Action func)
        {
            button.onClick.AddListener(delegate { func(); });
        }
    }
}