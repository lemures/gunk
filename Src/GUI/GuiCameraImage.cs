﻿using Gunk_Scripts.Camera_Behavoiur;
using UnityEngine;

namespace Gunk_Scripts.GUI
{
    public class GuiCameraImage : GuiElement
    {
        private Camera _camera;
        private CameraMovements cameraMovements;

        public GuiCameraImage(int x, int y, int width, int height, int layer,float renderTextureScale) : base(x, y, layer, "Gui camera image", null)
        {
            registerSize(width,height);
            //TODO make renderTexture size relative to ScreenSize, not fixed 1920x1080
            var renderTexture = new RenderTexture((int)(width*renderTextureScale), (int)(height*renderTextureScale),24);
            var cameraImage = new GuiImage(0, 0, width, height, renderTexture, layer);
            var cameraObject = MainCamera.getCopy();
            _camera = cameraObject.GetComponentInChildren<Camera>();
            cameraMovements = new CameraMovements(cameraObject.transform);
            cameraMovements.addNewCameraController(new StaticCamera("GuiCameraImageStaticCamera", Vector3.zero,Vector3.zero));
            cameraMovements.changeCameraState("GuiCameraImageStaticCamera");
            camera.targetTexture = renderTexture;
            cameraImage.setMaterial(DatabasesManager.getObjectLatestVersion("GuiCameraImage") as Material);
            GuiManager.updateCaller.addFunction(this, () => cameraMovements.updateCamera());
            //so that it will be destroyed together with this object when dispose() is called
            cameraObject.transform.parent = myObject.transform;
            cameraImage.attachToObject(this);
        }

        public Camera camera
        {
            get { return _camera; }
        }

        /// <summary>
        /// sets given CameraController as an active controller of the camera that renders to the image
        /// </summary>
        /// <param name="controller"></param>
        public void setCameraBehaviour(CameraController controller)
        {
            cameraMovements.addNewCameraController(controller);
            cameraMovements.changeCameraState(controller.name);
        }

        public Vector3 worldToScreenPoint(Vector3 position)
        {
            var transformationMatrix = camera.projectionMatrix * camera.worldToCameraMatrix;
            // now x and y are (-1,1)
            var unCorrectedPosition = transformationMatrix.MultiplyPoint(position);
            // and now will be scaled to (0,1)
            unCorrectedPosition.x = (unCorrectedPosition.x * 0.5f + 0.5f);
            unCorrectedPosition.y = (unCorrectedPosition.y * 0.5f + 0.5f);

            var screenDimensions = getScreenDimensions();
            var screenPosition = getScreenPosition();

            return  new Vector2(
                screenDimensions.x * unCorrectedPosition.x + screenPosition.x,
                screenDimensions.y * unCorrectedPosition.y + screenPosition.y - screenDimensions.y);
        }
    }
}
