﻿using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.GUI
{
    public class GuiColorPicker : GuiElement
    {
        public delegate void SendEvent(Color color);

        public enum PickerMode
        {
            RgbPicker,
            RgbaPicker
        }

        private readonly GuiSlider _alphaSlider;
        private readonly GuiSlider _blueSlider;
        private readonly GuiImage _colorImage;

        private Color _currentColor;
        private readonly GuiSlider _greenSlider;

        private List<SendEvent> _onValueChangedListeners;
        private readonly GuiSlider _redSlider;
        private readonly GuiTextfield _variableField;

        public GuiColorPicker(PickerMode pickerMode, int x, int y, Color currentValue,
            string variableName, int layer) :
            base(x, y, layer, "Color picker: " + variableName, null)
        {
            _variableField = new GuiTextfield(0, 0, 300, 30, variableName, layer);
            _variableField.attachToObject(this);
            _colorImage = new GuiImage(300, 0, 30, 30, null, layer);
            _colorImage.setImageColor(currentValue);
            _colorImage.attachToObject(this);

            _redSlider = new GuiSlider(GuiSlider.SliderType.FloatSlider, 0, 30, 0, 1, currentValue.r,
                "R", true, layer);
            _redSlider.registerSize(330, 30);
            _redSlider.attachToObject(this);
            _redSlider.addValueChangedListener(updateColor);

            _greenSlider = new GuiSlider(GuiSlider.SliderType.FloatSlider, 0, 60, 0, 1,
                currentValue.g, "G", true, layer);
            _greenSlider.registerSize(330, 30);
            _greenSlider.attachToObject(this);
            _greenSlider.addValueChangedListener(updateColor);

            _blueSlider = new GuiSlider(GuiSlider.SliderType.FloatSlider, 0, 90, 0, 1, currentValue.b,
                "B", true, layer);
            _blueSlider.registerSize(330, 30);
            _blueSlider.attachToObject(this);
            _blueSlider.addValueChangedListener(updateColor);

            _alphaSlider = new GuiSlider(GuiSlider.SliderType.FloatSlider, 0, 120, 0, 1,
                currentValue.a, "A", true, layer);
            _alphaSlider.registerSize(330, 30);
            _alphaSlider.attachToObject(this);
            _alphaSlider.addValueChangedListener(updateColor);

            if (pickerMode == PickerMode.RgbPicker) _alphaSlider.disable();
        }

        public void setPropertyField(string val)
        {
            _variableField.setText(val);
            myObject.name = "Color picker:" + val;
        }

        private void updateColor(float irrelevantVal)
        {
            _currentColor = new Color(_redSlider.getValue(), _greenSlider.getValue(), _blueSlider.getValue(),
                _alphaSlider.getValue());
            _colorImage.setImageColor(_currentColor);
        }

        private void sendValueChangedEvents()
        {
            foreach (var func in _onValueChangedListeners) func(_currentColor);
        }

        public void addOnValueChangedListener(SendEvent func)
        {
            _onValueChangedListeners.Add(func);
        }
    }
}