using System.Collections.Generic;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.GunkObjects;

namespace Gunk_Scripts.GUI
{
    public class GuiConstructionMenu : GuiElement
    {
        private static readonly int optionDimension = 100;
        private static readonly int targetWidth = 1000;

        private static int positionX = 1920 - targetWidth / 2;
        private static int positionY = 1080;
        private readonly List<GuiConstructionOption>options = new List<GuiConstructionOption>();

        public GuiConstructionMenu(int layer, IEnumerable<GunkObjectType> constructionOptions, IPrefabManager prefabManager, GameStateManager gameStateManager) : base(positionX, positionY, layer,
            "construction menu", null)
        {
            int currentPositionX = 0;
            int currentPositionY = -optionDimension;
            foreach (var constructionOption in constructionOptions)
            {
                if (currentPositionX + optionDimension == targetWidth)
                {
                    currentPositionX = 0;
                    currentPositionY -= optionDimension;
                }

                var option = new GuiConstructionOption(currentPositionX, currentPositionY, constructionOption, prefabManager, gameStateManager);
                option.attachToObject(this);
                options.Add(option);

                currentPositionX += optionDimension;
            }
        }
    }
}