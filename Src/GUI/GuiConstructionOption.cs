using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.GunkObjects;

namespace Gunk_Scripts.GUI
{
    public class GuiConstructionOption : GuiElement
    {
        public GuiConstructionOption(int x, int y, GunkObjectType objectType, IPrefabManager prefabManager,
            GameStateManager stateManager) : base(x, y, 3, "construction option", null)
        {
            var prefabName = GunkPrefabNames.getName(objectType);
            var prefab = prefabManager.findPrefab(prefabName);
            var miniatureTexture = prefab.miniature;
            var miniature = new GuiImage(0, 0, 100, 100, miniatureTexture, 3);
            miniature.attachToObject(this);
            miniature.addOnClickListener(() => stateManager.setStateToBuildingMode(objectType));
        }
    }
}