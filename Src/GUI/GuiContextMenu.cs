﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.GUI
{
    public class GuiContextMenu : GuiElement
    {

        private readonly GuiImage _background;

        private static readonly int SingleOptionHeight=30;
        private static readonly int SingleOptionWidth = 200;

        private readonly List<GuiElement> _currentOptions=new List<GuiElement>();


        private readonly int _frameCreated;

        static Vector2 getMousePositionInGuiCoords(Vector3 mousePos)
        {
            //Canvas scales so that height is always 1080 so only height scales
            return new Vector2((mousePos.x / Screen.height) * 1080.0f , ((Screen.height- mousePos.y) / Screen.height)* 1080.0f );
        }

        //Creates fancy context menu at mousePos that will automatically be closed(Destroyed) when one of the options will be choosen
        public GuiContextMenu(string name) : base((int)getMousePositionInGuiCoords(Input.mousePosition).x,(int)getMousePositionInGuiCoords(Input.mousePosition).y , 5, name, null)
        {
            _frameCreated = Time.frameCount;
            _background = new GuiImage(0, 0, 0, 0, null, 5);
            _background.setImageColor(GuiManager.colors.basicBackgroundColor);
            _background.attachToObject(this);
            GuiManager.updateCaller.addFunction(this, () =>
            {
                if (!_background.isPointerOnObject() && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))&&Time.frameCount!=_frameCreated)
                {
                   dispose();
                }
            });
        }

        public void setOptions(List<string> names, List<Action> correspondingEvents)
        {
            foreach (var element in _currentOptions)
            {
                element.dispose();
            }

            if (names.Count != correspondingEvents.Count)
            {
                Debug.Log("Warning! you tried to set options of GuiContextMenu and names list was not the same size as corresponding events list");
                return;
            }

            _background.registerSize(SingleOptionWidth, SingleOptionHeight * names.Count);

            for (var i = 0; i < names.Count; i++)
            {
                var button = new GuiButton(GuiButton.ButtonStyle.Decorated, 0, i * SingleOptionHeight, SingleOptionWidth, SingleOptionHeight,names[i],5);
                button.attachToObject(_background);
                var temp = i;
                button.addOnClickListener(()=>
                {
                    correspondingEvents[temp]();
                    dispose();
                });
            }
        }

    }
}
