﻿using System.Collections.Generic;
using Gunk_Scripts;
using Gunk_Scripts.Data_Structures;
using UnityEngine;

namespace Gunk_Scripts.GUI
{
    public class GuiCurveEditor : GuiImage
    {
        private BezierCpu _currentlyEditedCurve;
        private readonly int _myId;

        [System.NonSerialized] private bool _isDragging = false;
        [System.NonSerialized] private int _selectedPoint = -1;


        public GuiCurveEditor(int x, int y, int width, int height, int layer) : base(
            x, y, width, height, null, layer)
        {
            var myMaterialCopy = Object.Instantiate(GuiManager.GuiPrimitives.visualizationMat);
            setImageColor(Color.black);
            _myId = GuiManager.guiCurveEditorManager.findFreeId();
            setMaterial(myMaterialCopy);
            initShaderResources();
            GuiManager.updateCaller.addFunction(this,updateShaderResources);
            addGetMousePosInTexCoordsListener(handleMouse);
        }

        /// <summary>
        /// returns pair of index of the point closest to mouse and its distance from mouse
        /// </summary>
        /// <param name="scaledMousePos"></param>
        /// <returns></returns>
        private KeyValuePair<int, float> findPointCloseToMouse(Vector2 scaledMousePos)
        {
            var minimum = 2.0f;
            var selectedPoint = -1;
            for (var i = 0; i < _currentlyEditedCurve.points.Count; i++)
            {
                if (Vector2.Distance(_currentlyEditedCurve.points[i], scaledMousePos) < minimum)
                {
                    minimum = Vector2.Distance(_currentlyEditedCurve.points[i], scaledMousePos);
                    selectedPoint = i;
                }
            }

            return new KeyValuePair<int, float>(selectedPoint, Vector2.Distance(_currentlyEditedCurve.points[selectedPoint], scaledMousePos));
        }

        private void movePointToMousePosition(Vector2 scaledMousePos, int selectedPoint)
        {
            var diff = -_currentlyEditedCurve.points[selectedPoint];
            _currentlyEditedCurve.points[selectedPoint] = scaledMousePos;
            _currentlyEditedCurve.checkBoundriesAndRepair();
            diff += _currentlyEditedCurve.points[selectedPoint];
            if (selectedPoint % 3 == 0)
            {
                if (selectedPoint != 0)
                {
                    _currentlyEditedCurve.points[-1 + selectedPoint] += diff;
                }
                if (selectedPoint != _currentlyEditedCurve.points.Count - 1)
                {
                    _currentlyEditedCurve.points[1 + selectedPoint] += diff;
                }
            }
            _currentlyEditedCurve.checkBoundriesAndRepair();
        }

        public delegate void CurveChangedFunction();

        [System.NonSerialized] private CurveChangedFunction _changedFunction;

        /// <summary>
        /// sets a function that will be called every time mouse moves happen to change the curve
        /// </summary>
        /// <param name="functionToSet"></param>
        public void setCurveChangedFunction(CurveChangedFunction functionToSet)
        {
            _changedFunction = functionToSet;
        }

        private void handleMouse(Vector2 mousePos)
        {
            var hasChanged = false;

            //changing range of y from (0,1) to (-1,1)
            mousePos.y -= 0.5f;
            mousePos.y *= 2.0f;

            if (mousePos.x < 0.0f)
            { return; }
            if (mousePos.x > 1.0f)
            { return; }
            if (mousePos.y < -1.0f)
            { return; }
            if (mousePos.y > 1.0f)
            { return; }

            if (_isDragging == false)
            {
                var closestData = findPointCloseToMouse(mousePos);
                //point is close enough to treat it as above a point
                if (closestData.Value <= 0.1f)
                {
                    _selectedPoint = closestData.Key;
                }
                else
                {
                    _selectedPoint = -1;
                }
            }
            //Left Button was just pressed
            if (Input.GetMouseButtonDown(0))
            {
                if (_selectedPoint == -1)
                {
                    _currentlyEditedCurve.insertPoint(mousePos);
                }
                else
                {
                    _isDragging = true;
                }
                hasChanged = true;
            }
            //Left Button is no longer pressed
            if (Input.GetMouseButtonUp(0))
            {
                _isDragging = false;
            }

            if (_isDragging)
            {
                movePointToMousePosition(mousePos, _selectedPoint);
                hasChanged = true;
            }

            //Right Button was just pressed, check if we should erase certain point
            if (Input.GetMouseButtonDown(1))
            {
                //only "main" points can be erased
                if (_selectedPoint % 3 == 0)
                {
                    _currentlyEditedCurve.removePoint(_selectedPoint / 3);
                }
                hasChanged = true;
            }

            if (hasChanged)
            {
                _changedFunction?.Invoke();
                GuiManager.guiCurveEditorManager.sendCurveData(_myId, _currentlyEditedCurve.toGpu());
            }
        }

        public void setEditedCurve(BezierCpu curve)
        {
            if (curve == null) { return; }
            var gpuCurve = curve.toGpu();
            _currentlyEditedCurve = curve;
            GuiManager.guiCurveEditorManager.sendCurveData(_myId, gpuCurve);
            KernelUtils.setInt("is_initialized", 1);

        }

        private void initShaderResources()
        {
            KernelUtils.setInt("my_curve_ID", _myId);
            KernelUtils.setFloat("lenght", 1.0f);
            KernelUtils.setInt("selected_point", _selectedPoint);
        }

        private void updateShaderResources()
        {
            KernelUtils.setInt("selected_point", _selectedPoint);

        }


    }
}