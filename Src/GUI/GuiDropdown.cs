﻿using System.Collections.Generic;
using Used_Assets.Devdog.SciFiDesign.FirstVersion.Scripts.UI.Dropdown;

namespace Gunk_Scripts.GUI
{
    public class GuiDropdown : GuiElement
    {
        public delegate void SendEvent(string val);

        protected Dropdown dropdown;

        public GuiDropdown(int x, int y, int width, int height, string title, int layer) :
            base(x, y, layer, "Dropdown: " + title, GuiManager.GuiPrimitives.getPrimitive(GuiManager.GuiPrimitives.PrimitiveType.Dropdown))
        {
            dropdown = myObject.GetComponent<Dropdown>();
            registerSize(width, height);
        }

        public void setValue(string value)
        {
            var options = dropdown.options;
            if (options != null)
                for (var i = 0; i < options.Count; i++)
                    if (options[i].text == value)
                    {
                        dropdown.value = i;
                        return;
                    }
        }

        public void setOptions(List<string> possibleValues, string current)
        {
            var options = new List<TextMeshProDropdown.OptionData>();
            var currentPosition = 0;
            for (var i = 0; i < possibleValues.Count; i++)
            {
                var value = possibleValues[i];
                if (value == current) currentPosition = i;
                options.Add(new TextMeshProDropdown.OptionData(value));
            }

            dropdown.options = options;
            dropdown.value = currentPosition;
        }

        /// <summary>
        ///     Add function that will be called every time the the Dropdown value is changed
        /// </summary>
        public void addOnClickListener(SendEvent func)
        {
            dropdown.onValueChanged.AddListener(delegate { func(dropdown.options[dropdown.value].text); });
        }
    }
}