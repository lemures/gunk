﻿using UnityEngine;
using UnityEngine.EventSystems;
using Object = UnityEngine.Object;

namespace Gunk_Scripts.GUI
{
    /// <summary>
    ///     Basic class for all fancy UI elements written on top of RectTransforms
    /// </summary>
    public class GuiElement
    {
        private bool isPointerOnMe;

        protected GameObject myObject { get; private set; }
        protected GuiElement myParent;
        private readonly RectTransform myTransform;

        protected GuiElement(int x, int y, int layer, string name, GameObject prefabToInstantiate)
        {
            myObject = prefabToInstantiate != null ? Object.Instantiate(prefabToInstantiate) : GuiManager.GuiPrimitives.createEmptyRectTransform("GUI_Element");
            myTransform = (RectTransform) myObject.transform;
            myTransform.tag = "UI_Element";
            setPosition(x, y);
            registerSize((int) getDimensions().x, (int) getDimensions().y);

            if (myObject.GetComponent<EventTrigger>() == null) myObject.AddComponent<EventTrigger>();
            var eventTrigger = myObject.GetComponent<EventTrigger>();

            var onEnterEntry = new EventTrigger.Entry
            {
                eventID = EventTriggerType.PointerEnter
            };
            onEnterEntry.callback.AddListener(data => { isPointerOnMe = true; });
            eventTrigger.triggers.Add(onEnterEntry);

            var onExitEntry = new EventTrigger.Entry
            {
                eventID = EventTriggerType.PointerExit
            };
            onExitEntry.callback.AddListener(data => { isPointerOnMe = false; });
            eventTrigger.triggers.Add(onExitEntry);
            attachToLayer(layer);
            myObject.name = name;
        }

        private void attachToLayer(int layer)
        {
            var currentPos = myTransform.anchoredPosition;
            GuiManager.layersManager.attachToLayer(this, layer);
            myTransform.anchoredPosition = currentPos;
        }

        public void makeMostVisibleChild()
        {
            myObject.transform.SetAsLastSibling();
        }

        public void attachToObject(GuiElement objectToAttachTo)
        {
            myTransform.SetParent(objectToAttachTo.myTransform, false);
        }

        public void attachToObject(RectTransform transformToAttachTo)
        {
            myTransform.SetParent(transformToAttachTo, false);
        }

        public void registerSize(Vector2 size)
        {
            registerSize((int) size.x, (int) size.y);
        }

        public void registerSize(int width, int height)
        {
            myTransform.sizeDelta = new Vector2(width, height);
        }

        public void setPosition(int x, int y)
        {
            myTransform.anchoredPosition = new Vector2(x, -y);
        }

        public Vector2 getDimensions()
        {
            var maxX = 0;
            var maxY = 0;
            var transforms = myObject.GetComponentsInChildren<RectTransform>();
            foreach (var childTransform in transforms)
                if (childTransform.CompareTag("UI_Element"))
                {
                    maxX = Mathf.Max(maxX,
                        (int) childTransform.rect.size.x +
                        Mathf.Abs((int) getRelativePosition(myTransform, childTransform).x -
                                  (int) myTransform.anchoredPosition.x));
                    maxY = Mathf.Max(maxY,
                        (int) childTransform.rect.size.y +
                        Mathf.Abs((int) getRelativePosition(myTransform, childTransform).y -
                                  (int) myTransform.anchoredPosition.y));
                }

            return new Vector2(maxX, maxY);
        }

        public Vector2 getScreenDimensions()
        {
            var myDimensions = getDimensions();
            var layerRect = getLayer();
            return myDimensions * layerRect.localScale.x;
        }

        private RectTransform getLayer()
        {
            var currentTransform = myTransform;
            while (currentTransform.name.Length < 5 || currentTransform.name.Substring(0, 5) != "layer")
            {
                currentTransform = (RectTransform)currentTransform.parent;
            }
            return currentTransform;
        }


        /// <summary>
        ///     Destroys all unity GUI objects attached to this object
        /// </summary>
        public virtual void dispose()
        {
            Object.Destroy(myObject);
            myObject = null;
        }

        /// <summary>
        ///     Calculates screenspace relative position of child in relation to its parent
        /// </summary>
        private Vector2 getRelativePosition(RectTransform parent, RectTransform child)
        {
            var currentTransform = child;
            var relativePosition = child.anchoredPosition;
            while (currentTransform != parent)
            {
                currentTransform = (RectTransform) currentTransform.parent;
                relativePosition += currentTransform.anchoredPosition;
            }

            return relativePosition;
        }

        public Vector2 getPosition()
        {
            var layerRect = getLayer();
            var myPosition = getRelativePosition(layerRect, myTransform) - layerRect.anchoredPosition;
            return new Vector2(myPosition.x, myPosition.y * -1);
        }

        /// <summary>
        ///     return screen position calculated versus the same corner as mousePosition
        /// </summary>
        /// <returns></returns>
        public Vector2 getScreenPosition()
        {
            var myPosition = getPosition();
            var layerRect = getLayer();
            myPosition *= layerRect.localScale.x;
            myPosition.y = Screen.height - myPosition.y;
            return myPosition;
        }

        public bool isPointerOnObject()
        {
            var myScreenDimensions = getScreenDimensions();
            var myScreenPosition = getScreenPosition();
            Vector2 mousePos = Input.mousePosition;
            return myScreenPosition.x <= mousePos.x
                   && mousePos.x <= myScreenDimensions.x + myScreenPosition.x
                   && myScreenPosition.y >= mousePos.y
                   && mousePos.y >= myScreenPosition.y - myScreenDimensions.y
                   && isPointerOnMe; //y position is calculated "from top to bottom"
        }

        /// <summary>
        ///     makes object invisible and its' update functions will no longer be called
        /// </summary>
        public void disable()
        {
            if (myObject.activeSelf) myObject.SetActive(false);
        }

        /// <summary>
        ///     makes the object visible and it's update functions will be called every frame
        /// </summary>
        public void enable()
        {
            if (!myObject.activeSelf) myObject.SetActive(true);
        }

        public bool isActive()
        {
            return myObject != null && myObject.activeInHierarchy;
        }
    }
}