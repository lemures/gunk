﻿using UnityEngine;

namespace Gunk_Scripts.GUI
{
    public class GuiFullScreenImage:GuiImage
    {
        public GuiFullScreenImage(RenderTexture renderTexture) : base(0, 0, Screen.width, Screen.height, renderTexture, 0)
        {
            var material = DatabasesManager.getObjectLatestVersion("GuiFullScreenImageMat") as Material;
            if (material == null)
            {
                throw new MissingResourcesException("GuiFullScreenImageMat");
            }
            setMaterial(material);
            GuiManager.updateCaller.addFunction(this, () =>
            {
                setPosition(0, 0);
                var size = GuiManager.layersManager.getScreenGuiDimensions();
                registerSize((int)size.x, (int)size.y);
            });
        }
    }
}
