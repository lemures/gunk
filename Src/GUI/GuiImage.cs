﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Gunk_Scripts.GUI
{
    public class GuiImage : GuiElement
    {
        private Button button;
        private RawImage image;

        private readonly List<Action<Vector2>> mousePosInTexCoordsListeners = new List<Action<Vector2>>();

        public GuiImage(int x, int y, int width, int height, Texture texture,
            int layer) :
            base(x, y, layer, "Image", GuiManager.GuiPrimitives.getPrimitive(GuiManager.GuiPrimitives.PrimitiveType.SimpleImage))
        {
            image = myObject.GetComponent<RawImage>();
            image.texture = texture;
            image.color = Color.white;
            button = myObject.GetComponent<Button>();
            button.transition = Selectable.Transition.None;
            registerSize(width, height);


            GuiManager.updateCaller.addFunction(this, update);
        }

        public void setImageColor(Color color)
        {
            image.color = color;
        }

        public void setImage(Texture texture)
        {
            image.texture = texture;
        }

        public void setMaterial(Material material)
        {
            image.material = material;
        }

        public void addOnClickListener(Action func)
        {
            button.onClick.AddListener(delegate { func(); });
        }

        public Vector2 getMousePosInTexCoords()
        {
            Vector2 mousePosTexCoords = Input.mousePosition;
            var texPos = getScreenPosition();
            var texSize = getScreenDimensions();
            mousePosTexCoords -= texPos;
            mousePosTexCoords.x /= texSize.x;
            mousePosTexCoords.y /= texSize.y;
            mousePosTexCoords.y += 1.0f;

            return mousePosTexCoords;
        }

        public void addGetMousePosInTexCoordsListener(Action<Vector2> fun)
        {
            mousePosInTexCoordsListeners.Add(fun);
        }

        private void update()
        {
            if (mousePosInTexCoordsListeners == null) return;

            var mousePos = getMousePosInTexCoords();
            foreach (var fun in mousePosInTexCoordsListeners) fun(mousePos);
        }
    }
}