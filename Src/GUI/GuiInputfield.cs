﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gunk_Scripts.GUI
{
    public class GuiInputfield : GuiElement
    {
        private readonly InputField inputfield;
        private readonly TextMeshProUGUI propertyField;
        private List<Action<string>> onValueSubmittedListeners = new List<Action<string>>();

        public GuiInputfield(int x, int y, int width, int height, string title, int layer) :
            base(x, y, layer, "Inputfield: " + title, GuiManager.GuiPrimitives.getPrimitive(GuiManager.GuiPrimitives.PrimitiveType.InputField))
        {
            inputfield = myObject.GetComponent<InputField>();
            propertyField = (TextMeshProUGUI) inputfield.placeholder;
            setPropertyName(title);
            registerSize(width, height);
        }

        public void setPropertyName(string title)
        {
            propertyField.text = title;
        }

        public void setValue(string val)
        {
            inputfield.text = val;
        }

        public string text => inputfield.text;

        /// <summary>
        ///     Add function that will be called every time the input text value is changed
        /// </summary>
        public void addOnValueChangedListener(Action<string> func)
        {
            inputfield.onValueChanged.AddListener(delegate { func(inputfield.text); });
        }

        /// <summary>
        ///     Add function that will be called every time the editing is finished
        /// </summary>
        public void addOnEndEditListener(Action<string> func)
        {
            inputfield.onEndEdit.AddListener(delegate { func(inputfield.text); });
        }

        public void addOnValueSubmittedListener(Action<string> func)
        {
            onValueSubmittedListeners.Add(func);
            inputfield.onEndEdit.AddListener(delegate
            {
                if (Input.GetKey(KeyCode.Return))
                {
                    func(inputfield.text);
                }
            });
        }

        private bool multiLineEnabled;

        private bool wasReturnSubmitted()
        {
            return inputfield.text.Contains("\n");
        }

        public void enableMultiLineEditing()
        {
            if (multiLineEnabled)
            {
                return;
            }

            multiLineEnabled = true;

            inputfield.lineType = InputField.LineType.MultiLineNewline;
            addOnValueChangedListener(x =>
            {
                if (!wasReturnSubmitted()) return;

                onValueSubmittedListeners.ForEach(func => func.Invoke(inputfield.text.Replace("\n", "")));
                inputfield.text = "";
            });
        }

        public bool isBeingEdited()
        {
            return inputfield.isFocused;
        }
    }
}