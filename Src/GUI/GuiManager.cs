﻿using System;
using System.Collections.Generic;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.SkeletonAnimations;
using UnityEngine;

namespace Gunk_Scripts.GUI
{
    [Serializable]
    public static class GuiManager
    {
        public static GuiColors colors=new GuiColors();

        public static GuiCurveEditorManager guiCurveEditorManager;

        public static LayersManager layersManager = new LayersManager();

        public static UpdateCaller<GuiElement> updateCaller = new UpdateCaller<GuiElement>(x=>x.isActive());

        public static void update()
        {
            updateCaller.invokeAll();
        }

        [Serializable]
        public static class GuiPrimitives
        {
            public enum PrimitiveType
            {
                Button,
                DecoratedButton,
                SimpleImage,
                Slider,
                Toggle,
                WindowHeader,
                Dropdown,
                InputField,
                TextField,
                SimpleNotification
            }

            public static Material visualizationMat
            {
                get
                {
                    if (_visualizationMat == null)
                    {
                        _visualizationMat = (Material)DatabasesManager.getObjectLatestVersion("Curve visualization");
                    }
                    return _visualizationMat;
                }
            }

            private static string primitiveTypeToString(PrimitiveType type)
            {
                switch (type)
                {
                    case PrimitiveType.Button:
                        return "Button";
                    case PrimitiveType.DecoratedButton:
                        return "Decorated Button";
                    case PrimitiveType.SimpleImage:
                        return "Simple image";
                    case PrimitiveType.WindowHeader:
                        return "Window Header";
                    case PrimitiveType.Dropdown:
                        return "Dropdown";
                    case PrimitiveType.Slider:
                        return "Slider";
                    case PrimitiveType.InputField:
                        return "Input Field";
                    case PrimitiveType.Toggle:
                        return "Toggle";
                    case PrimitiveType.TextField:
                        return "Text Field";
                    case PrimitiveType.SimpleNotification:
                        return "Simple Notification";
                }

                return "";
            }

            private static Dictionary<string, GameObject> primitives = new Dictionary<string, GameObject>();

            private static Material _visualizationMat;

            public static GameObject getPrimitive(PrimitiveType type)
            {
                string name = primitiveTypeToString(type);
                if (!primitives.ContainsKey(name))
                {
                    primitives[name] = (GameObject)DatabasesManager.getObjectLatestVersion(name, "GUI");
                    if (primitives[name] == null)
                    {
                        throw new MissingResourcesException(type.ToString());
                    }
                }
                return primitives[name];
            }

            public static GameObject createEmptyCanvas(string name)
            {
                var pom = createEmptyRectTransform(name);
                var canvas = pom.AddComponent<Canvas>();
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;
                canvas.pixelPerfect = true;
                return pom;
            }

            public static GameObject createEmptyRectTransform(string name)
            {
                var pom = new GameObject();
                var rect = pom.AddComponent<RectTransform>();
                rect.sizeDelta = new Vector2(0, 0);
                rect.pivot = new Vector2(0, 1);
                rect.anchorMin = new Vector2(0, 1);
                rect.anchorMax = new Vector2(0, 1);
                rect.anchoredPosition = new Vector2(0, 0);
                pom.name = name;
                return pom;
            }
        }

        public class GuiCurveEditorManager
        {
            private const int MAX_CURVE_EDITORS = 100;

            public BezierGpu[] allCurvesGpuData =
                new BezierGpu[MAX_CURVE_EDITORS];

            public IdManager visualizationTexturesIDs = new IdManager();

            public GuiCurveEditorManager()
            {

            }

            public int findFreeId()
            {
                return visualizationTexturesIDs.getFreeId();
            }

            public void deregisterId(int id)
            {
                visualizationTexturesIDs.deregisterId(id);
            }

            public void sendCurveData(int id, BezierGpu curveGpu)
            {
                allCurvesGpuData[id] = curveGpu;
                KernelUtils.getBuffer("Current_bezier").
                    SetData(allCurvesGpuData, id, id, 1);
            }
        }

        [Serializable]
        public class GuiColors
        {
            public Color basicBackgroundColor=Color.gray;
            public Color decorationColor;
            public Color selectedBackgroundColor;
        }

        public static void reset()
        {
            layersManager = new LayersManager();
            updateCaller = new UpdateCaller<GuiElement>(x=>x.isActive());
        }
    }
}