﻿using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.GUI
{
    /// <summary>
    ///     Object with several GUI elements attached, with a bar with buttons at its top to choose which one of that elements is currently
    ///     visible
    /// </summary>
    public class GuiMultimenu : GuiElement
    {
        public delegate void VoidFunction();

        private GuiImage _barBackground;
        private readonly int _barHeight = 30;
        private readonly List<GuiButton> _buttons = new List<GuiButton>();

        private readonly int _estimatedLetterWidth = 10;

        private readonly List<MultimenuOption> _options = new List<MultimenuOption>();
        private int _totalButtonsWidth;



        public GuiMultimenu(int x, int y, int layer, string name) : base(x, y,
            layer, "Multimenu:" + name, null)
        {
        }

        /// <summary>
        /// Adds Button to show/hide one option. Clicking on buttons only hides them without disabling anything. Attaches object to this Multimenu.
        /// </summary>
        public void addOption(GuiElement element, string name, VoidFunction optionEnabledFunction,
            VoidFunction optionDisabledFunction)
        {
            element.setPosition(-10000, 0);
            var buttonWidth = 100;
            if (name != null)
                buttonWidth = 30 + _estimatedLetterWidth * name.Length;
            else
                Debug.Log("Warning! null name option was tried to be added to GUI_Multimenu");

            var newButton = new GuiButton(GuiButton.ButtonStyle.Simple, _totalButtonsWidth, 0,
                buttonWidth, _barHeight, name, 0);
            _totalButtonsWidth += buttonWidth;
            _buttons.Add(newButton);
            newButton.attachToObject(this);
            var myPosition = getPosition();
            element.attachToObject(this);
            newButton.addOnClickListener(
                () => { setOptionAsActive(element); });

            _options.Add(new MultimenuOption(element, newButton, optionEnabledFunction, optionDisabledFunction));
        }

        public void setOptionAsActive(GuiElement element)
        {
            foreach (var current in _options)
            {
                current.element.setPosition(-10000, 0);
                if (element == current.element)
                    current.optionEnabledFunction?.Invoke();
                else
                    current.optionDisabledFunction?.Invoke();
            }

            element.setPosition(10, 10 + _barHeight);
        }

        private class MultimenuOption
        {
            public GuiButton button;
            public readonly GuiElement element;
            public readonly VoidFunction optionDisabledFunction;

            public readonly VoidFunction optionEnabledFunction;

            public MultimenuOption(GuiElement element, GuiButton button, VoidFunction optionEnabledFunction,
                VoidFunction optionDisabledFunction)
            {
                this.element = element;
                this.button = button;
                this.optionDisabledFunction = optionDisabledFunction;
                this.optionEnabledFunction = optionEnabledFunction;
            }
        }
    }
}