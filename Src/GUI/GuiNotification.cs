using Gunk_Scripts.GUI;
using UnityEngine;
using UnityEngine.UI;

namespace Src.GUI
{
    public class GuiNotification : GuiElement
    {
        private float timeAlive = 0;

        private void checkAutoDestruction()
        {
            timeAlive += Time.unscaledDeltaTime;
            if (timeAlive > 3.0f)
            {
                dispose();
            }
        }

        public GuiNotification(string text) : base(Screen.height/2, Screen.width/2, 10, "Notification",
            GuiManager.GuiPrimitives.getPrimitive(GuiManager.GuiPrimitives.PrimitiveType.SimpleNotification))
        {
            GuiManager.updateCaller.addFunction(this, checkAutoDestruction);
            myObject.GetComponentInChildren<Text>().text = text;
        }
    }
}