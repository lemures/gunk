﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.GUI
{
    public class GuiSearchInputfield : GuiElement
    {
        private readonly List<KeyValuePair<string, int>> currentlyMatching = new List<KeyValuePair<string, int>>();
        private int currentlySelectedOptionIndex = -1;

        private int firstVisibleMatchingOptionIndex;

        private readonly GuiInputfield inputfield;

        private float lastTimeSelectionChanged;
        private int lastVisibleMatchingOptionIndex = -1;
        private readonly List<Action<int>> listeners = new List<Action<int>>();
        private readonly List<SearchOptionObject> optionsObjects = new List<SearchOptionObject>();
        private List<KeyValuePair<string, int>> searchOptions = new List<KeyValuePair<string, int>>();
        private readonly float timeDiffNeeded = 0.12f;
        private float timePressed;
        private readonly float timePressedNeeded = 0.3f;

        public GuiSearchInputfield(int x, int y, int width, int height, string title,
            int layer) : base(x, y, layer, "Inputfield: " + title, null)
        {
            inputfield = new GuiInputfield(0, 0, width, height, title, layer);
            for (var i = 0; i < 5; i++) optionsObjects.Add(new SearchOptionObject(width, height));
            inputfield.attachToObject(this);
            inputfield.addOnValueChangedListener(updateVisibleOptions);
            inputfield.addOnEndEditListener(checkMouseAndSendCurrentlySelectedOption);

            GuiManager.updateCaller.addFunction(this, handleKeyboard);
            GuiManager.updateCaller.addFunction(this, handleMouse);

            registerSize(width, height);
        }

        private void showMatchingOptionsRange()
        {
            foreach (var optionObject in optionsObjects)
            {
                optionObject.deselectMe();
                optionObject.disable();
            }

            if (firstVisibleMatchingOptionIndex > lastVisibleMatchingOptionIndex) return;

            var myPosition = getPosition();
            var myDimensions = getDimensions();

            for (var i = firstVisibleMatchingOptionIndex; i <= lastVisibleMatchingOptionIndex; i++)
            {
                var currentNumber = i - firstVisibleMatchingOptionIndex;
                var editedField = optionsObjects[currentNumber];
                editedField.enable();
                editedField.setPosition((int) myPosition.x,
                    (int) myPosition.y + currentNumber * (int) myDimensions.y + (int) myDimensions.y);
                editedField.setOption(currentlyMatching[i]);
            }

            optionsObjects[currentlySelectedOptionIndex - firstVisibleMatchingOptionIndex].selectMe();
        }

        private void hideAllOptions()
        {
            currentlyMatching.Clear();
            firstVisibleMatchingOptionIndex = 0;
            lastVisibleMatchingOptionIndex = -1;
            currentlySelectedOptionIndex = -1;
            showMatchingOptionsRange();
        }

        private void updateVisibleOptions(string val)
        {
            currentlyMatching.Clear();

            if (val.Length == 0)
            {
                hideAllOptions();
            }

            foreach (var option in searchOptions)
                if (val.Length < 2
                    || option.Key.Length >= val.Length && option.Key.ToLower().Contains(val.ToLower()))
                    currentlyMatching.Add(option);
            firstVisibleMatchingOptionIndex = 0;
            lastVisibleMatchingOptionIndex = Mathf.Min(currentlyMatching.Count - 1, optionsObjects.Count - 1);
            currentlySelectedOptionIndex = 0;
            showMatchingOptionsRange();
        }

        /// <param name="dummmy">this parameter will not be used</param>
        private void checkMouseAndSendCurrentlySelectedOption(string dummmy)
        {
            //Left mouse Button was pressed, check if any option was clicked
            bool wasClicked = false;
            if (Input.GetMouseButtonDown(0) && firstVisibleMatchingOptionIndex <= lastVisibleMatchingOptionIndex)
            {
                for (var i = firstVisibleMatchingOptionIndex; i <= lastVisibleMatchingOptionIndex; i++)
                {
                    var currentNumber = i - firstVisibleMatchingOptionIndex;
                    var editedField = optionsObjects[currentNumber];
                    if (editedField.isPointerOnObject())
                    {
                        currentlySelectedOptionIndex = i;
                        wasClicked = true;
                    }
                }
            }

            if (Input.GetKey(KeyCode.Return) || wasClicked)
            {
                callAllListeners();
            }

            inputfield.setValue("");
            hideAllOptions();
        }

        private void callAllListeners()
        {
            foreach (var fun in listeners)
            {
                if (firstVisibleMatchingOptionIndex > lastVisibleMatchingOptionIndex) continue;

                fun(currentlyMatching[currentlySelectedOptionIndex].Value);
            }
        }

        private void handleMouse()
        {
            //Left mouse Button was pressed
            if (MouseUtils.instance.wasClicked())
            {
                if (isPointerOnObject())
                {
                    updateVisibleOptions(" ");
                }
                else
                {
                    hideAllOptions();
                }
            }

            //mark option that mouse is over
            if (firstVisibleMatchingOptionIndex <= lastVisibleMatchingOptionIndex)
                for (var i = firstVisibleMatchingOptionIndex; i <= lastVisibleMatchingOptionIndex; i++)
                {
                    var currentNumber = i - firstVisibleMatchingOptionIndex;
                    var editedField = optionsObjects[currentNumber];
                    if (editedField.isPointerOnObject())
                    {
                        editedField.setMouseIsOver();
                    }
                    else
                    {
                        editedField.unsetMouseIsOver();
                    }
                }

            //Right mouse Button wa pressed, canceling
            if (Input.GetMouseButtonDown(1)) hideAllOptions();
        }

        private void handleKeyboard()
        {
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.UpArrow))
                timePressed += Time.unscaledDeltaTime;
            else
                timePressed = 0;
            if (Input.GetKeyDown(KeyCode.DownArrow) || timePressed > timePressedNeeded &&
                Input.GetKey(KeyCode.DownArrow) && lastTimeSelectionChanged + timeDiffNeeded < Time.unscaledTime)
            {
                lastTimeSelectionChanged = Time.unscaledTime;
                if (currentlySelectedOptionIndex != currentlyMatching.Count - 1)
                {
                    currentlySelectedOptionIndex++;
                    if (lastVisibleMatchingOptionIndex != currentlyMatching.Count - 1 &&
                        currentlySelectedOptionIndex > lastVisibleMatchingOptionIndex)
                    {
                        firstVisibleMatchingOptionIndex++;
                        lastVisibleMatchingOptionIndex++;
                    }
                }

                showMatchingOptionsRange();
            }


            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKey(KeyCode.UpArrow) &&
                lastTimeSelectionChanged + timeDiffNeeded < Time.unscaledTime)
            {
                lastTimeSelectionChanged = Time.unscaledTime;
                if (currentlySelectedOptionIndex != 0)
                {
                    currentlySelectedOptionIndex--;
                    if (firstVisibleMatchingOptionIndex != 0 &&
                        currentlySelectedOptionIndex < firstVisibleMatchingOptionIndex)
                    {
                        firstVisibleMatchingOptionIndex--;
                        lastVisibleMatchingOptionIndex--;
                    }
                }

                showMatchingOptionsRange();
            }
        }

        public void setSearchOptions(List<KeyValuePair<string, int>> options)
        {
            searchOptions.Clear();
            foreach (var option in options) searchOptions.Add(option);
            searchOptions.Sort((a, b) => a.Key.CompareTo(b.Key));
            hideAllOptions();
        }

        public void setPropertyName(string title)
        {
            inputfield.setPropertyName(title);
        }

        public void setValue(string val)
        {
            inputfield.setValue(val);
        }

        public void addSubmitValueListener(Action<int> fun)
        {
            listeners.Add(fun);
        }

        private class SearchOptionObject : GuiElement
        {
            private readonly GuiImage backgroundBorder;
            private readonly GuiImage backgroundFront;
            private readonly Color deselectedColorBorder = new Color(0.2f, 0.2f, 0.2f, 1.0f);
            private readonly Color deselectedColorFront = new Color(0.4f, 0.4f, 0.4f, 1.0f);

            private readonly Color selectedColorBorder = new Color(0.9f, 0.2f, 0.2f, 1.0f);
            private readonly Color selectedColorFront = new Color(0.5f, 0.5f, 0.5f, 0.9f);

            private readonly Color mouseColorBorder = new Color(0.8f, 0.6f, 0.4f, 1.0f);
            private readonly Color mouseColorFront = new Color(0.45f, 0.45f, 0.45f, 0.9f);

            private readonly GuiTextfield value;

            private bool isSelected;
            private bool isMouseOverMe;

            public SearchOptionObject(int width, int height) : base(0, 0, 7,
                "search option object", null)
            {
                backgroundBorder = new GuiImage(0, 0, width, height, null, 7);
                backgroundBorder.attachToObject(this);

                backgroundFront = new GuiImage(2, 2, width - 4, height - 4, null,7);
                backgroundFront.attachToObject(backgroundBorder);

                value = new GuiTextfield(2, 2, width - 4, height - 4, "", 7);
                value.attachToObject(backgroundFront);

                updateColors();
                disable();
            }

            public void selectMe()
            {
                isSelected = true;
                updateColors();
            }

            public void deselectMe()
            {
                isSelected = false;
                updateColors();
            }

            public void setMouseIsOver()
            {
                isMouseOverMe = true;
                updateColors();
            }

            public void unsetMouseIsOver()
            {
                isMouseOverMe = false;
                updateColors();
            }

            private void updateColors()
            {
                if (isMouseOverMe)
                {
                    backgroundFront.setImageColor(mouseColorFront);
                    backgroundBorder.setImageColor(mouseColorBorder);
                    return;
                }

                if (isSelected)
                {
                    backgroundFront.setImageColor(selectedColorFront);
                    backgroundBorder.setImageColor(selectedColorBorder);
                    return;
                }

                backgroundFront.setImageColor(deselectedColorFront);
                backgroundBorder.setImageColor(deselectedColorBorder);
            }

            public void setOption(KeyValuePair<string, int> option)
            {
                value.setText(option.Key);
            }
        }
    }
}