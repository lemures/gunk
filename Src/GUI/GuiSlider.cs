﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gunk_Scripts.GUI
{
    public class GuiSlider : GuiElement
    {
        /// <summary>
        ///     Add function that will be called every time the value is changed
        /// </summary>
        public delegate void SendFloat(float val);

        public enum SliderType
        {
            FloatSlider,
            IntSlider,
            ExponentialSlider,
            IntExponentialSlider
        }

        private readonly SliderType _myType;
        private readonly TextMeshProUGUI _propertyField;
        private bool _showValue;
        private readonly Slider _slider;
        private readonly TextMeshProUGUI _valueField;

        public GuiSlider(SliderType myType, int x, int y, float minValue, float maxValue,
            float currentValue, string variableName, bool showValue, int layer) :
            base(x, y, layer, "Slider: " + variableName, GuiManager.GuiPrimitives.getPrimitive(GuiManager.GuiPrimitives.PrimitiveType.Slider))
        {
            _slider = myObject.GetComponent<Slider>();
            var myTexts = myObject.GetComponentsInChildren<TextMeshProUGUI>();
            foreach (var text in myTexts)
            {
                if (text.transform.gameObject.name == "Name")
                {
                    _propertyField = text;
                    _propertyField.text = "";
                }

                if (text.transform.gameObject.name == "Value")
                {
                    _valueField = text;
                    _valueField.text = "";
                }
            }

            _myType = myType;
            _slider.wholeNumbers = myType == SliderType.IntSlider;
            addValueChangedListener(updateValueField);
            setRange(minValue, maxValue, currentValue);
            if (showValue)
                this.showValue();
            else
                hideValue();
            setPropertyName(variableName);
        }

        public void setRange(float min, float max, float currentValue)
        {
            if (_myType == SliderType.ExponentialSlider || _myType == SliderType.IntExponentialSlider)
            {
                _slider.minValue = Mathf.Log(min);
                _slider.maxValue = Mathf.Log(max);
                _slider.value = Mathf.Log(currentValue);
                return;
            }

            _slider.minValue = min;
            _slider.maxValue = max;
            _slider.value = currentValue;
        }

        public void setPropertyName(string title)
        {
            _propertyField.text = title;
            myObject.name = "Slider: " + title;
        }

        public void showValue()
        {
            _showValue = true;
            updateValueField(getRealValue(_slider.value));
        }

        public void hideValue()
        {
            _showValue = false;
            updateValueField(getRealValue(_slider.value));
        }

        public void updateValueField(float val)
        {
            _valueField.text = _showValue ? " " + PrettyPrinter.reformatFloat(val) : "";
        }

        private float getRealValue(float val)
        {
            switch (_myType)
            {
                case SliderType.ExponentialSlider:
                    return Mathf.Exp(val);
                case SliderType.IntExponentialSlider:
                    return Mathf.Max((int) _slider.minValue, (int) Mathf.Exp(val));
            }

            return val;
        }

        public float getValue()
        {
            return getRealValue(_slider.value);
        }

        public void addValueChangedListener(SendFloat func)
        {
            _slider.onValueChanged.AddListener(delegate { func(getRealValue(_slider.value)); });
        }
    }
}