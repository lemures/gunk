﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

namespace Gunk_Scripts.GUI
{
    public class GuiTextfield : GuiElement
    {
        private readonly TextMeshProUGUI textField;

        public void setText(string value)
        {
            textField.text = value;
            textField.ForceMeshUpdate();
            lastSetValue = value;
            resetScroll();
        }

        public string getText()
        {
            return lastSetValue;
        }

        private string lastSetValue;

        public GuiTextfield(int x, int y, int width, int height, string text, int layer) :
            base(x, y, layer, "Textfield", GuiManager.GuiPrimitives.getPrimitive(GuiManager.GuiPrimitives.PrimitiveType.TextField))
        {
            textField = myObject.GetComponent<TextMeshProUGUI>();
            textField.text = text;
            registerSize(width, height);
            GuiManager.updateCaller.addFunction(this, handleScrolling);
        }

        public void setModeToOverflow()
        {
            textField.overflowMode = TextOverflowModes.Overflow;
        }

        public void setModeToTruncate()
        {
            textField.overflowMode = TextOverflowModes.Truncate;
        }

        public void setTextAlignmentToBottom()
        {
            textField.alignment = TextAlignmentOptions.BottomLeft;
        }

        public void setTextAlignmentToTop()
        {
            textField.alignment = TextAlignmentOptions.TopLeft;
        }

        public void setTextFontSize(int size)
        {
            textField.enableAutoSizing = false;
            textField.fontSizeMax = size;
            textField.fontSizeMin = size;
        }

        private int getCharacterCount()
        {
            var count = 0;
            textField.textInfo.lineInfo.ToList().ForEach(x => count+=x.characterCount);
            return count;
        }

        private int getRichTextTagLength(string richText, int startIndex)
        {
            if (richText.Length <= startIndex || richText[startIndex] != '<')
            {
                return 0;
            }

            bool equalFound = false;
            var currentIndex = startIndex;
            while (currentIndex < richText.Length && richText[currentIndex] != '>')
            {
                if (richText[currentIndex] == '=')
                {
                    equalFound = true;
                }
                currentIndex++;
            }

            return equalFound && richText[currentIndex] == '>' ? currentIndex - startIndex + 1 : 0;
        }

        private int getLinesCount()
        {
            var result = 0;
            var lines = textField.textInfo;
            var charIndex = 0;
            foreach (var line in lines.lineInfo)
            {
                charIndex += line.characterCount;
                if (line.characterCount != 0 || charIndex != getCharacterCount())
                {
                    result++;
                }
            }
            return result + 1;
        }

        private List<string> getLines()
        {
            var linesGenerated = new List<string>();
            int linesCount = getLinesCount();
            var lines = textField.textInfo;
            var currentIndex = 0;

            foreach (var line in lines.lineInfo)
            {
                var currentLine = "";
                for (var i = 0; i < line.characterCount; i++)
                {
                    while (getRichTextTagLength(getText(), currentIndex) > 0)
                    {
                        var tagLength = getRichTextTagLength(getText(), currentIndex);
                        currentLine += getText().Substring(currentIndex, tagLength);
                        currentIndex += tagLength;
                    }

                    currentLine += getText()[currentIndex];
                    currentIndex++;
                }

                linesGenerated.Add(currentLine);
                if (linesGenerated.Count == linesCount)
                {
                    break;
                }
            }
            return linesGenerated;
        }

        private bool scrollingEnabled;
        private int linesScrolled = 0;
        private List<string> cachedLines = new List<string>();

        private void resetScroll()
        {
            linesScrolled = 0;
            cachedLines = getLines();
        }

        private void setSkippedLines(int skippedCount)
        {
            var text = "";
            int firstVisibleLine = Math.Max(0, cachedLines.Count - skippedCount - 16);
            for (int i = firstVisibleLine; i < cachedLines.Count - skippedCount; i++)
            {
                text += cachedLines[i];
            }

            textField.text = text;
        }

        private void handleScrolling()
        {
            if (scrollingEnabled)
            {
                var maxLinesOnScreen = 14;
                linesScrolled += (int) Input.mouseScrollDelta.y;
                linesScrolled = Mathf.Clamp(linesScrolled, 0, Mathf.Max(0, cachedLines.Count - maxLinesOnScreen));
                setSkippedLines(linesScrolled);
            }
        }

        public void enableScrolling()
        {
            scrollingEnabled = true;
        }
        public void disableScrolling()
        {
            scrollingEnabled = false;
        }
    }
}