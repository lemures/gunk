﻿using TMPro;
using UnityEngine.UI;

namespace Gunk_Scripts.GUI
{
    public class GuiToggle : GuiElement
    {
        public delegate void SendEvent(bool val);

        private readonly TextMeshProUGUI _propertyField;
        private readonly Toggle _toggle;

        public GuiToggle(int x, int y, bool value, string title, int layer) :
            base(x, y, layer, "Toggle: " + title, GuiManager.GuiPrimitives.getPrimitive(GuiManager.GuiPrimitives.PrimitiveType.Toggle))
        {
            _toggle = myObject.GetComponent<Toggle>();
            _propertyField = myObject.GetComponentInChildren<TextMeshProUGUI>();
            setValue(value);
            setPropertyName(title);
        }

        public void setPropertyName(string title)
        {
            _propertyField.text = title;
        }

        public void setValue(bool value)
        {
            _toggle.isOn = value;
        }

        /// <summary>
        ///     Add function that will be called every time the the Toggle state is changed
        /// </summary>
        public void addOnClickListener(SendEvent func)
        {
            _toggle.onValueChanged.AddListener(delegate { func(_toggle.isOn); });
        }
    }
}