using UnityEngine;

namespace Gunk_Scripts.GUI
{
    public class GuiWindow : GuiElement
    {
        private static readonly int topWidth = 40;
        private static readonly int borderSize = 3;
        private readonly GuiElement insides;

        /// <summary>
        ///  Adds Closable window border around element
        /// </summary>
        public GuiWindow(GuiElement insides, string title) : base(
            (int) insides.getPosition().x,
            (int) insides.getPosition().y, 1, "GuiWindow", null)
        {
            this.insides = insides;
            insides.attachToObject(this);
            addBorders();
            addTitle(title);
            addCloseButton();
        }


        private void addTitle(string title)
        {
            var name = new GuiTextfield(10, 10, 500, 20, title, 0);
            name.attachToObject(this);
        }

        private void addCloseButton()
        {
            var button = new GuiButton(GuiButton.ButtonStyle.Simple, (int)getDimensions().x-topWidth, 0, topWidth, topWidth, "X",1);
            button.addOnClickListener(() =>
            {
                dispose();
                insides.dispose();
            });
            button.attachToObject(this);
        }

        private void addBorders()
        {
            var totalDimensions = insides.getDimensions() + new Vector2(2 * borderSize, borderSize + topWidth);
            registerSize(totalDimensions);
            insides.setPosition(borderSize, topWidth);

            var top = new GuiImage(0, 0, (int) totalDimensions.x, topWidth, null, 0);
            top.attachToObject(this);
            top.setImageColor(Color.gray);

            var left = new GuiImage(0, 0, borderSize, (int) totalDimensions.y, null, 0);
            left.attachToObject(this);
            left.setImageColor(Color.gray);

            var right = new GuiImage((int) totalDimensions.x - borderSize, 0, borderSize, (int) totalDimensions.y, null,
                0);
            right.attachToObject(this);
            right.setImageColor(Color.grey);

            var bottom = new GuiImage(0, (int) totalDimensions.y - borderSize, (int) totalDimensions.x, borderSize, null,
                0);
            bottom.attachToObject(this);
            bottom.setImageColor(Color.grey);
        }
    }
}