﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Gunk_Scripts.GUI
{
    public class LayersManager
    {
        private GameObject canvasRoot;
        private int lastInitializedLayer = -1;
        private readonly List<GameObject> layers = new List<GameObject>();

        public void attachToLayer(GuiElement myObject, int layer)
        {
            while (layer > lastInitializedLayer) createNewLayer();
            myObject.attachToObject((RectTransform) layers[layer].transform);
        }

        private void createNewLayer()
        {
            lastInitializedLayer++;
            layers.Add(initializeEmptyLayer(lastInitializedLayer));
            layers[lastInitializedLayer].GetComponent<Canvas>().sortingOrder = lastInitializedLayer;
        }

        private GameObject initializeEmptyLayer(int layerId)
        {
            var pom = GuiManager.GuiPrimitives.createEmptyCanvas("layer " + layerId);

            pom.AddComponent<GraphicRaycaster>();
            var scaler = pom.AddComponent<CanvasScaler>();
            scaler.referencePixelsPerUnit = 100;
            scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
            scaler.matchWidthOrHeight = 1;
            scaler.referenceResolution = new Vector2(1920, 1080);
            scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            if (canvasRoot == null)
            {
                canvasRoot = new GameObject {name = "Global UI Root"};
            }
            pom.transform.SetParent(canvasRoot.transform, false);

            return pom;
        }

        public LayersManager()
        {
            initializeEmptyLayer(0);
        }

        public Vector2 getScreenGuiDimensions() => ((layers[0].transform) as RectTransform).sizeDelta;
    }
}