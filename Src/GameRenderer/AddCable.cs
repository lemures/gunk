using Gunk_Scripts.Data_Structures;
using UnityEngine;

namespace Src.GameRenderer
{
    public class AddCable : ExtraObjectsChange
    {
        private Edge edge;
        private Vector3 startPosition;
        private Vector3 endPosition;

        public AddCable(Edge edge, Vector3 startPosition, Vector3 endPosition)
        {
            this.edge = edge;
            this.startPosition = startPosition;
            this.endPosition = endPosition;
        }

        public override void execute(GunkExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.cableRenderer.addCable(edge, startPosition, endPosition);
        }
    }
}