using Gunk_Scripts.Data_Structures;

namespace Src.GameRenderer
{
    public class DeleteCable : ExtraObjectsChange
    {
        private readonly Edge edge;

        public DeleteCable(Edge edge)
        {
            this.edge = edge;
        }

        public override void execute(GunkExtraObjectsRenderer objectsRenderer)
        {
            objectsRenderer.cableRenderer.removeCable(edge);
        }
    }
}