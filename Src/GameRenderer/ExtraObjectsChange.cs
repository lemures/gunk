using DefaultNamespace;

namespace Src.GameRenderer
{
    public abstract class ExtraObjectsChange
    {
        public abstract void execute(GunkExtraObjectsRenderer objectsRenderer);
    }
}