using DefaultNamespace;
using Gunk_Scripts.Renderers;
using UnityEngine;

namespace Src.GameRenderer
{
    public class GunkExtraObjectsRenderer : IExtraObjectsRenderer
    {
        private Camera camera;

        public CableRenderer cableRenderer;

        public GunkExtraObjectsRenderer(Camera mainCamera)
        {
            this.camera = mainCamera;
            cableRenderer = new CableRenderer(camera);
        }

        public void receiveChange(ExtraObjectsChange change)
        {
            change.execute(this);
        }

        public void render()
        {
            cableRenderer.render();
        }
    }
}