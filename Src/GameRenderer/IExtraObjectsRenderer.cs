using Src.GameRenderer;

namespace DefaultNamespace
{
    public interface IExtraObjectsRenderer
    {
        void receiveChange(ExtraObjectsChange change);

        void render();
    }
}