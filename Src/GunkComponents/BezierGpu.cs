﻿using UnityEngine;

namespace Gunk_Scripts.SkeletonAnimations
{
    public struct BezierGpu
    {
        public Matrix4x4 basicPointsX;
        public Matrix4x4 basicPointsY;
        public Matrix4x4 outputPointsX;
        public Matrix4x4 outputPointsY;
        public Matrix4x4 inputPointsX;
        public Matrix4x4 inputPointsY;
        public float animationLenght;
        public int numberOfPoints;
        public int specialType;
        public int needAnimation;
    };
}