using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Renderers;
using Src.GameRenderer;
using UnityEngine;

namespace Src.GunkComponents
{
    public class CableChangesReceiver : ICableCollection
    {
        private IGunkObjectManagerInternalData parent;

        public CableChangesReceiver(IGunkObjectManagerInternalData parent)
        {
            this.parent = parent;
        }

        public void addCable(Edge edge, Vector3 startPosition, Vector3 endPosition)
        {
            parent.addVisibleObjectsChange(new AddCable(edge, startPosition, endPosition));
        }

        public void removeCable(Edge edge)
        {
            parent.addVisibleObjectsChange(new DeleteCable(edge));
        }
    }
}