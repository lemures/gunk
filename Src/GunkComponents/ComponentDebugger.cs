using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;

namespace Gunk_Scripts.GunkComponents
{
    public class ComponentDebugger : MonoBehaviour
    {
        [Serializable]
        public class ComponentData
        {
            public string componentType;
            public List<string> data;

            public ComponentData(Type componentType, string data)
            {
                this.componentType = componentType.Name;
                this.data = data.Split('\n').ToList();
            }
        }

        public int currentId;

        public List<ComponentData> allComponentsData = new List<ComponentData>();
        public static Dictionary<int, ComponentDebugger> debuggers = new Dictionary<int, ComponentDebugger>();
        private readonly List<IGunkComponent> components =new List<IGunkComponent>();

        private void Start()
        {
            debuggers[currentId] = this;
        }

        public void addComponent(IGunkComponent component)
        {
            components.Add(component);
        }

        public void removeComponent(IGunkComponent component)
        {
            components.Remove(component);
        }

        public void update()
        {
            Profiler.BeginSample("updating component debugger");
            allComponentsData.Clear();
            foreach (var component in components)
            {
                var componentString = component.ToString();
                allComponentsData.Add(new ComponentData(component.GetType(),componentString));
            }
            Profiler.EndSample();
        }
    }
}