﻿using UnityEditor;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(ComponentDebugger))]
    public class ComponentDebuggerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var currentId = serializedObject.FindProperty("currentId").intValue;
            ComponentDebugger.debuggers[currentId].update();
            foreach (var component in ComponentDebugger.debuggers[currentId].allComponentsData)
            {
                //now component type is already being printed in the first line of data
                //EditorGUILayout.LabelField(component.componentType);
                component.data.ForEach(x => EditorGUILayout.LabelField(x));
                EditorGUILayout.Separator();
            }
        }
    }
}
