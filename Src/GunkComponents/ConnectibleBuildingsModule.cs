﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.Renderers;
using Gunk_Scripts.SkeletonAnimations;
using Src.SkeletonAnimations;
using UnityEngine;
using Enumerable = System.Linq.Enumerable;

namespace Gunk_Scripts.GunkComponents
{
    public class ConnectablesBuildingsModule : IConnectablesNetwork
    {
        public struct ConnectibleObjectData
        {
            public readonly int id;
            public readonly Faction faction;
            public Vector3 position;
            public readonly List<SocketData> socketBones;

            public ConnectibleObjectData(int id,Faction faction, Vector3 position, List<SocketData> socketBones)
            {
                this.faction = faction;
                this.position = position;
                this.socketBones = socketBones;
                this.id = id;
            }
        }

        private readonly SortedDictionary<Faction, ObjectNetworks> buildingNetworks = new SortedDictionary<Faction, ObjectNetworks>();
        private readonly ICableCollection cableCollection;
        private readonly List<ConnectibleObjectData> connectibleBuildingData=new List<ConnectibleObjectData>();
        private Edge? temporaryEdge;

        /// <summary>
        /// returns closest connector position in range or null if there are none
        /// </summary>
        private int? getClosestConnectorId(Vector3 position, Faction faction) => buildingNetworks[faction].getClosestConnectorIdInRange(position);

        private static Vector3 getClosestSocketPosition(List<SocketData> socketBones, Vector3 otherObjectPosition, Vector3 myPosition)
        {
            var solution=socketBones.OrderBy(x => Vector3.Distance(myPosition + x.position, otherObjectPosition)).First();
            return solution.position + myPosition;
        }

        private ConnectibleObjectData findObject(int id) => connectibleBuildingData.Find(x => x.id == id);

        private void validateNoObjectWithIdExists(int id)
        {
            if (connectibleBuildingData.Exists(x => x.id==id))
            {
                throw new Exception("building with id "+ id + " already Exists");
            }
        }

        private void validateObjectWithIdExists(int id)
        {
            if (!connectibleBuildingData.Exists(x => x.id==id))
            {
                throw new Exception("building with id "+ id + " doesn't exist");
            }
        }

        private void validateThereIsAtLeastOneSocket(List<SocketData> sockets)
        {
            if (sockets.Count == 0)
            {
                throw new InvalidOperationException("you cannot connect building that has no socket bones");
            }
        }

        public void addConnector(ConnectibleObjectData objectToAdd, float connectorRange)
        {
            validateThereIsAtLeastOneSocket(objectToAdd.socketBones);
            validateNoObjectWithIdExists(objectToAdd.id);
            var operationResult = buildingNetworks[objectToAdd.faction]
                .addConnector(objectToAdd.id, objectToAdd.position, connectorRange);
            connectibleBuildingData.Add(objectToAdd);
            executeBuildingNetworksOperationResult(operationResult);
        }

        public void addObject(ConnectibleObjectData data)
        {
            validateThereIsAtLeastOneSocket(data.socketBones);
            validateNoObjectWithIdExists(data.id);
            var operationResult = buildingNetworks[data.faction]
                .addObject(data.id, data.position);
            connectibleBuildingData.Add(data);
            executeBuildingNetworksOperationResult(operationResult);
        }

        private Faction findObjectFaction(int id)
        {
            validateObjectWithIdExists(id);
            return connectibleBuildingData.Find(x => x.id == id).faction;
        }

        public void transformIntoConnector(int objectId, float range)
        {
            validateObjectWithIdExists(objectId);
            var faction = findObjectFaction(objectId);
            buildingNetworks[faction].transformIntoConnector(objectId,range);
        }

        public bool isConnectorNearby(Vector3 position, Faction faction) => buildingNetworks[faction].isConnectorNearby(position);

        public void clearTemporaryConnections()
        {
            if (temporaryEdge == null) return;

            cableCollection.removeCable(temporaryEdge.Value);
            temporaryEdge = null;
        }

        private void executeBuildingNetworksOperationResult(ObjectNetworks.OperationResult operationResult)
        {
            foreach (var edge in operationResult.addedEdges)
            {
                var start = findObject(edge.x);
                var end = findObject(edge.y);
                var objectStartPosition = start.position;
                var objectEndPosition = end.position;
                var socketStartPosition = getClosestSocketPosition(start.socketBones, objectEndPosition,start.position);
                var socketEndPosition = getClosestSocketPosition(end.socketBones, objectStartPosition,end.position);
                cableCollection.addCable(edge, socketStartPosition, socketEndPosition);
            }

            operationResult.removedEdges.ForEach(cableCollection.removeCable);
        }

        public void removeObject(int id)
        {
            validateObjectWithIdExists(id);
            var building = findObject(id);
            connectibleBuildingData.Remove(building);
            var result = buildingNetworks[building.faction].removeObject(building.id);
            executeBuildingNetworksOperationResult(result);
        }

        /// <summary>
        /// temporarily adds connection between socket from provided object and closest connector from it
        /// and erases the one that already exists
        /// or throws InvalidOperationException if building cannot be connected
        /// </summary>
        public void temporarilyConnectBuilding(List<SocketData> sockets, Vector3 position, Faction faction)
        {
            validateThereIsAtLeastOneSocket(sockets);
            var closestId = getClosestConnectorId(position, faction);
            if (closestId==null)
            {
                throw new InvalidOperationException("building cannot be connected");
            }
            clearTemporaryConnections();
            var closestBuilding = findObject(closestId.Value);
            var closestPosition = getClosestSocketPosition(closestBuilding.socketBones,position, closestBuilding.position);
            var closestSocket = sockets
                .OrderBy(z => Vector3.Distance(position + z.position, closestPosition))
                .First();
            temporaryEdge = new Edge(-1, closestId.Value);
            cableCollection.addCable(temporaryEdge.Value, position+closestSocket.position, closestPosition);
        }

        public ConnectablesBuildingsModule(ICableCollection cableCollection)
        {
            foreach (Faction factionOption in Enum.GetValues(typeof(Faction)))
            {
                buildingNetworks[factionOption] = new ObjectNetworks();
            }

            this.cableCollection = cableCollection;
        }

        public List<List<int>> getAllConnectibleNetworks()
        {
            var result = new List<List<int>>();
            foreach (var network in buildingNetworks.Values)
            {
                result.AddRange(network.getAllConnectedObjectsSets());
            }

            return result;
        }
    }
}