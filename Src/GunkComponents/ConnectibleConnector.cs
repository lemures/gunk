using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class ConnectibleConnector:Connectible,IConstructionFinishedCallbackReceiver
    {
        private readonly float connectorRange;

        public ConnectibleConnector(IGunkObject parent, float connectorRange) : base(parent)
        {
            this.connectorRange = connectorRange;
        }

        public void finishConstruction()
        {
            parent.objectManager.connectablesNetwork.transformIntoConnector(parent.id,connectorRange);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(ConnectibleConnector))
                .addVariable(connectorRange, nameof(connectorRange))
                .print();
        }
    }
}