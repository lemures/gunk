namespace Gunk_Scripts.GunkComponents
{
    public class ConnectibleObject:Connectible
    {
        public ConnectibleObject(IGunkObject parent) : base(parent)
        {
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(ConnectibleConnector))
                .print();
        }
    }
}