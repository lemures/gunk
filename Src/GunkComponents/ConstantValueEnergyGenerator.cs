namespace Gunk_Scripts.GunkComponents
{
    public class ConstantValueEnergyGenerator : IEnergyGenerator
    {
        public float productionLevel { get; }

        public ConstantValueEnergyGenerator(float productionLevel)
        {
            this.productionLevel = productionLevel;
        }

        public float getEnergyGenerationLevel() => productionLevel;
    }
}