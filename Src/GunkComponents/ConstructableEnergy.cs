namespace Gunk_Scripts.GunkComponents
{
    public class ConstructableEnergy: Energy, IConstructionFinishedCallbackReceiver
    {
        public ConstructableEnergy(IGunkObject parent, EntryEnergyState state) : base(parent, state)
        {

        }

        public void finishConstruction()
        {
            parent.currentEnergyUser = this;
        }
    }
}