namespace Gunk_Scripts.GunkComponents
{
    public class UnderConstructionImmediatelyFinished : IConstructionState
    {

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(UnderConstructionImmediatelyFinished))
                .print();
        }

        public bool constructionFinished => true;
        public float constructionProgress => 1;
    }
}