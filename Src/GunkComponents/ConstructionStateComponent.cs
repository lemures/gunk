namespace Gunk_Scripts.GunkComponents
{
    public class ConstructionStateComponent : GunkComponent, IConstructionState, IEnergyUser, IUpdateCallbackReceiver
    {
        private IConstructionState currentState;
        private bool constructionReported;

        private void checkConstructionFinished()
        {
            if (currentState.constructionFinished && !constructionReported)
            {
                parent.addLog(this, "reported construction finished");
                constructionReported = true;
                parent.reportConstructionFinished();
            }
        }

        public void update(float deltaTime)
        {
            checkConstructionFinished();
        }

        public ConstructionStateComponent(IGunkObject parent) : base(parent)
        {
            currentState = new UnderConstructionImmediatelyFinished();
        }

        public void changeState(IConstructionState newState)
        {
            parent.addLog(this, "new construction state: " + newState.GetType().Name);
            currentState = newState;
        }

        public override void destroy()
        {}

        public float calculateEnergyNeeds()
        {
            return (currentState as IEnergyUser)?.calculateEnergyNeeds() ?? 0;
        }

        public void chargeEnergy(float amount)
        {
            parent.addLog(this, "construction charged with energy with: " + PrettyPrinter.reformatFloat(amount) + " energy");
            (currentState as IEnergyUser)?.chargeEnergy(amount);
            checkConstructionFinished();
        }

        public bool constructionFinished => currentState.constructionFinished;
        public float constructionProgress => currentState.constructionProgress;

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(ConstructionStateComponent))
                .addVariable(constructionFinished, nameof(constructionFinished))
                .addVariable(currentState, nameof(currentState))
                .print();
        }
    }
}