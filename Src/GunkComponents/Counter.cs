﻿using System;

namespace Gunk_Scripts.GunkComponents
{
    [System.Serializable]
    public struct Counter
    {
        private float currentValueInternal;
        private float maxValueInternal;

        public Counter(float currentValueInternal, float maxValueInternal)
        {
            this.currentValueInternal = currentValueInternal;
            this.maxValueInternal = maxValueInternal;
        }

        public float maxValue => maxValueInternal;

        public float currentValue => currentValueInternal;

        public float valueToFull => maxValueInternal - currentValueInternal;

        public bool isFull() => (maxValueInternal - currentValueInternal) == 0.0f;

        public bool isEmpty() => currentValueInternal == 0.0f;

        public void decreaseValue(float howMuchToDecrease)
        {
            currentValueInternal = currentValue - howMuchToDecrease;
            currentValueInternal = Math.Max(0, currentValue);
        }

        public void increaseValue(float howMuchToIncrease)
        {
            currentValueInternal = currentValue + howMuchToIncrease;
            currentValueInternal = Math.Min(maxValueInternal, currentValue);
        }

        public void changeValue(float newValue)
        {
            currentValueInternal = newValue;
            currentValueInternal = Math.Min(maxValueInternal, currentValue);
            currentValueInternal = Math.Max(0, currentValue);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(Counter))
                .addVariable(currentValue, nameof(currentValue))
                .addVariable(maxValue,nameof(maxValue))
                .print();
        }
    }
}
