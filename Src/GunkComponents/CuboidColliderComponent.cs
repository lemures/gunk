using Gunk_Scripts.Maps;
using Gunk_Scripts.Objects_Selection;
using Gunk_Scripts.SerializationHelpers;
using Src.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class CuboidColliderComponent : GunkComponent, IPositionChangedCallbackReceiver
    {
        private Vector3 dimensions;

        public CuboidColliderComponent(IGunkObject parent, Vector3 dimensions, SelectionPriority selectionPriority) : base(parent)
        {
            var collider = new CuboidCollider(parent.transform.position, dimensions);
            this.dimensions = dimensions;
            parent.objectManager.collidersCollection.addCollider(parent.id, collider, selectionPriority);
        }

        public override void destroy()
        {
            parent.objectManager.collidersCollection.removeCollider(parent.id);
        }

        public void changePosition(Vector3 newPosition)
        {
            parent.objectManager.collidersCollection.setColliderCenter(parent.id,newPosition);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(CuboidColliderComponent))
                .addVariable(dimensions, nameof(dimensions))
                .print();
        }
    }
}