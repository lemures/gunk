﻿using System;
using Gunk_Scripts.Renderers;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class Energy:GunkComponent, IEnergyUser
    {
        public struct EntryEnergyState
        {
            public readonly float energy;
            public readonly float maxEnergy;
            public readonly float maxEnergyProvidedPerSecond;

            public EntryEnergyState(float energy, float maxEnergy, float maxEnergyProvidedPerSecond)
            {
                this.energy = energy;
                this.maxEnergy = maxEnergy;
                this.maxEnergyProvidedPerSecond = maxEnergyProvidedPerSecond;
            }
        }

        public enum EnergyCounterDrawingBehaviour
        {
            AlwaysVisible,
            AlwaysHidden,
            VisibleIfNotFull,
        }

        public Energy(IGunkObject parent, EntryEnergyState state):base(parent)
        {
            maxEnergyProvidedPerSecond = state.maxEnergyProvidedPerSecond;
            addEnergyCounter(state.energy,state.maxEnergy);
            updateCounterDrawingBasedOnBehaviour();
        }

        public void setCounterDrawingBehaviour(EnergyCounterDrawingBehaviour newBehaviour)
        {
            behaviour = newBehaviour;
            updateCounterDrawingBasedOnBehaviour();
        }
        public override void destroy()
        {
            parent.objectManager.countersCollection.hideCounter(parent.id, CountersRenderer.DrawStyle.Energy);
        }

        private void addEnergyCounter(float currentValue, float maxValue)
        {
            energy = new Counter(currentValue, maxValue);
            parent.objectManager.countersCollection.addHiddenCounter(parent.id, energy, CountersRenderer.DrawStyle.Energy);
        }

        public float getCurrentValue() => energy.currentValue;

        public float calculateEnergyNeeds()
        {
            return Math.Min(maxEnergyProvidedPerSecond, energy.valueToFull);
        }

        public void chargeEnergy(float amount)
        {
            energy.increaseValue(amount);
            updateCounterDrawingBasedOnBehaviour();
        }

        public bool isEnergyAvailable(float amount) => energy.currentValue >= amount;

        public void useEnergy(float amount)
        {
            if (energy.currentValue < amount)
            {
                throw new ArgumentException("there is not enough energy available");
            }
            parent.addLog(this, "discharged with "+amount+ " energy");
            energy.decreaseValue(amount);
            updateCounterDrawingBasedOnBehaviour();
        }

        private Counter energy;
        private readonly float maxEnergyProvidedPerSecond;
        private EnergyCounterDrawingBehaviour behaviour;

        private void updateCounterDrawingBasedOnBehaviour()
        {
            var drawStyle = CountersRenderer.DrawStyle.Energy;
            parent.objectManager.countersCollection.setCounterValue(parent.id,drawStyle,energy.currentValue);
            switch (behaviour)
            {
                case EnergyCounterDrawingBehaviour.AlwaysVisible:
                    parent.objectManager.countersCollection.showCounter(parent.id, drawStyle);
                    break;
                case EnergyCounterDrawingBehaviour.AlwaysHidden:
                    parent.objectManager.countersCollection.hideCounter(parent.id, drawStyle);
                    break;
                case EnergyCounterDrawingBehaviour.VisibleIfNotFull:
                    if (energy.isFull())
                    {
                        parent.objectManager.countersCollection.showCounter(parent.id, drawStyle);
                    }
                    else
                    {
                        parent.objectManager.countersCollection.hideCounter(parent.id, drawStyle);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(behaviour), behaviour, null);
            }
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(this)
                .addVariable(energy, nameof(energy))
                .addVariable(maxEnergyProvidedPerSecond, nameof(maxEnergyProvidedPerSecond))
                .addVariable(behaviour, "drawing behaviour")
                .addVariable(calculateEnergyNeeds(), "current energy needs")
                .print();
        }
    }
}
