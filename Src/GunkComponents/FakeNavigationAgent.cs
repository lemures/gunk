using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class FakeNavigationAgent : INavigationAgent, IUpdateCallbackReceiver
    {
        public FakeNavigationAgent(Vector3 currentPosition)
        {
            this.currentPosition = currentPosition;
            this.destination = currentPosition;
        }

        public Vector3 destination { get; set; }
        public Vector3 currentPosition { get; private set; }
        public bool isStopped { get; private set; } = true;
        private float maxSpeed = 10.0f;

        public void stop()
        {
            isStopped = false;
        }


        public void resume()
        {
            isStopped = true;
        }

        public float remainingDistance => Vector3.Distance(destination, currentPosition);
        public void setMaxSpeed(float value)
        {
            maxSpeed = value;
        }

        public void update(float deltaTime)
        {
            if (!isStopped) return;

            if (remainingDistance < maxSpeed * deltaTime)
            {
                currentPosition = destination;
                return;
            }

            currentPosition += Vector3.Normalize(destination - currentPosition) * maxSpeed * deltaTime;
        }

        public Vector3 position => currentPosition;

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(FakeNavigationAgent))
                .addVariable(remainingDistance, nameof(remainingDistance))
                .addVariable(currentPosition, nameof(currentPosition))
                .addVariable(destination, nameof(destination))
                .addVariable(isStopped, nameof(isStopped))
                .print();
        }
    }
}