using Gunk_Scripts.Renderers;
using Src.GunkComponents;
using Src.Utilities.Data_Structures;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class FogOfWarComponent: GunkComponent, IPositionChangedCallbackReceiver, IFactionChangedCallbackReceiver, IUpdateCallbackReceiver
    {
        private readonly PerManagerObject<IFogOfWarObjectsCollection> fogOfWarRenderer;
        private readonly PerManagerObject<IntObject> lastFrameRendered;

        private readonly float fogOfWarRange;

        public void changePosition(Vector3 newPosition)
        {
            if (fogOfWarRenderer.getObject().hasObject(parent.id))
            {
                fogOfWarRenderer.getObject().updatePosition(parent.id, newPosition);
            }
        }

        public void changeFaction(Faction newFaction)
        {
            if (fogOfWarRenderer.getObject().hasObject(parent.id))
            {
                fogOfWarRenderer.getObject().removeObject(parent.id);
            }

            if (newFaction == Faction.MainPlayer)
            {
                fogOfWarRenderer.getObject().addObject(parent.id, parent.transform.position);
                fogOfWarRenderer.getObject().setRange(parent.id, fogOfWarRange);
            }
        }

        public FogOfWarComponent(IGunkObject parent, float fogOfWarRange = 15.0f) : base(parent)
        {
            this.fogOfWarRange = fogOfWarRange;
            fogOfWarRenderer = new PerManagerObject<IFogOfWarObjectsCollection>("fog of war renderer", parent, () => new FogOfWarRenderer(10000, parent.objectManager.mapHeight, parent.objectManager.mapWidth));
            lastFrameRendered = new PerManagerObject<IntObject>("last frame fog of war rendered", parent, () => new IntObject(0));
        }

        public override void destroy()
        {
            if (fogOfWarRenderer.getObject().hasObject(parent.id))
                fogOfWarRenderer.getObject().removeObject(parent.id);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(FogOfWarComponent))
                .addVariable(fogOfWarRange, nameof(fogOfWarRange))
                .print();
        }

        public void update(float deltaTime)
        {
            if (lastFrameRendered.getObject().value != Time.frameCount)
            {
                lastFrameRendered.getObject().value = Time.frameCount;
                (fogOfWarRenderer.getObject() as FogOfWarRenderer)?.render();
            }
        }
    }
}