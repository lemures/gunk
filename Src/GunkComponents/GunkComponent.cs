﻿namespace Gunk_Scripts.GunkComponents
{
    public abstract class GunkComponent : IGunkComponent
    {
        protected readonly IGunkObject parent;

        protected GunkComponent(IGunkObject parent)
        {
            this.parent = parent;
        }

        public abstract void destroy();
    }
}
