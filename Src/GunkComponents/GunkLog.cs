using System;
using JetBrains.Annotations;

namespace Gunk_Scripts.GunkComponents
{
    [Serializable]
    public class GunkLog
    {
        public float time;
        public string author;
        public string message;

        public GunkLog(float time, [NotNull] string author, [NotNull] string message)
        {
            if (author == null) throw new ArgumentNullException(nameof(author));
            if (message == null) throw new ArgumentNullException(nameof(message));

            this.time = time;
            this.author = author;
            this.message = message;
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(GunkLog))
                .addVariable(time, nameof(time))
                .addVariable(author, nameof(author))
                .addVariable(message, nameof(message))
                .print();
        }
    }
}