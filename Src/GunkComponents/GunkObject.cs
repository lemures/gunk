﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.Maps;
using Gunk_Scripts.Objects_Selection;
using Gunk_Scripts.SerializationHelpers;
using Gunk_Scripts.SkeletonAnimations;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.Internal;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class GunkObject : IGunkObject
    {
        private readonly List<IConstructionFinishedCallbackReceiver> constructionFinishedCallbackReceivers =
            new List<IConstructionFinishedCallbackReceiver>();

        private readonly List<IFactionChangedCallbackReceiver> factionChangedCallbackReceivers =
            new List<IFactionChangedCallbackReceiver>();

        private readonly List<IPositionChangedCallbackReceiver> positionChangedCallbackReceivers =
            new List<IPositionChangedCallbackReceiver>();

        public ConstructionStateComponent constructionState { get; private set; }

        private IEnergyGenerator energyGenerator;
        public List<IGunkComponent> otherComponents = new List<IGunkComponent>();
        public PlacedObject placedObject;

        private readonly string prefabName;
        private bool isFinished;

 //       private ComponentDebugger componentDebugger;

        public GunkObject(IGunkObjectManager objectManagerFacade, string prefabName, Vector3 position, string startClipName = "")
        {
            this.prefabName = prefabName;
            objectManager = objectManagerFacade.getInternalData(this);
            myId = objectManager.registerGunkObject(this);
            transform = objectManager.instantiatePrefab(this, prefabName, position, Quaternion.identity);
            addComponent(transform);
            constructionState = new ConstructionStateComponent(this);
            currentEnergyUser = constructionState;
            addComponent(constructionState);
            addComponent(new FogOfWarComponent(this));
            addAnimator(startClipName);
        }

        public void finishAddingComponents()
        {
            isFinished = true;
            constructionState.update(0);
        }

        public int myId { get; }

        public bool isDestroyed { get; private set; }
        public Faction faction { get; private set; }
        public Energy energy { get; private set; }
        public IEnergyUser currentEnergyUser { get; set; }

        public IGunkObjectManagerInternalData objectManager { get; }
        public INavigationAgent agent { get; private set; }
        public IGunkTransform transform { get; }

        public ISkeletonAnimator animator { get; private set; }
        public IResourcesContainer resourcesContainer { get; private set; }

        public int id => myId;

        public bool isPlayer()
        {
            return faction == Faction.MainPlayer;
        }

        bool IGunkObject.isDestroyed()
        {
            return isDestroyed;
        }

        public float getSelectionRadius()
        {
            return 1.0f;
        }

        public SelectionPriority selectionPriority { get; protected set; } =
            SelectionPriorityUtils.getNonSpecifiedGroupSelectionPriority();

        public void addEnergyGenerator(IEnergyGenerator generator)
        {
            energyGenerator = generator;
        }

        public float getEnergyGenerationLevel()
        {
            return energyGenerator?.getEnergyGenerationLevel() ?? 0.0f;
        }

        public float getEnergyNeeds()
        {
            return currentEnergyUser?.calculateEnergyNeeds() ?? 0;
        }

        public void provideWithEnergy(float amount)
        {
            currentEnergyUser?.chargeEnergy(amount);
        }

        public void destroy()
        {
            Debug.Log(id + " is destroyed");
            otherComponents.ForEach(x => x?.destroy());
            objectManager.destroyObject(myId);
            isDestroyed = true;
        }

        public void reportConstructionFinished()
        {
            constructionFinishedCallbackReceivers.ForEach(x => x.finishConstruction());
            currentEnergyUser = energy;
        }

        public void reportPositionChanged(Vector3 newPosition)
        {
            positionChangedCallbackReceivers.ForEach(x => x.changePosition(newPosition));
        }

        public void update(float deltaTime)
        {
            if (!isFinished)
            {
                throw new InvalidOperationException("parent class was not informed that constructor has ended adding components(you forgot to call finishAddingComponents() on GunkObject based on prefab " + prefabName);
            }
            otherComponents.ForEach(component => (component as IUpdateCallbackReceiver)?.update(deltaTime));
        }

        protected void addHealth(float healthValue, float maxHealth)
        {
            var health = new Health(this, healthValue, maxHealth);
            addComponent(health);
        }

        public Vector3 getClosestSocketPosition(Vector3 position)
        {
            if (animator == null)
                throw new MissingComponentException("closest socket calculations require animator component");

            var allSockets = animator.getAllSocketBones();

            if (allSockets.Count == 0) throw new Exception("prefab" + prefabName + " has no sockets");

            var solution = allSockets.OrderBy(x => Vector3.Distance(transform.position + x.position, position)).First();
            return solution.position + transform.position;
        }

        public void setFaction(Faction newFaction)
        {
            faction = newFaction;
            factionChangedCallbackReceivers.ForEach(x => x.changeFaction(newFaction));
        }

        public void addConnectible(Connectible component)
        {
            if (animator == null) throw new NullReferenceException("adding connectible requires animator to exist");

            addComponent(component);
        }

        public void addPlacedObject(ObjectPattern specialPattern = null)
        {
            var pattern = specialPattern ?? objectManager.calculatePrefabMeshDimensions(prefabName);
            placedObject = new PlacedObject(this, pattern);
            addComponent(placedObject);
        }

        public void addEnergy(float startEnergy, float maxEnergy, float maxEnergyPerSecond)
        {
            var state = new Energy.EntryEnergyState(startEnergy, maxEnergy, maxEnergyPerSecond);
            energy = new ConstructableEnergy(this, state);
            addComponent(energy);
        }

        public void addAnimator(string startClipName)
        {
            animator = objectManager.instantiateAnimator(this, prefabName, startClipName);
            addComponent((IGunkComponent) animator);
        }

        public void addResource(IResourcesContainer resource, bool visibleByWorkers, bool visibleByMiners)
        {
            var component = new ResourceComponent(this, resource, visibleByWorkers, visibleByMiners);
            resourcesContainer = component;
            addComponent(component);
        }

        private bool isAgentFake;

        public void replaceAgentWithFake()
        {
            if (agent != null && !isAgentFake)
            {
                removeComponent(agent as GunkComponent);
                objectManager.agentProducer.destroyAgent(id);
            }
            var fakeAgent = new FakeNavigationAgent(transform.position);
            isAgentFake = true;
            agent = new NavigationAgentComponent(this, fakeAgent);
            addComponent((GunkComponent)agent);
        }

        public void addNavmeshAgent()
        {
            removeComponent((GunkComponent)agent);
            agent = new NavigationAgentComponent(this, objectManager.agentProducer.createAgent(transform.id));
            isAgentFake = false;
            addComponent((GunkComponent) agent);
            //enforce position update
            transform.position = transform.position;
        }

        public void addCuboidCollider(Vector3 dimensions, SelectionPriority selectionPriority)
        {
            addComponent(new CuboidColliderComponent(this, dimensions, selectionPriority));
        }

        private void checkCallbackListeners(IGunkComponent component)
        {
            var constructionFinishedCallbackReceiver = component as IConstructionFinishedCallbackReceiver;
            if (constructionFinishedCallbackReceiver != null)
                constructionFinishedCallbackReceivers.Add(constructionFinishedCallbackReceiver);

            var positionChangedCallbackReceiver = component as IPositionChangedCallbackReceiver;
            if (positionChangedCallbackReceiver != null)
                positionChangedCallbackReceivers.Add(positionChangedCallbackReceiver);

            var factionChangedCallbackReceiver = component as IFactionChangedCallbackReceiver;
            if (factionChangedCallbackReceiver != null)
                factionChangedCallbackReceivers.Add(factionChangedCallbackReceiver);
        }

        private void removeCallbackListeners(IGunkComponent component)
        {
            var constructionFinishedCallbackReceiver = component as IConstructionFinishedCallbackReceiver;
            if (constructionFinishedCallbackReceiver != null)
                constructionFinishedCallbackReceivers.Remove(constructionFinishedCallbackReceiver);

            var positionChangedCallbackReceiver = component as IPositionChangedCallbackReceiver;
            if (positionChangedCallbackReceiver != null)
                positionChangedCallbackReceivers.Remove(positionChangedCallbackReceiver);

            var factionChangedCallbackReceiver = component as IFactionChangedCallbackReceiver;
            if (factionChangedCallbackReceiver != null)
                factionChangedCallbackReceivers.Remove(factionChangedCallbackReceiver);
        }

        public void addLog(IGunkComponent component, string message)
        {
            objectManager.addLog(id, component?.GetType()?.Name, message);
        }

        public void addEnergyNeededForConstruction(float amountNeeded)
        {
            constructionState.changeState(new UnderConstructionNeedingEnergy(amountNeeded));
        }

        public T getComponent<T>() where T : class, IGunkComponent
        {
            var type = typeof(T);
            return otherComponents.Find(x => x.GetType() == type) as T;
        }

        public void addComponent(IGunkComponent component)
        {
            addLog(component, "component was added");
            otherComponents.Add(component);
            checkCallbackListeners(component);
            objectManager.addComponent(id,component);
        }

        public void removeComponent(IGunkComponent component)
        {
            if (component == null)
            {
                return;
            }
            addLog(component, "component was removed");
            otherComponents.Remove(component);
            removeCallbackListeners(component);
            objectManager.removeComponent(id,component);
        }

        public bool isConstructed() => constructionState.constructionFinished;

        public float constructionProgress => constructionState.constructionProgress;
    }
}