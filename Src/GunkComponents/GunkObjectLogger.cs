﻿using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class GunkObjectLogger : MonoBehaviour
    {
        public int currentId ;
        public List<GunkLog>allObjectLogs = new List<GunkLog>();

        public static Dictionary<int, GunkObjectLogger> loggers = new Dictionary<int, GunkObjectLogger>();

        public void addLog(GunkLog log)
        {
            allObjectLogs.Add(log);
        }

        private void Start()
        {
            loggers[currentId] = this;
        }
    }
}