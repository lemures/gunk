using UnityEditor;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(GunkObjectLogger))]
    public class GunkObjectLoggerEditor : Editor
    {
        private Vector2 scrollPos;
        public override void OnInspectorGUI()
        {
            int editedId = serializedObject.FindProperty("currentId").intValue;

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Height(200));

            foreach (var component in GunkObjectLogger.loggers[editedId].allObjectLogs)
            {
                EditorGUILayout.LabelField(PrettyPrinter.reformatFloat(component.time) + " " + component.author);
                EditorGUILayout.LabelField(component.message);
                EditorGUILayout.Separator();
            }
            EditorGUILayout.EndScrollView();
        }
    }
}
