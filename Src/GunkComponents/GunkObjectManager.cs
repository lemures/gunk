﻿using System.Collections.Generic;
using DefaultNamespace;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.Maps;
using Src.SkeletonAnimations;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class GunkObjectManager : IGunkObjectManager
    {
        private readonly GunkObjectManagerInternalData internalData;

        public bool isAnythingSelected() => internalData.isAnythingSelected();

        public void enableSelectingObjects() => internalData.canSelectObjects = true;

        public void disableSelectingObjects() => internalData.canSelectObjects = false;

        public GunkObjectManager(int mapHeight, int mapWidth, Camera mainCamera, IPrefabManager prefabManager, IExtraObjectsRenderer extraObjectsRenderer)
        {
            internalData = new GunkObjectManagerInternalData(this, mapHeight, mapWidth, mainCamera, prefabManager, extraObjectsRenderer);
        }

        public IGunkObjectManagerInternalData getInternalData(IGunkObject gunkObject)
        {
            return internalData;
        }

        public bool canPlaceObject(Vector3 position, string prefabName)
        {
            var pattern = calculatePrefabMeshDimensions(prefabName);
            return internalData.canSpaceBeLocked(position, pattern);
        }

        public Vector3? getClosestFreePosition(Vector3 position, string prefabName)
        {
            var pattern = calculatePrefabMeshDimensions(prefabName);
            return internalData.calculateClosestAvailablePosition(position, pattern);
        }

        public bool canConnectBuilding(Vector3 position, Faction faction)
        {
            return internalData.connectablesBuildingsModule.isConnectorNearby(position, faction);
        }

        public void temporarilyConnectBuilding(List<SocketData> socketBones, Vector3 position, Faction faction)
        {
            internalData.connectablesBuildingsModule.temporarilyConnectBuilding(socketBones, position, faction);
        }

        public void clearTemporaryConnections()
        {
            internalData.connectablesBuildingsModule.clearTemporaryConnections();
        }

        public void update(float deltaTime)
        {
            internalData.update(deltaTime);
        }

        public void deselectAllObjects()
        {
            internalData.deselectAllObjects();
        }

        public ObjectPattern calculatePrefabMeshDimensions(string prefabName)
        {
            return internalData.calculatePrefabMeshDimensions(prefabName);
        }

        public List<List<int>> getAllConnectibleNetworks()=>internalData.getAllConnectibleNetworks();
    }
}