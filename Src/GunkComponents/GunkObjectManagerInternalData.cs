using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GUI;
using Gunk_Scripts.Maps;
using Gunk_Scripts.Objects_Selection;
using Gunk_Scripts.Renderers;
using Src.Console;
using Src.GameRenderer;
using Src.GunkComponents;
using Src.Libraries;
using Src.Logic;
using Src.Renderers;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Gunk_Scripts.GunkComponents
{
    [ExecutesCommand("showPlayers")]
    public class GunkObjectManagerInternalData : IGunkObjectManagerInternalData
    {
        public static string showPlayers(string[]args)
        {
            return activeManager.players.ToString();
        }

        private static GunkObjectManagerInternalData activeManager;

        public Players players = new Players();

        public void addResources(Faction player, GatheredResources resources)
        {
            players.addResources(player, resources);
        }

        public Dictionary<string, object>otherVariables { get; private set; }

        public IGunkObjectManager parent { get; }

        private readonly SortedDictionary<int, IGunkObject> allObjects = new SortedDictionary<int, IGunkObject>();

        public readonly ConnectablesBuildingsModule connectablesBuildingsModule;

        public IConnectablesNetwork connectablesNetwork => connectablesBuildingsModule;

        private readonly CountersRenderer countersRenderer;

        public ICountersCollection countersCollection => countersRenderer;

        private readonly List<int> currentlySelectedObjects = new List<int>();

        public GameObjectManager gameObjectManager { get; }

        private readonly IdManager idManager = new IdManager();

        public IColliderCollection collidersCollection => selectableCollectionEventsSender;

        private readonly SelectableCollectionEventsSender selectableCollectionEventsSender;

        private readonly SelectedObjectsRenderer selectedObjectsRenderer;

        public CorruptedLand corruptedLand { get; private set; }

        public IClosestResourceFinder closestResourceFinder { get; }

        public INavigationAgentProducer agentProducer { get; private set; }

        private readonly UpdateCaller<IGunkObject> updateCaller =
            new UpdateCaller<IGunkObject>(x => !x.isDestroyed());

        public IPrefabManager prefabManager { get; }

        private readonly EnergyNetwork energyNetwork = new EnergyNetwork();

        public Camera mainCamera { get; private set; }

        public int mapHeight { get; }
        public int mapWidth { get; }

        private Dictionary<int, ComponentDebugger> debuggers = new Dictionary<int, ComponentDebugger>();
        private Dictionary<int, GunkObjectLogger> loggers = new Dictionary<int, GunkObjectLogger>();

        private IExtraObjectsRenderer extraObjectsRenderer;

        public GunkObjectManagerInternalData(GunkObjectManager parent,int mapHeight, int mapWidth, Camera mainCamera,
            IPrefabManager prefabManager, IExtraObjectsRenderer extraObjectsRenderer)
        {
            activeManager = this;
            this.extraObjectsRenderer = extraObjectsRenderer;
            this.mapHeight = mapHeight;
            this.mapWidth = mapWidth;
            this.mainCamera = mainCamera;
            this.parent = parent;
            if (MainCamera.getInstance() == null) throw new LibriaryUninitializedException("MainCamera");

            selectableCollectionEventsSender =
                SelectableCollectionEventsSender.createInstance(new CameraScreenPositionCalculator(mainCamera),
                    MouseUtils.instance);
            selectableCollectionEventsSender.objectRightClickedAction = displayDebugSpecialOptions;
            selectableCollectionEventsSender.objectClickedAction = objectClicked;
            selectableCollectionEventsSender.objectDoubleClickedAction = objectDoubleClicked;
            selectableCollectionEventsSender.objectsTemporarilySelectedAction = objectsTemporarilySelected;
            selectableCollectionEventsSender.objectsSelectedAction = objectsSelected;

            selectedObjectsRenderer = new SelectedObjectsRenderer(10000, mapWidth, mapHeight);

            var cableRenderer = new CableChangesReceiver(this);
            connectablesBuildingsModule = new ConnectablesBuildingsModule(cableRenderer);
            countersRenderer = new CountersRenderer(mainCamera);
            gameObjectManager = new GameObjectManager(prefabManager);
            closestResourceFinder = new ClosestResourceFinder(mapWidth, mapHeight);
            this.prefabManager = prefabManager;

            agentProducer = new NavmeshAgentProducer(gameObjectManager);
            otherVariables = new Dictionary<string, object>();

            corruptedLand = new CorruptedLand(mapHeight, mapWidth);
            CorruptionRenderer.init(mapHeight, mapWidth);
            corruptedLand.infectField(new Vector2Int(300, 700));


            foreach (var xPos in Enumerable.Range(0, mapWidth))
            foreach (var yPos in Enumerable.Range(0, mapHeight))
            {
                if (NewMapManager.loadedMap.getFieldType(new Vector2Int(xPos, yPos)) != TerrainFieldType.Ground)
                {
                    corruptedLand.markFieldAsNonInfectable(new Vector2Int(xPos, yPos));
                }
            }
        }

        public void setNavigationAgentProducer(INavigationAgentProducer newAgentProducer)
        {
            agentProducer = newAgentProducer;
        }

        public bool canSelectObjects;

        public bool isAnythingSelected() => currentlySelectedObjects.Count != 0;

        public void deselectAllObjects()
        {
            selectedObjectsRenderer.removeAllObjects();
        }

        private void changeCurrentlySelectedObjects(List<int> newSelected)
        {
            if (!canSelectObjects) return;

            currentlySelectedObjects.ForEach(x => selectedObjectsRenderer.removeObject(x));
            currentlySelectedObjects.Clear();
            foreach (var id in newSelected)
            {
                currentlySelectedObjects.Add(id);
                var myObject = allObjects[id];
                selectedObjectsRenderer.addObject(id,
                    myObject.transform.position.x,
                    myObject.transform.position.z,
                    myObject.getSelectionRadius());
            }
        }

        public bool canSpaceBeLocked(Vector3 position, ObjectPattern pattern)
        {
            return NewMapManager.loadedMap.canBuild(position, pattern);
        }

        public Vector3? calculateClosestAvailablePosition(Vector3 position, ObjectPattern pattern)
        {
            return NewMapManager.loadedMap.calculateClosestAvailablePosition(position, pattern);
        }

        public void setSpecialFieldType(Vector3 position, TerrainFieldType type)
        {
            NewMapManager.loadedMap.setFieldType(position, type);
        }

        public void lockSpaceUnderObject(IGunkObject gunkObject, ObjectPattern pattern)
        {
            var position = gunkObject.transform.position;
            if (!NewMapManager.loadedMap.canBuild(position, pattern))
                throw new InvalidOperationException(
                    "you tried to instantiate building in a place that is not free");

            NewMapManager.loadedMap.construct(position, pattern);
        }

        public void unlockSpaceUnderObject(IGunkObject gunkObject, ObjectPattern pattern)
        {
            var position = gunkObject.transform.position;
            NewMapManager.loadedMap.deconstruct(position, pattern);
        }

        public void addVisibleObjectsChange(ExtraObjectsChange change)
        {
            extraObjectsRenderer.receiveChange(change);
        }

        public void addComponent(int id, IGunkComponent component)
        {
            debuggers[id].addComponent(component);
        }

        public void removeComponent(int id, IGunkComponent component)
        {
            debuggers[id].removeComponent(component);
        }

        public IGunkTransform instantiatePrefab(IGunkObject gunkObject, string prefabName, Vector3 positions,
            Quaternion rotation)
        {
            var spawnedId = gameObjectManager.instantiatePrefab(prefabName, positions, rotation);
            loggers.Add(gunkObject.id,gameObjectManager.addComponent<GunkObjectLogger>(spawnedId));
            loggers[gunkObject.id].currentId = gunkObject.id;
            debuggers.Add(gunkObject.id, gameObjectManager.addComponent<ComponentDebugger>(spawnedId));
            debuggers[gunkObject.id].currentId = gunkObject.id;
            return gameObjectManager.createTransformComponent(gunkObject, spawnedId);
        }

        public void addLog(int id, string author, string message)
        {
            loggers[id].addLog(new GunkLog(Time.time, author, message));
        }

        public int registerGunkObject(IGunkObject objectToRegister)
        {
            var id = idManager.getFreeId();
            allObjects[id] = objectToRegister;
            return id;
        }

        public IResourcesContainer findResourcesWithId(int id)
        {
            return allObjects[id].resourcesContainer;
        }

        public Vector3? getObjectPosition(int id)
        {
            return allObjects[id]?.transform?.position;
        }

        public IGunkObject getObjectWithId(int id)
        {
            return allObjects.ContainsKey(id) ? allObjects[id] : null;
        }

        public ObjectPattern calculatePrefabMeshDimensions(string prefabName)
        {
            var pattern = new ObjectPattern(MeshBoundsUtils.getPrefabDimensions(prefabManager.findPrefab(prefabName)));
            //TODO move this code out of here
            if (prefabName.ToLower() == "miner")
            {
                pattern.setFieldType(new Vector2Int(0,0), TerrainFieldType.CrystalMine);
            }

            return pattern;
        }

        public void destroyObject(int id)
        {
            updateCaller.deleteAllObjectFunctions(allObjects[id]);
            allObjects.Remove(id);
            countersRenderer.hideAndResetObjectCounters(id);
        }

        public SkeletonAnimatorComponent instantiateAnimator(IGunkObject gunkObject, string prefabName, string startClipName)
        {
            var animator = new SkeletonAnimatorComponent(gunkObject, prefabName, gameObjectManager, gunkObject.transform.id, startClipName);
            updateCaller.addFunction(gunkObject, () => animator.updateAnimation(Time.deltaTime));
            return animator;
        }

        public INavigationAgent instantiateNavmeshAgent(IGunkObject gunkObject)
        {
            return gameObjectManager.createNavmeshComponent(gunkObject.transform.id);
        }

        public List<List<int>> getAllConnectibleNetworks()
        {
            return connectablesBuildingsModule.getAllConnectibleNetworks();
        }

        private void objectClicked(int id)
        {
            Debug.Log(id + " clicked");
            changeCurrentlySelectedObjects(new List<int> {id});
        }

        private void objectDoubleClicked(int id)
        {
            changeCurrentlySelectedObjects(new List<int> {id});
        }

        private void objectsTemporarilySelected(List<int> ids)
        {
            changeCurrentlySelectedObjects(ids);
        }

        private void objectsSelected(List<int> ids)
        {
            Debug.Log("selected");
            foreach (var id in ids)
            {
                Debug.Log(id);
            }
            changeCurrentlySelectedObjects(ids);
        }

        private void displayDebugSpecialOptions(int id)
        {
            Debug.Log(id + " right clicked");
            var contextMenu = new GuiContextMenu("gunkObject debug options");
            contextMenu.setOptions(
                new List<string>
                {
                    "destroy object"
                },
                new List<Action>
                {
                    allObjects[id].destroy
                });
        }

        private void updateEnergyNetworks(float deltaTime)
        {
            UnityEngine.Profiling.Profiler.BeginSample("updating energy networks");
            var energyData = new List<EnergyNetwork.EnergyObjectData>();
            foreach (var gunkObject in allObjects.Values)
            {
                var id = gunkObject.id;

                var generated = gunkObject.getEnergyGenerationLevel() * deltaTime;
                var needs = gunkObject.getEnergyNeeds();

                energyData.Add(new EnergyNetwork.EnergyObjectData(id, generated, needs));
            }

            var simulationResult = energyNetwork.simulateSingleTick(energyData, getAllConnectibleNetworks());
            foreach (var objectSimulationResult in simulationResult)
            {
                allObjects[objectSimulationResult.objectId]
                    .provideWithEnergy(objectSimulationResult.energyProvided);
            }
            UnityEngine.Profiling.Profiler.EndSample();
        }

        private void callGunkObjectUpdateFunctions(float deltaTime)
        {
            UnityEngine.Profiling.Profiler.BeginSample("calling gunkObject updateFunctions");
            foreach (var gunkObject in allObjects.ToList())
            {
                gunkObject.Value.update(deltaTime);
            }

            UnityEngine.Profiling.Profiler.EndSample();
        }

        public void update(float deltaTime)
        {
            callGunkObjectUpdateFunctions(deltaTime);
            updateCaller.invokeAll();
            selectedObjectsRenderer.render();
            updateEnergyNetworks(deltaTime);
            foreach (var selectedObject in selectedObjectsRenderer.getAllObjects())
            {
                var position = allObjects[selectedObject].transform.position;
                selectedObjectsRenderer.updatePosition(selectedObject,position.x, position.y, position.z);
            }
            corruptedLand.update(deltaTime);
            CorruptionRenderer.addChanges(corruptedLand.getChanges());
        }
    }
}