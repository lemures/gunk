using System.Text;
using Gunk_Scripts.GunkObjects;

namespace Gunk_Scripts.GunkComponents
{
    public static class GunkPrefabNames
    {
        public static string getName(GunkObjectType type)
        {
            return insertSpaceBeforeUpperCase(type.ToString());
        }

        private static string insertSpaceBeforeUpperCase(string str)
        {
            var sb = new StringBuilder();
            var previousChar = char.MinValue;

            foreach (var c in str)
            {
                if (char.IsUpper(c))
                {
                    if (sb.Length != 0 && previousChar != ' ')
                    {
                        sb.Append(' ');
                    }
                }

                sb.Append(c);

                previousChar = c;
            }

            return sb.ToString();
        }
    }
}