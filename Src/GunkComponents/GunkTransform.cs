﻿using Src.Libraries;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class GunkTransform:GunkComponent, IGunkTransform
    {
        public int id { get; private set; }

        private GameObjectManager objectManager;

        public GunkTransform(IGunkObject parent, GameObjectManager gameObjectManager, int gameObjectId) : base(parent)
        {
            init(gameObjectManager, gameObjectId);
        }

        private Vector3 lastPosition;

        public Vector3 position
        {
            get { return lastPosition; }
            set
            {
                objectManager.setPosition(id,value);
                updatePosition();
            }
        }

        public Quaternion rotation
        {
            get { return objectManager.getRotation(id); }
            set { objectManager.setRotation(id,value); }
        }

        private void init(GameObjectManager gameObjectManager, int gameObjectId)
        {
            objectManager = gameObjectManager;
            id = gameObjectId;
            updatePosition(true);
        }

        public void reportPositionChangedExternally()
        {
            updatePosition();
        }

        private void updatePosition(bool forceUpdate=false)
        {
            if (lastPosition != objectManager.getPosition(id) || forceUpdate)
            {
                lastPosition = objectManager.getPosition(id);
                //TODO move this somewhere else
                parent.objectManager.countersCollection.setObjectCountersPositions(parent.id,lastPosition);
                parent.reportPositionChanged(lastPosition);
            }
        }

        public override void destroy()
        {
            objectManager.destroyObject(id);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(this)
                .addVariable(id, "gameObject id")
                .addVariable(lastPosition, "position")
                .addVariable(objectManager.getRotation(id), "rotation")
                .print();
        }
    }
}
