using Gunk_Scripts.Renderers;

namespace Gunk_Scripts.GunkComponents
{
    public class Health : GunkComponent
    {
        private Counter health;

        public Health(IGunkObject parent, float value, float maxValue) : base(parent)
        {
            health = new Counter(value, maxValue);
            parent.objectManager.countersCollection.addHiddenCounter(parent.id, health, CountersRenderer.DrawStyle.Health);
            parent.objectManager.countersCollection.showCounter(base.parent.id, CountersRenderer.DrawStyle.Health);
        }

        public override void destroy()
        {
            parent.objectManager.countersCollection.hideCounter(parent.id, CountersRenderer.DrawStyle.Health);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(health))
                .addVariable(health, nameof(health))
                .print();
        }
    }
}