﻿namespace Gunk_Scripts.GunkComponents
{
    public interface IConnectablesNetwork
    {
        void addObject(ConnectablesBuildingsModule.ConnectibleObjectData data);
        void addConnector(ConnectablesBuildingsModule.ConnectibleObjectData objectToAdd, float range);
        void transformIntoConnector(int objectId, float range);
        void removeObject(int id);
    }
}