namespace Gunk_Scripts.GunkComponents
{
    public interface IConstructionFinishedCallbackReceiver
    {
        void finishConstruction();
    }
}