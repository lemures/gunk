using Gunk_Scripts.SkeletonAnimations;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.Internal;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public interface IGunkObject
    {
        IGunkObjectManagerInternalData objectManager { get; }
        IGunkTransform transform { get; }
        ISkeletonAnimator animator { get; }
        Faction faction { get; }
        Energy energy { get;  }
        IEnergyUser currentEnergyUser { get; set;}
        IResourcesContainer resourcesContainer { get; }
        INavigationAgent agent { get; }

        int id { get; }
        float constructionProgress { get; }

        bool isPlayer();

        bool isDestroyed();

        void destroy();

        float getSelectionRadius();

        void addEnergyGenerator(IEnergyGenerator generator);

        float getEnergyGenerationLevel();

        float getEnergyNeeds();

        void provideWithEnergy(float amount);

        void reportConstructionFinished();

        void reportPositionChanged(Vector3 newPosition);
        void update(float deltaTime);

        bool isConstructed();

        void addLog(IGunkComponent author, string message);

        T getComponent<T>() where T : class, IGunkComponent;
        void replaceAgentWithFake();
    }
}