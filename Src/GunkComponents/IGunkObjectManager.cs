using System.Collections.Generic;
using Gunk_Scripts.Maps;
using Gunk_Scripts.SkeletonAnimations;
using Src.SkeletonAnimations;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public interface IGunkObjectManager
    {
        IGunkObjectManagerInternalData getInternalData(IGunkObject gunkObject);

//TODO remove this or uncomment
/*
        bool isAnythingSelected();

        void enableSelectingObjects();

        void disableSelectingObjects();

        void temporarilyConnectBuilding(List<SocketData> socketBones, Vector3 position, Faction faction);

        bool canConnectBuilding(Vector3 position, Faction faction);
        bool canPlaceObject(Vector3 position, ObjectPattern pattern);
        ObjectPattern calculatePrefabMeshDimensions(string prefabName);
*/
    }
}