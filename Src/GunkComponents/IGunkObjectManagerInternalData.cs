using System.Collections.Generic;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.Maps;
using Gunk_Scripts.Objects_Selection;
using Gunk_Scripts.Renderers;
using Gunk_Scripts.SkeletonAnimations;
using Src.GameRenderer;
using Src.GunkComponents;
using Src.Libraries;
using Src.SkeletonAnimations;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public interface IGunkObjectManagerInternalData
    {
        IGunkObjectManager parent { get; }
        void lockSpaceUnderObject(IGunkObject gunkObject, ObjectPattern buildingPattern);
        void unlockSpaceUnderObject(IGunkObject gunkObject, ObjectPattern buildingPattern);

        IColliderCollection collidersCollection { get; }
        IConnectablesNetwork connectablesNetwork { get; }
        ICountersCollection countersCollection { get; }
        INavigationAgentProducer agentProducer { get; }
        GameObjectManager gameObjectManager { get; }

        void addVisibleObjectsChange(ExtraObjectsChange change);

        void addComponent(int id, IGunkComponent component);

        void setSpecialFieldType(Vector3 position, TerrainFieldType type);
        void removeComponent(int id, IGunkComponent component);

        IGunkTransform instantiatePrefab(IGunkObject gunkObject, string prefabName, Vector3 positions,
            Quaternion rotation);
        Camera mainCamera { get; }
        int mapHeight { get; }
        int mapWidth { get; }
        IPrefabManager prefabManager { get; }

        Dictionary<string, object> otherVariables { get; }
        CorruptedLand corruptedLand { get; }

        int registerGunkObject(IGunkObject objectToRegister);

        void destroyObject(int id);
        SkeletonAnimatorComponent instantiateAnimator(IGunkObject gunkObject, string prefabName, string startClipName);
        List<List<int>> getAllConnectibleNetworks();

        ObjectPattern calculatePrefabMeshDimensions(string prefabName);
        IGunkObject getObjectWithId(int id);
        void addLog(int id, string author, string message);
        void addResources(Faction player, GatheredResources resources);
    }
}