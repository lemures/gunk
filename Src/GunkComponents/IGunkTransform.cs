﻿using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public interface IGunkTransform : IGunkComponent
    {
        int id { get; }
        Vector3 position { get; set; }
        Quaternion rotation { get; set; }
    }
}