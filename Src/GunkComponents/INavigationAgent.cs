﻿using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public interface INavigationAgent
    {
        void stop();
        bool isStopped { get; }
        Vector3 destination { get; set; }
        void resume();
        float remainingDistance { get; }
        void setMaxSpeed(float value);
        Vector3 position { get; }
    }
}