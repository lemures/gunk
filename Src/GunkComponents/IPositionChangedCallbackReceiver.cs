using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public interface IPositionChangedCallbackReceiver
    {
        void changePosition(Vector3 newPosition);
    }
}