﻿using System.Collections.Generic;
using Gunk_Scripts.SkeletonAnimations;
using Src.SkeletonAnimations.Events;
using UnityEngine;

namespace Src.SkeletonAnimations
{
    public interface ISkeletonAnimator
    {
        string currentClip { get; }

        List<SocketData> getAllSocketBones();

        void playClip(string name, float argument = 0.0f);

        void changeClipArgument(float value);

        List<EventData> collectTriggeredEvents();

        Vector3 getBarrelBoneWorldPosition(int index, Vector3 objectPosition);

        bool isCurrentClipLoaded();
    }
}