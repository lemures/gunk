namespace Gunk_Scripts.GunkComponents
{
    public interface IUpdateCallbackReceiver
    {
        void update(float deltaTime);
    }
}