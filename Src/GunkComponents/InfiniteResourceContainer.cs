using Gunk_Scripts.Maps;

namespace Gunk_Scripts.GunkComponents
{
    public class InfiniteResourcesContainer : IResourcesContainer
    {
        private readonly float amountGatheredPerSecond;

        public InfiniteResourcesContainer(ResourceType resourceType, float amountGatheredPerSecond)
        {
            this.type = resourceType;
            this.amountGatheredPerSecond = amountGatheredPerSecond;
        }

        public bool isDepleted() => false;

        public GatheredResources gatherResources(float deltaTime)
        {
            return new GatheredResources(type, amountGatheredPerSecond * deltaTime);
        }

        public ResourceType type { get; }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(InfiniteResourcesContainer))
                .addVariable(amountGatheredPerSecond, nameof(amountGatheredPerSecond))
                .print();
        }
    }
}