using System;
using Gunk_Scripts.Maps;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class LimitedResourcesSource : IResourcesContainer
    {
        private Counter resourcesLeft;
        private readonly float amountGatheredPerSecond;

        public LimitedResourcesSource(ResourceType resourceType, float startAmount, float amountGatheredPerSecond)
        {
            resourcesLeft=new Counter(startAmount,startAmount);
            this.amountGatheredPerSecond = amountGatheredPerSecond;
            this.type = resourceType;
        }

        public bool isDepleted() => resourcesLeft.isEmpty();

        public GatheredResources gatherResources(float deltaTime)
        {
            var gatheredAmount = Math.Min(deltaTime * amountGatheredPerSecond, resourcesLeft.currentValue);
            resourcesLeft.decreaseValue(gatheredAmount);
            return new GatheredResources(type, gatheredAmount);
        }

        public ResourceType type { get; }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(LimitedResourcesSource))
                .addVariable(amountGatheredPerSecond, nameof(amountGatheredPerSecond))
                .addVariable(resourcesLeft, nameof(resourcesLeft))
                .print();
        }
    }
}