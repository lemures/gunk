﻿using System;
using Gunk_Scripts;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.GunkObjects;
using Gunk_Scripts.Renderers;
using Src.Libraries;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.BoneTypes;
using Src.SkeletonAnimations.CurvesTypes;
using Src.SkeletonAnimations.Internal;
using Src.Utilities.Data_Structures;
using UnityEngine;

namespace Src.GunkComponents
{
    [NeedsClip(idleClipName)]
    [NeedsClip(manufacturingInProgressClipName)]
    public class ManufacturerAiComponent : GunkComponent, IUpdateCallbackReceiver
    {
        private const string idleClipName = "Manufacturer Idle";
        private const string manufacturingInProgressClipName = "Manufacturing work in progress";

        private PerManagerObject<RandomObjectFinder> closestPlatformFinder;

        private PerManagerObject<PlainColorLineRenderer> manufacturerRayRenderer;
        private PerManagerObject<IntObject> manufacturerLastFrameRendered;

        private int? constructedManufacturerId;
        private float timeManufacturing;
        private float maxTimeManufacturing = 2.0f;
        private float timeSinceLastTargetChange;

        private float calculateManufactureClipArgument(Vector3 targetPosition)
        {
            var myPosition = parent.transform.position;
            var difference = targetPosition - myPosition;
            if (Vector3.Distance(targetPosition, myPosition) < 0.001f)
            {
                return 0;
            }
            var sin = -difference.x / Vector3.Distance(targetPosition, myPosition);
            return -0.5f + (difference.z < 0 ? 0 : 0.5f) + Math.Sign(difference.z)*(float)(Math.Asin(sin) / (2 * Math.PI));
        }

        private static void makeIdleClip()
        {
            var clip = AnimationClipDatabase.getClip(idleClipName);
            var mainBodyBoneId = new BasicBone(0,0,new Float4(0,0,0,0), null);
            clip.addOrReplaceCurve(SkeletonData.getBoneId(mainBodyBoneId),SkeletonAnimationClip.BoneAnimationParameter.XRotation,new ConstantValueCurve(0));
            AnimationClipDatabase.saveClipChangesToDisk(idleClipName);
        }

        private static void makeManufactureClip()
        {
            var clip = AnimationClipDatabase.getClip(manufacturingInProgressClipName);
            var mainBodyBoneId = new BasicBone(0,0,new Float4(0,0,0,0), null);
            clip.addOrReplaceCurve(SkeletonData.getBoneId(mainBodyBoneId),SkeletonAnimationClip.BoneAnimationParameter.YRotation,new FirstArgumentDependentCurve());
            AnimationClipDatabase.saveClipChangesToDisk(manufacturingInProgressClipName);
        }

        public ManufacturerAiComponent(IGunkObject parent) : base(parent)
        {
            var generator = new System.Random(parent.id);
            maxTimeManufacturing += (float)generator.NextDouble();
            closestPlatformFinder =
                new PerManagerObject<RandomObjectFinder>("closestPlatformFinder", parent, () => new RandomObjectFinder());

            manufacturerRayRenderer = new PerManagerObject<PlainColorLineRenderer>(
                nameof(manufacturerRayRenderer),
                parent,
                () => new PlainColorLineRenderer(parent.objectManager.mainCamera));
            manufacturerLastFrameRendered = new PerManagerObject<IntObject>(nameof(manufacturerLastFrameRendered), parent, () => new IntObject(0));
            parent.addLog(this, "added ray");
            makeIdleClip();
            makeManufactureClip();
            parent.animator.playClip(manufacturingInProgressClipName);
        }

        public override void destroy()
        {

        }

        private void renderRays()
        {
            Debug.Log(manufacturerLastFrameRendered.getObject().value);
            if (manufacturerLastFrameRendered.getObject().value != Time.frameCount)
            {
                manufacturerLastFrameRendered.getObject().value = Time.frameCount;
                manufacturerRayRenderer.getObject().render();
            }
        }

        private bool canManufacture()
        {
            if (timeSinceLastTargetChange < 0.5f)
            {
                return false;
            }
            if (constructedManufacturerId == null)
            {
                return false;
            }
            if (parent.objectManager.getObjectWithId(constructedManufacturerId.Value) == null)
            {
                return false;
            }
            return parent.animator.currentClip == manufacturingInProgressClipName &&
                   parent.animator.isCurrentClipLoaded();
        }
        private bool hasRay;
        private void updateRay()
        {
            if (hasRay)
            {
                manufacturerRayRenderer.getObject().removeObject(parent.id);
                hasRay = false;
            }

            if (!canManufacture())
            {
                return;
            }
            var rayStart = parent.animator.getBarrelBoneWorldPosition(0, parent.transform.position);
            var closestPosition = parent.objectManager.getObjectWithId(constructedManufacturerId.Value).transform.position;
            manufacturerRayRenderer.getObject().addObject(parent.id, closestPosition, rayStart);
            hasRay = true;
        }

        private void updateManufacturedObject()
        {
            var maxDistance = 200.0f;
            var myPosition = new Float2(parent.transform.position.x, parent.transform.position.z);
            var closestObject = closestPlatformFinder.getObject().findRandomObjectInRange(myPosition.x, myPosition.y, parent.faction, maxDistance);
            if (closestObject == -1)
            {
                constructedManufacturerId = null;
                if (parent.animator.currentClip != idleClipName)
                parent.animator.playClip(idleClipName);
                return;
            }

            constructedManufacturerId = closestObject;
            timeManufacturing = 0;
            timeSinceLastTargetChange = 0;
            var closestPosition = parent.objectManager.getObjectWithId(constructedManufacturerId.Value).transform.position;

            if (parent.animator.currentClip == idleClipName)
            parent.animator.playClip(manufacturingInProgressClipName, calculateManufactureClipArgument(closestPosition));
        }

        public void update(float deltaTime)
        {
            timeSinceLastTargetChange += deltaTime;
            if (constructedManufacturerId != null)
            {
                var platform = parent.objectManager.getObjectWithId(constructedManufacturerId.Value);
                if (platform == null || !platform.getComponent<PlatformAiComponent>().isConstructing)
                {
                    updateManufacturedObject();
                }
                else
                {
                    if (canManufacture())
                    {
                        timeManufacturing += deltaTime;
                        if (timeManufacturing >= maxTimeManufacturing)
                        {
                            updateManufacturedObject();
                        }
                    }
                }
            }
            else
            {
                updateManufacturedObject();
            }

            updateRay();
            chargeConstructedObject(calculateEnergyToProvide(deltaTime));
            renderRays();
        }

        private float calculateEnergyToProvide(float deltaTime)
        {
            if (!canManufacture())
            {
                return 0;
            }
            var platform = parent.objectManager.getObjectWithId(constructedManufacturerId.Value);
            var limit1 = platform.getComponent<PlatformAiComponent>().getConstructionEnergyNeeds() * deltaTime;
            var limit2 = parent.energy.getCurrentValue();
            return Math.Min(limit1, limit2);
        }

        private void chargeConstructedObject(float amount)
        {
            if (!canManufacture())
            {
                return;
            }
            parent.energy.useEnergy(amount);
            var platform = parent.objectManager.getObjectWithId(constructedManufacturerId.Value);
            platform.getComponent<PlatformAiComponent>().chargeConstructedObject(amount);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(ManufacturerAiComponent))
                .addVariable(constructedManufacturerId, nameof(constructedManufacturerId))
                .addVariable(timeManufacturing, nameof(timeManufacturing))
                .addVariable(maxTimeManufacturing, nameof(maxTimeManufacturing))
                .addVariable(hasRay, nameof(hasRay))
                .print();
        }
    }
}
