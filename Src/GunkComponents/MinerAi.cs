using System;
using Gunk_Scripts;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;

namespace Src.GunkComponents
{
    public class MinerAi : GunkComponent, IUpdateCallbackReceiver
    {
        private PerManagerObject<IClosestResourceFinder> closestMineFinder;

        private int mineId;

        public MinerAi(IGunkObject parent) : base(parent)
        {
            var position = new Float2(parent.transform.position.x, parent.transform.position.z);
            var foundMine = ResourceComponent.getClosestMineFinder(parent).getObject().findClosestObject(position, 1.0f);
            if (foundMine == null)
            {
                throw new InvalidOperationException("you tried to place Miner where there is are no mines!");
            }

            mineId = foundMine.id;
        }

        public override void destroy()
        {
        }

        public void update(float deltaTime)
        {
            var mine = parent.objectManager.getObjectWithId(mineId);

            //check if mine still exists(may have been depleted)
            if (mine != null)
            {
                var gathered = mine.resourcesContainer.gatherResources(deltaTime);

                parent.objectManager.addResources(parent.faction, gathered);
            }
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(MinerAi))
                .addVariable(mineId, nameof(mineId))
                .print();
        }
    }
}