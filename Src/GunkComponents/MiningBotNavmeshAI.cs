using System;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.Maps;
using Gunk_Scripts.Renderers;
using Src.GunkComponents;
using Src.Libraries;
using Src.Utilities.Data_Structures;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class MiningBotNavmeshAi : GunkComponent, IUpdateCallbackReceiver, IConstructionFinishedCallbackReceiver
    {
        private PerManagerObject<ILineRenderer> miningRayRenderer;
        private PerManagerObject<IntObject> lastFrameRendered;
        private int currentlyMinedId;

        private StateMachine<MiningBotNavmeshAi> stateMachine;

        private readonly float maxDistanceToContinueMining = 50.0f;

        private readonly float maximumMiningDistance = 15.0f;
        private readonly float optimalMiningDistance = 10.0f;

        private ResourceType resourceType;

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(MiningBotNavmeshAi))
                .addVariable(currentlyMinedId, nameof(currentlyMinedId))
                .addVariable(resourceType, nameof(resourceType))
                .addVariable(stateMachine, nameof(stateMachine))
                .print();
        }

        public void finishConstruction()
        {
            stateMachine.changeState(new Idle(this));
        }

        public MiningBotNavmeshAi(IGunkObject parent) : base(parent)
        {
            stateMachine = new StateMachine<MiningBotNavmeshAi>(new UnderConstruction(this));
            miningRayRenderer = new PerManagerObject<ILineRenderer>(nameof(miningRayRenderer), parent, () => new MiningRayRenderer(parent.objectManager.mainCamera));
            lastFrameRendered = new PerManagerObject<IntObject>(nameof(lastFrameRendered), parent, () => new IntObject(0));
        }

        public void update(float deltaTime)
        {
            stateMachine.update(deltaTime);
            if (lastFrameRendered.getObject().value != Time.frameCount)
            {
                lastFrameRendered.getObject().value = Time.frameCount;
                miningRayRenderer.getObject().render();
            }
        }

        private void changeState(State<MiningBotNavmeshAi> newState)
        {
            parent.addLog(this, "state was changed to: "+newState.GetType().Name);
            stateMachine.changeState(newState);
        }

        public override void destroy()
        {
        }

        public void goMineSpecificResource(int resourceObjectId)
        {
            if (stateMachine.currentState is UnderConstruction)
            {
                throw new Exception("mining cannot be performed when object is under construction");
            }
            currentlyMinedId = resourceObjectId;
            resourceType = parent.objectManager.getObjectWithId(resourceObjectId).resourcesContainer.type;
            changeState(new GoingToMine(this));
        }

        private class UnderConstruction : State<MiningBotNavmeshAi>
        {
            public UnderConstruction(MiningBotNavmeshAi parent) : base(parent)
            {
            }

            public override string ToString()
            {
                return new VariablesReportBuilder(this).print();
            }
        }

        private class Idle : State<MiningBotNavmeshAi>
        {
            public Idle(MiningBotNavmeshAi parent) : base(parent)
            {
            }

            public override string ToString()
            {
                return new VariablesReportBuilder(this).print();
            }
        }

        private class GoingToMine : State<MiningBotNavmeshAi>
        {
            public GoingToMine(MiningBotNavmeshAi parent) : base(parent)
            {
            }

            public override void setAsActive()
            {
                var resourceObject = parent.parent.objectManager.getObjectWithId(parent.currentlyMinedId);
                if (resourceObject != null)
                    parent.parent.agent.destination=resourceObject.transform.position;
                else
                    parent.changeState(new Idle(parent));
            }

            public override void deactivate()
            {
                parent.parent.agent.stop();
            }

            public override void update(float deltaTime)
            {
                var agent = parent.parent.agent;
                if (agent.remainingDistance <= parent.optimalMiningDistance)
                {
                    agent.stop();
                    parent.changeState(new Mining(parent));
                }
            }

            public override string ToString()
            {
                return new VariablesReportBuilder(this)
                    .print();
            }
        }

        private class Mining : State<MiningBotNavmeshAi>
        {
            private int? miningRayId;

            public Mining(MiningBotNavmeshAi parent) : base(parent)
            {
            }

            private void destroyExistingMiningRay()
            {
                if (miningRayId != null)
                {
                    parent.miningRayRenderer.getObject().removeObject(miningRayId.Value);
                    miningRayId = null;
                }
            }

            public override void deactivate()
            {
                destroyExistingMiningRay();
            }

            public override void update(float deltaTime)
            {
                var agent = parent.parent.agent;

                //check if distance bigger than maximal mining range
                if (agent.remainingDistance > parent.maximumMiningDistance)
                {
                    parent.changeState(new GoingToMine(parent));
                    return;
                }

                //trying to gather resource
                var minedResource = parent.parent.objectManager.getObjectWithId(parent.currentlyMinedId);
                if (minedResource?.resourcesContainer?.isDepleted() != false)
                {
                    parent.changeState(new MineDepleted(parent));
                    return;
                }

                destroyExistingMiningRay();
                var myPosition = parent.parent.transform.position;
                var resourcePosition = minedResource.transform.position;
                var myId = parent.parent.transform.id;
                parent.miningRayRenderer.getObject().addObject(myId, myPosition, resourcePosition);
                miningRayId = myId;

                minedResource.resourcesContainer.gatherResources(deltaTime);
            }

            public override string ToString()
            {
                return new VariablesReportBuilder(this)
                    .addVariable(miningRayId, nameof(miningRayId)).print();
            }
        }

        private class MineDepleted : State<MiningBotNavmeshAi>
        {
            public MineDepleted(MiningBotNavmeshAi parent) : base(parent)
            {
            }

            public override void setAsActive()
            {
                var myPosition = parent.parent.transform.position;
                var closestId = ResourceComponent.getClosestResourceFinder(parent.parent)
                    .getObject()
                    .findClosestObject(new Float2(myPosition.x, myPosition.z), parent.resourceType,
                        parent.maxDistanceToContinueMining)?.id;
                if (closestId == null) return;

                var closestObject = parent.parent.objectManager.getObjectWithId(closestId.Value);
                parent.currentlyMinedId = closestId.Value;
                parent.resourceType = closestObject.resourcesContainer.type;
                parent.changeState(new GoingToMine(parent));
            }

            public override string ToString()
            {
                return new VariablesReportBuilder(this)
                    .print();
            }
        }
    }
}