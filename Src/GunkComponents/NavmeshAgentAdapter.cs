﻿using UnityEngine;
using UnityEngine.AI;

namespace Gunk_Scripts.GunkComponents
{
    public class NavmeshAgentAdapter: INavigationAgent
    {
        private readonly NavMeshAgent agent;

        public NavmeshAgentAdapter(NavMeshAgent agent)
        {
            this.agent = agent;
            var currentPosition = agent.transform.position;
            agent.transform.position = new Vector3(currentPosition.x,
                Terrain.activeTerrain.SampleHeight(currentPosition), currentPosition.z);
        }

        public void stop()
        {
            agent.isStopped = true;
        }

        public bool isStopped => agent.isStopped;

        private Vector3 destinationInternal;

        public Vector3 destination
        {
            set
            {
                var correctedPosition = new Vector3(value.x, Terrain.activeTerrain.SampleHeight(value),value.z);
                Debug.Log("agent destination set to " + correctedPosition);
                agent.SetDestination(correctedPosition);
                resume();
                destinationInternal = value;
            }
            get { return destinationInternal; }
        }

        public void resume()
        {
            agent.isStopped = false;
        }

        public float remainingDistance => Vector3.Distance(agent.transform.position, destination);

        public void setMaxSpeed(float value) => agent.speed = value;

        public Vector3 position => agent.transform.position;
    }
}
