using System;
using System.Collections.Generic;
using System.IO;
using Gunk_Scripts.GunkComponents;

namespace Src.GunkComponents
{
    /// <summary>
    /// object that for a given type and name has only one instance per GunkObjectManager
    /// </summary>
    public class PerManagerObject<T> where T : class
    {
        //for testing purposes
        private static T forcedNextCreatedObject;

        private string name;
        private Func<T> constructor;
        private IGunkObject parent;

        public PerManagerObject(string name, IGunkObject parent, Func<T> constructor)
        {
            this.parent = parent;
            this.constructor = constructor;
            this.name = name;
        }

        private bool hasCachedValue;
        private T cachedValue;

        public T getObject()
        {
            if (forcedNextCreatedObject != null)
            {
                return forcedNextCreatedObject;
            }

            if (hasCachedValue)
            {
                return cachedValue;
            }
            object foundValue;

            parent.objectManager.otherVariables.TryGetValue(name, out foundValue);
            if (foundValue is T) return (T) foundValue;

            foundValue = constructor();
            parent.objectManager.otherVariables.Add(name, foundValue);
            hasCachedValue = true;
            cachedValue = (T)foundValue;
            return (T) foundValue;
        }

        /// <summary>
        /// method used only for testing. sets object that will be returned by getObject until removeForcedObject is called 
        /// </summary>
        public static void forceSetCreatedObject(T objectToReturn)
        {
            forcedNextCreatedObject = objectToReturn;
        }

        /// <summary>
        /// removes object set by forceSetCreatedObject method
        /// </summary>
        public static void removeForcedObject()
        {
            forcedNextCreatedObject = null;
        }
    }
}