﻿using Gunk_Scripts.Maps;

namespace Gunk_Scripts.GunkComponents
{
    public class PlacedObject:GunkComponent
    {
        private ObjectPattern pattern;

        public PlacedObject(IGunkObject parent, ObjectPattern objectPattern) : base(parent)
        {
            parent.objectManager.lockSpaceUnderObject(parent,objectPattern);
            pattern = objectPattern;
        }

        public override void destroy()
        {
            parent.objectManager.unlockSpaceUnderObject(parent,pattern);
        }
    }
}
