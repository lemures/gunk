using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Renderers;

namespace Src.GunkComponents
{
    public class ProgressComponent : GunkComponent
    {
        public override void destroy()
        {
            parent.objectManager.countersCollection.hideCounter(parent.id, CountersRenderer.DrawStyle.Progress);
        }

        private Counter progressCounter = new Counter(0, 1.0f);

        public void setValue(float value)
        {
            progressCounter.changeValue(value);
            parent.objectManager.countersCollection.setCounterValue(parent.id, CountersRenderer.DrawStyle.Progress, value);
        }

        public void show()
        {
            parent.objectManager.countersCollection.showCounter(parent.id, CountersRenderer.DrawStyle.Progress);
        }

        public void hide()
        {
            parent.objectManager.countersCollection.hideCounter(parent.id, CountersRenderer.DrawStyle.Progress);
        }

        public ProgressComponent(IGunkObject parent) : base(parent)
        {
            parent.objectManager.countersCollection.addHiddenCounter(parent.id, progressCounter, CountersRenderer.DrawStyle.Progress);
        }
    }
}