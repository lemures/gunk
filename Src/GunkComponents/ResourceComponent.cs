using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.Maps;
using JetBrains.Annotations;
using Src.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.GunkComponents
{
    public class ResourceComponent : GunkComponent, IPositionChangedCallbackReceiver, IResourcesContainer
    {
        private PerManagerObject<IClosestResourceFinder> closestResourceFinder;
        private PerManagerObject<IClosestResourceFinder> closestMineFinder;

        public static PerManagerObject<IClosestResourceFinder> getClosestResourceFinder(IGunkObject user)
        {
            return new PerManagerObject<IClosestResourceFinder>(nameof(closestResourceFinder), user,
                () => new ClosestResourceFinder(user.objectManager.mapWidth, user.objectManager.mapHeight));
        }

        public static PerManagerObject<IClosestResourceFinder> getClosestMineFinder(IGunkObject user)
        {
            return new PerManagerObject<IClosestResourceFinder>(nameof(closestMineFinder), user,
                () => new ClosestResourceFinder(user.objectManager.mapWidth, user.objectManager.mapHeight)
            );
        }

        private IResourcesContainer resourcesContainer;

        public void changePosition(Vector3 newPosition)
        {
            closestResourceFinder.getObject().updateObjectPosition(parent.id, new Float2(newPosition.x, newPosition.z));
        }

        private bool visibleByWorkers;
        private bool visibleByMiners;

        //TODO create separate classes for visibleByWorkers and visibleByMiners
        public ResourceComponent(IGunkObject parent, [NotNull]IResourcesContainer resource, bool visibleByWorkers, bool visibleByMiners) : base(parent)
        {
            closestResourceFinder = getClosestResourceFinder(parent);
            closestMineFinder = getClosestMineFinder(parent);

            this.visibleByWorkers = visibleByWorkers;
            this.visibleByMiners = visibleByMiners;
            var myPosition = parent.transform.position;
            resourcesContainer = resource;
            if (visibleByWorkers)
            closestResourceFinder.getObject().addObject(parent.id, new Float2(myPosition.x, myPosition.z), type);
            if (visibleByMiners)
            closestMineFinder.getObject().addObject(parent.id, new Float2(myPosition.x, myPosition.z), type);
        }

        public override void destroy()
        {
            if (resourcesContainer == null) return;

            if (visibleByWorkers)
            closestResourceFinder.getObject().removeObject(parent.id);
            if (visibleByMiners)
            closestMineFinder.getObject().removeObject(parent.id);
            resourcesContainer = null;
        }

        public GatheredResources gatherResources(float deltaTime)
        {
            var result = resourcesContainer.gatherResources(deltaTime);
            if (resourcesContainer?.isDepleted() == true)
            {
                parent.destroy();
            }
            return result;
        }

        public ResourceType type => resourcesContainer.type;

        public bool isDepleted()
        {
            if (!resourcesContainer.isDepleted()) return false;

            parent.destroy();
            return true;

        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(ResourceComponent))
                .addVariable(resourcesContainer, nameof(resourcesContainer))
                .addVariable(visibleByMiners, nameof(visibleByMiners))
                .addVariable(visibleByWorkers, nameof(visibleByWorkers))
                .print();
        }
    }
}