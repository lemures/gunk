using Gunk_Scripts.Maps;

namespace Gunk_Scripts.GunkComponents
{
    public interface IResourcesContainer
    {
        bool isDepleted();

        GatheredResources gatherResources(float deltaTime);

        ResourceType type { get; }
    }
}