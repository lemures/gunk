﻿using System.Collections.Generic;
using Gunk_Scripts;
using Gunk_Scripts.GunkComponents;
using Src.Libraries;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.Events;
using UnityEngine;

namespace Src.GunkComponents
{
    public class SkeletonAnimatorComponent:GunkComponent, ISkeletonAnimator
    {
        private readonly SkeletonAnimator animator;

        public void playClip(string name, float argument = 0)
        {
            animator.playClip(name, argument);
        }

        public string currentClip => animator.currentClip;

        public List<SocketData> getAllSocketBones()
        {
            return animator.getAllSocketBones();
        }

        public SkeletonAnimatorComponent(IGunkObject parent,string prefabName, GameObjectManager objectManager,int objectId, string startClipName):base(parent)
        {
            animator = new SkeletonAnimator(prefabName, objectManager, objectId, startClipName);
        }

        public void changeClipArgument(float value)
        {
            animator.changeClipArgument(value);
        }

        public List<EventData> collectTriggeredEvents()
        {
            return animator.collectTriggeredEvents();
        }

        public Vector3 getBarrelBoneWorldPosition(int index, Vector3 objectPosition)
        {
            return animator.getBarrelBoneWorldPosition(index, objectPosition);
        }

        public bool isCurrentClipLoaded()
        {
            return animator.isCurrentClipLoaded();
        }

        public void updateAnimation(float timeElapsed)
        {
            animator.updateAnimation(timeElapsed);
        }

        public override void destroy()
        {
            animator.destroy();
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(SkeletonAnimatorComponent))
                .addVariable(animator, nameof(animator))
                .print();
        }
    }
}
