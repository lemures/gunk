using Gunk_Scripts.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class AntiCorruptionLaser : GunkObject
    {
        private static string prefabName = GunkPrefabNames.getName(GunkObjectType.AntiCorruptionLaser);
        private static int maxHealth = 1000;

        public AntiCorruptionLaser(IGunkObjectManager objectManagerFacade, Vector3 position, Faction faction) : base(objectManagerFacade, prefabName, position, "")
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addPlacedObject();
            addEnergyNeededForConstruction(500.0f);
            addEnergy(100, 500, 50.0f);
            finishAddingComponents();
        }
    }
}