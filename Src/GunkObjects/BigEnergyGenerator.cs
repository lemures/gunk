using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Objects_Selection;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class BigEnergyGenerator : GunkObject
    {
        private static string prefabName = GunkPrefabNames.getName(GunkObjectType.BigEnergyGenerator);
        private static int maxHealth = 1000;

        public BigEnergyGenerator(IGunkObjectManager objectManagerFacade, Vector3 position, Faction faction) : base(objectManagerFacade, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addConnectible(new ConnectibleObject(this));
            addPlacedObject();
            addCuboidCollider(new Vector3(2, 3, 2), SelectionPriorityUtils.getBuildingSelectionPriority());
            addEnergyGenerator(new ConstantValueEnergyGenerator(500.0f));
            finishAddingComponents();
        }
    }
}