using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class BlueCrystal:Crystal
    {
        private static string prefabName = GunkPrefabNames.getName(GunkObjectType.CrystalBlue);

        private static LimitedResourcesSource createResourcesSource() =>
            new LimitedResourcesSource(ResourceType.BlueCrystals, 100.0f, 20.0f);

        public BlueCrystal(IGunkObjectManager objectManagerFacade, Vector3 position)
            : base(objectManagerFacade, prefabName, position, createResourcesSource())
        {}
    }
}