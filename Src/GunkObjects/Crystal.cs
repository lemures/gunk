using Gunk_Scripts.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class Crystal : GunkObject
    {
        protected Crystal(IGunkObjectManager objectManagerFacade, string prefabName, Vector3 position, IResourcesContainer resourcesContainer) : base(objectManagerFacade, prefabName, position)
        {
            addResource(resourcesContainer, true, false);
            finishAddingComponents();
        }
    }
}