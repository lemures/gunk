using System;
using Gunk_Scripts.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public static class GunkObjectFactory
    {
        private static IGunkObject nextObject;
        public static void setNextSpawnedObject(IGunkObject objectToSpawn)
        {
            nextObject = objectToSpawn;
        }

        public static void resetNextSpawnedObject() => nextObject = null;

        public static bool hasNextObjectToSpawn() => nextObject != null;

        public static IGunkObject spawnObject(IGunkObjectManager objectManager, GunkObjectType type, Vector3 position,
            Faction faction)
        {
            if (nextObject != null)
            {
                var temp = nextObject;
                nextObject = null;
                return temp;
            }
            switch (type)
            {
                case GunkObjectType.Relay:
                    return new Relay(objectManager, position, faction);
                case GunkObjectType.WeldingBot:
                    return new WeldingBot(objectManager, position, faction);
                case GunkObjectType.Worker:
                    return new Worker(objectManager, position, faction);
                case GunkObjectType.Connector:
                    return new Connector(objectManager, position, faction);
                case GunkObjectType.SmallEnergyGenerator:
                    return new SmallEnergyGenerator(objectManager, position, faction);
                case GunkObjectType.BigEnergyGenerator:
                    return new BigEnergyGenerator(objectManager, position, faction);
                case GunkObjectType.CrystalPink:
                    return new PinkCrystal(objectManager,position);
                case GunkObjectType.CrystalBlue:
                    return new BlueCrystal(objectManager,position);
                case GunkObjectType.Platform:
                    return new Platform(objectManager,position,faction);
                case GunkObjectType.Manufacturer:
                    return new Manufacturer(objectManager,position,faction);
                case GunkObjectType.MineBlue:
                    return new MineBlue(objectManager, position);
                case GunkObjectType.Miner:
                    return new Miner(objectManager,position, faction);
                case GunkObjectType.AntiCorruptionLaser:
                    return new AntiCorruptionLaser(objectManager, position, faction);
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}