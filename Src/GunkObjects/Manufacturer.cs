using System;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Objects_Selection;
using Gunk_Scripts.SkeletonAnimations;
using Src.GunkComponents;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.BoneTypes;
using Src.SkeletonAnimations.CurvesTypes;
using Src.SkeletonAnimations.Internal;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class Manufacturer : GunkObject
    {
        private static string prefabName = GunkPrefabNames.getName(GunkObjectType.Manufacturer);
        private static int maxHealth = 1000;

        public Manufacturer(IGunkObjectManager objectManager, Vector3 position, Faction faction):
            base(objectManager, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addConnectible(new ConnectibleObject(this));
            addPlacedObject();
            addCuboidCollider(new Vector3(2, 4, 2), SelectionPriorityUtils.getBuildingSelectionPriority());
            addEnergy(100,500,50);
            addComponent(new ManufacturerAiComponent(this));
            finishAddingComponents();
        }
    }
}