using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using Src.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class MineBlue : GunkObject
    {
        private static string prefabName = GunkPrefabNames.getName(GunkObjectType.MineBlue);

        public MineBlue(IGunkObjectManager objectManagerFacade, Vector3 position) : base(objectManagerFacade, prefabName, position)
        {
            addResource(createResourcesSource(), false, true);
            objectManager.setSpecialFieldType(position, TerrainFieldType.CrystalMine);
            finishAddingComponents();
        }
        private static LimitedResourcesSource createResourcesSource() =>
            new LimitedResourcesSource(ResourceType.BlueCrystals, 100.0f, 20.0f);
    }
}