using Gunk_Scripts.GunkComponents;
using Src.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class Miner : GunkObject
    {
        private float maxHealth = 500;
        private float maxEnergy = 2000.0f;
        private static string prefabName = GunkPrefabNames.getName(GunkObjectType.Miner);

        public Miner(IGunkObjectManager objectManagerFacade, Vector3 position, Faction faction) : base(objectManagerFacade, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth,maxHealth);
            addPlacedObject(objectManager.calculatePrefabMeshDimensions(prefabName));
            addEnergyNeededForConstruction(300.0f);
            addConnectible(new ConnectibleObject(this));
            addComponent(new MinerAi(this));
            finishAddingComponents();
        }
    }
}