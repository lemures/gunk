using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class PinkCrystal:Crystal
    {
        private static string prefabName = GunkPrefabNames.getName(GunkObjectType.CrystalPink);

        private static LimitedResourcesSource createResourcesSource() =>
            new LimitedResourcesSource(ResourceType.PinkCrystals, 100.0f, 20.0f);

        public PinkCrystal(IGunkObjectManager objectManagerFacade, Vector3 position)
            : base(objectManagerFacade, prefabName, position, createResourcesSource())
        {}
    }
}