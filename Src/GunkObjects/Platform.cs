using Gunk_Scripts.GunkComponents;
using Src.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class Platform : GunkObject
    {
        private float maxHealth = 500;
        private float maxEnergy = 2000.0f;
        private static string prefabName = GunkPrefabNames.getName(GunkObjectType.Platform);

        private PlatformAiComponent platformAiComponent;

        public Platform(IGunkObjectManager objectManagerFacade, Vector3 position, Faction faction) : base(objectManagerFacade, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth,maxHealth);
            addPlacedObject();
            addEnergyNeededForConstruction(300.0f);
            addConnectible(new ConnectibleObject(this));
            platformAiComponent = new PlatformAiComponent(this);
            addComponent(platformAiComponent);
            addObjectToConstructionQueue(GunkObjectType.Worker);
            addObjectToConstructionQueue(GunkObjectType.WeldingBot);
            addComponent(new ProgressComponent(this));
            getComponent<ProgressComponent>().show();
            getComponent<ProgressComponent>().setValue(0.3f);
            finishAddingComponents();
        }

        public void addObjectToConstructionQueue(GunkObjectType objectType)
        {
            platformAiComponent.addObjectToQueue(objectType);
        }
    }
}