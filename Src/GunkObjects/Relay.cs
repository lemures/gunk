using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Objects_Selection;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class Relay : GunkObject
    {
        private static string prefabName = GunkPrefabNames.getName(GunkObjectType.Relay);
        private static int maxHealth = 1000;

        public Relay(IGunkObjectManager objectManager, Vector3 position, Faction faction):
            base(objectManager, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth, maxHealth);
            addConnectible(new ConnectibleConnector(this,20));
            addPlacedObject();
            addCuboidCollider(new Vector3(2, 6, 2), SelectionPriorityUtils.getBuildingSelectionPriority());
            addEnergyGenerator(new ConstantValueEnergyGenerator(300.0f));
            addEnergy(100,500,50);
            finishAddingComponents();
        }
    }
}