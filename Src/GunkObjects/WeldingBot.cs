﻿using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Objects_Selection;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class WeldingBot:GunkObject
    {
        private static string prefabName = GunkPrefabNames.getName(GunkObjectType.WeldingBot);
        private static float maxHealth=1000;

        public WeldingBot(IGunkObjectManager objectManager, Vector3 position, Faction faction) : base(objectManager, prefabName, position)
        {
            setFaction(faction);
            addHealth(maxHealth,maxHealth);
            addNavmeshAgent();
            addCuboidCollider(new Vector3(2, 2, 2),SelectionPriorityUtils.getWeldingBotSelectionPriority());
            selectionPriority = SelectionPriorityUtils.getWeldingBotSelectionPriority();
            finishAddingComponents();
        }
    }
}
