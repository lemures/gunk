using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Objects_Selection;
using UnityEngine;

namespace Gunk_Scripts.GunkObjects
{
    public class Worker : GunkObject
    {
        private static readonly string PrefabName = GunkPrefabNames.getName(GunkObjectType.Worker);

        private MiningBotNavmeshAi aiComponent;

        private void addMiningBotAi()
        {
            aiComponent = new MiningBotNavmeshAi(this);
            addComponent(aiComponent);
        }

        public void giveOrderToMine(int resourceId)
        {
            aiComponent.goMineSpecificResource(resourceId);
        }

        public Worker(IGunkObjectManager objectManagerFacade, Vector3 position, Faction faction) : base(objectManagerFacade, PrefabName, position)
        {
            setFaction(faction);
            addNavmeshAgent();
            addCuboidCollider(Vector3.one,SelectionPriorityUtils.getWorkerSelectionPriority());
            selectionPriority = SelectionPriorityUtils.getWorkerSelectionPriority();
            addMiningBotAi();
            addEnergyNeededForConstruction(200.0f);
            finishAddingComponents();
        }
    }
}