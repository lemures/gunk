﻿using System;
using System.Collections.Generic;
using Src.Console;
using Src.Libraries;
using UnityEngine;

namespace Gunk_Scripts
{
    [ExecutesCommand("sun")]
    public class AutoIntensity : MonoBehaviour
    {
        public static string sun(string[] args)
        {
            switch (args[1])
            {
                case "off":
                    nightDisabled = true;
                    break;
                case "on":
                    nightDisabled = false;
                    break;
                default:
                    return "option unrecognized, try on or off";
            }

            return "";
        }

        private float maxIntensity = 1.0f;
        private float maxAmbient = 1f;

        private float skySpeed = 1.0f;
        private readonly float fullCycleTime = 30.0f;
        private float passed;
        private static bool nightDisabled = false;

        Light mainLight;
        private DayHourSimulation currentHour;

        void Start()
        {
            mainLight = GetComponent<Light>();
            currentHour = new DayHourSimulation(15.0f, 0.0f, 10.0f, 10.0f, 5.0f);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q)) { skySpeed /= 2; }
            if (Input.GetKeyDown(KeyCode.E)) { skySpeed *= 2; }

            currentHour.update(Time.deltaTime);
            float sunAngle = (currentHour.getHour() / 24.0f) * 180.0f;

            if (nightDisabled)
            {
                sunAngle = 100.0f;
            }

            transform.rotation = Quaternion.Euler(sunAngle, sunAngle, 0.0f);
            passed += Time.deltaTime *skySpeed;

            var dot = Mathf.Clamp01(Vector3.Dot(transform.forward, Vector3.down));
            float intensity = dot * 1.3f;
            float ambientIntensity = (maxAmbient * dot);

            RenderSettings.ambientLight = ambientIntensity * Color.white;
            RenderSettings.ambientIntensity = ambientIntensity;
            mainLight.intensity = intensity;
        }
    }
}
