﻿using System.Collections.Generic;
using Gunk_Scripts.SkeletonAnimations;
using UnityEngine;

//using UnityEngine;

namespace Gunk_Scripts.Data_Structures
{
    [System.Serializable]
    public class BezierCpu
    {
        public List<Float2> points;
        private int _numberOfPoints;

        public float animationLenght;

        //extra piece of data tied to a curve
        public int specialType;

        public BezierCpu()
        {
            points = new List<Float2>
            {
                new Float2(0, 0),
                new Float2(0.25f, 0.75f),
                new Float2(0.75f, 0.75f),
                new Float2(1, 0)
            };
            _numberOfPoints = 2;
            animationLenght = 1.0f;
            specialType = 0;

        }

        public BezierCpu(BezierCpu oryginal)
        {
            points = new List<Float2>();
            for (var i = 0; i < oryginal.points.Count; i++)
            {
                points.Add(oryginal.points[i]);
            }
            animationLenght = oryginal.animationLenght;
            _numberOfPoints = oryginal._numberOfPoints;
            specialType = oryginal.specialType;
        }

        /// <summary>
        /// Checks if a curve has any changed parameters versus default vaule
        /// </summary>
        /// <param name="curve"></param>
        /// <returns></returns>
        public bool wasModified()
        {
            var clearCurve = new BezierCpu();
            if (_numberOfPoints != clearCurve._numberOfPoints)
            { return true; }
            if (specialType != clearCurve.specialType)
            { return true; }
            for (var i = 0; i < clearCurve.points.Count; i++)
            {
                if (clearCurve.points[i].x != points[i].x)
                { return true; }
                if (clearCurve.points[i].y != points[i].y)
                { return true; }
            }
            return false;
        }

        private Float2 getOutputVector(int num)
        {
            return points[1 + num * 3];
        }

        private Float2 getInputVector(int num)
        {
            return points[-1 + num * 3];
        }

        private Float2 getPoint(int num)
        {
            return points[num * 3];
        }

        public void checkBoundriesAndRepair()
        {
            points[0] = new Float2(0.0f, points[0].y);
            points[points.Count - 1] = new Float2(1.0f, points[points.Count - 1].y);
            for (var i = 0; i < points.Count; i++)
            {
                var val = points[i];
                val.y = Mathf.Max(val.y, -1.0f);
                val.y = Mathf.Min(val.y, 1.0f);
                val.x = Mathf.Max(val.x, 0.0f);
                val.x = Mathf.Min(val.x, 1.0f);
                points[i] = val;
            }
            for (var num = 0; num < _numberOfPoints; num++)
            {

                var val1 = points[num * 3];

                if (num != 0)
                {
                    var pom = points[-1 + num * 3];
                    var val2 = points[-3 + num * 3];
                    pom.x = Mathf.Min(pom.x, val1.x);
                    pom.x = Mathf.Max(pom.x, val2.x);
                    points[-1 + num * 3] = pom;

                }
                if (3 * num != points.Count - 1)
                {
                    var pom = points[1 + num * 3];
                    var val2 = points[3 + num * 3];
                    pom.x = Mathf.Max(pom.x, val1.x);
                    pom.x = Mathf.Min(pom.x, val2.x);
                    points[1 + num * 3] = pom;
                    var nextVal = points[num * 3 + 3];
                    val1.x = Mathf.Min(val1.x, nextVal.x);
                    points[num * 3] = val1;

                }
            }
        }

        public void insertPoint(Float2 val)
        {
            if (_numberOfPoints == 16)
            { return; }
            var previousPointIndex = 0;
            if (val.x > 0.95f)
            { val.x = 0.95f; }
            while (points[previousPointIndex].x <= val.x)
            {
                previousPointIndex += 3;
            }
            points.Insert(previousPointIndex - 1, val - (getInputVector(previousPointIndex / 3) - getPoint(previousPointIndex / 3)) * (2f / 5f));
            points.Insert(previousPointIndex - 1, val);
            points.Insert(previousPointIndex - 1, val - (getOutputVector(previousPointIndex / 3 - 1) - getPoint(previousPointIndex / 3 - 1)) * (2f / 5f));
            _numberOfPoints++;
            checkBoundriesAndRepair();
        }

        public void removePoint(int num)
        {
            if (num == 0)
            { return; }
            if (num == _numberOfPoints - 1)
            { return; }
            points.RemoveAt(num * 3 - 1);
            points.RemoveAt(num * 3 - 1);
            points.RemoveAt(num * 3 - 1);
            _numberOfPoints--;
        }

        
        public BezierGpu toGpu()
        {
            var res = new BezierGpu
            {
                animationLenght = animationLenght,
                numberOfPoints = _numberOfPoints,
                specialType = specialType,
                needAnimation = 1
            };
            for (var i = 0; i < _numberOfPoints; i++)
            {
                if (i != 0)
                {
                    res.inputPointsX[i] = points[-1 + i * 3].x;
                    res.inputPointsY[i] = points[-1 + i * 3].y;
                }
                res.basicPointsX[i] = points[i * 3].x;
                res.basicPointsY[i] = points[i * 3].y;
                if (i != _numberOfPoints - 1)
                {
                    res.outputPointsX[i] = points[1 + i * 3].x;
                    res.outputPointsY[i] = points[1 + i * 3].y;
                }
            }
            return res;
        }
    }
}
