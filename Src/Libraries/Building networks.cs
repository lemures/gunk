﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.Data_Structures;
using NUnit.Framework.Constraints;
using NUnit.Framework.Internal.Commands;
using UnityEngine;

namespace Gunk_Scripts
{
    public class ObjectNetworks
    {
        private readonly List<Edge> allConnections = new List<Edge>();
        private readonly List<PlacedObject> placedObjects = new List<PlacedObject>();
        private readonly DisjointSet disjointSet = new DisjointSet();

        public class OperationResult
        {
            public readonly List<Edge> addedEdges;
            public readonly List<Edge> removedEdges;

            public OperationResult(List<Edge> addedEdges, List<Edge> removedEdges)
            {
                this.addedEdges = addedEdges;
                this.removedEdges = removedEdges;
            }
        }

        private class PlacedObject
        {
            public readonly int objectId;
            public readonly Vector3 position;
            public bool isConnector;
            public float range;

            public PlacedObject(int objectId, Vector3 position)
            {
                this.objectId = objectId;
                this.position = position;
            }

            public PlacedObject(int objectId, Vector3 position, float range)
            {
                this.objectId = objectId;
                this.position = position;
                this.range = range;
                isConnector = true;
            }

            public void transformIntoConnector(float connectorRange)
            {
                isConnector = true;
                range = connectorRange;
            }
        }

        private List<Edge> repairConnections()
        {
            UnityEngine.Profiling.Profiler.BeginSample("repairing cable connections");
            var result = new List<Edge>();
            var potentialEdges = new List<KeyValuePair<PlacedObject,PlacedObject>>();

            foreach (var connector in placedObjects.FindAll(x => x.isConnector))
            {
                foreach (var building in placedObjects)
                {
                    if (building.objectId != connector.objectId && canConnect(building,connector))
                    {
                        potentialEdges.Add(new KeyValuePair<PlacedObject, PlacedObject>(building,connector));
                    }
                }
            }

            if (potentialEdges.Count == 0)
            {
                return new List<Edge>();
            }

            potentialEdges =potentialEdges
                .OrderBy(x => (x.Value.isConnector) ? 0 : 1)
                .ThenBy(x=>Vector3.Distance(x.Key.position,x.Value.position))
                .ToList();

            foreach (var edge in potentialEdges)
            {
                if (!disjointSet.oneSet(edge.Key.objectId, edge.Value.objectId))
                {
                    disjointSet.union(edge.Key.objectId, edge.Value.objectId);
                    result.Add(new Edge(edge.Key.objectId, edge.Value.objectId));
                }
            }

            UnityEngine.Profiling.Profiler.EndSample();
            return result;
        }

        private OperationResult addObject(PlacedObject objectToAdd)
        {
            if (placedObjects.Exists(x => x.objectId == objectToAdd.objectId))
            {
                throw new InvalidOperationException("there is already an object with this ID " + objectToAdd.objectId);
            }

            placedObjects.Add(objectToAdd);
            var addedEdges = repairConnections();
            allConnections.AddRange(addedEdges);
            return new OperationResult(addedEdges, new List<Edge>());
        }

        public OperationResult addConnector(int objectId, Vector3 position, float range)
        {
            return addObject(new PlacedObject(objectId, position,range));
        }

        public OperationResult addObject(int objectId, Vector3 position)
        {
            return addObject(new PlacedObject(objectId, position));
        }

        private static bool canConnect(PlacedObject x, PlacedObject y)
        {
            var dist= Vector3.Distance(x.position, y.position);
            return (x.isConnector && dist < x.range) || (y.isConnector && dist < y.range);
        }

        public OperationResult removeObject(int objectId)
        {
            if (!placedObjects.Exists(x => x.objectId == objectId))
            {
                throw new InvalidOperationException("object with ID " + objectId + " doesn't exist");
            }

            placedObjects.RemoveAll(x => x.objectId == objectId);
            disjointSet.removeConnectionsWithElement(objectId);
            var result=allConnections.FindAll(x => x.x == objectId || x.y == objectId).ToList();
            //remove edges that has an objectID in one of its ends
            allConnections.RemoveAll(x => x.x == objectId || x.y == objectId);
            return new OperationResult(repairConnections(), result);
        }

        /// <summary>
        /// returns existing connector in range closest from position or null if there are none
        /// </summary>
        public int? getClosestConnectorIdInRange(Vector3 position)
        {
            if (!isConnectorNearby(position)) return null;

            var closest = placedObjects
                .FindAll(x => canConnect(x, new PlacedObject(0, position)))
                .OrderBy(x => Vector3.Distance(position, x.position))
                .First();
            return closest.objectId;
        }

        public bool isConnectorNearby(Vector3 position)
        {
            return placedObjects.FindAll(x => canConnect(x, new PlacedObject(0, position))).Count != 0;
        }

        public OperationResult transformIntoConnector(int objectId, float range)
        {
            Debug.Log("transforming "+objectId);
            var building=placedObjects.Find(x => x.objectId == objectId);
            if (building == null)
            {
                throw new InvalidOperationException("there is no building with ID" + objectId);
            }
            if (building.isConnector)
            {
                throw new InvalidOperationException("building with ID" + objectId + " is already a connector");
            }
            building.transformIntoConnector(range);
            var addedEdges = repairConnections();
            return new OperationResult(addedEdges,new List<Edge>());
        }

        public List<List<int>> getAllConnectedObjectsSets()
        {
            var allIds = (from building in placedObjects select building.objectId).ToList();
            return disjointSet.groupIntoSets(allIds);
        }
    }
}
