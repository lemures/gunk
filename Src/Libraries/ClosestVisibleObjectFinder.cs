﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Gunk_Scripts.Data_Structures;
using UnityEngine;

namespace Gunk_Scripts
{
    [Serializable]
    public class ClosestVisibleObjectFinder:ISerializable
    {
        public enum Visibility
        {
            Visible,
            Invisible,
        }

        [Serializable]
        private class ObjectData
        {
            public readonly float x;
            public readonly float y;
            public readonly int id;

            public ObjectData(int id, float x, float y)
            {
                this.x = x;
                this.y = y;
                this.id = id;
            }
        }

        private readonly List<ClosestObjectFinder> layerCollections = new List<ClosestObjectFinder>();
        //stores all objects positions
        private readonly SortedDictionary<int, ObjectData> allObjects = new SortedDictionary<int, ObjectData>();
        private readonly Visibility[,] relationships;
        //dictionary to know what type certain object is of
        private readonly SortedDictionary<int, int> typesMap = new SortedDictionary<int, int>();

        //mainly for Serialization
        private readonly int areaWidth;
        private readonly int areaHeight;
        private readonly int typesCount;

        public ClosestVisibleObjectFinder(int areaWidth, int areaHeight,int typesCount, Visibility[,]typesVisibility)
        {
            this.areaHeight = areaHeight;
            this.areaWidth = areaWidth;
            this.typesCount = typesCount;

            for (int i = 0; i < typesCount; i++)
            {
                layerCollections.Add(new ClosestObjectFinder(areaWidth, areaHeight));
            }

            this.relationships = (Visibility[,])typesVisibility.Clone();
        }

        /// <summary>
        /// adds new object with certain id from a certain type
        /// </summary>
        public void addObject(int id, float x, float y, int type)
        {
            layerCollections[type].addObject(id, new Float2(x, y));
            typesMap[id] = type;
            allObjects[id] = new ObjectData(id, x, y);
        }

        /// <summary>
        /// removes object with a certain id
        /// </summary>
        public void removeObject(int id)
        {
            var type = typesMap[id];
            layerCollections[type].removeObject(id);
            allObjects[id] = null;
        }

        /// <summary>
        /// updates saved position of a certain object
        /// </summary>
        public void updatePosition(int id, float x,float y)
        {
            var type = typesMap[id];
            layerCollections[type].updatePosition(id, new Vector2(x, y));
            allObjects[id] = new ObjectData(id, x, y);
        }

        /// <summary>
        /// changes saved type of an object
        /// </summary>
        public void changeType(int id, int newtype)
        {
            var position = allObjects[id];
            removeObject(id);
            addObject(id, position.x, position.y, newtype);
        }

        /// <summary>
        /// finds closest hostile object to the object with certain id
        /// </summary>
        public FoundObject getClosestTarget(int id,float range)
        {
            int type = typesMap[id];
            var position = allObjects[id];
            int resultId = -1;
            float closest = float.MaxValue;
            for (int i = 0; i < typesCount; i++)
            {
                var distanceToLayer = layerCollections[i].findObject(new Vector2(position.x,position.y), range);
                if (distanceToLayer!=null && relationships[type,i] == Visibility.Visible&&distanceToLayer.distance<closest)
                {
                    closest = distanceToLayer.distance;
                    resultId = distanceToLayer.id;
                }
            }

            if (resultId == -1)
            {
                return null;
            }
            return new FoundObject(closest, resultId);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("areaWidth", areaWidth);
            info.AddValue("areaHeight", areaHeight);
            info.AddValue("typeCount", typesCount);
            info.AddValue("relationships", relationships);

            var typesMapKeys= new int[typesMap.Count];
            var typesMapValues = new int[typesMap.Count];
            typesMap.Values.CopyTo(typesMapValues,0);
            typesMap.Keys.CopyTo(typesMapKeys, 0);

            info.AddValue("typesMapKeys", typesMapKeys);
            info.AddValue("typesMapValues", typesMapValues);

            var allObjectsValues = new ObjectData[allObjects.Count];
            allObjects.Values.CopyTo(allObjectsValues, 0);
            info.AddValue("allObjectsValues", allObjectsValues);
        }

        protected ClosestVisibleObjectFinder(SerializationInfo info, StreamingContext context)
        {
            areaHeight = info.GetInt32("areaHeight");
            areaWidth = info.GetInt32("areaWidth");
            typesCount = info.GetInt32("typeCount");

            for (int i = 0; i < typesCount; i++)
            {
                layerCollections.Add(new ClosestObjectFinder(areaWidth, areaHeight));
            }

            relationships = (Visibility[,])info.GetValue("relationships",typeof(Visibility[,]));

            var typesMapKeys =(int[])info.GetValue("typesMapKeys",typeof(int[]));
            var typesMapValues = (int[])info.GetValue("typesMapValues", typeof(int[]));
            for (int i = 0; i < typesMapKeys.Length; i++)
            {
                typesMap[typesMapKeys[i]] = typesMapValues[i];
            }

            var allObjectValues = (ObjectData[]) info.GetValue("allObjectsValues", typeof(ObjectData[]));

            foreach (var position in allObjectValues)
            {
                addObject(position.id, position.x, position.y, typesMap[position.id]);
            }
        }
    }
}

