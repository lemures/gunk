﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

using Object = UnityEngine.Object;

namespace Gunk_Scripts
{
    public static class DatabasesManager
    {

        private enum DatabaseType
        {
            RawText,
            Prefab,
            Material,
            ScriptableObject,
            DatabaseNotFound,
            MultipleTypesFound
        }

        private const string OBJECTS_BACKUP_PATH = "Assets/DatabasesManager/Backups";
        private const string OBJECTS_NEWEST_VERSION_PATH= "Assets/DatabasesManager/Resources";

        private static  string getObjectBackupFullPath(string name, string categoryFolderName = "")
        {
            return OBJECTS_BACKUP_PATH + "/" + (categoryFolderName != "" ? categoryFolderName + "/" : "") + name;
        }

        private static string getObjectCurrentFullPath(string name, string categoryFolderName = "")
        {
            return getCurrentVersionCategoryDirectoryPath(categoryFolderName) + name;
        }

        private static string getCurrentVersionCategoryDirectoryPath(string categoryFolderName)
        {
            return OBJECTS_NEWEST_VERSION_PATH + (categoryFolderName != "" ? "/" + categoryFolderName : "") + "/";
        }

        private static string getCurrentVersionFilename(string name)
        {
            return name;
        }

        /// <summary>
        /// looks for database with certain name and returns its type or null if there is no database with that name
        /// </summary>
        private static DatabaseType getExistingDatabaseType(string name, string categoryFolderName)
        {
            var folderName = getObjectCurrentFullPath(name, categoryFolderName);
            var files = FileManager.getFilesInDir(folderName);
            if (files.Count == 0)
            {
                return DatabaseType.DatabaseNotFound;
            }

            if (files.Count != 1)
            {
                return DatabaseType.MultipleTypesFound;
            }
            var extension = FileManager.getFileExtension(files[0]);
            switch (extension)
            {
                case ".prefab":
                    return DatabaseType.Prefab;
                case ".txt":
                    return DatabaseType.RawText;
                case ".mat":
                    return DatabaseType.Material;
                default:
                    return DatabaseType.ScriptableObject;
            }
        }

        private static string getExistingDatabaseExtension(string name)
        {
            var folderName = getObjectCurrentFullPath(name);
            var files = FileManager.getFilesInDir(folderName);
            return FileManager.getFileExtension(files[0]);
        }

        /// <summary>
        /// Saves value of an object so it can be later accessed at any time
        /// </summary>
        public static void saveObject(object objectToSave, string name, string categoryFolderName = "")
        {
#if UNITY_EDITOR
            var currentDate = System.DateTime.Now;
            //So it will be both human readable and sortable
            var dateString = currentDate.Ticks + " " + currentDate.ToLongDateString();
            var fileBackupPath = getObjectBackupFullPath(name, categoryFolderName) + "/" + dateString+".txt";
            var fileCurrentPath = getObjectCurrentFullPath(name, categoryFolderName) + "/" + getCurrentVersionFilename(name) + ".txt";

            FileManager.createFolder(getObjectCurrentFullPath(name, categoryFolderName));
            FileManager.createFolder(getObjectBackupFullPath(name, categoryFolderName));

            if (File.Exists(fileCurrentPath))
            {
                File.Move(fileCurrentPath, fileBackupPath);
            }

            FileManager.binarySerializeToFile(fileCurrentPath,objectToSave);
#endif
        }

        /// <summary>
        /// generic function for saving assets, saves copy of an object as asset and creates backup from current version if it existed
        /// </summary>
        private static void saveObject(Object objectToSave, string name, string extension)
        {
            var clone = Object.Instantiate(objectToSave);
#if UNITY_EDITOR
            var currentDate = System.DateTime.Now;
            //So it will be both human readable and sortable
            var dateString = currentDate.Ticks + " " + currentDate.ToLongDateString();
            var fileBackupPath = getObjectBackupFullPath(name) + "/" + dateString + extension;
            var fileCurrentPath = getObjectCurrentFullPath(name) + "/" + getCurrentVersionFilename(name) + extension;

            FileManager.createFolder(getObjectCurrentFullPath(name));
            FileManager.createFolder(getObjectBackupFullPath(name));
            if (File.Exists(fileCurrentPath))
            {
                File.Move(fileCurrentPath,fileBackupPath);
            }

            if (clone is GameObject)
            {
                PrefabUtility.CreatePrefab(fileCurrentPath, clone as GameObject);
            }
            else
            {
                InternalEditorUtility.SaveToSerializedFileAndForget(new[] {clone}, fileCurrentPath, true);
                //AssetDatabase.CreateAsset(clone, fileCurrentPath);
                //EditorUtility.SetDirty(clone);
            }
#endif
        }

        public static List<string> getAllSavedObjectsInDirectory(string categoryFolderName = "")
        {
            var directory = getCurrentVersionCategoryDirectoryPath(categoryFolderName);
            var files = FileManager.getFoldersInDir(directory);

            var result = new List<string>();
            foreach (var file in files)
            {
                result.Add(FileManager.getFileName(file));
            }

            return result;
        }

        /// <summary>
        /// creates instance of ScriptableObject and saves it's state under certain name, returns saved copy
        /// </summary>
        public static void saveScriptableObject(ScriptableObject objectToSave, string name)
        {
            if (objectToSave.GetType().IsNested)
            {
                Debug.Log("Warning! you tried to save object of type " + objectToSave.GetType() +
                          " that is nested class. ScriptableObjects MUST NOT be nested");
            }

            saveObject(objectToSave, name, ".asset");
        }

        /// <summary>
        /// creates instance of material and saves it's state under certain name and returns it's saved copy
        /// </summary>
        public static void saveMaterial(Material materialToSave, string name)
        {
            saveObject(materialToSave, name, ".mat");
        }

        /// <summary>
        /// Serializes GameObject with all its unity references(Data needs to be wrapped up in Monobehaviours), unity GameObjects that are not prefabs will not be saved!
        /// </summary>
        public static void saveObjectWithUnityReferences(GameObject objectToSave, string name)
        {
            saveObject(objectToSave, name, ".prefab");
        }

        private static object getResourceFromLocation(string name, string filename, bool unitySerializationUsed, string categoryFolderName)
        {
            bool isUnityEditor = false;
#if UNITY_EDITOR
            isUnityEditor = true;
#endif
            if (unitySerializationUsed)
            {
                Object resultRaw = null;
#if UNITY_EDITOR
                if (getExistingDatabaseType(name, categoryFolderName) == DatabaseType.Prefab)
                {
                    resultRaw = Resources.Load((categoryFolderName == "" ? "" : (categoryFolderName + "\\")) + filename);
                }
                else
                {
                    resultRaw=InternalEditorUtility.LoadSerializedFileAndForget(Path.Combine(OBJECTS_NEWEST_VERSION_PATH + "/" + categoryFolderName, filename + getExistingDatabaseExtension(name)))[0];
                }
#else
                resultRaw = Resources.Load(filename);
#endif
                if (resultRaw == null)
                {
                    return null;
                }
                return resultRaw;
            }

            if (isUnityEditor)
            {
                return FileManager.binaryDeserializeFromFile(Path.Combine(OBJECTS_NEWEST_VERSION_PATH + "/" + categoryFolderName , filename+".txt"));
            }
            else
            {
                try
                {
                    var resultRaw = Resources.Load(filename);
                    IFormatter formatter = new BinaryFormatter();
                    var memoryStream = new MemoryStream(((TextAsset) resultRaw).bytes);
                    var result = formatter.Deserialize(memoryStream);
                    return result;
                }
                catch (System.Exception)
                {
                    return null;
                }
            }
        }

        private static bool usesUnitySerialization(DatabaseType type)
        {
            return type == DatabaseType.Material ||
                   type == DatabaseType.Prefab ||
                   type == DatabaseType.ScriptableObject;
        }

        /// <summary>
        /// Retrieves the most recently saved version of an object or null if nothing was saved so far
        /// </summary>
        public static object getObjectLatestVersion(string name, string categoryFolderName = "")
        {
            var unitySerializationUsed = usesUnitySerialization(getExistingDatabaseType(name, categoryFolderName));
            return getResourceFromLocation(name, name + "\\" + getCurrentVersionFilename(name), unitySerializationUsed, categoryFolderName);
        }

        private static void restoreLatestBackupVersion(string name, string categoryFolderName = "")
        {
#if UNITY_EDITOR
            var files = FileManager.getFilesInDir(getObjectBackupFullPath(name, categoryFolderName));
            files.Sort();

            if (files.Count == 0)
            {
                return;
            }

            var bytes = FileManager.getBytesFromFile(files[files.Count - 1]);
            var fileCurrentPath = getObjectCurrentFullPath(name, categoryFolderName) + "/" + getCurrentVersionFilename(name) + FileManager.getFileExtension(files[files.Count - 1]);
            FileManager.deleteFile(files[files.Count - 1]);
            FileManager.putBytesToFile(fileCurrentPath, bytes);
#endif
        }

        public static void moveObject(string name, string source, string destination)
        {
            var destinationCurrentDir = getObjectCurrentFullPath(name, destination);
            var sourceCurrentDir = getObjectCurrentFullPath(name, source);
            FileManager.createFolder(getCurrentVersionCategoryDirectoryPath(destination));
            FileManager.moveDirectory(sourceCurrentDir, destinationCurrentDir);
            removeEmptyCategoryFolder(source);
        }

        private static void removeEmptyCategoryFolder(string categoryFolderName)
        {
            if (getAllSavedObjectsInDirectory(categoryFolderName).Count == 0)
            {
                FileManager.deleteFolder(getCurrentVersionCategoryDirectoryPath(categoryFolderName), false);
            }
        }

        /// <summary>
        /// deletes saved version of an object with all backups
        /// </summary>
        public static void deleteAllVersions(string name, string categoryFolderName = "")
        {
            if (name == null || categoryFolderName == null)
            {
                return;
            }
#if UNITY_EDITOR
            foreach (var filename in FileManager.getFilesInDir(getObjectCurrentFullPath(name, categoryFolderName)))
            {
                FileManager.deleteFile(filename);
            }
            FileManager.deleteFolder(getObjectCurrentFullPath(name, categoryFolderName));
            FileManager.deleteFolder(getObjectBackupFullPath(name, categoryFolderName));
            removeEmptyCategoryFolder(categoryFolderName);
#endif
        }

        /// <summary>
        /// deletes Object last saved version or does nothing if nothing was saved so far
        /// </summary>
        public static void deleteLatestVersion(string name)
        {
#if UNITY_EDITOR
            var type = getExistingDatabaseType(name, "");
            switch (type)
            {
                case DatabaseType.DatabaseNotFound:
                    return;
                case DatabaseType.MultipleTypesFound:
                    Debug.Log("Warning! multiple databases with same name found for name: " + name);
                    return;
            }

            bool unitySerializationUsed = usesUnitySerialization(getExistingDatabaseType(name, ""));
            foreach (var filename in FileManager.getFilesInDir(getObjectCurrentFullPath(name)))
            {
                FileManager.deleteFile(filename);
            }

            var files=FileManager.getFilesInDir(getObjectBackupFullPath(name));
            files.Sort();

            if (files.Count == 0)
            {
                return;
            }

            restoreLatestBackupVersion(name);
#endif
        }
    }
}
