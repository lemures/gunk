﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace Gunk_Scripts
{
    public class DatabasesManagerEditorGuiCustomEditor : Editor
    {
        [MenuItem("Assets/Save Object in Databases Manager")]
        private static void saveObject()
        {
            if (Selection.activeObject is Material)
            {
                DatabasesManager.saveMaterial((Material)Selection.activeObject, Selection.activeObject.name);
                return;
            }
            DatabasesManager.saveObjectWithUnityReferences((GameObject)Selection.activeObject, Selection.activeObject.name);
        }

        [MenuItem("Assets/Save Object in Databases Manager", true)]
        private static bool optionValidation()
        {
            return (Selection.activeObject is GameObject || Selection.activeObject is Material) && DatabasesManager.getObjectLatestVersion(Selection.activeObject.name)==null;
        }
    }
}
#endif