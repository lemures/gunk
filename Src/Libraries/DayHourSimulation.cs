using System;
using System.Collections.Generic;

namespace Src.Libraries
{
    public class DayHourSimulation
    {
        /// <summary>
        /// construct simulation that starts at the beginning of the day 0
        /// </summary>
        public DayHourSimulation(float dayHour, float nightHour, float dayLength, float nightLength, float transitionLength)
        {
            partsOfTheDay.Add(new PartOfDay(dayLength,dayHour,dayHour));
            partsOfTheDay.Add(new PartOfDay(transitionLength,dayHour,nightHour));
            partsOfTheDay.Add(new PartOfDay(nightLength,nightHour,nightHour));
            partsOfTheDay.Add(new PartOfDay(transitionLength,nightHour,dayHour));
        }

        private class PartOfDay
        {
            private float length;
            private float startHour;
            private float endHour;
            private float passedSoFar;

            public PartOfDay(float length, float startHour, float endHour)
            {
                this.length = length;
                this.startHour = startHour;
                this.endHour = endHour;
            }

            //simulates time passing and returns how much time passed(time flow stops at the end of the Part)
            public float elapseTime(float time)
            {
                var res = Math.Min(length - passedSoFar, time);
                passedSoFar += res;
                return res;
            }

            public bool isFinished() => Math.Abs(length - passedSoFar) < 0.01f;

            public float getCurrentHour()
            {
                var duration = endHour - startHour;
                if (duration < 0)
                {
                    duration += 24.0f;
                }

                return (startHour + duration * (passedSoFar / length)) % 24.0f;
            }

            public void reset()
            {
                passedSoFar = 0;
            }
        }

        private List<PartOfDay> partsOfTheDay = new List<PartOfDay>();
        private int currentPartIndex;

        public float getHour()
        {
            return partsOfTheDay[currentPartIndex].getCurrentHour();
        }

        public void update(float deltaTime)
        {
            while (deltaTime > 0.00001f)
            {
                float passed = partsOfTheDay[currentPartIndex].elapseTime(deltaTime);
                deltaTime -= passed;
                if (partsOfTheDay[currentPartIndex].isFinished())
                {
                    partsOfTheDay[currentPartIndex].reset();
                    currentPartIndex++;
                    currentPartIndex %= partsOfTheDay.Count;
                }
            }
        }
    }
}