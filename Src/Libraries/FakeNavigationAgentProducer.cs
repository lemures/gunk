using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Renderers;
using Src.Libraries;

namespace Gunk_Scripts
{
    public class FakeNavigationAgentProducer : INavigationAgentProducer
    {
        private readonly GameObjectManager gameObjectManager;

        public INavigationAgent createAgent(int gameObjectId)
        {
            return new FakeNavigationAgent(gameObjectManager.getPosition(gameObjectId));
        }

        public void destroyAgent(int gameObjectId)
        {
            //nothing really to do here
        }

        public FakeNavigationAgentProducer(GameObjectManager gameObjectManager)
        {
            this.gameObjectManager = gameObjectManager;
        }
    }
}