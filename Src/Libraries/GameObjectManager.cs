﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GunkComponents;
using UnityEngine;
using UnityEngine.AI;
using Object = UnityEngine.Object;

namespace Src.Libraries
{
    public class GameObjectManager: IRenderersParametersManager
    {
        private IdManager idManager=new IdManager();
        private Dictionary<int, GameObject> allObjects=new Dictionary<int, GameObject>();
        private Dictionary<int, MaterialPropertyBlock> materialBlocks = new Dictionary<int, MaterialPropertyBlock>();

        private IPrefabManager prefabManager;
        public GameObjectManager(IPrefabManager prefabManager)
        {
            this.prefabManager = prefabManager;
        }

        private int instantiateGameObject(GameObject gameObject, Vector3 position, Quaternion rotation)
        {
            int myId = idManager.getFreeId();
            var spawnedObject = Object.Instantiate(gameObject, position, rotation);
            spawnedObject.name += "   "+myId;
            allObjects[myId] = spawnedObject;
            return myId;
        }

        public int instantiatePrefab(string prefabName, Vector3 position, Quaternion rotation)
        {
            var foundPrefab=prefabManager.findPrefab(prefabName);
            if (foundPrefab == null)
            {
                throw new InvalidOperationException("prefab named" + prefabName + " does not exist");
            }
            return instantiateGameObject(foundPrefab.go,position,rotation);
        }

        public void destroyObject(int id)
        {
            if (Application.isPlaying)
            {
                Object.DestroyObject(allObjects[id]);
            }
            else
            {
                Object.DestroyImmediate(allObjects[id]);
            }

            allObjects.Remove(id);
        }

        public void destroyAllObjects()
        {
            foreach (var entry in allObjects.ToList())
            {
                destroyObject(entry.Key);
            }
            allObjects.Clear();
            idManager.deregisterAllIds();
        }

        private GameObject getObjectById(int id)
        {
            if (!allObjects.ContainsKey(id))
            {
                throw new InvalidOperationException("object with id " + id + " does not exist!");
            }
            return allObjects[id];
        }

        public void setFloatParameter(int id, string propertyName, float newValue)
        {
            var go = getObjectById(id);
            var renderers = go.GetComponentsInChildren<MeshRenderer>();
            foreach (var renderer in renderers)
            {
                var propertyBlock = new MaterialPropertyBlock();
                renderer.GetPropertyBlock(propertyBlock);
                propertyBlock.SetFloat(propertyName, newValue);
                renderer.SetPropertyBlock(propertyBlock);
            }
        }

        public void setIntParameter(int id, string propertyName, int newValue)
        {
            setFloatParameter(id, propertyName, newValue);
        }

        public IGunkTransform createTransformComponent(IGunkObject gunkObject,int id)
        {
            var result=new GunkTransform(gunkObject, this, id);
            return result;
        }

        public INavigationAgent createNavmeshComponent(int id)
        {
            var go = getObjectById(id);
            var agent = go.GetComponent<NavMeshAgent>();
            if (agent == null)
            {
                agent=go.AddComponent<NavMeshAgent>();
            }

            return new NavmeshAgentAdapter(agent);
        }

        public void removeNavmeshComponent(int id)
        {
            var go = getObjectById(id);
            var agent = go.GetComponent<NavMeshAgent>();
            Object.Destroy(agent);
        }

        public Vector3 getPosition(int id) => getObjectById(id).transform.position;

        public void setPosition(int id, Vector3 newPosition)
        {
            getObjectById(id).transform.position = newPosition;
        }

        public Quaternion getRotation(int id) => getObjectById(id).transform.rotation;

        public void setRotation(int id, Quaternion newRotation)
        {
            getObjectById(id).transform.rotation = newRotation;
        }

        public T addComponent<T>(int id) where T : MonoBehaviour
        {
            return getObjectById(id).AddComponent<T>();
        }
    }
}
