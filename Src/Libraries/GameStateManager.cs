﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.GunkObjects;
using Gunk_Scripts.GUI;
using Gunk_Scripts.Renderers;
using Gunk_Scripts.SkeletonAnimations;
using Src.Libraries;
using Src.SkeletonAnimations.Internal;
using UnityEngine;

namespace Gunk_Scripts
{
    public class GameStateManager:MonoBehaviour
    {
        public abstract class GameState
        {
            protected GameStateManager manager { get; }

            protected GameState(GameStateManager manager)
            {
                this.manager = manager;
            }

            public abstract void setAsActive();

            public abstract void deactivate();

            public abstract void update();

            protected void drawDragRectangle()
            {
                var selectedRectangle = MouseUtils.instance.getDragRectangle();
                if (selectedRectangle!=null)
                    DragRectangleRenderer.drawDragRectangle(selectedRectangle.firstPosition,selectedRectangle.secondPosition);
            }

            protected void exitToIdleIfEscapePressed()
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    manager.setStateToIdle();
                }
            }

            protected void changeStateIfAnythingSelected()
            {
                if (manager.gunkObjectManager.isAnythingSelected())
                {
                    manager. changeState(new ObjectsSelected(manager));
                }
            }

            public void enableSelectingObjects()
            {
                manager.gunkObjectManager.enableSelectingObjects();
            }

            public void disableSelectingObjects()
            {
                manager.gunkObjectManager.disableSelectingObjects();
            }

            private GuiElement constructionMenu;

            protected void createConstructionMenu()
            {
                var options = ((GunkObjectType[])Enum.GetValues(typeof(GunkObjectType))).ToList();
                constructionMenu = new GuiConstructionMenu(4,options, GlobalPrefabsManager.getInstance(), manager);
            }

            protected void disposeConstructionMenu()
            {
                constructionMenu?.dispose();
            }

            protected void exitToIdleIfNothingSelected()
            {
                if (!manager.gunkObjectManager.isAnythingSelected())
                {
                    manager.setStateToIdle();
                }
            }
        }

        public class ObjectsSelected : GameState
        {
            public override void setAsActive()
            {
                enableSelectingObjects();
            }

            public override void deactivate()
            {
                disableSelectingObjects();
            }

            private void goBackToIdleIfNothingSelected()
            {
                if (!manager.gunkObjectManager.isAnythingSelected())
                {
                    manager.setStateToIdle();
                }
            }

            public override void update()
            {
                exitToIdleIfEscapePressed();
                exitToIdleIfNothingSelected();
                drawDragRectangle();
            }

            public ObjectsSelected(GameStateManager manager) : base(manager)
            {
            }
        }

        public class Idle :GameState
        {
            public override void setAsActive()
            {
                createConstructionMenu();
                enableSelectingObjects();
            }

            public override void deactivate()
            {
                disposeConstructionMenu();
                disableSelectingObjects();
            }

            public override void update()
            {
                drawDragRectangle();
                exitToIdleIfEscapePressed();
                changeStateIfAnythingSelected();
            }

            public Idle(GameStateManager manager) : base(manager)
            {
            }
        }

        public class BuildingMode : GameState
        {
            public override void setAsActive()
            {
                createConstructionMenu();
            }

            private readonly GameObjectManager temporaryObjectsManager = new GameObjectManager(GlobalPrefabsManager.getInstance());

            private GunkObjectType? type;
            private readonly GunkObjectManager gunkObjectManager;

            private void setBuiltObject(GunkObjectType objectType)
            {
                type = objectType;
            }

            public override void deactivate()
            {
                temporaryObjectsManager.destroyAllObjects();
                disposeConstructionMenu();
                gunkObjectManager.clearTemporaryConnections();
            }

            public override void update()
            {
                if (type == null)
                {
                    return;
                }
                const Faction faction = Faction.MainPlayer;
                Vector3 terrainPosition = MouseUtils.instance.getTerrainMousePos();

                var prefabName = GunkPrefabNames.getName(type.Value);
                if (!gunkObjectManager.canPlaceObject(terrainPosition, prefabName))
                terrainPosition = gunkObjectManager.getClosestFreePosition(terrainPosition, prefabName) ?? terrainPosition;

                var socketBones = new Skeleton(prefabName).getAllSocketBones();

                temporaryObjectsManager.destroyAllObjects();
                temporaryObjectsManager.instantiatePrefab(prefabName, terrainPosition, Quaternion.identity);

                gunkObjectManager.clearTemporaryConnections();

                if (gunkObjectManager.canConnectBuilding(terrainPosition, faction) &&
                    gunkObjectManager.canPlaceObject(terrainPosition, prefabName))
                {
                    gunkObjectManager.temporarilyConnectBuilding(socketBones, terrainPosition, faction);
                }

                if (MouseUtils.instance.wasClicked() &&
                    gunkObjectManager.canConnectBuilding(terrainPosition, faction) &&
                    gunkObjectManager.canPlaceObject(terrainPosition, prefabName)
                    )
                {
                    GunkObjectFactory.spawnObject(gunkObjectManager, type.Value, terrainPosition, faction);
                }
                exitToIdleIfEscapePressed();
            }

            public BuildingMode(GameStateManager manager, GunkObjectManager gunkObjectManager, GunkObjectType type) : base(manager)
            {
                this.gunkObjectManager = gunkObjectManager;
                setBuiltObject(type);
            }
        }

        public class Paused : GameState
        {
            public override void setAsActive()
            {
                TimeManager.instance.freezeTime();
            }

            public override void deactivate()
            {
                TimeManager.instance.unfreezeTime();
            }

            public override void update()
            {
            }

            public Paused(GameStateManager manager) : base(manager)
            {
            }
        }

        private GameState currentState;

        public void changeState(GameState newState)
        {
            if (newState == null)
            {
                throw new NullReferenceException(nameof(newState));
            }
            currentState?.deactivate();
            currentState = newState;
            currentState.setAsActive();
        }

        private GunkObjectManager gunkObjectManager;

        private GameStateManager()
        {
        }

        public static GameStateManager createInstance (GunkObjectManager gunkObjectManager)
        {
            GameObject gameObject = new GameObject();
            var comp=gameObject.AddComponent<GameStateManager>();
            comp.gunkObjectManager = gunkObjectManager;
            gameObject.name = "Game State Manager" + comp.GetHashCode();
            comp.changeState(new Idle(comp));
            return comp;
        }

        public void setStateToBuildingMode(GunkObjectType type)
        {
            changeState(new BuildingMode(this,  gunkObjectManager, type));
        }

        public void setStateToIdle()
        {
            changeState(new Idle(this));
        }

        public void Update()
        {
            currentState.update();
        }
    }
}