﻿using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.SkeletonAnimations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Gunk_Scripts
{
    [System.Serializable]
    public class GlobalPrefabsManager : IPrefabManager
    {
        public static string needFbxRotationVariableName="needsFbxFix";

        private List<Prefab> allPrefabsInternal;

        private static string prefabCollectionName = "prefab collection";

        private static readonly GlobalPrefabsManager Singleton=new GlobalPrefabsManager();
        public static GlobalPrefabsManager getInstance() => Singleton;

        private GlobalPrefabsManager()
        {
            loadDataFromDatabasesManager();
        }

        private List<Prefab> allPrefabs
        {
            get
            {
                if (allPrefabsInternal == null)
                {
                    loadDataFromDatabasesManager();
                }

                return allPrefabsInternal;
            }
        }

        private void loadDataFromDatabasesManager()
        {
            var loadedDatabase = (PrefabManagerHelper)DatabasesManager.getObjectLatestVersion(prefabCollectionName);

            if (loadedDatabase != null)
            {
                allPrefabsInternal = loadedDatabase.allPrefabs;
            }
            else
            {
                allPrefabsInternal = new List<Prefab>();
                saveDatabases();
            }

            foreach (var prefab in allPrefabs)
            {
                if (prefab.go == null)
                {
                    Debug.Log("Warning! prefab " + prefab.name + " has been destroyed outside the PrefabManager");
                    continue;
                }

                if (PrefabVariablesManager.hasVariable(prefab.name, needFbxRotationVariableName) &&
                    (bool) PrefabVariablesManager.getVariable(prefab.name, needFbxRotationVariableName))
                {
                    (from filter in prefab.go.GetComponentsInChildren<MeshFilter>() select filter.sharedMesh).
                        ToList().ForEach(MeshFbxUtils.fixMeshRotation);
                }

            }

            Debug.Log("Prefab Manager loaded with " + allPrefabs.Count + " prefabs");
        }

        public IEnumerable<Object> getAllPrefabsUnityReferences()
        {
            var result = new List<Object>();
            foreach (var gameObject in (from prefab in allPrefabs where prefab.go != null select prefab.go))
            {
                result.AddRange(getAllObjectUnityReferences(gameObject));
            }

            return result;
        }

        private List<Object> getAllObjectUnityReferences(GameObject go)
        {
            var allResources = new List<Object>();
            go.GetComponentsInChildren<MeshFilter>().ToList().ForEach(x=>allResources.Add(x.sharedMesh));
            foreach (var renderer in go.GetComponentsInChildren<MeshRenderer>())
            {
                renderer.sharedMaterials.ToList().ForEach(allResources.Add);
            }

            return allResources;
        }

        private void addPrefab(Prefab prefab)
        {
            if (prefab.go != null)
            {
                getAllObjectUnityReferences(prefab.go);
            }
            foreach (var akt in allPrefabs)
            {
                if (akt.name.ToLower() == prefab.name.ToLower())
                {
                    akt.meshSkeleton = prefab.meshSkeleton;
                    akt.go = prefab.go;
                }
            }
            allPrefabs.Add(prefab);
        }

        /// <summary>
        /// Creates prefab based on prefab name
        /// </summary>
        public void addPrefab(GameObject prefab)
        {
            addPrefab(new Prefab(prefab));
        }

        public void setSkeletonMesh(string prefabName, Mesh meshSkeleton)
        {
            findPrefab(prefabName).meshSkeleton = meshSkeleton;
        }

        public void setMiniature(string prefabName, Texture2D miniature)
        {
            findPrefab(prefabName).miniature = miniature;
        }

        public void deletePrefab(string prefabName)
        {
            Prefab prefabToDelete = null;
            foreach (var akt in allPrefabs)
            {
                if (akt.go != null && akt.name == prefabName)
                {
                    prefabToDelete = akt;
                }
            }

            allPrefabs.Remove(prefabToDelete);
        }

        public Prefab findPrefab(string prefabName)
        {
            var prefab = allPrefabs.Find(x => x.name.ToLower() == prefabName.ToLower());
            if (prefab==null)
                Debug.Log("prefab " + prefabName + " not found");
            return prefab;
        }

        public List<string> getAllPrefabNames()
        {
            var result = new List<string>();
            allPrefabs.ForEach(x => result.Add(x.name));
            return result;
        }

        /// <summary>
        /// saves all changes to disk
        /// </summary>
        public void saveDatabases()
        {
            var dataObject = ScriptableObject.CreateInstance<PrefabManagerHelper>();
            dataObject.allPrefabs = allPrefabs;
            DatabasesManager.saveScriptableObject(dataObject, prefabCollectionName);
        }
    }
}
