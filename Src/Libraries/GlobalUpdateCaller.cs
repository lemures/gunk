using System;
using System.Collections.Generic;
using UnityEngine;

namespace Src.Libraries
{
    public class GlobalUpdateCaller : MonoBehaviour
    {
        private static GlobalUpdateCaller invoker;
        private readonly List<Action> actions = new List<Action>();

        static GlobalUpdateCaller()
        {
            var go = new GameObject {hideFlags = HideFlags.HideInHierarchy};
            invoker = go.AddComponent<GlobalUpdateCaller>();
        }

        public static void addInvokeEveryFrameFunction(Action action)
        {
            invoker.actions.Add(action);
        }

        private void Update()
        {
            actions.ForEach(x => x.Invoke());
        }
    }
}