﻿using Src.Console;
using UnityEngine;
using Used_Assets.PostProcessing.Runtime;

namespace Gunk_Scripts
{
    [ExecutesCommand("grass")]
    [System.Serializable]
    public static class GraphicsSettingsManager
    {
        public static string grass(string[]arguments)
        {
            switch (arguments[1].ToLower())
            {
                case "off":
                    currentSettings.grassMode = GrassQuality.Off;
                    return "set grass mode to OFF";
                case "high":
                    currentSettings.grassMode = GrassQuality.High;
                    return "set grass mode to HIGH";
                case "low":
                    currentSettings.grassMode = GrassQuality.Low;
                    return "set grass mode to LOW";
                default:
                    return "arguments in wrong format, can only be off, low or high";
            }
        }

        public enum GrassQuality : byte
        {
            Off,
            Low,
            High
        }

        public class Settings
        {
            public string name = "custom";
            public bool ambientOcclusionEnabled = true;
            public bool bloomEnabled = true;
            public bool motionBlurEnabled = true;
            public bool antiAliasingEnabled = true;
            public bool forOfWarEnabled = true;
            public bool debugColliders = false;

            public GrassQuality grassMode = GrassQuality.High;
            public ShadowQuality shadowQuality = ShadowQuality.All;
            public ShadowResolution shadowResolution = ShadowResolution.VeryHigh;
            public float shadowDistance = 200;
        }

        [HideInInspector] public static Settings currentSettings = new Settings();

        public static Settings low;
        public static Settings medium;
        public static Settings high;

        private static PostProcessingProfile _myProfile;

        public static void changeEffects()
        {
            if (_myProfile == null)
            {
                _myProfile = GameObject.Find("Main Camera").GetComponent<PostProcessingBehaviour>().profile;
            }
            _myProfile.ambientOcclusion.enabled = currentSettings.ambientOcclusionEnabled;
            _myProfile.bloom.enabled = currentSettings.bloomEnabled;
            _myProfile.antialiasing.enabled = currentSettings.antiAliasingEnabled;
            _myProfile.motionBlur.enabled = currentSettings.motionBlurEnabled;
            QualitySettings.shadowResolution = currentSettings.shadowResolution;
            QualitySettings.shadows = currentSettings.shadowQuality;
            QualitySettings.shadowDistance = currentSettings.shadowDistance;
        }
        public static void loadSavedSettings()
        {
            var path = Application.dataPath + "/settings";
            var json = FileManager.getStringFromFile(path);
            if (json.Length != 0)
            {
                currentSettings = JsonUtility.FromJson<Settings>(json);
            }
        }
        public static void saveSettings()
        {
            var path = Application.dataPath + "/settings";
            var json = JsonUtility.ToJson(currentSettings);
            FileManager.putStringToFile(json, path);
        }
    }
}