namespace Src.Libraries
{
    public interface IRenderersParametersManager
    {
        void setFloatParameter(int id, string propertyName, float newValue);

        void setIntParameter(int id, string propertyName, int newValue);
    }
}