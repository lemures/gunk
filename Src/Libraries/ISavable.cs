﻿namespace Gunk_Scripts
{
    public interface ISavable
    {
        byte[] getAllData();

        void loadAllData(byte[] data);
    }
}
