using UnityEngine;

namespace Gunk_Scripts
{
    public interface IScreenPositionCalculator
    {
        Vector3 worldToScreenPoint(Vector3 worldPosition);
    }
}