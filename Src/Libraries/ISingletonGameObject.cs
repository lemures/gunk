﻿using UnityEngine;

namespace Gunk_Scripts
{
    public interface ISingletonGameObject
    {
        GameObject getObject();

        void spawnObject();

        void destroyObject();
    }
}
