﻿namespace Gunk_Scripts
{
    public interface ITimeManager
    {
        float time { get; }
        float deltaTime { get; }
        void freezeTime();
        void unfreezeTime();
        void reset();
    }
}