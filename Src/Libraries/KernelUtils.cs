﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.SerializationHelpers;
using UnityEngine;
using UnityEngine.Rendering;

namespace Gunk_Scripts
{
    [Serializable]
    public static class KernelUtils
    {
        static KernelUtils()
        {
            reset();
        }

        public static void reset()
        {
            runtimeResources = new RuntimeResources();
            databaseResources = (KernelUtilsHelper)DatabasesManager.getObjectLatestVersion(databaseResourcesName);
            if (databaseResources == null)
            {
                databaseResources = ScriptableObject.CreateInstance<KernelUtilsHelper>();
                DatabasesManager.saveScriptableObject(databaseResources, databaseResourcesName);
            }

            _buffers = new List<SerializableComputeBuffer>();
            processShaderFiles();
        }

        private class SerializableComputeBuffer
        {
            public string name;
            public ComputeBuffer buffer;
            public readonly ComputeBufferType type;
            public readonly bool needsSerialization;
            public readonly byte[] rawData;
            public SerializableComputeBuffer(string name,ComputeBuffer myBuffer, ComputeBufferType myType, bool myNeeds, byte[] myRawData)
            {
                this.name = name;
                buffer = myBuffer;
                needsSerialization = myNeeds;
                rawData = myRawData;
                type = myType;
            }
        }

        [Serializable]
        private class SerializedComputeBuffer
        {
            public byte[] rawData;
            public ComputeBufferType type;
            public int stride;
            public uint counterValue;
            public string name;
        }

        private static List<SerializableComputeBuffer> _buffers;
        private static bool _profilingEnabled=false;

        [Serializable]
        private class RuntimeResources
        {
            public readonly List<ShaderProperty> uniforms = new List<ShaderProperty>();
            public readonly List<SerializedComputeBuffer> buffers=new List<SerializedComputeBuffer>();
        }

        private static RuntimeResources runtimeResources;
        private static KernelUtilsHelper databaseResources;

        private static string databaseResourcesName = "kernelUtilsDatabaseResources";

        private static readonly float[] _tempFloatsVector = new float[4];
        private static readonly float[] _tempFloatsMatrix = new float[16];

        private static void addMissingUniform(ShaderProperty toReplace)
        {
            var foundField = (runtimeResources.uniforms.Find(akt => akt.name == toReplace.name));
            if (foundField == null)
            {
                runtimeResources.uniforms.Add(toReplace);
            }
            else
            {
                foundField.val = toReplace.val;
            }
        }

        public static void setFloat(string name, float val)
        {
            foreach (var shader in databaseResources.computeAssets)
            {
                shader.SetFloat(name, val);
            }
            Shader.SetGlobalFloat(name, val);
            addMissingUniform(new ShaderProperty(val, name));
        }

        public static void setInt(string name, int val)
        {
            foreach (var shader in databaseResources.computeAssets)
            {
                shader.SetInt(name, val);
            }
            Shader.SetGlobalInt(name, val);
            addMissingUniform(new ShaderProperty(val, name));
        }

        public static void setVector(string name, Vector4 val)
        {
            _tempFloatsVector[0] = val.x;
            _tempFloatsVector[1] = val.y;
            _tempFloatsVector[2] = val.z;
            _tempFloatsVector[3] = val.w;
            Shader.SetGlobalVector(name, val);
            foreach (var shader in databaseResources.computeAssets)
            {
                shader.SetVector(name, val);
            }
            addMissingUniform(new ShaderProperty(val, name));
        }

        public static void setMatrix(string name, Matrix4x4 val)
        {
            for (var i = 0; i < 16; i++)
            {
                _tempFloatsMatrix[i] = val[i];
            }
            Shader.SetGlobalMatrix(name, val);
            foreach (var shader in databaseResources.computeAssets)
            {
                shader.SetFloats(name, _tempFloatsMatrix);
            }
            addMissingUniform(new ShaderProperty(val, name));
        }

        private static bool isKernelDeclaration(string[] pom)
        {
            return (pom.Length == 3 && pom[1] == "kernel");
        }

        private static string myName = "Kernel Utilities";

        private static void processShaderFiles()
        {
#if UNITY_EDITOR
            databaseResources.kernels.Clear();
            databaseResources.computeAssets.Clear();
            databaseResources.computeShadersFilePaths.Clear();
            var myShaders = AssetUtils.findAllAssetsOfTypeWithPaths(typeof(ComputeShader),"Assets/Gunk_Shaders");
            myShaders.AddRange(AssetUtils.findAllAssetsOfTypeWithPaths(typeof(ComputeShader), "Assets/Src"));
            foreach (var myShader in myShaders)
            {
                databaseResources.computeShadersFilePaths.Add(myShader.Key);
                databaseResources.computeAssets.Add((ComputeShader)myShader.Value);
            }

            for (var i = 0; i < databaseResources.computeShadersFilePaths.Count; i++)
            {
                var path = databaseResources.computeShadersFilePaths[i];
                var asset = databaseResources.computeAssets[i];
                var lines = ScriptFileCacher.getFileLines(path);
                foreach (var pomSplit in lines)
                {
                    if (isKernelDeclaration(pomSplit))
                    {
                        var name = pomSplit[2];
                        var num = asset.FindKernel(name);
                        var myKernel = new KernelUtilsHelper.ShaderKernel(num, name, asset);
                        databaseResources.kernels.Add(myKernel);
                    }
                }
            }

            ScriptFileCacher.saveDatabase();
            DatabasesManager.saveScriptableObject(databaseResources, myName);
#endif
        }

        public static void releaseBuffers()
        {
            foreach (var pom in _buffers)
            {
                pom.buffer.Release();
            }
        }

        public static void printAllSetUniforms()
        {
            foreach (var uniform in runtimeResources.uniforms)
            {
                Debug.Log(uniform.name + "   " + uniform.ToString());
            }
        }

        public class WrongKernelName : Exception
        {
            public WrongKernelName(string message) : base(message) { }
        }

        public class WrongBufferName : Exception
        {
            public WrongBufferName(string message) : base(message) { }
        }

        public static void setGlobalTexture(string name, Texture tex)
        {
            Shader.SetGlobalTexture(name, tex);
            foreach (var shaderKernel in databaseResources.kernels)
            {
                shaderKernel.shader.SetTextureFromGlobal(shaderKernel.id, name, name);
            }
        }

        private static void installBuffer(ComputeBuffer buffer, string name)
        {
            Shader.SetGlobalBuffer(name, buffer);
            foreach (var shaderKernel in databaseResources.kernels)
            {
                shaderKernel.shader.SetBuffer(shaderKernel.id, name, buffer);
            }
        }

        private static KernelUtilsHelper.ShaderKernel getKernel(string name)
        {
            return (databaseResources.kernels.Find(akt => akt.name == name));
        }

        public static void enableProfiling()
        {
            _profilingEnabled = true;
        }

        public static void disableProfiling()
        {
            _profilingEnabled = false;
        }

        public static void dispatch(string name, int x = 1, int y = 1, int z = 1)
        {
            UnityEngine.Profiling.Profiler.BeginSample("Dispatching " + name);
            var kernel = getKernel(name);
            if (kernel == null)
            { throw new WrongKernelName(name); }
            if (_profilingEnabled)
            {
                var pom = new CommandBuffer();
                pom.BeginSample(name);
                pom.DispatchCompute(kernel.shader, kernel.id, x, y, z);
                pom.EndSample(name);
                Graphics.ExecuteCommandBuffer(pom);
                pom.Dispose();
            }
            else
            {
                kernel.shader.Dispatch(kernel.id, x, y, z);
            }
            UnityEngine.Profiling.Profiler.EndSample();
        }

        public static bool hasBuffer(string name)
        {
            try
            {
                getBuffer(name);
            }
            catch (WrongBufferName)
            {
                return false;
            }
            return true;
        }

        public static ComputeBuffer getBuffer(string name)
        {
            var res = (_buffers.Find(akt => akt.name == name));
            if (res == null)
            { throw new WrongBufferName(name); }
            return res.buffer;
        }

        public static void setBufferData(string name,Array data)
        {
            if (!hasBuffer(name))
            { throw new WrongBufferName(name); }
            var buffer = getBuffer(name);
            if (buffer.count < data.Length)
            {
                resizeBuffer(name, data.Length);
            }

            buffer = getBuffer(name);
            buffer.SetData(data);
        }

        public static void getDataFromBuffer(string name, Array data)
        {
            if (!hasBuffer(name))
            { throw new WrongBufferName(name); }
            var buffer = getBuffer(name);
            buffer.GetData(data);
        }

        public static void setCounterValue(string name, uint counterValue)
        {
            var buffer = getBuffer(name);
            buffer.SetCounterValue(counterValue);
        }

        public static void checkBufferIntergity()
        {
            foreach (var buffer in _buffers)
            {
                if (buffer.buffer==null)
                {
                    Debug.Log("Warning! ComputeBuffer named " + buffer + " was disposed!");
                }
            }
        }

        /// <summary>
        /// changes size of buffer, WARNING! erases all buffer contents
        /// </summary>
        public static void resizeBuffer(string name, int newBufferSize)
        {
            Debug.Log("buffer " + name + " was resized, new size is: "+newBufferSize);
            int stride = getBuffer(name).stride;
            var internalBuffer = _buffers.Find(x => x.name == name);
            _buffers.Remove(internalBuffer);
            initializeEmptyBuffer(name, newBufferSize, stride, internalBuffer.needsSerialization, internalBuffer.type);
        }

        public static void initializeEmptyBuffer(string name, int bufferSize, int stride, bool needsSerialization, ComputeBufferType bufferType = ComputeBufferType.Default)
        {
            var buffer = new ComputeBuffer(bufferSize, stride, bufferType);
            var helperBuffer = new byte[bufferSize * stride];
            for (var i = 0; i < helperBuffer.Length; i++)
            { helperBuffer[i] = 0; }
            buffer.SetData(helperBuffer);
            installBuffer(buffer, name);
            foreach (var t in _buffers)
            {
                if (t.name == name)
                {
                    _buffers.Remove(t);
                    break;
                }
            }
            _buffers.Add( new SerializableComputeBuffer(name, buffer, bufferType, needsSerialization, helperBuffer));
        }

        //some buffers serve as garbage collectors and require special initialization
        public static void populateIDsBuffer(ComputeBuffer buffer, int count, int howManyFirstEmpty)
        {
            var data = new int[buffer.count];
            for (var i = howManyFirstEmpty; i < count; i++)
            {
                data[i] = i;
            }
            //where pseudo-counter value is kept
            data[0] = count - 1;
            buffer.SetData(data);
            buffer.SetCounterValue((uint)buffer.count);
        }

        public static uint getCounterValue(string bufferName)
        {
            var temp = new ComputeBuffer(10, 4, ComputeBufferType.IndirectArguments);
            ComputeBuffer.CopyCount(getBuffer(bufferName), temp, 0);
            var tempData = new uint[1];
            temp.GetData(tempData);
            return tempData[0];
        }

        public static byte[] getRuntimeResourcesCopy()
        {
            runtimeResources.buffers.Clear();
            foreach (var akt in _buffers)
            {
                if (akt.needsSerialization != true) continue;

                var newSerialized = new SerializedComputeBuffer();
                akt.buffer.GetData(akt.rawData);
                //new_serialized.raw_data = akt.Value.raw_data;
                if (akt.type == ComputeBufferType.Counter || akt.type == ComputeBufferType.Append)
                {
                    newSerialized.counterValue = getCounterValue(akt.name);
                }
                newSerialized.rawData = (byte[])akt.rawData.Clone();
                newSerialized.name = akt.name;
                newSerialized.type = akt.type;
                newSerialized.stride = akt.buffer.stride;
                runtimeResources.buffers.Add(newSerialized);
            }

            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream();
            formatter.Serialize(memoryStream,runtimeResources);
            return memoryStream.GetBuffer();
        }

        public static void loadRuntimeResources(byte[] data)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream(data);
            var helper = new RuntimeResources();
            helper=(RuntimeResources)formatter.Deserialize(memoryStream);

            foreach (var akt in helper.uniforms)
            {
                switch (akt.type)
                {
                    case (ShaderProperty.FieldType.IntType):
                    {
                        setInt(akt.name, (int)(akt.val.m00));
                    }
                        break;
                    case (ShaderProperty.FieldType.FloatType):
                    {
                        setFloat(akt.name, akt.val.m00);
                    }
                        break;
                    case (ShaderProperty.FieldType.Vector4Type):
                    {
                        setVector(akt.name, akt.val.GetRow(0));
                    }
                        break;
                    case (ShaderProperty.FieldType.Matrix4X4Type):
                    {
                        setMatrix(akt.name, akt.val);
                    }
                        break;
                }
            }

            foreach (var akt in helper.buffers)
            {
                if (hasBuffer(akt.name))
                {
                    getBuffer(akt.name).Release();
                }
                initializeEmptyBuffer(akt.name, akt.rawData.Length, akt.stride, true, akt.type);
                if (akt.type == ComputeBufferType.Counter || akt.type == ComputeBufferType.Append)
                {
                    setCounterValue(akt.name, akt.counterValue);
                }
                setBufferData(akt.name,akt.rawData);
            }
        }
    }
}