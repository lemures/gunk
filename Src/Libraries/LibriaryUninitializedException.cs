﻿using System;

namespace Gunk_Scripts
{
    public class LibriaryUninitializedException : Exception
    {
        public LibriaryUninitializedException(string className) : base("This class needs class " + className + "initialized")
        {
        }
    }
}
