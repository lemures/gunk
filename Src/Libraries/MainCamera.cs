﻿using Gunk_Scripts.Camera_Behavoiur;
using UnityEngine;
using Used_Assets.Psychose_Interactive.NGSS;

namespace Gunk_Scripts
{
    public class MainCamera:ISingletonGameObject
    {
        private GameObject go;
        private static MainCamera instance;

        public static MainCamera getInstance()
        {
            return instance ?? (instance = new MainCamera());
        }

        public GameObject getObject()
        {
            return go;
        }

        public Camera getCamera()
        {
            return go.GetComponent<Camera>();
        }

        public void spawnObject()
        {
            go = Object.Instantiate((GameObject)DatabasesManager.getObjectLatestVersion("Main Camera"));
            if (SunManager.getInstance().getObject()!=null)
            go.GetComponent<NGSS_ContactShadows>().mainDirectionalLight = SunManager.getInstance().getObject().GetComponent<Light>();
            //TODO repair this
            //if (SunManager.getInstance().getObject()!=null)
            //this component is currently disabled as it doesn't work properly (unity problem?)
            //go.GetComponent<AtmosphericScattering>().Sun = SunManager.getInstance().getObject().GetComponent<Light>();
            cameraMovements = new CameraMovements(go.transform);
        }

        /// <summary>
        /// returns instantiated copy of Main Camera prefab cleared of MainCamera tag and other Singleton Main Camera components
        /// </summary>
        public static GameObject getCopy()
        {
            var goCopy = Object.Instantiate((GameObject)DatabasesManager.getObjectLatestVersion("Main Camera"));
            //only one object
            if (goCopy.GetComponent<AudioListener>())
            {
                Object.Destroy(goCopy.GetComponent<AudioListener>());
            }

            goCopy.tag = "Untagged";
            return goCopy;

        }

        private static void updateShaderUniforms()
        {
            var transform = getInstance().go.transform;

            KernelUtils.setFloat("camera_x", transform.position.x);
            KernelUtils.setFloat("camera_y", transform.position.y);
            KernelUtils.setFloat("camera_z", transform.position.z);
            //TODO repair this uniform functionality
            //KernelUtils.setInt("camera_mode", (int)_currentCamera.myMode);
            var myMat = Camera.main.projectionMatrix * Camera.main.worldToCameraMatrix;
            KernelUtils.setMatrix("WorldToCameraMatrix", myMat);

            //This is the best place I could think of for this code below
            KernelUtils.setFloat("screen_width", Screen.width);
            KernelUtils.setFloat("screen_height", Screen.height);
        }

        public void destroyObject()
        {
            if (go != null)
            {
                Object.DestroyImmediate(go);
            }
        }

        public static CameraMovements cameraMovements;

        public static void update()
        {
            cameraMovements.updateCamera();
            updateShaderUniforms();
        }

        private MainCamera()
        {

        }
    }
}
