﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.AI;
using Used_Assets.NavMeshComponents.Scripts;

namespace Gunk_Scripts
{
    public static class MapNavmeshBaker
    {
        private static string getAndEnsureTargetPath(NavMeshSurface surface)
        {
            // Create directory for the asset if it does not exist yet.
            var activeScenePath = surface.gameObject.scene.path;

            var targetPath = "Assets/Maps/Navmeshes";
            if (!string.IsNullOrEmpty(activeScenePath))
                // ReSharper disable once AssignNullToNotNullAttribute
                targetPath = Path.Combine(Path.GetDirectoryName(activeScenePath), Path.GetFileNameWithoutExtension(activeScenePath));
            if (!Directory.Exists(targetPath))
                Directory.CreateDirectory(targetPath);
            return targetPath;
        }

        private static void createNavMeshAsset(NavMeshSurface surface)
        {
            var targetPath = getAndEnsureTargetPath(surface);

            var combinedAssetPath = Path.Combine(targetPath, "NavMesh-" + surface.name + ".asset");
            AssetDatabase.DeleteAsset(combinedAssetPath);
            AssetDatabase.CreateAsset(surface.navMeshData, combinedAssetPath);
        }

        private static NavMeshData getNavMeshAssetToDelete(NavMeshSurface navSurface)
        {
            var prefabType = PrefabUtility.GetPrefabType(navSurface);
            if (prefabType == PrefabType.PrefabInstance || prefabType == PrefabType.DisconnectedPrefabInstance)
            {
                // Don't allow deleting the asset belonging to the prefab parent
                var parentSurface = PrefabUtility.GetPrefabParent(navSurface) as NavMeshSurface;
                if (parentSurface && navSurface.navMeshData == parentSurface.navMeshData)
                    return null;
            }
            return navSurface.navMeshData;
        }

        private struct AsyncBakeOperation
        {
            public NavMeshSurface surface;
            public NavMeshData bakeData;
            public AsyncOperation bakeOperation;
        }

        private static List<AsyncBakeOperation> bakeOperations = new List<AsyncBakeOperation>();

        private static NavMeshData initializeBakeData(NavMeshSurface surface)
        {
            var emptySources = new List<NavMeshBuildSource>();
            var emptyBounds = new Bounds();
            return NavMeshBuilder.BuildNavMeshData(surface.GetBuildSettings(), emptySources, emptyBounds
                , surface.transform.position, surface.transform.rotation);
        }

        private static GameObject spawnedTerrain;

        public static void bake(GameObject terrainPrefab,Vector3 terrainPosition)
        {
            if (terrainPrefab.GetComponent<NavMeshSurface>() == null)
            {
                terrainPrefab.AddComponent<NavMeshSurface>();
            }

            spawnedTerrain=Object.Instantiate(terrainPrefab,terrainPosition,Quaternion.identity);

            NavMeshSurface surf = terrainPrefab.GetComponent<NavMeshSurface>();
            var curOper = new AsyncBakeOperation();
            curOper.bakeData = initializeBakeData(surf);
            curOper.bakeOperation = surf.UpdateNavMesh(curOper.bakeData);
            curOper.surface = surf;
            bakeOperations.Add(curOper);
            // ReSharper disable once DelegateSubtraction
            EditorApplication.update -= updateOperationsAndDisplayProgress;
            EditorApplication.update += updateOperationsAndDisplayProgress;
        }

        public static bool isBakingInProgress() => bakeOperations.Count != 0;

        private static void updateOperationsAndDisplayProgress()
        {
            for (int i = bakeOperations.Count - 1; i >= 0; --i)
            {

                var oper = bakeOperations[i].bakeOperation;

                if (oper == null)
                    continue;

                var p = oper.progress;

                EditorUtility.DisplayProgressBar("Baking", "Baking: " + (int) (100 * p) + "%", p);
            }

            if (bakeOperations.Count == 0)
            {
                EditorUtility.ClearProgressBar();
                // ReSharper disable once DelegateSubtraction
                EditorApplication.update -= updateOperationsAndDisplayProgress;
                Object.DestroyImmediate(spawnedTerrain);
            }

            foreach (var oper in bakeOperations)
            {
                if (oper.surface == null || oper.bakeOperation == null)
                    continue;

                if (oper.bakeOperation.isDone)
                {
                    var surface = oper.surface;
                    var delete = getNavMeshAssetToDelete(surface);
                    if (delete != null)
                        AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(delete));

                    surface.RemoveData();
                    surface.navMeshData = oper.bakeData;
                    if (surface.isActiveAndEnabled)
                        surface.AddData();
                    createNavMeshAsset(surface);
                    EditorSceneManager.MarkSceneDirty(surface.gameObject.scene);
                }
            }
            bakeOperations.RemoveAll(o => o.bakeOperation == null || o.bakeOperation.isDone);
        }
    }
}
#endif