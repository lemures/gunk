using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Renderers;
using Src.Libraries;

namespace Gunk_Scripts
{
    public class NavmeshAgentProducer : INavigationAgentProducer
    {
        private GameObjectManager gameObjectManager;

        public INavigationAgent createAgent(int gameObjectId)
        {
           return gameObjectManager.createNavmeshComponent(gameObjectId);
        }

        public void destroyAgent(int gameObjectId)
        {
            gameObjectManager.removeNavmeshComponent(gameObjectId);
        }

        public NavmeshAgentProducer(GameObjectManager gameObjectManager)
        {
            this.gameObjectManager = gameObjectManager;
        }
    }
}