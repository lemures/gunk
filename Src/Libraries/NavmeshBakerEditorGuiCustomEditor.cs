﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace Gunk_Scripts
{
    public class NavmeshBakerEditorGuiCustomEditor : UnityEditor.Editor
    {
        [MenuItem("Assets/Bake navmesh based on this terrain")]
        private static void bakeNavmesh()
        {
            MapNavmeshBaker.bake(Selection.activeObject as GameObject, Vector3.zero);
        }

        [MenuItem("Assets/Bake navmesh based on this terrain", true)]
        private static bool optionValidation()
        {
            return Selection.activeGameObject != null && Selection.activeGameObject.GetComponent<Terrain>() != null;
        }
    }
}
#endif