using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Object = UnityEngine.Object;

namespace Gunk_Scripts
{
    /// <summary>
    /// a helper for serializing unity Object references. Is able to generate unique id based on objects name and type
    /// </summary>
    public class ObjectReferenceCollector
    {
        public ObjectReferenceCollector(IEnumerable<Object> startReferences)
        {
            foreach (var reference in startReferences)
            {
                addResource(reference);
            }
        }

        public ObjectReferenceCollector()
        {

        }

        private readonly SortedDictionary<int,Object>allObjects=new SortedDictionary<int, Object>();

        private int calculateResourceId(Object myObject)
        {
            string nameToHash = myObject.name + "  " + myObject.GetType();
            return StringHasher.calculateHash(nameToHash);
        }

        /// <summary>
        /// adds resource and if one already existst with the same type and name, then overwrites it
        /// </summary>
        public void addResource([NotNull]Object myObject)
        {
            var id = calculateResourceId(myObject);
            if (!allObjects.ContainsKey(id))
            {
                allObjects.Add(id,myObject);
            }
            else
            {
                allObjects[id] = myObject;
            }
        }

        /// <summary>
        /// returns id of an object but only if it was already added and throws exception if the object wasn't added before
        /// </summary>
        public int getResourceId([NotNull]Object myObject)
        {
            if (myObject == null)
            {
                throw new NullReferenceException("resource cannot be null");
            }

            var key = calculateResourceId(myObject);
            if (!allObjects.ContainsKey(key))
            {
                throw new Exception("this object was never added so it has no id");
            }
            return key;
        }

        public Object getResourceById(int id)
        {
            if (!allObjects.ContainsKey(id))
            {
                throw new Exception("this object was never added so it has no id");
            }
            return allObjects[id];
        }

        public void removeResource([NotNull]Object myObject)
        {
            allObjects.Remove(calculateResourceId(myObject));
        }
    }
}