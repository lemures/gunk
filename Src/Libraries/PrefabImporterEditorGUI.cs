﻿#if UNITY_EDITOR

using System;
using System.Linq;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.GunkObjects;
using UnityEditor;
using UnityEngine;

namespace Gunk_Scripts
{
    public class PrefabImporterEditorGUI : Editor
    {
        /// <summary>
        /// imports all native gunk prefabs(i.e. the ones designed by the author of the game and not purchased from AssetStore) to PrefabManager
        /// </summary>
        [MenuItem("Prefab Importer/Reimport all Gunk native prefabs")]
        private static void reimportAllGunkPrefabs()
        {
            var defaultModelProcessor = new DefaultModelProcessor();
            var textures = (from texture in AssetUtils.findAllAssetsOfType(typeof(Texture2D), "Assets/Prefabs") select texture as Texture2D).ToList();
            var meshes = (from mesh in AssetUtils.findAllAssetsOfType(typeof(Mesh), "Assets/Prefabs") select mesh as Mesh).ToList();
            var importedPrefabs= defaultModelProcessor.process(meshes,textures);
            foreach (var prefab in importedPrefabs)
            {
                GlobalPrefabsManager.getInstance().addPrefab(prefab.go);
                GlobalPrefabsManager.getInstance().setSkeletonMesh(prefab.go.name, prefab.meshSkeleton);
            }
            GlobalPrefabsManager.getInstance().saveDatabases();
        }

        [MenuItem("Prefab Importer/Reimport all Low Poly Trees prefabs")]
        private static void reimportAllLowPolyTreesPrefabs()
        {
            var lowPolyModelProcessor = new LowPolyModelProcessor();
            var textures = LowPolyTreesResourcesFinder.findAllTextures();
            var meshes = LowPolyTreesResourcesFinder.findAllMeshes();
            var importedPrefabs= lowPolyModelProcessor.process(meshes,textures);
            foreach (var prefab in importedPrefabs)
            {
                GlobalPrefabsManager.getInstance().addPrefab(prefab.go);
            }
            GlobalPrefabsManager.getInstance().saveDatabases();
        }

        [MenuItem("Prefab Importer/Make Miniatures for all standard gunk prefabs")]
        private static void makeMiniaturesGunkPrefabs()
        {
            MainCamera.getInstance().spawnObject();
            SunManager.getInstance().spawnObject();
            foreach (var type in Enum.GetValues(typeof(GunkObjectType)))
            {
                var prefabName = GunkPrefabNames.getName((GunkObjectType)type);
                var prefab = GlobalPrefabsManager.getInstance().findPrefab(prefabName);
                if (prefab.go != null)
                {
                    var miniature = MiniatureMaker.produceMiniature(prefab);
                    if (miniature != null)
                    {
                        GlobalPrefabsManager.getInstance().setMiniature(prefabName, miniature);
                        Debug.Log("miniature imported for " + prefabName);
                    }
                }
            }
            GlobalPrefabsManager.getInstance().saveDatabases();
            SunManager.getInstance().destroyObject();
            MainCamera.getInstance().destroyObject();
        }

        [MenuItem("Prefab Importer/Make Miniatures for All prefabs")]
        private static void makeMiniatures()
        {
            MainCamera.getInstance().spawnObject();
            SunManager.getInstance().spawnObject();
            foreach (var prefabName in GlobalPrefabsManager.getInstance().getAllPrefabNames())
            {
                var prefab = GlobalPrefabsManager.getInstance().findPrefab(prefabName);
                if (prefab.go != null)
                {
                    var miniature = MiniatureMaker.produceMiniature(prefab);
                    if (miniature != null)
                    {
                        GlobalPrefabsManager.getInstance().setMiniature(prefabName, miniature);
                    }
                }
            }
            GlobalPrefabsManager.getInstance().saveDatabases();
            SunManager.getInstance().destroyObject();
            MainCamera.getInstance().destroyObject();
        }
    }
}
#endif