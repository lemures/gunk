﻿#if UNITY_EDITOR
using Gunk_Scripts.Asset_Importing;
using UnityEditor;

namespace Gunk_Scripts
{
    public class PrefabVariablesEditorGuiCustomEditor : Editor
    {
        private static void markPrefabForFbxFix(bool newValue)
        {
            var name = Selection.activeGameObject.name.ToLower();
            PrefabVariablesManager.saveVariable(name, GlobalPrefabsManager.needFbxRotationVariableName, newValue);
        }

        private static bool validatePrefabForFbxFix(bool newValue)
        {
            if (Selection.activeGameObject == null) return false;

            var name = Selection.activeGameObject.name.ToLower();
            return GlobalPrefabsManager.getInstance().findPrefab(name) != null &&
                   (!PrefabVariablesManager.hasVariable(name, GlobalPrefabsManager.needFbxRotationVariableName) ||
                    (bool)PrefabVariablesManager.getVariable(name, GlobalPrefabsManager.needFbxRotationVariableName) == !newValue);

        }

        /// <summary>
        /// imports all native gunk prefabs(i.e. the ones designed by the author of the game and not purchased from AssetStore) to PregabManager
        /// </summary>
        [MenuItem("Assets/Prefab Variables/mark prefab as one needing fbx rotation correction")]
        public static void markPrefabForFbxFix()
        {
            markPrefabForFbxFix(true);
        }

        [MenuItem("Assets/Prefab Variables/mark prefab as one needing fbx rotation correction", true)]
        public static bool validate1() => validatePrefabForFbxFix(true);

        [MenuItem("Assets/Prefab Variables/mark prefab as one no longer needing fbx rotation correction")]
        public static void markPrefabNoLongerForFbxFix()
        {
            markPrefabForFbxFix(false);
        }

        [MenuItem("Assets/Prefab Variables/mark prefab as one no longer needing fbx rotation correction", true)]
        public static bool validatePrefabForFbxFix() => validatePrefabForFbxFix(false);
    }
}

#endif