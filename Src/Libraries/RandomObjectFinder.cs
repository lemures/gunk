using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Gunk_Scripts;
using Gunk_Scripts.Data_Structures;
using UnityEngine;
using Random = System.Random;

namespace Src.Libraries
{
    public class RandomObjectFinder
    {
        readonly Random generator = new Random();

        private List<ObjectData>allObjects = new List<ObjectData>();

        public class ObjectData
        {
            public Float2 position;
            public int id;
            public Faction faction;

            public ObjectData(Float2 position, int id, Faction faction)
            {
                this.position = position;
                this.id = id;
                this.faction = faction;
            }
        }

        private void validateObjectDoesNotExist(int id)
        {
            if (allObjects.Find(x => x.id == id) != null)
            {
                throw new InvalidOperationException("object with this id already exists");
            }
        }

        private void validateObjectExists(int id)
        {
            if (allObjects.Find(x => x.id == id) == null)
            {
                throw new InvalidOperationException("object with this id does not exist");
            }
        }

        public void addObject(int id, float x, float y, Faction faction)
        {
            validateObjectDoesNotExist(id);
            allObjects.Add(new ObjectData(new Float2(x,y), id, faction));
        }

        public void removeObject(int id)
        {
            validateObjectExists(id);
            allObjects.RemoveAll(x => x.id == id);
        }

        public int findRandomObjectInRange(float x, float y, Faction faction, float maxDistance)
        {
            var matching = allObjects.FindAll(objectData => objectData.faction == faction
                                                   && objectData.position.distance(new Float2(x, y)) < maxDistance);
            return matching.Count == 0 ? -1 : matching[generator.Next(0, matching.Count)].id;
        }
    }
}