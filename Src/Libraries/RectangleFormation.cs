using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Gunk_Scripts
{
    public class RectangleFormation : IUnitFormation
    {
        private readonly Vector3 center;
        private readonly List<int> objectIds;
        private readonly float targetDistanceBetweenObjects;

        public RectangleFormation(IEnumerable<int> objectIds, Vector3 center, float targetDistanceBetweenObjects)
        {
            this.objectIds = objectIds.ToList();
            this.targetDistanceBetweenObjects = targetDistanceBetweenObjects;
            this.center = center;
        }

        public Vector3 getPosition(int id)
        {
            if (!objectIds.Contains(id))
                throw new ArgumentException("this id was never added so position can not be calculated");

            var row = getRow(id);
            var column = getColumn(id);
            var sideLength = getFormationSideLength();
            var offset = targetDistanceBetweenObjects *
                         new Vector3(-(sideLength - 1) / 2.0f, 0, -(sideLength - 1) / 2.0f);
            return center + offset +
                   new Vector3(row * targetDistanceBetweenObjects, 0, column * targetDistanceBetweenObjects);
        }

        private int getRow(int id)
        {
            return objectIds.IndexOf(id) % getFormationSideLength();
        }

        private int getColumn(int id)
        {
            return objectIds.IndexOf(id) / getFormationSideLength();
        }

        private int getFormationSideLength()
        {
            return (int) Math.Ceiling(Math.Sqrt(objectIds.Count));
        }
    }
}