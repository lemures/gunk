﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Used_Assets.PostProcessing.Runtime;
using Used_Assets.Psychose_Interactive.NGSS;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
#endif

namespace Gunk_Scripts
{
    public static class ScreenshotMaker
    {
        public class ScreenshotSettings
        {
            public Vector3 position = Vector3.zero;
            public Vector3 rotation = Vector3.zero;
            public int resolutionX = 1000;
            public int resolutionY = 1000;
            public string filePath = "Assets/Screenshots";
            public string name = "screenshot";
            public bool needsCompression = true;
            public int? forcedFov = null;
        }

        /// <summary>
        /// Creates camera with certain rotation and position and captures screenshot with it of a certain dimensions and returns RenderTexture with screenshot in its content
        /// </summary>
        private static RenderTexture makeScreenshot(Vector3 position,Vector3 rotation, int resolutionX,int resolutionY,int? forcedFov)
        {
            if (Camera.main == null)
            {
                throw new NullReferenceException("main camera cannot be null");
            }

            var res = new RenderTexture(resolutionX, resolutionY, 24,RenderTextureFormat.Default,RenderTextureReadWrite.Linear);
            var dummy = new GameObject("dummy");
            var camera=dummy.AddComponent<Camera>();

            var contactShadows= dummy.AddComponent<NGSS_ContactShadows>();
            var originalShadows = Camera.main.transform.GetComponent<NGSS_ContactShadows>();

            contactShadows.mainDirectionalLight = originalShadows.mainDirectionalLight;
            contactShadows.noiseFilter = true;
            contactShadows.raySamples = 128;
            contactShadows.rayWidth = originalShadows.rayWidth;
            contactShadows.shadowsBias = originalShadows.shadowsBias;
            contactShadows.shadowsFade = originalShadows.shadowsFade;
            contactShadows.shadowsSoftness = originalShadows.shadowsSoftness;

            camera.CopyFrom(Camera.main);

            dummy.transform.position = position;
            dummy.transform.rotation = Quaternion.Euler(rotation);

            camera.enabled = false;
            camera.forceIntoRenderTexture = true;
            camera.targetTexture = res;

            if (forcedFov!=null)
            {
                camera.fieldOfView = forcedFov.Value;
            }

            var postProcessing = dummy.AddComponent<PostProcessingBehaviour>();

            postProcessing.profile = Camera.main.transform.GetComponent<PostProcessingBehaviour>().profile;

            //produces weird artifacts if enabled
            postProcessing.profile.antialiasing.enabled = false;
            camera.Render();
            postProcessing.profile.antialiasing.enabled = true;
            dummy.hideFlags = HideFlags.HideAndDontSave;
            if (!Application.isPlaying)
            Object.DestroyImmediate(dummy.GetComponent<PostProcessingBehaviour>());
            return res;
        }

        /// <summary>
        /// Creates camera with certain rotation and position and captures screenshot with it of a certain dimensions and then saves it to the path.
        /// Can either save file to .raw or .png (needsCompression). Use Texture.LoadImage to decompress images.
        /// </summary>
        public static void saveScreenshotToFile(ScreenshotSettings settings)
        {
            if (Camera.main == null)
            {
                throw new NullReferenceException("main camera cannot be null");
            }
            var resolutionX = settings.resolutionX;
            var resolutionY = settings.resolutionY;
            var tex = makeScreenshot(settings.position, settings.rotation, resolutionX, resolutionY, settings.forcedFov);

            var filePath = Path.Combine(settings.filePath, settings.name) + (settings.needsCompression
                ? ".png"
                : ".raw");

            var colorsArray = TextureCapturer.getTexturePixels(tex);

            if (settings.needsCompression)
            {
                //we use .png compression
                var temp = new Texture2D(resolutionX, resolutionY,TextureFormat.ARGB32,false,true);
                var tempTab = new Color[resolutionX * resolutionY];
                for (var i = 0; i < resolutionX; i++)
                {
                    for (var q = 0; q < resolutionY; q++)
                    {
                        tempTab[i * resolutionY + q] = colorsArray[i * resolutionY + q].gamma;
                    }
                }

                temp.SetPixels(tempTab);
                temp.Apply();
                var bytes = temp.EncodeToPNG();
                FileManager.putBytesToFile(filePath, bytes);
            }
            else
            {
                //we use .raw format
                var bytes = new byte[resolutionX * resolutionY * 4];
                for (var i = 0; i < resolutionX; i++)
                {
                    for (var q = 0; q < resolutionY; q++)
                    {
                        Color32 pom = colorsArray[i * resolutionY + q].linear;
                        bytes[(i * resolutionY + q) * 4 + 0] = pom.r;
                        bytes[(i * resolutionY + q) * 4 + 1] = pom.g;
                        bytes[(i * resolutionY + q) * 4 + 2] = pom.b;
                        bytes[(i * resolutionY + q) * 4 + 3] = pom.a;
                    }
                }
                FileManager.putBytesToFile(filePath, bytes);
            }
#if UNITY_EDITOR
            AssetDatabase.ImportAsset(filePath);
#endif
        }
    }
}
