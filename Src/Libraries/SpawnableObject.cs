using Gunk_Scripts.Data_Structures;

namespace Gunk_Scripts
{
    public struct SpawnableObject
    {
        public Float4 position;
        public Float4 rotation;
        public float scale;
        public string prefabName;
    }
}