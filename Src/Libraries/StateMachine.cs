using System;
using Gunk_Scripts;

namespace Src.Libraries
{
    public class StateMachine<T>
    {
        private State<T> _currentState;

        public StateMachine(State<T> startState)
        {
            _currentState = startState;
            startState.setAsActive();
        }

        public void changeState(State<T> newState)
        {
            if (newState == null)
            {
                throw new NullReferenceException("new state cannot be null!");
            }
            _currentState.deactivate();
            _currentState = newState;
            newState.setAsActive();
        }

        public State<T> currentState => _currentState;

        public void update(float deltaTime)
        {
            _currentState.update(deltaTime);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(StateMachine<T>))
                .addVariable(_currentState, nameof(_currentState))
                .print();
        }
    }
}