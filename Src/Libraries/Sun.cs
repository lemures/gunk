﻿using UnityEngine;
using Used_Assets.Compute_Assets;
using Used_Assets.Psychose_Interactive.NGSS;

namespace Gunk_Scripts
{
    public class SunManager:ISingletonGameObject
    {
        private GameObject go;
        private static SunManager instance;

        public static SunManager getInstance()
        {
            return instance ?? (instance = new SunManager());
        }

        public GameObject getObject()
        {
            return go;
        }

        public void spawnObject()
        {
            go = Object.Instantiate((GameObject)DatabasesManager.getObjectLatestVersion("sun"));
            if (MainCamera.getInstance().getObject() != null)
                MainCamera.getInstance().getCamera().GetComponent<NGSS_ContactShadows>().mainDirectionalLight = getObject().GetComponent<Light>();
            if (MainCamera.getInstance().getObject() != null)
                MainCamera.getInstance().getObject().GetComponent<AtmosphericScattering>().Sun =
                    go.GetComponent<Light>();
        }

        public void destroyObject()
        {
            if (go == null)
            {
                return;
            }
            if (Application.isPlaying)
            {
                Object.DestroyObject(go);
            }
            else
            {
                Object.DestroyImmediate(go);
            }
        }

        private SunManager()
        {

        }
    }
}
