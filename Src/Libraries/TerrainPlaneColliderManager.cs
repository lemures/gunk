﻿using UnityEngine;

namespace Gunk_Scripts
{
    public static class TerrainPlaneColliderManager
    {
        private static readonly BoxCollider PlaneCollider;

        public static BoxCollider getPlaneCollider() => PlaneCollider;

        static TerrainPlaneColliderManager()
        {
            var planeColliderObject = new GameObject();
            planeColliderObject.transform.localScale = new Vector3(100 * 100.0f, 0, 100 * 100.0f);
            planeColliderObject.name = "Camera plane collider";
            PlaneCollider = planeColliderObject.AddComponent<BoxCollider>();
        }
    }
}