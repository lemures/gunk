﻿using System;
using UnityEngine;

namespace Gunk_Scripts
{
    [Serializable]
    public class TimeManager:MonoBehaviour, ITimeManager
    {
        public float time { get; private set; } = 0;
        private static GameObject updateObject;
        public static TimeManager instance { get; private set; }

        private void Update()
        {
            time += Time.deltaTime;
            KernelUtils.setFloat("deltaTime", Time.deltaTime);
            KernelUtils.setFloat("Time", time);
            KernelUtils.setFloat("unscaledDeltaTime", Time.unscaledDeltaTime);
            KernelUtils.setFloat("unscaledTime", Time.unscaledTime);
        }

        public void freezeTime()
        {
            Time.timeScale = 0f;
        }

        public void unfreezeTime()
        {
            Time.timeScale = 1.0f;
        }

        public void reset()
        {
            time = 0;
        }

        public float deltaTime => Time.deltaTime;

        public static void install()
        {
            updateObject = new GameObject();
            instance=updateObject.AddComponent<TimeManager>();
            updateObject.name = "Time Manager " + instance.GetHashCode();
        }
    }
}