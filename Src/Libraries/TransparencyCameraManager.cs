﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Gunk_Scripts
{
    public class TransparencyCameraManager : MonoBehaviour
    {
        private GameObject transparencyCamera;
        private Material transparencyMaterial;
        private RenderTexture transparencyRenderTexture;
        private Mesh lowPolySphere;

        public void install(int mapSizeX, int mapSizeY)
        {
            transparencyRenderTexture = new RenderTexture(Screen.width / 4, Screen.height / 4, 0);
            transparencyRenderTexture.Create();

            transparencyCamera = new GameObject("Transparency Camera");

            var transparencyCameraComp = transparencyCamera.AddComponent<Camera>();
            transparencyCameraComp.targetTexture = transparencyRenderTexture;
            transparencyCameraComp.cullingMask = 1 << 11;
            transparencyCameraComp.clearFlags = CameraClearFlags.SolidColor;
            transparencyCameraComp.backgroundColor = new Color32(0, 0, 0, 0);
            transparencyCameraComp.allowMSAA = false;
            transparencyCameraComp.renderingPath = RenderingPath.Forward;

            KernelUtils.setGlobalTexture("Transparency_RenderTexture", transparencyRenderTexture);
            lowPolySphere = (DatabasesManager.getObjectLatestVersion("Low Poly Sphere") as GameObject)
                .GetComponent<MeshFilter>().sharedMesh;
            transparencyMaterial = DatabasesManager.getObjectLatestVersion("Transparency Sphere mat") as Material;
        }

        public void updateTransparencyCamera()
        {
            if (MainCamera.getInstance().getObject() == null)
            {
                Debug.Log("Warning! Main Camera has to exist for transparency camera to work");
                return;
            }
            transparencyCamera.transform.rotation = MainCamera.getInstance().getObject().transform.rotation;
            transparencyCamera.transform.position = MainCamera.getInstance().getObject().transform.position + new Vector3(0, -1000, 0);

            var indirectDataRenderers = KernelUtils.getBuffer("Indirect_Data_renderers");

            //Drawing transparency spheres
            Graphics.DrawMeshInstancedIndirect(lowPolySphere, 0, transparencyMaterial,
                new Bounds(new Vector3(0.0f, 0.0f, 0.0f), new Vector3(10000.0f, 10000.0f, 10000.0f)),
                indirectDataRenderers, 60, null, ShadowCastingMode.Off, false, 11,
                transparencyCamera.GetComponent<Camera>());

        }
    }
}
