using System.Collections.Generic;
using Gunk_Scripts;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;

namespace Src.Logic
{
    public class OwnedResources
    {
        private Dictionary<ResourceType, float> currentLevels = new Dictionary<ResourceType, float>();

        public OwnedResources()
        {
            foreach (ResourceType resourceType in System.Enum.GetValues(typeof(ResourceType)))
            {
                currentLevels.Add(resourceType, 0);
            }
        }

        public void addResources(GatheredResources resources)
        {
            currentLevels[resources.type] += resources.amount;
        }

        public float getResourcesCount(ResourceType resourceType)
        {
            return currentLevels[resourceType];
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(OwnedResources))
                .addVariable(currentLevels, nameof(currentLevels))
                .print();
        }
    }
}