using Gunk_Scripts;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;

namespace Src.Logic
{
    public class Player
    {
        public readonly Faction representingFaction;

        private OwnedResources ownedResources = new OwnedResources();

        public Player(Faction representingFaction)
        {
            this.representingFaction = representingFaction;
        }

        public void addResources(GatheredResources resources)
        {
            ownedResources.addResources(resources);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(Player))
                .addVariable(representingFaction, nameof(representingFaction))
                .addVariable(ownedResources, nameof(ownedResources))
                .print();
        }
    }
}