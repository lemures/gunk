using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using Gunk_Scripts;
using Gunk_Scripts.GunkComponents;
using NUnit.Framework;

namespace Src.Logic
{
    public class Players
    {
        List<Player>allPlayers = new List<Player>();

        public Players()
        {
            foreach (Faction faction in Enum.GetValues(typeof(Faction)))
            {
                allPlayers.Add(new Player(faction));
            }
        }

        public void addResources(Faction faction, GatheredResources resources)
        {
            allPlayers.Find(x => x.representingFaction == faction).addResources(resources);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(allPlayers))
                .addVariable(allPlayers, nameof(allPlayers))
                .print();
        }
    }
}