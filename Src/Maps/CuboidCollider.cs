﻿using Gunk_Scripts.Objects_Selection;
using UnityEngine;

namespace Gunk_Scripts.Maps
{
    public class CuboidCollider:ICollider
    {
        public Vector3 center { get; set; }

        private readonly Vector3 dimensions;

        private static bool triangleContains(Vector2Int s, Vector2Int a, Vector2Int b, Vector2Int c)
        {
            float asX = s.x - a.x;
            float asY = s.y - a.y;

            var sAb = (b.x - a.x) * asY - (b.y - a.y) * asX > 0;

            if ((c.x - a.x) * asY - (c.y - a.y) * asX > 0 == sAb) return false;

            return (c.x - b.x) * (s.y - b.y) - (c.y - b.y) * (s.x - b.x) > 0 == sAb;
        }

        private static bool checkBoundaries(Vector2Int data)
        {
            return data.x > -100 && data.y > -100 && data.x < Screen.width + 100 && data.y < Screen.height + 100;
        }

        public bool isUnderPoint(IScreenPositionCalculator positionCalculator, Vector2 point)
        {
            var pointerPositionInt = new Vector2Int ((int)point.x, (int)point.y);
            var upperSide = new Vector3[4];
            var lowerSide = new Vector3[4];

            for (var i = 0; i < 4; i++)
            {

                upperSide[i] = center;
                lowerSide[i] = center;

                upperSide[i].y += dimensions.y;
                lowerSide[i].y -= dimensions.y;

                upperSide[i].x += (-1 + 2 * (i & 1)) * dimensions.x;
                lowerSide[i].x += (-1 + 2 * (i & 1)) * dimensions.x;

                upperSide[i].z += (-1 + (i & 2)) * dimensions.z;
                lowerSide[i].z += (-1 + (i & 2)) * dimensions.z;

            }
            var screenUpperSide = new Vector2Int[4];
            var screenLowerSide = new Vector2Int[4];
            for (var i = 0; i < 4; i++)
            {
                var upper = positionCalculator.worldToScreenPoint(upperSide[i]);
                var lower = positionCalculator.worldToScreenPoint(lowerSide[i]);
                screenUpperSide[i] = new Vector2Int((int)upper.x,(int)upper.y);
                screenLowerSide[i] = new Vector2Int((int)lower.x, (int)lower.y);
                if (!checkBoundaries(screenUpperSide[i]) || !checkBoundaries(screenLowerSide[i]))
                {
                    return false;
                }
            }
            for (var i = 0; i < 2; i++)
            {
                if (triangleContains(pointerPositionInt, screenUpperSide[i * 3], screenUpperSide[1], screenUpperSide[2])) { return true; }
                if (triangleContains(pointerPositionInt, screenLowerSide[i * 3], screenLowerSide[1], screenLowerSide[2])) { return true; }
            }
            for (var i = 0; i < 4; i++)
            {
                if (triangleContains(pointerPositionInt, screenLowerSide[i], screenUpperSide[i], screenUpperSide[(i + 3) % 4])) { return true; }
                if (triangleContains(pointerPositionInt, screenLowerSide[i], screenUpperSide[(i + 3) % 4], screenLowerSide[(i + 3) % 4])) { return true; }
            }
            return false;

        }

        public CuboidCollider(Vector3 center, Vector3 dimensions)
        {
            this.center = center;
            this.dimensions = dimensions;
        }

        public bool isInScreenRectangle(Vector2 leftCorner, Vector2 rightCorner, IScreenPositionCalculator positionCalculator)
        {
            Vector2 myPos = positionCalculator.worldToScreenPoint(center);

            float temp;
            if (leftCorner.x > rightCorner.x)
            {
                temp = leftCorner.x;
                leftCorner.x = rightCorner.x;
                rightCorner.x = temp;
            }
            if (leftCorner.y > rightCorner.y)
            {
                temp = leftCorner.y;
                leftCorner.y = rightCorner.y;
                rightCorner.y = temp;
            }

            return (leftCorner.x <= myPos.x &&
                    myPos.x <= rightCorner.x &&
                    leftCorner.y <= myPos.y &&
                    myPos.y <= rightCorner.y);
        }

    }
}