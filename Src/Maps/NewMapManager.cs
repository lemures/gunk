﻿#if UNITY_EDITOR
#endif
using System.Linq;
using Gunk_Scripts.SerializationHelpers;
using UnityEngine;
using Used_Assets.Compute_Assets;
using Object = UnityEngine.Object;

namespace Gunk_Scripts.Maps
{
    public class NewMapManager
    {
        public class MapLoaderGui
        {
            //TODO
        }

        public class MapLoader
        {
            private static GameObject currentMap;

            private static Texture2D heightMap;

            private static double frac(double x)
            {
                //works only for positive numbers, but it is enough
                return x - (long)x;
            }

            public static void uploadTerrainDataToShaders(int mapHeight,int mapWidth)
            {
                heightMap = new Texture2D(mapHeight, mapWidth, TextureFormat.ARGB32, false, true);

                for (var i = 0; i < mapWidth; i++)
                {
                    for (var q = 0; q < mapHeight; q++)
                    {

                        double v = 0.001f * Terrain.activeTerrain.SampleHeight(new Vector3(i + 0.5f, 0, q + 0.5f));

                        var encX = frac(1.0F * v);
                        var encY = frac(255.0F * v);
                        var encZ = frac(65025.0F * v);
                        var encW = frac(160581375.0F * v);

                        encX -= encY * (1.0f / 255.0f);
                        encY -= encZ * (1.0f / 255.0f);
                        encZ -= encW * (1.0f / 255.0f);

                        heightMap.SetPixel(i, q, new Color((float)encX, (float)encY, (float)encZ, (float)(encW)));
                    }
                }
                heightMap.filterMode = FilterMode.Bilinear;
                heightMap.wrapMode = TextureWrapMode.Clamp;
                heightMap.Apply();
                KernelUtils.setGlobalTexture("height_map", heightMap);
            }

            private static void loadShaderUniforms(Map mapToLoad)
            {
                if (mapToLoad.splatMap != null)
                {
                    KernelUtils.setGlobalTexture("Splat_map", mapToLoad.splatMap);
                }

                if (mapToLoad.normalMap != null)
                {
                    KernelUtils.setGlobalTexture("Terrain_normals", mapToLoad.normalMap);
                }

                int mapHeight= (int)mapToLoad.terrainData.size.x;
                int mapWidth = (int) mapToLoad.terrainData.size.x;

                KernelUtils.setInt("map_height", mapHeight);
                KernelUtils.setInt("map_width", mapWidth);
                uploadTerrainDataToShaders(mapHeight,mapWidth);
            }

            public static void loadMap(Map mapToLoad)
            {
                Object.Instantiate(mapToLoad.prefab);
                loadShaderUniforms(mapToLoad);
                loadedMap = new LoadedMap(mapToLoad.getMapHeight(),mapToLoad.getMapWidth());
                var colors = TextureCapturer.getTexturePixels(mapToLoad.splatMap);
                float scale = mapToLoad.getMapWidth() / (float) mapToLoad.splatMap.width;
                foreach (var xPos in Enumerable.Range(0, mapToLoad.splatMap.width))
                foreach (var yPos in Enumerable.Range(0, mapToLoad.splatMap.height))
                {
                    int id = yPos * mapToLoad.splatMap.width + xPos;
                    if (colors[id].r + colors[id].g + colors[id].b > 0.01)
                    {
                        loadedMap.setFieldType(new Vector3(xPos * scale, 0, yPos * scale), TerrainFieldType.Slope);
                    }
                }
            }
        }

        public class LoadedMap
        {
            private PlacedObjectsManager placedObjectsManager;

            public int mapHeight{ get; private set; }
            public int mapWidth { get; private set; }

            public LoadedMap(int mapHeight, int mapWidth)
            {
                this.mapHeight = mapHeight;
                this.mapWidth = mapWidth;
                placedObjectsManager = new PlacedObjectsManager(mapHeight, mapWidth);
            }

            public bool canBuild(Vector3 position, ObjectPattern pattern)
            {
                var intPosition = new Vector2Int((int)position.x, (int)position.z);
                return placedObjectsManager.canObjectBeAdded(intPosition, pattern);
            }

            public void construct(Vector3 position, ObjectPattern pattern)
            {
                var intPosition = new Vector2Int((int)position.x, (int)position.z);
                placedObjectsManager.addObject(intPosition, pattern);
            }

            public void deconstruct(Vector3 position, ObjectPattern pattern)
            {
                var intPosition = new Vector2Int((int)position.x, (int)position.z);
                placedObjectsManager.removeObject(intPosition,pattern);
            }

            public void setFieldType(Vector3 position, TerrainFieldType newType)
            {
                var intPosition = new Vector2Int((int)position.x, (int)position.z);
                placedObjectsManager.changeFieldType(intPosition, newType);
            }

            public Vector3? calculateClosestAvailablePosition(Vector3 position, ObjectPattern pattern)
            {
                var intPosition = new Vector2Int((int)position.x, (int)position.z);
                var calculatedPosition = placedObjectsManager.calculateClosestAvailablePosition(intPosition, pattern);
                if (calculatedPosition != null)
                {
                    var uncorrectedPosition = new Vector3(calculatedPosition.Value.x, position.y, calculatedPosition.Value.y);
                    var correctedPosition = new Vector3(uncorrectedPosition.x, Terrain.activeTerrain.SampleHeight(uncorrectedPosition), uncorrectedPosition.z);
                    return correctedPosition;
                }

                return null;
            }

            public TerrainFieldType getFieldType(Vector2Int location) => placedObjectsManager.getFieldType(location);
        }

        public static LoadedMap loadedMap{ get; private set; }

        public static bool isAnyMapLoaded() => loadedMap == null;

        //public static Map getCurrenlyLocatedMap() => currentlyLoadedMap;

        public static int getCurrentMapHeight() => loadedMap.mapHeight;

        public static int getCurrentMapWidth() => loadedMap.mapWidth;

        public static float sampleTerrainHeight(Vector3 position) => Terrain.activeTerrain.SampleHeight(position);
    }
}
