﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.Maps
{
    [Serializable]
    public class ObjectPattern
    {
        public struct Field
        {
            public TerrainFieldType type;

            public int x { get; }
            public int y { get; }

            public Vector2Int position => new Vector2Int(x,y);

            public Field(TerrainFieldType type, Vector2Int position)
            {
                this.type = type;
                x = position.x;
                y = position.y;
            }

            public Field(TerrainFieldType type, int x, int y)
            {
                this.type = type;
                this.x = x;
                this.y = y;
            }

            public Field(int x, int y)
            {
                this.x = x;
                this.y = y;
                this.type = TerrainFieldType.Ground;
            }

            public override string ToString()
            {
                return "(" + x + "," + y + ")" + "  " + type.ToString();
            }
        }

        public List<Field> fieldsLocations { get; private set; } = new List<Field>();

        private ObjectPattern(List<Field> points)
        {
            fieldsLocations = points;
        }

        public ObjectPattern(List<Vector2Int> points)
        {
            fieldsLocations = new List<Field>();
            points.ForEach(x=>fieldsLocations.Add(new Field(x.x,x.y)));
        }

        public ObjectPattern(Mesh mesh)
        {
            var bounds = MeshBoundsUtils.getMeshDimensions(mesh);
            addFieldsInBounds(bounds);
        }

        public ObjectPattern(Bounds bounds)
        {
            addFieldsInBounds(bounds);
        }

        private void addFieldsInBounds(Bounds bounds)
        {
            var minX = (int)(Math.Floor(bounds.min.x));
            var minZ = (int)(Math.Floor(bounds.min.z));
            var maxX = (int)(Math.Ceiling(bounds.max.x));
            var maxZ = (int)(Math.Ceiling(bounds.max.z));

            addFieldsInRectangle(new Vector2Int(minX,minZ), maxX-minX, maxZ-minZ );
        }

        private void addFieldsInRectangle(Vector2Int lowerLeftVertex, int width, int height)
        {
            for (var i = lowerLeftVertex.x; i < lowerLeftVertex.x + width; i++)
            {
                for (var q = lowerLeftVertex.y; q < lowerLeftVertex.y + height; q++)
                {
                    fieldsLocations.Add(new Field(i,q));
                }
            }
        }

        /// <summary>
        /// creates shape with center in (0,0) and with it's fields lying in rectangle. Rectangle sides must be odd.
        /// </summary>
        /// <exception cref="InvalidOperationException">if sides are not odd</exception>
        public ObjectPattern(int width, int height)
        {
            if (height % 2 == 0 || width % 2 == 0)
            {
                throw new InvalidOperationException("sides must be odd");
            }

            var lowerLeftVertex=new Vector2Int(-width/2, -height/2);
            addFieldsInRectangle(lowerLeftVertex,width,height);
        }

        private static Vector2Int rotatePoint(Vector2Int pointToRotate, float angleInDegrees)
        {
            var angleInRadians = angleInDegrees * (Mathf.PI / 180);
            var cosTheta = Mathf.Cos(angleInRadians);
            var sinTheta = Mathf.Sin(angleInRadians);
            return new Vector2Int
            (
                (int)Math.Round((cosTheta * (pointToRotate.x) - sinTheta * (pointToRotate.y))),
                (int)Math.Round((sinTheta * (pointToRotate.x) + cosTheta * (pointToRotate.y)))
            );
        }

        public ObjectPattern translate(Vector2Int translationVector)
        {
            return mapValues(this, (field => new Field(field.type, field.position + translationVector)));
        }

        public ObjectPattern rotate(float anglesInDegrees)
        {
            return mapValues(this, (field => new Field(field.type, rotatePoint(field.position, anglesInDegrees))));
        }

        private ObjectPattern mapValues(ObjectPattern pattern, Func<Field, Field> transformationFunction)
        {
            var original = pattern.fieldsLocations;
            var result = new List<Field>();

            original.ForEach(x => result.Add(transformationFunction(x)));
            return new ObjectPattern(result);
        }

        public void setFieldType(Vector2Int position, TerrainFieldType newType)
        {
            if (!fieldsLocations.Exists(x => x.position == position))
            {
                throw new ArgumentException("field with certain position does not exist");
            }

            var foundIndex = fieldsLocations.FindIndex(field => field.position == position);
            fieldsLocations[foundIndex] = new Field(newType, fieldsLocations[foundIndex].position);
        }

        public override string ToString()
        {
            var result = "total fields count: "+fieldsLocations.Count+"\n";
            fieldsLocations.ForEach(field=>result+=field+"\n");
            return result;
        }
    }
}
