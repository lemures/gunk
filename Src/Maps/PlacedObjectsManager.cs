﻿using System;
using System.Linq;
using UnityEngine;

namespace Gunk_Scripts.Maps
{
    [System.Serializable]
    public class PlacedObjectsManager
    {
        private readonly bool[,] fieldUsed;
        private readonly TerrainFieldType[,] fieldTypes;

        private readonly int mapHeight;
        private readonly int mapWidth;

        /// <summary>
        /// constructs object manager where all fields are of type default(TerrainFieldType)
        /// </summary>
        public PlacedObjectsManager(int mapHeight,int mapWidth)
        {
            fieldUsed = new bool[mapWidth,mapHeight];
            fieldTypes = new TerrainFieldType[mapWidth,mapHeight];
            this.mapHeight = mapHeight;
            this.mapWidth = mapWidth;
        }

        private bool isFieldInBoundaries(int xPos,int yPos)
        {
            return xPos >= 0
                   && xPos < mapWidth
                   && yPos >= 0
                   && yPos < mapHeight;
        }

        private bool isFieldValidAndFree(int xPos, int yPos) =>
            isFieldInBoundaries(xPos, yPos) && fieldUsed[xPos, yPos] == false;

        private bool isFieldValidFreeAndRightType(int xPos, int yPos, TerrainFieldType type) =>
            isFieldValidAndFree(xPos,yPos) && fieldTypes[xPos,yPos] == type;

        /// <summary>
        /// returns true if pattern fields types match with fields "under" it and all fields are free
        /// </summary>
        public bool canObjectBeAdded(Vector2Int position, ObjectPattern pattern)
        {
            return pattern.fieldsLocations.TrueForAll(x => isFieldValidFreeAndRightType(position.x+x.x, position.y+x.y, x.type));
        }

        /// <summary>
        /// adds object on certain position and marks fields under it as not free
        /// </summary>
        /// <exception cref="InvalidOperationException"> if any field is wrong type or not free</exception>
        public void addObject(Vector2Int position, ObjectPattern pattern)
        {
            if (!canObjectBeAdded(position, pattern))
            {
                throw new InvalidOperationException("some fields are not free or are wrong type");
            }

            pattern.fieldsLocations.ForEach(x => fieldUsed[position.x + x.x, position.y + x.y] = true);
        }

        /// <summary>
        /// removes object and marks all fields that were under it as free
        /// </summary>
        public void removeObject(Vector2Int position, ObjectPattern pattern)
        {
            pattern.fieldsLocations.ForEach(x => fieldUsed[position.x + x.x, position.y + x.y] = false);
        }

        /// <summary>
        /// changes type of a field in a certain location
        /// </summary>
        /// <exception cref="InvalidOperationException"> if location is outside bounds or field is not free</exception>
        public void changeFieldType(Vector2Int location, TerrainFieldType newFieldType)
        {
            if (!isFieldInBoundaries(location.x, location.y))
            {
                throw new InvalidOperationException("location is outside bounds");
            }
            if (!isFieldValidAndFree(location.x, location.y))
            {
                throw new InvalidOperationException("field that is not free cannot change its type");
            }
            fieldTypes[location.x, location.y] = newFieldType;
        }

        /// <summary>
        /// returns closest available position for object or null if there is none nearby
        /// </summary>
        public Vector2Int? calculateClosestAvailablePosition(Vector2Int position, ObjectPattern pattern)
        {
            Vector2Int? solution = null;

            foreach (var offsetX in Enumerable.Range(-5, 10))
            {
                foreach (var offsetY in Enumerable.Range(-5,10))
                {
                    var distance = Math.Abs(offsetX) + Math.Abs(offsetY);
                    if (canObjectBeAdded(position + new Vector2Int(offsetX, offsetY), pattern) &&
                        (solution == null || distance < Math.Abs(solution.Value.x) + Math.Abs((solution.Value.y))))
                    {
                        solution = new Vector2Int(offsetX, offsetY);
                    }
                }
            }

            return solution + position;
        }

        public TerrainFieldType getFieldType(Vector2Int location)
        {
            if (!isFieldInBoundaries(location.x, location.y))
            {
                throw new InvalidOperationException("location is outside bounds");
            }

            return fieldTypes[location.x, location.y];
        }
    }
}