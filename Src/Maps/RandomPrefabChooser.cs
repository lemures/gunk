using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace Gunk_Scripts.Maps
{
    [System.Serializable]
    public class RandomPrefabChooser
    {
        public string chooserName;

        List<Option>optionsToChoose=new List<Option>();

        private class Option
        {
            public string name;
            public float probability;

            public Option(string name, float probability)
            {
                this.name = name;
                this.probability = probability;
            }
        }

        public RandomPrefabChooser(string chooserName)
        {
            this.chooserName = chooserName;
        }

        private bool optionExists(string name) => optionsToChoose.Exists(x => x.name == name);

        public void addOrReplaceObject(string name, float probabilityWeight)
        {
            if (optionExists(name))
            {
                optionsToChoose.Find(x => x.name == name).probability = probabilityWeight;
            }
            else
            {
                optionsToChoose.Add(new Option(name,probabilityWeight));
            }
        }

        public void eraseObject(string name)
        {
            optionsToChoose.RemoveAll(x => x.name == name);
        }

        public string getRandomObject()
        {
            float probabilitiesTotalWeights = 0;
            optionsToChoose.ForEach(x=>probabilitiesTotalWeights+=x.probability);
            var random = Random.Range(0.0f, probabilitiesTotalWeights);
            float weightsPassedSoFar = 0;
            foreach (var option in optionsToChoose)
            {
                weightsPassedSoFar += option.probability;
                if (weightsPassedSoFar > random)
                {
                    return option.name;
                }
            }

            return default(string);
        }
    }
}