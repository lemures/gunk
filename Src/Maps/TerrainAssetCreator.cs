﻿#if UNITY_EDITOR
using System.IO;
using Gunk_Scripts.SerializationHelpers;
using Gunk_Scripts.Shader_Constants;
using UnityEditor;
using UnityEngine;

namespace Gunk_Scripts.Maps
{
    /// <summary>
    ///     Helper class for creating terrain assets
    ///     can automatically search for map files in folders and produce GameObject with fully created terrain with assigned
    ///     raw terrain material
    ///     files should be located in Assets/Maps/Data/ folder. So far you can include:
    ///     -mapName.r16 [raw height map]
    ///     -mapName_splat [splatMap texture]
    ///     -mapName_normals [normalMap texture]
    /// </summary>
    public static class TerrainAssetCreator
    {
        private const string MAPS_DATA_PATH = "Assets/Maps/Data/";
        private const string SPLAT_MAP_SUFFIX = "_splat";
        private const string NORMAL_MAP_SUFFIX = "_normals";

        private static void loadTerrain(string fileName, TerrainData terrainData)
        {
            var h = terrainData.heightmapHeight;
            var w = terrainData.heightmapWidth;
            var data = new float[h, w];
            var file = File.OpenRead(fileName);
            var reader = new BinaryReader(file);
            for (var y = 0; y < h; y++)
            for (var x = 0; x < w; x++)
            {
                var v = (float) reader.ReadUInt16() / 0xFFFF;
                data[w - y - 1, x] = v;
            }

            terrainData.SetHeights(0, 0, data);
        }

        /// <summary>
        ///     returns path to folder where all terrain resources are located
        /// </summary>
        private static string getMapResourcesPath(string mapName)
        {
            return MAPS_DATA_PATH + mapName;
        }

        /// <summary>
        ///     returns path to the folder where the terrain material should be saved
        /// </summary>
        private static string getMaterialSavePath(string mapName)
        {
            return MAPS_DATA_PATH + mapName + "/Generated";
        }

        /// <summary>
        ///     returns full path of Material for certain mapName
        /// </summary>
        private static string getMaterialFullFilePath(string mapName)
        {
            return Path.Combine(getTerrainDataSavePath(mapName), "Material-" + mapName + ".mat");
        }

        /// <summary>
        ///     returns path to the folder where the terrain .prefab should be saved
        /// </summary>
        private static string getTerrainPrefabSavePath(string mapName)
        {
            return MAPS_DATA_PATH + mapName + "/Generated";
        }

        /// <summary>
        ///     returns path to the folder where TerrainData asset should be saved
        /// </summary>
        private static string getTerrainDataSavePath(string mapName)
        {
            return MAPS_DATA_PATH + mapName + "/Generated";
        }

        /// <summary>
        ///     returns full filepath of TerrainData for certain mapName
        /// </summary>
        private static string getTerrainDataFullFilePath(string mapName)
        {
            return Path.Combine(getTerrainDataSavePath(mapName), "TerrainData-" + mapName + ".asset");
        }

        /// <summary>
        ///     returns full filepath of Prefab for certain mapName
        /// </summary>
        private static string getTerrainPrefabFullFilePath(string mapName)
        {
            return getTerrainPrefabSavePath(mapName) + "/" + mapName + "_Terrain" + ".prefab";
        }

        /// <summary>
        ///     creates terrain GameObject with TerrainData loaded from a proper file
        /// </summary>
        /// <param name="mapName">name of the map for search of files</param>
        /// <returns></returns>
        private static GameObject createTerrain(string mapName)
        {
#if UNITY_EDITOR
            var myTerrainData = new TerrainData
            {
                name = mapName,
                heightmapResolution = 2049,
                size = new Vector3(1024, 400, 1024)
            };

            FileManager.createFolder(getTerrainDataSavePath(mapName));
            AssetDatabase.CreateAsset(myTerrainData, getTerrainDataFullFilePath(mapName));
            loadTerrain(getMapResourcesPath(mapName) + "/" + mapName + ".r16", myTerrainData);
            var prefabCopy = Terrain.CreateTerrainGameObject(myTerrainData);
            FileManager.createFolder(getTerrainPrefabSavePath(mapName));
            var prefab = PrefabUtility.CreatePrefab(getTerrainPrefabFullFilePath(mapName), prefabCopy);
            Object.DestroyImmediate(prefabCopy);

            prefab.GetComponent<TerrainCollider>().terrainData = myTerrainData;
            prefab.GetComponent<Terrain>().materialType = Terrain.MaterialType.Custom;
            prefab.GetComponent<Terrain>().castShadows = false;
            prefab.GetComponent<Terrain>().basemapDistance = 10000.0f;
            prefab.GetComponent<Terrain>().heightmapPixelError = 30;
            prefab.transform.position = new Vector3(0, 0, 0);
            prefab.name = mapName + "_Terrain";

            return prefab;
#else
                return null;
#endif
        }

        private static Material createTerrainMaterial(string mapName)
        {
            var myMat = new Material(Shader.Find(ToonTerrainShaderConstants.shaderName));

            FileManager.createFolder(getMaterialSavePath(mapName));
            AssetDatabase.CreateAsset(myMat, getMaterialFullFilePath(mapName));

            return myMat;
        }

        private static Texture2D findNormalMap(string mapName)
        {
            return AssetDatabase.LoadAssetAtPath(
                getMapResourcesPath(mapName) + "/" + mapName + NORMAL_MAP_SUFFIX + ".bmp",
                typeof(Texture2D)) as Texture2D;
        }

        private static string getMapSaveName(string mapName)
        {
            return Map.getDatabaseName(mapName);
        }


        private static Texture2D findSplatMap(string mapName)
        {
            return AssetDatabase.LoadAssetAtPath(
                getMapResourcesPath(mapName) + "/" + mapName + SPLAT_MAP_SUFFIX + ".bmp",
                typeof(Texture2D)) as Texture2D;
        }

        public static Map importMapAssets(string mapName)
        {
            Debug.Log("importing map assets for map named " + mapName);
            var result = ScriptableObject.CreateInstance<Map>();
            result.hideFlags = HideFlags.HideAndDontSave;
            result.mapName = mapName;
            result.prefab = createTerrain(mapName);
            result.terrainData = result.prefab.GetComponent<Terrain>().terrainData;
            result.splatMap = findSplatMap(mapName);
            result.normalMap = findNormalMap(mapName);
            result.mat = createTerrainMaterial(mapName);
            result.prefab.GetComponent<Terrain>().materialType = Terrain.MaterialType.Custom;
            result.prefab.GetComponent<Terrain>().materialTemplate = result.mat;
            DatabasesManager.saveScriptableObject(result, getMapSaveName(mapName));
            return DatabasesManager.getObjectLatestVersion(getMapSaveName(mapName)) as Map;
        }

        public static void deleteCreatedMapAssets(string mapName)
        {
            AssetDatabase.DeleteAsset(getTerrainPrefabFullFilePath(mapName));
            AssetDatabase.DeleteAsset(getMaterialFullFilePath(mapName));
            AssetDatabase.DeleteAsset(getTerrainDataSavePath(mapName));
            DatabasesManager.deleteAllVersions(getMapSaveName(mapName));

        }
    }
}
#endif