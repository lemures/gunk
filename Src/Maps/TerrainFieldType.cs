namespace Gunk_Scripts.Maps
{
    public enum TerrainFieldType
    {
        Ground,
        Water,
        Slope,
        CrystalMine
    }
}