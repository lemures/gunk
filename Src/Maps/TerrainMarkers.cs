﻿using System;
using System.Collections.Generic;
using Gunk_Scripts;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.Renderers;
using Src.Libraries;
using UnityEngine;

namespace Src.Maps
{
    public static class TerrainMarkers
    {
        private static GameObject rendererGameObject;

        static TerrainMarkers()
        {
            GlobalUpdateCaller.addInvokeEveryFrameFunction(render);
        }

        private class AddedObject
        {
            private static IdManager markerIds = new IdManager();
            private readonly List<int> ids;
            public readonly int id;

            public AddedObject(IEnumerable<int> ids)
            {
                this.ids = new List<int>(ids);
                id = markerIds.getFreeId();
            }

            public List<int> getPoints()
            {
                return new List<int>(ids);
            }

            ~AddedObject()
            {
                markerIds.deregisterId(id);
            }
        }

        private static PlainColorLineRenderer lineRenderer=new PlainColorLineRenderer(MainCamera.getInstance().getCamera());
        private static IdManager idManager = new IdManager();
        private static List<AddedObject>markers = new List<AddedObject>();

        private static float getTerrainHeight(float xPos, float yPos)
        {
            return Terrain.activeTerrain.SampleHeight(new Vector3(xPos, 0, yPos));
        }

        /// <summary>
        /// adds cross marker on map and returns it's id that can be later used to delete it
        /// </summary>
        public static int addCrossMarker(float xPos,float yPos, Color color)
        {
            const float offset = 1.0f;
            var center = new Vector3(xPos, offset+getTerrainHeight(xPos, yPos), yPos);

            var points=new List<Vector3>();
            var ids = new List<int>();

            points.Add(new Vector3(xPos + 0.5f, offset + getTerrainHeight(xPos + 0.5f, yPos + 0.5f), yPos + 0.5f));
            points.Add(new Vector3(xPos + 0.5f, offset + getTerrainHeight(xPos + 0.5f, yPos - 0.5f), yPos - 0.5f));
            points.Add(new Vector3(xPos - 0.5f, offset + getTerrainHeight(xPos - 0.5f, yPos + 0.5f), yPos + 0.5f));
            points.Add(new Vector3(xPos - 0.5f, offset + getTerrainHeight(xPos - 0.5f, yPos - 0.5f), yPos - 0.5f));

            foreach (var point in points)
            {
                int id = idManager.getFreeId();
                lineRenderer.addObject(id,center,point);
                lineRenderer.setWidth(id,0.2f);
                lineRenderer.setColor(id, color);
                ids.Add(id);
            }

            var marker = new AddedObject(ids);
            markers.Add(marker);
            return marker.id;
        }

        /// <summary>
        /// removes marked with given id or throws exception if element with this id does not exist
        /// </summary>
        public static void removeMarker(int id)
        {
            var foundMarker = markers.Find(x => x.id == id);
            if (foundMarker == null)
            {
                throw new InvalidOperationException("marker with this id doesn't exist");
            }
            markers.Remove(foundMarker);
            foundMarker.getPoints().ForEach(lineId => lineRenderer.removeObject(lineId));
        }

        private static void render()
        {
            lineRenderer.render();
        }
    }
}

