using UnityEngine;

namespace Gunk_Scripts.Objects_Selection
{
    public class CameraScreenPositionCalculator : IScreenPositionCalculator
    {
        private readonly Camera camera;

        public CameraScreenPositionCalculator(Camera camera)
        {
            this.camera = camera;
        }

        public Vector3 worldToScreenPoint(Vector3 worldPosition) => camera.WorldToScreenPoint(worldPosition);
    }
}