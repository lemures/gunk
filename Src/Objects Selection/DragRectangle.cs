﻿using UnityEngine;

namespace Gunk_Scripts.Objects_Selection
{
    public class DragRectangle
    {
        public Vector3 firstPosition;
        public Vector3 secondPosition;

        public DragRectangle(Vector3 firstPosition, Vector3 secondPosition)
        {
            this.firstPosition = firstPosition;
            this.secondPosition = secondPosition;
        }

        public override string ToString()
        {
            return "firstPosition: " + firstPosition.ToString() + " secondPosition: " + secondPosition.ToString();
        }
    }
}