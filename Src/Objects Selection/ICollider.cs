﻿using UnityEngine;

namespace Gunk_Scripts.Objects_Selection
{
    public interface ICollider
    {
        Vector3 center { get; set; }
        /// <summary>
        /// returns true if mouse is on object
        /// </summary>
        bool isUnderPoint(IScreenPositionCalculator positionCalculator, Vector2 point);

        /// <summary>
        /// returns true if the object is inside screen space rectangle with given corners
        /// </summary>
        bool isInScreenRectangle(Vector2 leftCorner, Vector2 rightCorner, IScreenPositionCalculator positionCalculator);
    }
}