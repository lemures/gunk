using Gunk_Scripts.SerializationHelpers;
using UnityEngine;

namespace Gunk_Scripts.Objects_Selection
{
    public interface IColliderCollection
    {
        bool hasCollider(int id);
        void addCollider(int id, ICollider colliderToAdd, SelectionPriority priority);
        void removeCollider(int id);
        void setColliderCenter(int id, Vector3 newCenter);
    }
}