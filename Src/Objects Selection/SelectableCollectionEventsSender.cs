﻿using System;
using System.Collections.Generic;
using Gunk_Scripts.SerializationHelpers;
using UnityEngine;

namespace Gunk_Scripts.Objects_Selection
{
    /// <summary>
    /// class that can invoke callback functions for certain events related to objects Being Selected
    /// </summary>
    public class SelectableCollectionEventsSender:MonoBehaviour, IColliderCollection
    {
        private IPointer clicksManager;
        [SerializeField]
        private SelectableColliderCollection colliderCollection;

        public Action<List<int>> objectsTemporarilySelectedAction { set; private get; }
        public Action<List<int>> objectsSelectedAction { set; private get; }
        public Action<int> objectClickedAction { set; private get; }
        public Action<int> objectDoubleClickedAction { set; private get; }
        public Action<int> objectRightClickedAction { set; private get; }

        public void removeCollider(int id)
        {
            colliderCollection.removeCollider(id);
        }

        public bool hasCollider(int id) => colliderCollection.hasCollider(id);

        public void addCollider(int id, ICollider colliderToAdd, SelectionPriority priority)
        {
            colliderCollection.addCollider(id, colliderToAdd, priority);
        }

        public void setColliderCenter(int id, Vector3 newCenter)
        {
            colliderCollection.setColliderCenter(id, newCenter);
        }

        private void processClickedObject()
        {
            if (!clicksManager.wasClicked()) return;

            var clickedObject = colliderCollection.getObjectUnderPointer(clicksManager.getPointerPosition());
            if (clickedObject == null) return;

            objectClickedAction?.Invoke(clickedObject.Value);
        }

        private void processDoubleClickedObject()
        {
            if (!clicksManager.wasDoubleClicked()) return;

            var doubleClickedObject = colliderCollection.getObjectUnderPointer(clicksManager.getPointerPosition());
            if (doubleClickedObject == null) return;

            objectDoubleClickedAction?.Invoke(doubleClickedObject.Value);
        }

        private void processTemporarilySelectedObjects()
        {
            var selectedRectangle = clicksManager.getDragRectangle();
            if (selectedRectangle == null) return;

            objectsTemporarilySelectedAction?.Invoke(colliderCollection.getObjectsInRectangle(selectedRectangle));
        }

        private void processRightClickedObject()
        {
            if (!clicksManager.wasRightClicked()) return;

            var rightClickedObject = colliderCollection.getObjectUnderPointer(clicksManager.getPointerPosition());
            if (rightClickedObject == null) return;

            objectRightClickedAction?.Invoke(rightClickedObject.Value);
        }

        private void processSelectedObjects()
        {
            if (!clicksManager.hasJustStoppedDragging()) return;

            var selectedRectangle = clicksManager.getDragRectangle();
            if (selectedRectangle == null) return;

            objectsSelectedAction?.Invoke(colliderCollection.getObjectsInRectangle(selectedRectangle));
        }

        /// <summary>
        /// will be called automatically each frame in Player
        /// </summary>
        public void Update()
        {
            processClickedObject();
            processDoubleClickedObject();
            processTemporarilySelectedObjects();
            processRightClickedObject();
            processSelectedObjects();
        }

        private SelectableCollectionEventsSender()
        {
        }

        public static SelectableCollectionEventsSender createInstance(IScreenPositionCalculator positionCalculator, IPointer pointer)
        {
            if (positionCalculator == null)
            {
                throw new NullReferenceException("position calculator can not be null");
            }

            var updateObject = new GameObject{hideFlags = HideFlags.HideInHierarchy};
            var component=updateObject.AddComponent<SelectableCollectionEventsSender>();
            updateObject.name = "SelectedObjectsManager " + component.GetHashCode();
            component.colliderCollection = new SelectableColliderCollection(positionCalculator);
            component.clicksManager = pointer;
            return component;
        }
    }
}
