using Gunk_Scripts.SerializationHelpers;

namespace Gunk_Scripts.Objects_Selection
{
    public static class SelectionPriorityUtils
    {
        public static SelectionPriority getUnitSelectionPriority() => SelectionPriority.VeryHigh;

        public static SelectionPriority getWorkerSelectionPriority() => SelectionPriority.High;

        public static SelectionPriority getWeldingBotSelectionPriority() => SelectionPriority.Medium;

        public static SelectionPriority getBuildingSelectionPriority() => SelectionPriority.Low;

        public static SelectionPriority getNonSpecifiedGroupSelectionPriority() => SelectionPriority.VeryLow;
    }
}