﻿using System;
using Gunk_Scripts.Data_Structures;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    /// <summary>
    /// class that will draw quad meshes with given material to a renderTexture
    /// three buffers will be set:
    /// billboardPositions which will contain position set when adding object;
    /// billboardSizes which will contain billboard size that was previously set
    /// billboardColor which will contain billboard colors
    /// </summary>
    public class BillboardRenderer : IBillboardRenderer
    {
        public GameObject camera;
        public RenderTexture renderTexture;

        private RenderTexture installAdditionalCamera(GameObject cam, RenderTexture textureToRenderTo, int textureSizeX,
            int textureSizeY,
            CameraClearFlags flags, int layer, float scale)
        {
            if (textureToRenderTo != null) textureToRenderTo.DiscardContents();
            textureToRenderTo = new RenderTexture((int)(scale * textureSizeX) , (int)(scale * textureSizeY) , 0);
            textureToRenderTo.Create();

            cam.transform.position = new Vector3((float)textureSizeX / 2, 0, (float)textureSizeY / 2);
            cam.transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));

            var cameraComp = cam.GetComponent<Camera>();
            cameraComp.orthographic = true;
            cameraComp.renderingPath = RenderingPath.Forward;
            cameraComp.orthographicSize = textureSizeY / 2.0f;
            cameraComp.aspect = textureSizeX / (float) textureSizeY;
            cameraComp.targetTexture = textureToRenderTo;
            cameraComp.cullingMask = 1 << layer;
            cameraComp.allowMSAA = false;
            cameraComp.backgroundColor = new Color32(0, 0, 0, 0);
            cameraComp.clearFlags = flags;
            cameraComp.enabled = false;
            return textureToRenderTo;
        }

        private Vector3[] positions;
        private Vector2[] sizes;
        private Color[] colors;

        private string positionsBufferName = "billboardPositions";
        private string sizesBufferName = "billboardSizes";
        private string colorsBufferName = "billboardColors";

        private Mesh quad;
        private Bounds bounds = new Bounds(Vector3.zero, new Vector3(100000.0f, 100000.0f, 100000.0f));
        private IndirectRenderer quadRenderer;

        private IdMap idMap;

        public void updateRenderTextureSize(int newWidth, int newHeight)
        {
            if (renderTexture.height == newHeight && renderTexture.width == newWidth)
            {
                //nothing to do
                return;
            }

            renderTexture=installAdditionalCamera(camera, renderTexture, newWidth, newHeight, CameraClearFlags.SolidColor, 10, 1.0f);
        }

        public void render()
        {
            positionsBuffer.SetData(positions);
            sizesBuffer.SetData(sizes);
            colorsBuffer.SetData(colors);
            quadRenderer.setInstanceCount(idMap.getObjectCount());
            quadRenderer.render();
            camera.GetComponent<Camera>().Render();
        }

        /// <summary>
        /// adds new object that will be drawn with it's lower left corner equal to position
        /// </summary>
        /// <param name="id"></param>
        /// <param name="position"></param>
        public void addObjectCenter(int id, Vector3 position, Vector2 size)
        {
            //BUG objects aren't drawn in order told by y position
            if (Math.Abs(position.y) >= 100.0f)
            {
                throw new ArgumentOutOfRangeException();
            }
            idMap.addObject(id);
            int mappedId = idMap.getMappedId(id);
            if (mappedId == currentlyAllocated)
            {
                currentlyAllocated *= 2;
                resize(currentlyAllocated);
            }

            sizes[mappedId] = size;
            positions[idMap.getMappedId(id)] = position;
        }

        public void addObjectRightLowerCorner(int id, Vector3 position, Vector2 size)
        {
            addObjectCenter(id, position + new Vector3(size.x / 2, 0, size.y / 2), size);
        }

        public void setColor(int id, Color color)
        {
            colors[idMap.getMappedId(id)] = color;
        }

        public void removeObject(int id)
        {
            idMap.removeObject(id);
        }

        public void removeAllObjects()
        {
            idMap.removeAllObjects();
        }

        public void updatePosition(int id, float newX, float newY, float newZ)
        {
            positions[idMap.getMappedId(id)] = new Vector3(newX, newY, newZ);
        }

        public RenderTexture getRenderTexture()
        {
            return renderTexture;
        }

        public void setCameraClearFlags(CameraClearFlags flags)
        {
            camera.GetComponent<Camera>().clearFlags = flags;
        }

        private int currentlyAllocated = 1;

        private void resize(int newSize)
        {
            Array.Resize(ref positions, newSize);
            Array.Resize(ref sizes,newSize);
            Array.Resize(ref colors, newSize);

            positionsBuffer.Release();
            sizesBuffer.Release();
            colorsBuffer.Release();

            positionsBuffer = new ComputeBuffer(currentlyAllocated, 12);
            sizesBuffer = new ComputeBuffer(currentlyAllocated, 8);
            colorsBuffer = new ComputeBuffer(currentlyAllocated, 16);

            quadRenderer.setBuffer(positionsBufferName, positionsBuffer);
            quadRenderer.setBuffer(sizesBufferName, sizesBuffer);
            quadRenderer.setBuffer(colorsBufferName, colorsBuffer);
        }

        ComputeBuffer positionsBuffer;
        ComputeBuffer sizesBuffer;
        ComputeBuffer colorsBuffer;

        public BillboardRenderer(int textureSizeX, int textureSizeY,float renderTextureScale, Material material)
        {
            if (material == null)
            {
                throw new NullReferenceException("material is null");
            }

            idMap = new IdMap();

            positions = new Vector3[currentlyAllocated];
            sizes = new Vector2[currentlyAllocated];
            colors = new Color[currentlyAllocated];

            camera = new GameObject("Billboard Renderer Camera");
            camera.AddComponent<Camera>();
            renderTexture = installAdditionalCamera(camera, renderTexture, textureSizeX, textureSizeY, CameraClearFlags.SolidColor, 10, renderTextureScale);

            colorsBuffer = new ComputeBuffer(currentlyAllocated, 16);
            positionsBuffer = new ComputeBuffer(currentlyAllocated, 12);
            sizesBuffer = new ComputeBuffer(currentlyAllocated, 8);

            quad = (DatabasesManager.getObjectLatestVersion("Quad") as GameObject)?.GetComponent<MeshFilter>().sharedMesh;
            if (quad == null)
            {
                throw new MissingResourcesException("quad");
            }

            quadRenderer = new IndirectRenderer(quad, material, camera.GetComponent<Camera>(), bounds, 10);

            quadRenderer.setBuffer(positionsBufferName, positionsBuffer);
            quadRenderer.setBuffer(sizesBufferName, sizesBuffer);
            quadRenderer.setBuffer(colorsBufferName, colorsBuffer);
        }
    }
}
