﻿using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.Data_Structures;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public class CableRenderer
    {
        private InstancedLineRenderer lineRenderer;

        public CableRenderer(Camera camera)
        {
            var material = DatabasesManager.getObjectLatestVersion("CableMat") as Material;
            lineRenderer = new InstancedLineRenderer(material, camera);
        }

        private List<Edge> allConnections = new List<Edge>();

        public void addCable(Edge edge, Vector3 startPosition, Vector3 endPosition)
        {
            lineRenderer.addObject(edge.GetHashCode(), startPosition, endPosition);
            allConnections.Add(edge);
            lineRenderer.setWidth(edge.GetHashCode() , 5.0f);
        }

        public void removeCable(Edge edge)
        {
            lineRenderer.removeObject(edge.GetHashCode());
            allConnections.Remove(edge);
        }

        public List<Edge> getAllConnections() => allConnections.ToList();

        public void render()
        {
            lineRenderer.render();
        }
    }
}
