﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.GUI;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    /// <summary>
    /// fullScreen GuiElement that automatically renders all added counters every frame
    /// </summary>
    public class CountersRenderer:GuiElement, ICountersCollection
    {
        private readonly GuiFullscreenBillboardRenderer renderer;
        private GuiFullScreenImage guiFullScreenImage;

        private readonly Camera camera;

        public CountersRenderer(Camera camera) : base(0,0,0,"GuiCountersRenderer",null)
        {
            renderer = new GuiFullscreenBillboardRenderer(camera);
            this.camera = camera;

            GuiManager.updateCaller.addFunction(this, render);

        }

        private class ObjectCountersCollection
        {
            public class CounterDrawOrder
            {
                public bool isVisible;
                public float value;
                public float maxValue;
                public readonly DrawStyle drawStyle;

                public CounterDrawOrder(DrawStyle drawStyle)
                {
                    this.drawStyle = drawStyle;
                    isVisible = false;
                }

            }

            private readonly List<CounterDrawOrder> counters = new List<CounterDrawOrder>();
            public Vector3 worldPosition;
            public readonly int objectId;

            public ObjectCountersCollection(int objectId)
            {
                this.objectId = objectId;
            }

            public CounterDrawOrder getCounterOrCreateNewOne(DrawStyle drawStyle)
            {
                var result = counters.Find(x => x.drawStyle == drawStyle);
                if (result != null) return result;

                result = new CounterDrawOrder(drawStyle);
                counters.Add(result);
                return result;
            }

            public void setCounterValue(DrawStyle drawStyle, float newValue) => getCounterOrCreateNewOne(drawStyle).value = newValue;

            public void setCounterMaxValue(DrawStyle drawStyle, float newMaxValue) => getCounterOrCreateNewOne(drawStyle).maxValue = newMaxValue;

            public List<CounterDrawOrder> getAllVisibleCounters() => (from counterDrawOrder in counters
                where counterDrawOrder.isVisible
                select counterDrawOrder).ToList();

            public void removeCounter(DrawStyle drawStyle)
            {
                counters.RemoveAll(x => x.drawStyle == drawStyle);
            }

            public int calculateCounterSortedIndex(DrawStyle style) => getAllVisibleCounters().FindIndex(x => x.drawStyle == style);
        }

        public enum DrawStyle
        {
            Health,
            Energy,
            Progress
        }

        private readonly List<ObjectCountersCollection> counters = new List<ObjectCountersCollection>();

        private ObjectCountersCollection findCounterCollectionOrCreateNewOne(int objectId)
        {
            var result=counters.Find(x => objectId == x.objectId);
            if (result != null) return result;

            result = new ObjectCountersCollection(objectId);
            counters.Add(result);

            return result;
        }

        public void addHiddenCounter(int objectId, Counter counter, DrawStyle style)
        {
            findCounterCollectionOrCreateNewOne(objectId).getCounterOrCreateNewOne(style);
            setCounterValue(objectId, style, counter.currentValue);
            setCounterMaxValue(objectId, style, counter.maxValue);
        }

        public void showCounter(int objectId, DrawStyle style)
        {
            findCounterCollectionOrCreateNewOne(objectId).getCounterOrCreateNewOne(style).isVisible = true;
        }

        public void hideCounter(int objectId, DrawStyle style)
        {
            findCounterCollectionOrCreateNewOne(objectId).getCounterOrCreateNewOne(style).isVisible = false;
        }

        public void setObjectCountersPositions(int objectId, Vector3 newWorldPosition)
        {
            findCounterCollectionOrCreateNewOne(objectId).worldPosition = newWorldPosition;
        }

        public void hideAndResetObjectCounters(int objectId)
        {
            counters.RemoveAll(x => x.objectId == objectId);
        }

        public void setCounterValue(int objectId, DrawStyle style, float newValue) =>
            findCounterCollectionOrCreateNewOne(objectId).setCounterValue(style, newValue);

        public void setCounterMaxValue(int objectId, DrawStyle style, float newMaxValue) =>
            findCounterCollectionOrCreateNewOne(objectId).setCounterMaxValue(style, newMaxValue);

        private struct BillboardDrawOrder
        {
            public readonly Vector3 position;
            public readonly Vector2 size;
            public readonly Color color;

            public BillboardDrawOrder(Vector3 position, Vector2 size, Color color)
            {
                this.position = position;
                this.size = size;
                this.color = color;
            }
        }

        private static class DrawOrdersGenerator
        {
            private static Color getEnergyBarColor() => Color.white;

            private static Color getHealthBarColor(float percentage)
            {
                if (percentage < 10.0f)
                {
                    return Color.red;

                }
                if (percentage < 30.0f)
                {
                    return new Color32(255, 128, 0, 255);
                }
                if (percentage < 75.0f)
                {
                    return Color.yellow;
                }

                return Color.green;
            }

            private static Color getProgressBarColor() => Color.yellow;

            private static Color getBarColor(DrawStyle style, float percentage)
            {
                switch (style)
                {
                    case DrawStyle.Health:
                        return getHealthBarColor(percentage);
                    case DrawStyle.Energy:
                        return getEnergyBarColor();
                    case DrawStyle.Progress:
                        return getProgressBarColor();
                    default:
                        throw new Exception("draw style not supported");
                }
            }

            private static Vector2Int worldToScreenFunction(Vector3 worldPosition, Camera camera)
            {
                var unscaledPosition = camera.WorldToScreenPoint(worldPosition);
                if (unscaledPosition.z < 0)
                {
                    return new Vector2Int(-10000, -10000);
                }
                return new Vector2Int((int)(unscaledPosition.x), (int)(unscaledPosition.y));
            }

            public static List<BillboardDrawOrder> calculateDrawOrders(ObjectCountersCollection collection, Camera camera)
            {
                Vector3 worldPosition = collection.worldPosition;
                Vector2Int screenPosition = worldToScreenFunction(worldPosition,camera);
                List<BillboardDrawOrder> result = new List<BillboardDrawOrder>();
                foreach (var counterToDraw in collection.getAllVisibleCounters())
                {
                    int sortedIndex = collection.calculateCounterSortedIndex(counterToDraw.drawStyle);
                    float percentage = calculateCounterPercentage(counterToDraw);
                    float sortOrderValue = calculateSortOrderValueBasedOnScreenPosition(screenPosition);
                    var barScreenPosition = screenPosition - new Vector2Int(0, (sortedIndex * (1 + standardCounterHeight)));
                    var barColor = getBarColor(counterToDraw.drawStyle, percentage);

                    result.AddRange(calculateStandardDrawOrders(new Vector2Int(50, standardCounterHeight), percentage, barColor , barScreenPosition, sortOrderValue));
                }

                return result;
            }

            private static int standardCounterHeight = 10;

            /// <summary>
            /// calculates sort order value from range [0,99] based on objects position
            /// </summary>
            private static float calculateSortOrderValueBasedOnScreenPosition(Vector2 screenPosition)
            {
                float sortOrderValue = Mathf.Clamp01(screenPosition.y / Screen.height) * 99.0f;
                return sortOrderValue;
            }

            private static float calculateCounterPercentage(ObjectCountersCollection.CounterDrawOrder counter)
            {
                return 100.0f * counter.value / counter.maxValue;
            }

            private static IEnumerable<BillboardDrawOrder> calculateStandardDrawOrders(Vector2 dimensions,
                float barWidthInPercent, Color barColor, Vector2Int lowerLeftCorner, float sortOrderValue)
            {
                var transparencyLevel = 0.8f;
                var drawOrders = new List<BillboardDrawOrder>();

                //background
                var backgroundColor = Color.white*0.05f;
                backgroundColor.a = transparencyLevel;
                drawOrders.Add(new BillboardDrawOrder(new Vector3(lowerLeftCorner.x, sortOrderValue-0.001f, lowerLeftCorner.y), dimensions,
                    backgroundColor));

                //bar background
                var fullBarDimensions = dimensions - new Vector2(2, 2);
                var barBackgroundColor = Color.white*0.1f;
                barBackgroundColor.a = transparencyLevel;
                drawOrders.Add(new BillboardDrawOrder(new Vector3(lowerLeftCorner.x+1, sortOrderValue - 0.002f, lowerLeftCorner.y+1), fullBarDimensions, barBackgroundColor));

                //bar itself
                var barDimensions = dimensions - new Vector2(2, 2);
                barColor.a = transparencyLevel;
                barDimensions.x *= barWidthInPercent / 100.0f;
                drawOrders.Add(new BillboardDrawOrder(new Vector3(lowerLeftCorner.x + 1, sortOrderValue - 0.003f, lowerLeftCorner.y + 1), barDimensions, barColor));
                return drawOrders;
            }
        }

        private void sortAndSendDrawOrdersToRenderer(List<BillboardDrawOrder> drawOrders)
        {
            UnityEngine.Profiling.Profiler.BeginSample("Sorting and Sending");

            //billboards "higher" on the screen should be behind those that are lower
            drawOrders.Sort((x, y) => (-x.position.y).CompareTo(-y.position.y));
            var id = 0;
            foreach (var billboardDrawOrder in drawOrders)
            {
                renderer.addObjectRightLowerCorner(id, billboardDrawOrder.position, billboardDrawOrder.size);
                renderer.setColor(id, billboardDrawOrder.color);
                id++;
            }
            UnityEngine.Profiling.Profiler.EndSample();
        }

        private List<BillboardDrawOrder> getBillboardDrawOrders()
        {
            UnityEngine.Profiling.Profiler.BeginSample("Generating");
            var drawOrders = new List<BillboardDrawOrder>();
            counters.ForEach(x => drawOrders.AddRange(DrawOrdersGenerator.calculateDrawOrders(x,camera)));
            UnityEngine.Profiling.Profiler.EndSample();
            return drawOrders;
        }

        private void render()
        {
            UnityEngine.Profiling.Profiler.BeginSample("Gui counter rendering");
            UnityEngine.Profiling.Profiler.BeginSample("Generating and sending draw commands");

            renderer.removeAllObjects();

            var drawOrders = getBillboardDrawOrders();
            sortAndSendDrawOrdersToRenderer(drawOrders);

            UnityEngine.Profiling.Profiler.EndSample();

            UnityEngine.Profiling.Profiler.BeginSample("rendering");
            renderer.render();
            UnityEngine.Profiling.Profiler.EndSample();
            UnityEngine.Profiling.Profiler.EndSample();
        }
    }
}
