using System;
using Gunk_Scripts.GUI;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public class DragRectangleRenderer : MonoBehaviour
    {
        private GuiFullscreenBillboardRenderer billboardRenderer;
        private static DragRectangleRenderer instance;
        private int id;
        public static void drawDragRectangle(Vector2 p1, Vector2 p2)
        {
            var center = (p1 + p2) / 2.0f;
            var size = (p1 - p2);
            size.x = Math.Abs(size.x);
            size.y = Math.Abs(size.y);

            //border
            instance.billboardRenderer.addObjectCenter(instance.id,new Vector3(center.x,0,center.y),size);
            instance.billboardRenderer.setColor(instance.id,Color.white*0.6f);
            instance.id++;

            //insides
            instance.billboardRenderer.addObjectCenter(instance.id,new Vector3(center.x,0,center.y),size - new Vector2(2,2));
            instance.billboardRenderer.setColor(instance.id,Color.white*0.15f);
            instance.id++;
        }

        private void render()
        {
            id = 0;
            billboardRenderer.render();
            billboardRenderer.removeAllObjects();
        }

        public static void install(Camera camera)
        {
            var go=new GameObject(typeof(DragRectangleRenderer).ToString());
            instance = go.AddComponent<DragRectangleRenderer>();
            instance.billboardRenderer = new GuiFullscreenBillboardRenderer(camera);
            GuiManager.updateCaller.addFunction(instance.billboardRenderer,instance.render);
        }
    }
}