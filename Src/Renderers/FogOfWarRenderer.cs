﻿using System;
using Gunk_Scripts.Data_Structures;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    [Serializable]
    public class FogOfWarRenderer : IFogOfWarObjectsCollection
    {
        private class CamerasManager
        {
            private GameObject additionalCameras;

            public GameObject fowCamera1;
            public GameObject fowCamera2;

            public RenderTexture fowRenderTexture1;
            public RenderTexture fowRenderTexture2;

            private RenderTexture installAdditionalCamera(
                GameObject cam,
                int mapSizeX,
                int mapSizeY,
                CameraClearFlags flags,
                int layer,
                int scale)
            {
                var textureToRenderTo = new RenderTexture(scale * mapSizeX / 2, scale * mapSizeY / 2,0);
                textureToRenderTo.Create();

                cam.transform.position = new Vector3(mapSizeX / 2.0f, 0, mapSizeY / 2.0f);
                cam.transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));

                var cameraComp = cam.AddComponent<Camera>();
                cameraComp.orthographic = true;
                cameraComp.renderingPath = RenderingPath.Forward;
                cameraComp.orthographicSize = mapSizeX / 2.0f;
                cameraComp.targetTexture = textureToRenderTo;
                cameraComp.cullingMask = 1 << layer;
                cameraComp.allowMSAA = false;
                cameraComp.backgroundColor = new Color32(0, 0, 0, 0);
                cameraComp.clearFlags = flags;
                cameraComp.enabled = false;
                return textureToRenderTo;
            }

            public void installCamerasAndRenderTextures(int mapSizeX, int mapSizeY)
            {

                additionalCameras = new GameObject("Additional_cameras");

                fowCamera1 = new GameObject("FOW Camera1");
                fowCamera1.transform.parent = additionalCameras.transform;
                fowRenderTexture1 =
                    installAdditionalCamera(fowCamera1, mapSizeX, mapSizeY, CameraClearFlags.Color,
                        9, 1);

                fowCamera2 = new GameObject("FOW Camera2");
                fowCamera2.transform.parent = additionalCameras.transform;
                fowRenderTexture2 =
                    installAdditionalCamera(fowCamera2, mapSizeX, mapSizeY, CameraClearFlags.Nothing,
                        9, 1);

                KernelUtils.setGlobalTexture("FOW_RenderTexture1", fowRenderTexture1);
                KernelUtils.setGlobalTexture("FOW_RenderTexture2", fowRenderTexture2);
            }
        }

        private readonly CamerasManager camerasManager = new CamerasManager();

        private readonly Bounds bounds = new Bounds(new Vector3(0.0f, -500.0f, 0.0f), new Vector3(100000.0f, 100000.0f, 100000.0f));
        private readonly IndirectRenderer quadRenderer1;
        private readonly IndirectRenderer quadRenderer2;
        private readonly Vector3[] positions;
        private readonly float[] ranges;
        private static bool instanceExists = false;

        private readonly string positionsBufferName = "fogOfWarPositions";
        private readonly string rangesBufferName = "fogOfWarRanges";

        private readonly IdMap idMap;

        public void render()
        {
            UnityEngine.Profiling.Profiler.BeginSample("rendering fog of war");
            KernelUtils.setInt("FOW_enabled", GraphicsSettingsManager.currentSettings.forOfWarEnabled ? 1 : 0);
            KernelUtils.setBufferData(positionsBufferName, positions);
            KernelUtils.setBufferData(rangesBufferName, ranges);
            quadRenderer1.setInstanceCount(idMap.getObjectCount());
            quadRenderer2.setInstanceCount(idMap.getObjectCount());
            quadRenderer1.render();
            quadRenderer2.render();
            camerasManager.fowCamera1.GetComponent<Camera>().Render();
            camerasManager.fowCamera2.GetComponent<Camera>().Render();
            UnityEngine.Profiling.Profiler.EndSample();
        }

        public bool hasObject(int id)
        {
            return idMap.getMappedId(id) != -1;
        }

        public void addObject(int id, Vector3 position)
        {
            idMap.addObject(id);
            positions[idMap.getMappedId(id)] = position;
            ranges[idMap.getMappedId(id)] = 10;
        }

        public void setRange(int id, float range)
        {
            ranges[idMap.getMappedId(id)] = range;
        }

        public void updatePosition(int id, Vector3 position)
        {
            positions[idMap.getMappedId(id)] = position;
        }

        public void removeObject(int id)
        {
            idMap.removeObject(id);
        }

        public RenderTexture getActivelyVisibleTerrainRenderTexture()
        {
            return camerasManager.fowRenderTexture1;
        }

        public RenderTexture getDiscoveredTerrainRenderTexture()
        {
            return camerasManager.fowRenderTexture2;
        }

        /// <summary>
        /// returns minimal value of red channel of renderTextures that is treated as visible in shaders
        /// </summary>
        /// <returns></returns>
        public static float getVisiblePixelThreshold() => 0.2f;

        public FogOfWarRenderer(int maxObjectCount,int mapHeight,int mapWidth)
        {
            idMap = new IdMap();
            if (instanceExists)
            {
                Debug.Log("you really don't want to have more than one instance of FogOfWarRenderer as they render to texture with a fixed name set as global property");
            }
            positions = new Vector3[maxObjectCount];
            ranges = new float[maxObjectCount];
            KernelUtils.initializeEmptyBuffer(positionsBufferName, maxObjectCount, 12, false);
            KernelUtils.initializeEmptyBuffer(rangesBufferName, maxObjectCount, 4, false);
            camerasManager.installCamerasAndRenderTextures(mapWidth, mapHeight);

            var quad = (DatabasesManager.getObjectLatestVersion("Quad") as GameObject)?.GetComponent<MeshFilter>().sharedMesh;
            var fowMaterial = DatabasesManager.getObjectLatestVersion("FOW mat") as Material;
            if (fowMaterial == null)
            {
                throw new MissingResourcesException("FOW mat");
            }

            if (quad == null)
            {
                throw new MissingResourcesException("quad");
            }

            quadRenderer1 = new IndirectRenderer(quad, fowMaterial, camerasManager.fowCamera1.GetComponent<Camera>(), bounds, 9);
            quadRenderer2 = new IndirectRenderer(quad, fowMaterial, camerasManager.fowCamera2.GetComponent<Camera>(), bounds, 9);
        }
    }

}