﻿using Gunk_Scripts.Maps;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public class GrassRenderer
    {
        private static string grassLogicKernelName = "Grass_Logic";
        private static string grassKernelCounterIDsBuffer = "Grass_Active_IDs";
        private Material grassMatLow;
        private Material grassMatHigh;
        private Mesh plane;
        private IndirectRenderer renderer;

        public GrassRenderer()
        {
            if (MainCamera.getInstance().getCamera() == null)
            {
                throw new LibriaryUninitializedException("MainCamera");
            }

            grassMatLow = (Material)DatabasesManager.getObjectLatestVersion("Grass_LOW");
            grassMatHigh = (Material)DatabasesManager.getObjectLatestVersion("GrassMaterial");
            plane = ((GameObject)DatabasesManager.getObjectLatestVersion("Plane")).GetComponent<MeshFilter>().sharedMesh;

            if (plane == null)
            {
                throw new MissingResourcesException("plane");
            }

            if (grassMatHigh == null)
            {
                throw new MissingResourcesException("grassMatHigh");
            }

            if (grassMatLow == null)
            {
                throw new MissingResourcesException("grassMatLow");
            }

            KernelUtils.initializeEmptyBuffer(grassKernelCounterIDsBuffer, 20000, 4, false, ComputeBufferType.Counter);

            renderer = new IndirectRenderer(plane, grassMatHigh, MainCamera.getInstance().getCamera(), grassKernelCounterIDsBuffer);
        }

        public void render()
        {
            KernelUtils.getBuffer(grassKernelCounterIDsBuffer).SetCounterValue(0);
            KernelUtils.setFloat("grass_mode", (int)GraphicsSettingsManager.currentSettings.grassMode);
            if ((int) GraphicsSettingsManager.currentSettings.grassMode != 0)
            {
                KernelUtils.dispatch(grassLogicKernelName, 1 + (NewMapManager.getCurrentMapHeight() / 100),
                    1 + (NewMapManager.getCurrentMapWidth() / 100));
                renderer.material = (int) GraphicsSettingsManager.currentSettings.grassMode == 1 ? grassMatLow : grassMatHigh;
                renderer.render();

            }
        }
    }
}
