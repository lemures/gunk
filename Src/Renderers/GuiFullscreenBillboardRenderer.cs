using System;
using Gunk_Scripts.GUI;
using Gunk_Scripts.Renderers;
using UnityEngine;
using BillboardRenderer = Gunk_Scripts.Renderers.BillboardRenderer;

namespace Gunk_Scripts
{
    public class GuiFullscreenBillboardRenderer : GuiElement, IBillboardRenderer
    {
        private BillboardRenderer renderer;
        private GuiFullScreenImage guiFullScreenImage;

        private Camera camera;

        public GuiFullscreenBillboardRenderer(Camera camera) : base(0,0,0,"GuiFullScreenBillboardRenderer",null)
        {
            var material=DatabasesManager.getObjectLatestVersion("TestBillboardRenderer") as Material;
            renderer = new BillboardRenderer(Screen.width, Screen.height, 1.0f, material);
            guiFullScreenImage = new GuiFullScreenImage(renderer.renderTexture);
            guiFullScreenImage.attachToObject(this);
            this.camera = camera;
        }

        public void updateRenderTextureSize(int newWidth, int newHeight) =>
            renderer.updateRenderTextureSize(newWidth, newHeight);

        public void addObjectCenter(int id, Vector3 position, Vector2 size)
        {
            renderer.addObjectCenter(id, position, size);
            setColor(id, Color.white);
        }

        public void addObjectRightLowerCorner(int id, Vector3 position, Vector2 size)
        {
            renderer.addObjectRightLowerCorner(id, position, size);
            setColor(id, Color.white);
        }

        public void setColor(int id, Color color) => renderer.setColor(id, color);

        public void removeObject(int id) => renderer.removeObject(id);

        public void removeAllObjects() => renderer.removeAllObjects();

        public void updatePosition(int id, float newX, float newY, float newZ) =>
            renderer.updatePosition(id, newX, newY, newZ);

        public RenderTexture getRenderTexture()=> renderer.renderTexture;

        private void updateTextureSizesBasedOnScreenSize()
        {
            renderer.updateRenderTextureSize(Screen.width, Screen.height);
            guiFullScreenImage.setImage(renderer.renderTexture);
        }

        public void render()
        {
            updateTextureSizesBasedOnScreenSize();
            renderer.render();
        }
    }
}