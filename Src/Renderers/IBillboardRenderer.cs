﻿using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public interface IBillboardRenderer
    {
        void updateRenderTextureSize(int newWidth, int newHeight);
        void render();

        /// <summary>
        /// adds new object that will be drawn with it's lower left corner equal to position
        /// </summary>
        void addObjectCenter(int id, Vector3 position, Vector2 size);

        void addObjectRightLowerCorner(int id, Vector3 position, Vector2 size);
        void setColor(int id, Color color);
        void removeObject(int id);
        void removeAllObjects();
        void updatePosition(int id, float newX, float newY, float newZ);
        RenderTexture getRenderTexture();
    }
}