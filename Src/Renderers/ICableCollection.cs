﻿using Gunk_Scripts.Data_Structures;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public interface ICableCollection
    {
        void addCable(Edge edge, Vector3 startPosition, Vector3 endPosition);
        void removeCable(Edge edge);
    }
}