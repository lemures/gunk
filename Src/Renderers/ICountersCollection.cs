﻿using Gunk_Scripts.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public interface ICountersCollection
    {
        void addHiddenCounter(int objectId, Counter counter, CountersRenderer.DrawStyle style);
        void showCounter(int objectId, CountersRenderer.DrawStyle style);
        void hideCounter(int objectId, CountersRenderer.DrawStyle style);
        void setObjectCountersPositions(int objectId, Vector3 newWorldPosition);
        void hideAndResetObjectCounters(int objectId);
        void setCounterValue(int objectId, CountersRenderer.DrawStyle style, float newValue);
        void setCounterMaxValue(int objectId, CountersRenderer.DrawStyle style, float newMaxValue);
    }
}