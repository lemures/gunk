﻿using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public interface IFogOfWarObjectsCollection
    {
        bool hasObject(int id);
        void addObject(int id, Vector3 position);
        void setRange(int id, float range);
        void updatePosition(int id, Vector3 position);
        void removeObject(int id);
    }
}