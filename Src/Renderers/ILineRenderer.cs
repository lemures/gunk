﻿using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public interface ILineRenderer
    {
        void render();

        void addObject(int id, Vector3 startPosition, Vector3 endPosition);

        void updatePosition(int id, Vector3 newStart, Vector3 newEnd);

        void removeObject(int id);

        List<int> getExistingObjectsIds();

        Vector3 getStartPosition(int id);

        Vector3 getEndPosition(int id);
    }
}
