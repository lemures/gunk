﻿using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public class IndirectRenderer
    {
        private Mesh mesh;
        private Material _material;
        private Camera camera;
        private string idCounterBufferName;
        private int layer = 0;
        private UnityEngine.Rendering.ShadowCastingMode shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        private static Bounds _wholeMap = new Bounds(Vector3.zero, new Vector3(1000000.0f, 1000000.0f, 1000000.0f));
        private Bounds bounds;
        private bool receiveShadows=true;


        private MaterialPropertyBlock materialPropertyBlock;

        public void setBuffer(string propertyName, ComputeBuffer buffer)
        {
            if (materialPropertyBlock == null)
            {
                materialPropertyBlock = new MaterialPropertyBlock();
            }
            materialPropertyBlock.SetBuffer(propertyName, buffer);
        }

        private string getIndirectArgumentsBufferName()
        {
            return "IndirectArgumentsBuffer_"+GetHashCode();
        }

        private string getIdCounterBufferName()
        {
            return "IdCounterBuffer_" + GetHashCode();
        }

        public IndirectRenderer(Mesh mesh, Material material, Camera camera, string idCounterBufferName)
        {
            this.mesh = mesh;
            this.material = material;
            this.camera = camera;
            this.idCounterBufferName = idCounterBufferName ?? getIdCounterBufferName();
            this.bounds = _wholeMap;
            if (!KernelUtils.hasBuffer(this.idCounterBufferName))
            {
                KernelUtils.initializeEmptyBuffer(this.idCounterBufferName, 4*5, 4, false, ComputeBufferType.Counter);
            }
            initIndirectArgumentsBuffer();
        }

        private void initIndirectArgumentsBuffer()
        {
            KernelUtils.initializeEmptyBuffer(getIndirectArgumentsBufferName(), 14*5, 4, false, ComputeBufferType.IndirectArguments);
            var indData = new uint[4 * 5];
            indData[0] = mesh.GetIndexCount(0);
            KernelUtils.setBufferData(getIndirectArgumentsBufferName(), indData);
        }

        public IndirectRenderer(Mesh mesh, Material material, Camera camera, Bounds bounds, int layer,
            bool receiveShadows = false)
        {
            this.mesh = mesh;
            this.material = material;
            this.bounds = bounds;
            this.camera = camera;
            this.layer = layer;
            this.receiveShadows = receiveShadows;
            this.idCounterBufferName = getIdCounterBufferName();

            KernelUtils.initializeEmptyBuffer(idCounterBufferName, 4*5, 4, false, ComputeBufferType.Counter);
            initIndirectArgumentsBuffer();
        }

        public Material material
        {
            set { _material = value; }
        }

        public void setInstanceCount(uint newCount)
        {
            KernelUtils.setCounterValue(idCounterBufferName, newCount);
        }

        public void render()
        {
            var indirectDataRenderers = KernelUtils.getBuffer(getIndirectArgumentsBufferName());
            ComputeBuffer.CopyCount(KernelUtils.getBuffer(idCounterBufferName), indirectDataRenderers, 4);
            Graphics.DrawMeshInstancedIndirect(mesh, 0, _material, bounds, indirectDataRenderers, 0, materialPropertyBlock,
                shadowCastingMode, receiveShadows, layer, camera);
        }
    }
}
