﻿using System;
using System.Collections.Generic;
using Gunk_Scripts.Data_Structures;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public  class InstancedLineRenderer:ILineRenderer
    {
        /// <summary>
        /// base class for all lineRenderer
        /// </summary>
        public InstancedLineRenderer(Material material,Camera camera)
        {
            idMap = new IdMap();
            Mesh quad = (DatabasesManager.getObjectLatestVersion("Quad") as GameObject)?.GetComponent<MeshFilter>().sharedMesh;
            renderer = new IndirectRenderer(quad, material, camera, null);
            startPositions = new Vector3[currentlyAllocated];
            endPositions = new Vector3[currentlyAllocated];
            widths = new float[currentlyAllocated];
            if (quad == null)
            {
                throw new MissingResourcesException("quad");
            }

            startPositionsBuffer = new ComputeBuffer(currentlyAllocated, 12);
            endPositionsBuffer = new ComputeBuffer(currentlyAllocated, 12);
            lineWidthsBuffer = new ComputeBuffer(currentlyAllocated, 4);

            setBuffer(startPositionsBufferName, startPositionsBuffer);
            setBuffer(endPositionsBufferName,endPositionsBuffer);
            setBuffer(lineWidthsBufferName, lineWidthsBuffer);
        }

        private int currentlyAllocated=1;

        private ComputeBuffer startPositionsBuffer;
        private ComputeBuffer endPositionsBuffer;
        private ComputeBuffer lineWidthsBuffer;

        private IndirectRenderer renderer;


        private void resize(int newSize)
        {
            Array.Resize(ref startPositions, newSize);
            Array.Resize(ref endPositions, newSize);
            Array.Resize(ref widths, newSize);

            startPositionsBuffer.Release();
            endPositionsBuffer.Release();
            lineWidthsBuffer.Release();

            startPositionsBuffer = new ComputeBuffer(newSize, 12);
            endPositionsBuffer = new ComputeBuffer(newSize, 12);
            lineWidthsBuffer = new ComputeBuffer(newSize, 4);

            setBuffer(startPositionsBufferName, startPositionsBuffer);
            setBuffer(endPositionsBufferName, endPositionsBuffer);
            setBuffer(lineWidthsBufferName, lineWidthsBuffer);
        }

        public void setBuffer(string propertyName,ComputeBuffer buffer)
        {
            renderer.setBuffer(propertyName, buffer);
        }

        private static readonly string startPositionsBufferName = "startPositions";
        private static readonly string endPositionsBufferName = "endPositions";
        private static readonly string lineWidthsBufferName = "lineWidths";

        private Vector3[] startPositions;
        private Vector3[] endPositions;
        private float[] widths;
        private IdMap idMap;

        public void addObject(int id, Vector3 startPos,Vector3 endPos)
        {
            //may throw exception if the object already exists
            idMap.addObject(id);
            int mappedId = idMap.getMappedId(id);
            if (mappedId == currentlyAllocated)
            {
                currentlyAllocated *= 2;
                resize(currentlyAllocated);
            }
            widths[mappedId] = 0.5f;
            startPositions[mappedId] = startPos;
            endPositions[mappedId] = endPos;
        }

        public void setWidth(int id, float value)
        {
            var mappedId = idMap.getMappedId(id);
            widths[mappedId] = value;
        }

        public void removeObject(int id)
        {
            //may throw exception if the object doesn't exist

            //set width to zero so the object will be invisible(as if it is really removed)
            widths[idMap.getMappedId(id)] = 0;
            idMap.removeObject(id);
        }

        public List<int> getExistingObjectsIds()
        {
            return idMap.getAllObjectIds();
        }

        public Vector3 getStartPosition(int id) => startPositions[getMappedId(id)];

        public Vector3 getEndPosition(int id) => endPositions[getMappedId(id)];

        public void render()
        {
            startPositionsBuffer.SetData(startPositions);
            endPositionsBuffer.SetData(endPositions);
            lineWidthsBuffer.SetData(widths);
            renderer.setInstanceCount((uint)(idMap.getMaxTakenMappedId()+1));
            renderer.render();
        }

        public void updatePosition(int id, Vector3 newStartPos,Vector3 newEndPos)
        {
            int mappedId = idMap.getMappedId(id);
            startPositions[mappedId] = newStartPos;
            endPositions[mappedId] = newEndPos;
        }

        public int getMappedId(int id)
        {
            return idMap.getMappedId(id);
        }
    }
}

