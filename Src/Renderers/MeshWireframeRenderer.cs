﻿using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public class MeshWireframeRenderer
    {
        private readonly PlainColorLineRenderer lineRenderer;

        public MeshWireframeRenderer(Mesh mesh, Vector3 position,Camera camera)
        {
            if (mesh == null)
            {
                throw new System.NullReferenceException();
            }
            lineRenderer = new PlainColorLineRenderer(camera);
            var triangles = mesh.triangles;
            var vertices = mesh.vertices;
            for (var i = 0; i < triangles.Length; i += 3)
            {
                var v1 = vertices[triangles[i]];
                var v2 = vertices[triangles[i+1]];
                var v3 = vertices[triangles[i+2]];

                lineRenderer.addObject(i, position + v1, position + v2);
                lineRenderer.setWidth(i,0.01f);
                lineRenderer.setColor(i, Color.gray);

                lineRenderer.addObject(i + 1, position + v2, position + v3);
                lineRenderer.setWidth(i + 1,0.01f);
                lineRenderer.setColor(i + 1, Color.gray);

                lineRenderer.addObject(i + 2, position + v3, position + v1);
                lineRenderer.setWidth(i + 2,0.01f);
                lineRenderer.setColor(i + 2, Color.gray);
            }
        }

        public void render()
        {
            lineRenderer.render();
        }
    }
}
