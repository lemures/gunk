﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public class PlainColorLineRenderer:ILineRenderer
    {
        private InstancedLineRenderer instancedLineRenderer;

        private Vector3[] lineColors;

        private MaterialPropertyBlock materialPropertyBlock;

        private string lineColorsBufferName = "lineColors";
        private ComputeBuffer lineColorsBuffer;

        //size of all compute buffers and arrays
        private int currentlyAllocated = 1;

        /// <summary>
        /// most basic class for lineRenderer
        /// </summary>
        /// <param name="name"></param>
        /// <param name="material"></param>
        /// <param name="maxObjectCount"></param>
        public PlainColorLineRenderer(Camera camera)
        {
            var material = DatabasesManager.getObjectLatestVersion("PlainColorLineRenderer") as Material;
            instancedLineRenderer = new InstancedLineRenderer(material, camera);
            lineColorsBuffer = new ComputeBuffer(currentlyAllocated, 12);
            instancedLineRenderer.setBuffer(lineColorsBufferName, lineColorsBuffer);
            lineColors = new Vector3[currentlyAllocated];
        }

        public void addObject(int id, Vector3 startPos, Vector3 endPos)
        {
            instancedLineRenderer.addObject(id, startPos, endPos);
            int mappedId = instancedLineRenderer.getMappedId(id);
            if (mappedId == currentlyAllocated)
            {
                currentlyAllocated *= 2;
                resize(currentlyAllocated);
            }
            setColor(id, Color.white);
        }

        public void setColor(int id, Color color)
        {
            int mappedId = instancedLineRenderer.getMappedId(id);
            lineColors[mappedId] = new Vector3(color.r,color.g,color.b);
        }

        public void setWidth(int id, float value) => instancedLineRenderer.setWidth(id, value);

        private void resize(int newSize)
        {
            Array.Resize(ref lineColors, newSize);

            lineColorsBuffer = new ComputeBuffer(newSize, 12);

            instancedLineRenderer.setBuffer(lineColorsBufferName, lineColorsBuffer);
        }

        public void removeObject(int id)
        {
            instancedLineRenderer.removeObject(id);
        }

        public List<int> getExistingObjectsIds()
        {
            throw new NotImplementedException();
        }

        public Vector3 getStartPosition(int id) => instancedLineRenderer.getStartPosition(id);

        public Vector3 getEndPosition(int id)
        {
            throw new NotImplementedException();
        }

        public void render()
        {
            lineColorsBuffer.SetData(lineColors);
            instancedLineRenderer.render();
        }

        public void updatePosition(int id, Vector3 newStartPos, Vector3 newEndPos)
        {
            instancedLineRenderer.updatePosition(id, newStartPos, newEndPos);
        }

        public void updateColor(int id, Color newColor)
        {
            lineColors[instancedLineRenderer.getMappedId(id)]= new Vector3(newColor.r, newColor.g, newColor.b);
        }
    }
}
