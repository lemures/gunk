﻿using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.Data_Structures;
using UnityEngine;

namespace Gunk_Scripts.Renderers
{
    public class SelectedObjectsRenderer
    {
        private readonly GameObject selectedCamera;
        private readonly RenderTexture selectedRenderTexture;

        private RenderTexture installAdditionalCamera(GameObject cam, int mapSizeX,
            int mapSizeY,
            CameraClearFlags flags, int layer, int scale)
        {
            var textureToRenderTo = new RenderTexture(scale * mapSizeX / 2, scale * mapSizeY / 2, 0);
            textureToRenderTo.Create();

            cam.transform.position = new Vector3(mapSizeX / 2.0f, 0, mapSizeY / 2.0f);
            cam.transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));

            var cameraComp = cam.AddComponent<Camera>();
            cameraComp.orthographic = true;
            cameraComp.renderingPath = RenderingPath.Forward;
            cameraComp.orthographicSize = mapSizeX / 2.0f;
            cameraComp.targetTexture = textureToRenderTo;
            cameraComp.cullingMask = 1 << layer;
            cameraComp.allowMSAA = false;
            cameraComp.backgroundColor = new Color32(0, 0, 0, 0);
            cameraComp.clearFlags = flags;
            cameraComp.enabled = false;
            return textureToRenderTo;
        }

        private Vector3[] positions;
        private float[] radiuses;

        private string positionsBufferName = "selectedPositions";
        private string radiusesBufferName = "selectedRadiuses";

        private Bounds bounds = new Bounds(new Vector3(0.0f, -500.0f, 0.0f), new Vector3(100000.0f, 100000.0f, 100000.0f));
        private IndirectRenderer quadRenderer;

        private IdMap idMap;

        public void render()
        {
            KernelUtils.setBufferData(positionsBufferName, positions);
            KernelUtils.setBufferData(radiusesBufferName, radiuses);
            quadRenderer.setInstanceCount(idMap.getObjectCount());
            quadRenderer.render();
            selectedCamera.GetComponent<Camera>().Render();
        }

        public void addObject(int id, float x, float z, float radius)
        {
            idMap.addObject(id);
            positions[idMap.getMappedId(id)] = new Vector3(x, 0, z);
            setSelectedCircleRadius(id, radius);
        }

        public void setSelectedCircleRadius(int id, float range)
        {
            radiuses[idMap.getMappedId(id)] = range;
        }

        public void removeObject(int id)
        {
            idMap.removeObject(id);
        }

        public void updatePosition(int id, float newX, float newY, float newZ)
        {
            positions[idMap.getMappedId(id)] = new Vector3(newX, newY, newZ);
        }

        public RenderTexture getSelectedObjectsRenderTexture()
        {
            return selectedRenderTexture;
        }

        public float getSelectedObjectColorThreshold() => 0.5f;

        public void removeAllObjects()
        {
            idMap.removeAllObjects();
        }

        public List<int> getAllObjects() => idMap.getAllObjectIds().ToList();

        public SelectedObjectsRenderer(int maxObjectCount, int mapSizeX, int mapSizeY)
        {
            idMap = new IdMap();

            positions = new Vector3[maxObjectCount];
            radiuses = new float[maxObjectCount];

            var selectedMaterial = DatabasesManager.getObjectLatestVersion("Selected mat") as Material;
            selectedCamera = new GameObject("Selected Camera");
            selectedRenderTexture = installAdditionalCamera(selectedCamera, mapSizeX, mapSizeY, CameraClearFlags.SolidColor, 10, 4);
            KernelUtils.setGlobalTexture("Selected_RenderTexture", selectedRenderTexture);

            KernelUtils.initializeEmptyBuffer(positionsBufferName, maxObjectCount, 12, false);
            KernelUtils.initializeEmptyBuffer(radiusesBufferName, maxObjectCount, 4, false);

            if (selectedMaterial == null)
            {
                throw new MissingResourcesException("selectedMaterial");
            }

            var quad = (DatabasesManager.getObjectLatestVersion("Quad") as GameObject)?.GetComponent<MeshFilter>().sharedMesh;
            if (quad == null)
            {
                throw new MissingResourcesException("quad");
            }

            quadRenderer = new IndirectRenderer(quad, selectedMaterial, selectedCamera.GetComponent<Camera>(), bounds, 10);
        }
    }
}
