﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.SerializationHelpers
{
    public class KernelUtilsHelper : ScriptableObject
    {
        public List<string> computeShadersFilePaths = new List<string>();
        public List<ComputeShader> computeAssets = new List<ComputeShader>();
        public List<ShaderKernel> kernels = new List<ShaderKernel>();

        [Serializable]
        public class ShaderKernel
        {
            public string name;
            public int id;
            public ComputeShader shader;
            public ShaderKernel(int id, string name, ComputeShader shader)
            {
                this.id = id;
                this.shader = shader;
                this.name = name;
            }
        }
    }
}
