﻿using UnityEngine;

namespace Gunk_Scripts.SerializationHelpers
{
    public class Map : ScriptableObject
    {
        public TerrainData terrainData;
        public GameObject prefab;
        public Texture2D splatMap;
        public Texture2D normalMap;
        public Material mat;
        public string mapName;

        public int getMapHeight() => (int) terrainData.size.z;
        public int getMapWidth() => (int)terrainData.size.x;

        public static string getDatabaseName(string mapName) => mapName + "_mapData";
    }
}
