namespace Gunk_Scripts.SerializationHelpers
{
    public enum SelectionPriority
    {
        VeryLow,
        Low,
        Medium,
        High,
        VeryHigh
    }
}