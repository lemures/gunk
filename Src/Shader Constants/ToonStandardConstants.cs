﻿// ReSharper disable InconsistentNaming (naming deliberate)
namespace Gunk_Scripts.Shader_Constants
{
    public static class ToonStandardConstants
    {
        public const string shaderName = "Toon/Standard";
        public const string mainTexProperty = "_MainTex";
        public const string oclTexProperty = "_Occlusion";
        public const string emissionColor1Property= "_Emission";
        public const string emissionColor2Property = "_Emission2";
        public const string mainColorProperty = "_Color";
        public const string emissionIntensityProperty = "_Emission_intensity";
        public const string transparencyEnabledVariable = "transparency_enabled";
        public const string objectNumberProperty = "_object_number";
        public const string hasAnimatorProperty = "_has_animator";
    }
}
