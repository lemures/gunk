﻿using Gunk_Scripts.Data_Structures;

namespace Src.SkeletonAnimations.BoneTypes
{
    [System.Serializable]
    public class BarrelBone : Bone
    {
        public int parentSegmNum;
        public int parentLimbNum;
        public int index;

        public BarrelBone(int parentSegmNum, int parentLimbNum, int index, Float4 position, Bone previousBone):base(position,previousBone)
        {
            this.parentSegmNum = parentSegmNum;
            this.parentLimbNum = parentLimbNum;
            this.index = index;
            this.position = position;
        }

        public override bool Equals(object obj)
        {
            var bone = obj as BarrelBone;
            return bone != null &&
                   index == bone.index;
        }

        public override string getName() => "BarrelBone " + index + ", limb " + parentLimbNum + " segment " + parentSegmNum + 1;
    }
}