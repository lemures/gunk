﻿using Gunk_Scripts.Data_Structures;

namespace Src.SkeletonAnimations.BoneTypes
{
    [System.Serializable]
    public class BasicBone : Bone
    {
        public int segmNum;
        public int limbNum;

        public BasicBone(int segmNum, int limbNum, Float4 position, Bone previousBone):base(position,previousBone)
        {
            this.segmNum = segmNum;
            this.limbNum = limbNum;
            this.position = position;
        }

        public override bool Equals(object obj)
        {
            var bone = obj as BasicBone;
            return bone != null &&
                   segmNum == bone.segmNum &&
                   limbNum == bone.limbNum;
        }

        public override bool needsAssignedComponent() => true;

        public override string getName() => "BasicBone limb " + limbNum + " segment " + segmNum;

        public override string ToString()
        {
            return "BasicBone segmNum: " + segmNum + " limbNum:" + limbNum + " position: " + position;
        }
    }
}