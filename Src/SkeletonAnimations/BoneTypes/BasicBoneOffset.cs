﻿using Gunk_Scripts.Data_Structures;

namespace Src.SkeletonAnimations.BoneTypes
{
    [System.Serializable]
    public class BasicBoneOffset:Bone
    {
        public int segmNum;
        public int limbNum;

        public BasicBoneOffset(int segmNum, int limbNum, Float4 position, Bone previousBone):base(position,previousBone)
        {
            this.segmNum = segmNum;
            this.limbNum = limbNum;
            this.position = position;
        }

        public override bool Equals(object obj)
        {
            var bone = obj as BasicBoneOffset;
            return bone != null &&
                   segmNum == bone.segmNum &&
                   limbNum == bone.limbNum;
        }

        public override string ToString()
        {
            return "BasicBoneOffset segmNum: " + segmNum + " limbNum:" + limbNum + " position: " + position;
        }

        public override string getName() => "BasicBoneOffset limb " + limbNum + " segment" + segmNum;
    }
}