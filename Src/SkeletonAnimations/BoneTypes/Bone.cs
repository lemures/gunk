﻿using Gunk_Scripts.Data_Structures;

namespace Src.SkeletonAnimations.BoneTypes
{
    [System.Serializable]
    public abstract class Bone
    {
        public Float4 position;
        public Bone previousBone;
        public string label;

        protected Bone(Float4 position, Bone previousBone)
        {
            this.position = position;
            this.previousBone = previousBone;
        }

        /// <summary>
        /// returns true if this bone indicates where part of Mesh begins
        /// </summary>
        /// <returns></returns>
        public virtual bool needsAssignedComponent() => false;

        public abstract string getName();
    }
}