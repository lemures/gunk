﻿using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.SkeletonAnimations;
using UnityEngine;

namespace Src.SkeletonAnimations.BoneTypes
{
    [System.Serializable]
    public class MainBodyBone : Bone
    {
        public MainBodyBone():base(new Float4(Vector3.zero),null)
        {}

        public override bool needsAssignedComponent() => true;

        public override string ToString()
        {
            return "MainBodyBone position: " + position;
        }

        public override string getName() => "MainBodyBone";
    }
}