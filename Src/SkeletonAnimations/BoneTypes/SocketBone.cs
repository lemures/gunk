﻿using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.SkeletonAnimations;

namespace Src.SkeletonAnimations.BoneTypes
{
    [System.Serializable]
    public class SocketBone : Bone
    {
        public readonly int index;
        public float rotation;

        public SocketBone(int index, Float4 position, float rotation, Bone previousBone):base(position,previousBone)
        {
            this.index = index;
            this.position = position;
            this.rotation = rotation;
        }

        public override bool Equals(object obj)
        {
            var bone = obj as SocketBone;
            return bone != null &&
                   index == bone.index;
        }

        public override string ToString()
        {
            return "SocketBone index: " + index + " rotation:" + rotation + " position: " + position;
        }

        public override string getName() => "Socket Bone " + index;
    }
}