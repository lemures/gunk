using System;
using Gunk_Scripts.SkeletonAnimations;

namespace Src.SkeletonAnimations.CurvesTypes
{
    [Serializable]
    public class ConstantValueCurve : IAnimationCurve
    {
        private readonly float value;

        public ConstantValueCurve(float value)
        {
            this.value = value;
        }

        public float evaluate(float time, float argument) => value;
    }
}