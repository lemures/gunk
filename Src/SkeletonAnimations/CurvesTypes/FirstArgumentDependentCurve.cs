using System;

namespace Src.SkeletonAnimations.CurvesTypes
{
    //curve that will always return x value of argument
    [Serializable]
    public class FirstArgumentDependentCurve : IAnimationCurve
    {
        public float evaluate(float time, float argument) => argument;
    }
}