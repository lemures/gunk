﻿namespace Src.SkeletonAnimations.CurvesTypes
{
    public interface IAnimationCurve
    {
        float evaluate(float time, float argument);
    }
}
