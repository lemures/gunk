using System;

namespace Src.SkeletonAnimations.CurvesTypes
{
    [Serializable]
    public class SinusoidCurve : IAnimationCurve
    {
        private readonly float multiplier;
        private readonly float offset;

        /// <summary>
        /// creates curve that evaluates to multiplier*sin(time)+offset
        /// </summary>
        public SinusoidCurve(float multiplier, float offset)
        {
            this.multiplier = multiplier;
            this.offset = offset;
        }


        public float evaluate(float time, float argument) => multiplier * (float)Math.Sin(time) + offset;
    }
}