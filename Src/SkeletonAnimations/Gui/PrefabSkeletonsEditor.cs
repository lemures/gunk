using System.Collections.Generic;
using Gunk_Scripts;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.GUI;
using Src.Console;
using Src.Console.Internal;
using Src.GUI;

namespace Src.SkeletonAnimations.Gui
{
    [ExecutesCommand("skeletonsEditor")]
    public class PrefabSkeletonsEditor : GuiElement
    {
        private static GuiWindow editor;

        public static string skeletonsEditor(string[] arguments)
        {
            if (editor == null)
            {
                editor = new GuiWindow(new PrefabSkeletonsEditor(GlobalPrefabsManager.getInstance()),
                    "Skeletons Editor");
                return "editor opened";
            }
            else
            {
                return "Error! editor is already opened";
            }
        }

        private readonly IPrefabManager prefabManager;
        private GuiSearchInputfield menu;
        private GuiButton saveSkeletonButton;

        private SkeletonVisualEditor visualEditor;
        private Prefab currentPrefab;

        private PrefabSkeletonsEditor(IPrefabManager prefabManager) : base(0, 0, 0, "prefab skeletons editor", null)
        {
            this.prefabManager = prefabManager;
            installMenu();
            changeSelectedPrefab(prefabManager.findPrefab("welding bot"));
        }

        private void installMenu()
        {
            menu = new GuiSearchInputfield(0, 0, 300, 50, "choose new skeleton", 1);
            var prefabNames = prefabManager.getAllPrefabNames();
            var prefabsWithIds = new List<KeyValuePair<string, int>>();
            var i = 0;
            foreach (var prefabName in prefabNames)
            {
                prefabsWithIds.Add(new KeyValuePair<string, int>(prefabName, i));
                i++;
            }

            menu.setSearchOptions(prefabsWithIds);
            menu.addSubmitValueListener(
                x => changeSelectedPrefab(prefabManager.findPrefab(prefabNames[x])));
            menu.attachToObject(this);

            saveSkeletonButton = new GuiButton(GuiButton.ButtonStyle.Simple, 0, 50, 300, 50, "Save changes", 1);
            saveSkeletonButton.addOnClickListener(
                () =>
                {
                    visualEditor.saveEditedSkeleton();
                    var notification = new GuiNotification("Skeleton changes have been saved do disk!");
                });
            saveSkeletonButton.attachToObject(this);
        }

        private void makeMenuVisible()
        {
            saveSkeletonButton.makeMostVisibleChild();
            menu.makeMostVisibleChild();
        }

        private void changeSelectedPrefab(Prefab prefab)
        {
            currentPrefab = prefab;
            visualEditor?.dispose();
            visualEditor = new SkeletonVisualEditor(prefab);
            visualEditor.attachToObject(this);
            makeMenuVisible();
        }

        public override void dispose()
        {
            base.dispose();
            visualEditor?.dispose();
            editor = null;
        }
    }
}