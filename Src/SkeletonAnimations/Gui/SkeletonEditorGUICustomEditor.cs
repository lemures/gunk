﻿#if UNITY_EDITOR
using System.Collections.Generic;
using Gunk_Scripts;
using Gunk_Scripts.Asset_Importing;
using Src.SkeletonAnimations.Internal;
using UnityEditor;
using UnityEngine;

namespace Src.SkeletonAnimations.Gui
{
    public class SkeletonEditorGUICustomEditor : Editor
    {
        [MenuItem("Assets/Create SkeletonData based on this mesh")]
        private static void saveObject()
        {
            var skeleton = new Skeleton();
            var mesh = (Selection.activeObject as GameObject)?.GetComponent<MeshFilter>()?.sharedMesh;
            skeleton.importFromMesh(mesh);
            skeleton.saveSkeletonDataToFile(PrefabImporterUtils.removeNameSuffix(mesh.name, new List<string>() {"skeleton"}));
            Debug.Log(skeleton);
        }

        [MenuItem("Assets/Create SkeletonData based on this mesh", true)]
        private static bool optionValidation()
        {
            if ((Selection.activeObject as GameObject)?.GetComponent<MeshFilter>()==null)
            {
                return false;
            }
            var mesh = (Selection.activeObject as GameObject)?.GetComponent<MeshFilter>()?.sharedMesh;
            return mesh != null && PrefabImporterUtils.hasNameSuffix(mesh.name, "skeleton");
        }

        [MenuItem("Skeleton Animations/Remove broken Skeletons")]
        private static void removeSkeletons()
        {
            var allSkeletons = DatabasesManager.getAllSavedObjectsInDirectory("skeletons");
            foreach (var databaseName in allSkeletons)
            {
                if (!Skeleton.isDatabaseValid(databaseName))
                {
                    Debug.Log("removed invalid skeletonData: "+databaseName);
                    DatabasesManager.deleteAllVersions(databaseName, "skeletons");
                }
            }
        }
    }
}
#endif