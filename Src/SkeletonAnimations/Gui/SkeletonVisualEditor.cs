﻿using System;
using System.Collections.Generic;
using Gunk_Scripts;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.Camera_Behavoiur;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GUI;
using Gunk_Scripts.Renderers;
using Gunk_Scripts.Shader_Constants;
using Gunk_Scripts.SkeletonAnimations;
using Src.Camera_Behavoiur;
using Src.SkeletonAnimations.BoneTypes;
using Src.SkeletonAnimations.Internal;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Src.SkeletonAnimations.Gui
{
    public class SkeletonVisualEditor : GuiElement
    {
        private GuiCameraImage guiCameraImage;
        private static Vector3 modelSpawnPosition=new Vector3(10000,10000,10000);
        private MeshWireframeRenderer wireframeRenderer;
        private readonly SkeletonData skeletonData;
        private Skeleton skeleton;
        private string prefabName;

        public SkeletonVisualEditor(Prefab prefab) : base(0, 0, 0, "skeletonVisualEditor", null)
        {
            skeleton = new Skeleton(prefab.name);
            skeletonData = skeleton.skeletonData;
            guiCameraImage = new GuiCameraImage(0, 0, 1820, 980, 3, 0.75f);
            guiCameraImage.setCameraBehaviour(new OrbitAroundCamera("TestCamera", modelSpawnPosition));
            guiCameraImage.attachToObject(this);
            GuiManager.updateCaller.addFunction(this, () => handleInput(modelSpawnPosition));
            drawVisualization(modelSpawnPosition);

            prefabName = prefab.name;
            setMeshVisualization(prefab.go.GetComponentInChildren<MeshFilter>().sharedMesh);
            GuiManager.updateCaller.addFunction(this, () => wireframeRenderer.render());
        }

        public void saveEditedSkeleton()
        {
            skeleton.saveSkeletonDataToFile(prefabName);
        }

        private void setMeshVisualization(Mesh mesh)
        {
            if (mesh == null)
            {
                throw new System.NullReferenceException();
            }
            wireframeRenderer = new MeshWireframeRenderer(mesh, modelSpawnPosition, guiCameraImage.camera);
        }

        private readonly float moveSpeed = 0.005f;

        /// <summary>
        /// rotates vector by axis (0,1,0) by an angle
        /// </summary>
        /// <returns></returns>
        private static Vector3 rotatePoint(Vector3 pointToRotate, float angleInDegrees)
        {
            var angleInRadians = angleInDegrees * (Mathf.PI / 180);
            var cosTheta = Mathf.Cos(angleInRadians);
            var sinTheta = Mathf.Sin(angleInRadians);
            return new Vector3
            (
                (cosTheta * (pointToRotate.x) - sinTheta * (pointToRotate.z)),
                pointToRotate.y,
                (sinTheta * (pointToRotate.x) + cosTheta * (pointToRotate.z))
            );
        }

        private float timeMovingBone = 0.0f;

        /// <summary>
        /// Used to calculate vector delta based on user keypresses
        /// </summary>
        /// <returns></returns>
        private Vector3 calculateBoneMoveVectorBasedOnInput()
        {
            var cameraAngle =  guiCameraImage.camera.transform.rotation.eulerAngles;
            var moveDirection = Vector3.zero;
            if (Input.GetKey(KeyCode.DownArrow))
            {
                moveDirection += rotatePoint(new Vector3(0, 0, -1), -cameraAngle.y);
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                moveDirection += rotatePoint(new Vector3(0, 0, 1), -cameraAngle.y);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                moveDirection += rotatePoint(new Vector3(1, 0, 0), -cameraAngle.y);
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                moveDirection += rotatePoint(new Vector3(-1, 0, 0), -cameraAngle.y);
            }
            if (Input.GetKey(KeyCode.End))
            {
                moveDirection += new Vector3(0, -1, 0);
            }
            if (Input.GetKey(KeyCode.RightShift))
            {
                moveDirection += new Vector3(0, 1, 0);
            }

            timeMovingBone = moveDirection == Vector3.zero ? 0 : timeMovingBone + Time.unscaledDeltaTime;

            var timeBonus = Mathf.Clamp((timeMovingBone - 1.0f) * 3.0f, 1.0f, 8.0f);
            return moveDirection * moveSpeed * timeBonus;
        }

        private void handleInput(Vector3 objectPosition)
        {
            if (MouseUtils.instance.wasClicked())
            {
                currentlySelectedBone = getBoneCloseToMousePos(objectPosition);
                drawVisualization(objectPosition);
            }
            if (MouseUtils.instance.wasRightClicked())
            {
                if (getBoneCloseToMousePos(objectPosition) != null)
                {
                    var contextMenu = new GuiContextMenu("boneOptions");
                    var closestBone = getBoneCloseToMousePos(objectPosition);
                    var optionsNames = new List<string>();
                    var eventsToCall = new List<Action>();
                    if (closestBone.GetType() != typeof(MainBodyBone))
                    {
                        optionsNames.Add("Delete bone");
                        eventsToCall.Add(() =>
                        {
                            skeletonData.eraseBone(closestBone);
                            drawVisualization(objectPosition);
                        });
                    }

                    //Adding new limbs or sockets
                    if (closestBone.GetType() == typeof(MainBodyBone))
                    {
                        if (skeletonData.canAddNextLimb())
                        {
                            optionsNames.Add("Add new limb");
                            eventsToCall.Add(() =>
                            {
                                skeletonData.addNextLimb(new Float4(new Vector3(0, 1, 0)));
                                drawVisualization(objectPosition);
                            });
                        }
                        if (skeletonData.canAddNextSocket())
                        {
                            optionsNames.Add("Add new socket");
                            eventsToCall.Add(() =>
                            {
                                skeletonData.addNextSocketBone(new Float4(new Vector3(0, 1, 0)));
                                drawVisualization(objectPosition);
                            });
                        }
                    }

                    if (closestBone.GetType() == typeof(BasicBone))
                    {
                        if (skeletonData.getLowerBoneInLimb((BasicBone)closestBone).GetType() != typeof(BasicBoneOffset))
                        {
                            optionsNames.Add("Add bone offset");
                            eventsToCall.Add(() =>
                            {
                                skeletonData.addBasicBoneOffset(((BasicBone)closestBone).segmNum,
                                    ((BasicBone)closestBone).limbNum, new Float4((closestBone).position - new Vector3(0, 0.5f, 0)));
                                drawVisualization(objectPosition);
                            });
                        }

                        if (((BasicBone)closestBone).segmNum < SkeletonData.MAX_SEGMENT_NUMBER - 1 && skeletonData.getHigherBonesInLimb((BasicBone)closestBone).Count==0)
                        {
                            optionsNames.Add("Add next segment");
                            eventsToCall.Add(() =>
                            {
                                skeletonData.addBoneSegment((BasicBone)closestBone, new Float4((closestBone).position + new Vector3(0, 1, 0)));
                                drawVisualization(objectPosition);
                            });
                        }

                        if (skeletonData.canAddNextBarrel())
                        {
                            optionsNames.Add("Add barrel bone");
                            eventsToCall.Add(() =>
                            {
                                skeletonData.addBarrelBone(((BasicBone)closestBone).segmNum,
                                    ((BasicBone)closestBone).limbNum,  new Float4((closestBone).position - new Vector3(0, 0.5f, 0)));
                                drawVisualization(objectPosition);
                            });
                        }
                    }

                    contextMenu.setOptions(optionsNames, eventsToCall);
                }
            }
            //moving currently selected bone
            if (currentlySelectedBone == null) return;

            if (Input.GetKeyDown(KeyCode.Delete))
            {
                skeletonData.eraseBone(currentlySelectedBone);
                drawVisualization(objectPosition);
                return;
            }
            var moveDirection = calculateBoneMoveVectorBasedOnInput();

            if (moveDirection == Vector3.zero) return;

            currentlySelectedBone.position =
                new Float4(currentlySelectedBone.position + moveDirection);
            if (currentlySelectedBone is BasicBone)
            {
                var lowerBone = skeletonData.getLowerBoneInLimb(currentlySelectedBone as BasicBone) as BasicBoneOffset;
                if (lowerBone != null)
                {
                    lowerBone.position = new Float4( lowerBone.position - moveDirection);
                }
            }
            drawVisualization(objectPosition);
        }

        private List<GameObject> visualizedBones = new List<GameObject>();

        /// <summary>
        /// finds bone that is closest to current Mouse Position
        /// </summary>
        /// <param name="objectPosition"></param>
        /// <returns></returns>
        private Bone getBoneCloseToMousePos(Vector3 objectPosition)
        {
            var closestDistance = 20.0f;
            Bone closestBone = null;

            var allBones = skeletonData.getAllBones();

            foreach (var bone in allBones)
            {
                Vector2 bonePosition = guiCameraImage.worldToScreenPoint(skeletonData.getRealBonePosition(bone) + objectPosition);
                var distance = Vector2.Distance(bonePosition, new Vector2(Input.mousePosition.x, Input.mousePosition.y));
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closestBone = bone;
                }
            }

            return closestBone;
        }

        /// <summary>
        /// draws fancy marker in certain 3D position
        /// </summary>
        /// <param name="position">position of the marker</param>
        /// <param name="color">color of the marker</param>
        /// <param name="scale">size of the marker</param>
        /// <returns></returns>
        private static GameObject createBoneSphereMarker(Vector3 position, Color color, float scale)
        {
            var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            sphere.GetComponent<MeshRenderer>().material.shader = Shader.Find(ToonStandardConstants.shaderName);
            sphere.GetComponent<MeshRenderer>().material.SetColor(ToonStandardConstants.mainColorProperty, color);
            sphere.transform.localScale = scale * Vector3.one;
            sphere.transform.position = position;
            return sphere;
        }

        /// <summary>
        /// draws a fancy line between two 3D positions in worldSpace that can be either dashed or not
        /// </summary>
        private static GameObject createBoneLineMarker(Vector3 start, Vector3 end, Color color, bool isDashed)
        {
            var pointsCount = isDashed ? (int)(Vector3.Distance(start, end) * 8.0f) + 2 + 1 : 2;
            var pointsPositions = new Vector3[pointsCount];
            var parent = new GameObject("LineParents");
            for (var i = 0; i < pointsCount; i++)
            {
                pointsPositions[i] = start * (i / (pointsCount - 1.0f)) + end * (1.0f - (i / (pointsCount - 1.0f)));
            }

            for (var i = 0; i < pointsCount - 1; i++)
            {
                if (i % 2 != 0)
                {
                    continue;
                }
                var line = new GameObject("Line");
                var lineComp = line.AddComponent<LineRenderer>();
                lineComp.SetPosition(0, pointsPositions[i]);
                lineComp.SetPosition(1, pointsPositions[i + 1]);
                lineComp.material = new Material(Shader.Find("Unlit/Color"))
                {
                    color = color
                };
                lineComp.endColor = lineComp.startColor;
                line.transform.parent = parent.transform;

                lineComp.startWidth = 0.02f;
                lineComp.endWidth = 0.02f;
            }

            return parent;
        }

        private Bone currentlySelectedBone;

        private Color getBoneColor(Bone bone)
        {
            if (bone == currentlySelectedBone)
            {
                return Color.white * 3.0f;
            }
            if (bone is BasicBone)
            {
                return Color.blue;
            }
            if (bone is BasicBoneOffset)
            {
                return Color.red;
            }
            if (bone is BarrelBone)
            {
                return Color.green;
            }
            if (bone is SocketBone)
            {
                return Color.yellow;
            }
            if (bone is MainBodyBone)
            {
                return Color.grey;
            }
            return Color.white;
        }

        private void clearPreviousVisualization()
        {
            foreach (var go in visualizedBones)
            {
                Object.Destroy(go);
            }

            visualizedBones = new List<GameObject>();
        }

        private void drawVisualization(Vector3 objectPosition)
        {
            clearPreviousVisualization();

            var allBones = skeletonData.getAllBones();

            foreach (var bone in allBones)
            {
                visualizedBones.Add(createBoneSphereMarker(objectPosition + skeletonData.getRealBonePosition(bone), getBoneColor(bone), 0.15f));
                var myColor = getBoneColor(bone);
                var lower = skeletonData.getLowerBoneInLimb(bone);
                if (lower != null)
                {
                    var isDashed = lower.GetType() != typeof(BasicBone);
                    visualizedBones.Add(createBoneLineMarker(objectPosition + skeletonData.getRealBonePosition(lower),
                        objectPosition + skeletonData.getRealBonePosition(bone), myColor, isDashed));
                }
            }
        }

        public override void dispose()
        {
            base.dispose();
            clearPreviousVisualization();
        }
    }
}
