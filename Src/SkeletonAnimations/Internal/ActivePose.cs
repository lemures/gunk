using System.Collections.Generic;
using UnityEngine;

namespace Src.SkeletonAnimations.Internal
{
    public class ActivePose
    {
        private WeightedClipsCollection clips;
        private int numberOfPlayedClips = 0;
        private float currentArgument;
        private float currentTime = 0.0f;
        private List<ClipData>allClips = new List<ClipData>();

        public bool isCurrentClipLoaded() => clips.getCurrentClipWeight > 0.9f;
        
        private class ClipData
        {
            public int id;
            public float argument;
            public SkeletonAnimationClip clip;

            public ClipData(int id, float argument, SkeletonAnimationClip clip)
            {
                this.id = id;
                this.argument = argument;
                this.clip = clip;
            }
        }

        public void playNewClip(SkeletonAnimationClip newClip, float argument=0.0f)
        {
            numberOfPlayedClips++;
            this.currentArgument = argument;
            clips.addClip(numberOfPlayedClips.ToString());
            allClips.Add(new ClipData(numberOfPlayedClips, argument, newClip));
        }

        public void updateArgument(float newArgument)
        {
            allClips[allClips.Count - 1].argument = newArgument;
        }

        public void update(float timePassed)
        {
            clips.update(timePassed);
            currentTime += timePassed;
            var activeClips = clips.getAllClips();
            allClips.RemoveAll(activeClip => !activeClips.Exists(weightedClip => weightedClip.name == activeClip.id.ToString()));
        }

        public List<BoneAnimationOutput> evaluateCurrentPose()
        {
            var translations = new SortedDictionary<int, Vector3>();
            var rotations = new SortedDictionary<int, Vector3>();
            var weights = clips.getAllClips();
            foreach (var clipData in allClips)
            {
                var weight = weights.Find(x => x.name == clipData.id.ToString()).weight;
                foreach (var boneAnimationOutput in clipData.clip.evaluateAtPoint(currentTime, clipData.argument))
                {
                    if (!rotations.ContainsKey(boneAnimationOutput.boneId))
                    {
                        rotations.Add(boneAnimationOutput.boneId, Vector3.zero);
                    }
                    if (!translations.ContainsKey(boneAnimationOutput.boneId))
                    {
                        translations.Add(boneAnimationOutput.boneId, Vector3.zero);
                    }
                    rotations[boneAnimationOutput.boneId] += weight * boneAnimationOutput.rotation;
                    translations[boneAnimationOutput.boneId] += weight * boneAnimationOutput.translation;
                }
            }

            var result = new List<BoneAnimationOutput>();
            foreach (var boneId in rotations.Keys)
            {
                result.Add(new BoneAnimationOutput(boneId, translations[boneId], rotations[boneId]));
            }

            return result;
        }

        public ActivePose(SkeletonAnimationClip startClip)
        {
            clips = new WeightedClipsCollection(numberOfPlayedClips.ToString());
            allClips.Add(new ClipData(0, 0, startClip));
        }
    }
}