using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Gunk_Scripts;

namespace Src.SkeletonAnimations.Internal
{
    public static class AnimationClipDatabase
    {
        private class ClipNotFound : Exception
        {
            public ClipNotFound(string message) : base(message)
            {

            }
        }

        private static SortedDictionary<string,SkeletonAnimationClip>allLoadedClips = new SortedDictionary<string, SkeletonAnimationClip>();
        private static List<string> allAvailableClips = NeededClipsFinder.getAllNeededScripts();
        private static string clipsDatabaseCategory = "animationClips";

        private static void validateClipNameIsAvailable(string name)
        {
            if (!allAvailableClips.Contains(name))
            {
                throw new ClipNotFound(name);
            }
        }

        private static void createEmptyClipIfNecessary(string name)
        {
            if (allLoadedClips.ContainsKey(name))
            {
                return;
            }
            var newClip = new SkeletonAnimationClip(name);
            allLoadedClips.Add(name, newClip);
            DatabasesManager.saveObject(allLoadedClips[name], name, clipsDatabaseCategory);
        }

        private static void loadFromDiskIfAvailable(string name)
        {
            if (allLoadedClips.ContainsKey(name))
            {
                return;
            }
            var savedClip = DatabasesManager.getObjectLatestVersion(name, clipsDatabaseCategory) as SkeletonAnimationClip;
            if (savedClip != null)
            {
                allLoadedClips.Add(name, savedClip);
            }
        }

        public static SkeletonAnimationClip getClip(string name)
        {
            validateClipNameIsAvailable(name);
            loadFromDiskIfAvailable(name);
            createEmptyClipIfNecessary(name);
            return allLoadedClips[name];
        }

        public static void saveClipChangesToDisk(string name)
        {
            if (name == "")
            {
                return;
            }
            validateClipNameIsAvailable(name);
            createEmptyClipIfNecessary(name);
            DatabasesManager.saveObject(allLoadedClips[name], name, clipsDatabaseCategory);
        }
    }
}