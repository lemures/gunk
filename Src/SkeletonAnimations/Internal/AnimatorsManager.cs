﻿using System;
using System.Collections.Generic;
using Gunk_Scripts;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.Shader_Constants;
using Src.Libraries;
using UnityEngine;

// ReSharper disable NotAccessedField.Local (GpuBoneState Used only in shaders)

namespace Src.SkeletonAnimations.Internal
{
    public class AnimatorsManager : MonoBehaviour
    {
        public void Update()
        {
            sendAnimationDataToShaders();
        }

        private static IdManager animatorsIdManager = new IdManager();

        private const string BONE_STATES_BUFFER_NAME = "boneStates";
        private const string CALCULATED_TRANSFORMATIONS_BUFFER_NAME = "calculatedTransformations";
        private const string MAPPED_BONES_IDS_BUFFER_NAME = "mappedBonesIds";
        private const int MATRIX_COMPUTER_DISPATCH_SIZE = 128;
        private const string MATRIX_COMPUTER_KERNEL_NAME = "TransformationMatrixComputer";
        private const string MAX_BONE_COUNT_PROPERTY_NAME = "maxBoneCount";

        private static GpuBoneState[] gpuBoneStates = new GpuBoneState[1];

        private static List<BoneId> allBones=new List<BoneId>();
        private static int[] bonesMappedIds = new int[1];

        private struct GpuBoneState
        {
            //id of bone that this bone is attached to or -1 if the bone is MainBodyBone
            public int previousBoneId;
            //first translation, then rotation
            public Vector3 rotation;
            public Vector3 translation;
            public Vector3 boneCenter;
            //used by OffsetBones
            public Vector3 boneBasicTranslation;

            public GpuBoneState(int previousBoneId, Vector3 boneCenter, Vector3 boneBasicTranslation) : this()
            {
                this.previousBoneId = previousBoneId;
                this.boneCenter = boneCenter;
                this.boneBasicTranslation = boneBasicTranslation;
                this.rotation = Vector3.zero;
                this.translation = Vector3.zero;
            }
        }

        private struct BoneId
        {
            public int objectNumber;
            public int boneLocalId;

            public BoneId(int objectNumber, int boneLocalId)
            {
                this.objectNumber = objectNumber;
                this.boneLocalId = boneLocalId;
            }
        }

        private const int GPU_BONE_STATE_STRUCTURE_SIZE = 4 * (1 + 3 * 4);

        public static int addAnimator(IRenderersParametersManager objectManager,int gameObjectId)
        {
            int myId=animatorsIdManager.getFreeId();
            if (gpuBoneStates.Length < (myId+1) * SkeletonData.getMaxBoneCount())
            {
                resizeGpuBoneStates(2*(myId+1) * SkeletonData.getMaxBoneCount());
                resizeCalculatedTransformations(2*(myId + 1) * SkeletonData.getMaxBoneCount());
            }
            objectManager.setIntParameter(gameObjectId, ToonStandardConstants.objectNumberProperty, myId);
            objectManager.setIntParameter(gameObjectId, ToonStandardConstants.hasAnimatorProperty, 1);
            return myId;
        }

        public static void removeAnimator(int animatorId)
        {
            animatorsIdManager.deregisterId(animatorId);
            allBones.RemoveAll(x => x.objectNumber == animatorId);
        }

        private static int getBoneGlobalId(int animatorId, int boneLocalId)
        {
            return SkeletonData.getMaxBoneCount() * animatorId + boneLocalId;
        }

        private static void initShaderVariables()
        {
            KernelUtils.setInt(MAX_BONE_COUNT_PROPERTY_NAME, SkeletonData.getMaxBoneCount());
            KernelUtils.initializeEmptyBuffer(CALCULATED_TRANSFORMATIONS_BUFFER_NAME, 30000, 2*4*4, false);
            KernelUtils.initializeEmptyBuffer(MAPPED_BONES_IDS_BUFFER_NAME, 30000, 4, false);
            KernelUtils.initializeEmptyBuffer(BONE_STATES_BUFFER_NAME, 30000, GPU_BONE_STATE_STRUCTURE_SIZE, false);
        }

        private static void resizeMappedBoneIds(int newSize)
        {
            Array.Resize(ref bonesMappedIds, newSize);
        }

        private static void resizeGpuBoneStates(int newSize)
        {
            Array.Resize(ref gpuBoneStates, newSize);
        }

        private static void resizeCalculatedTransformations(int newSize)
        {
            //KernelUtils.resizeBuffer(CALCULATED_TRANSFORMATIONS_BUFFER_NAME, newSize);
        }

        public static void sendAnimationDataToShaders()
        {
            UnityEngine.Profiling.Profiler.BeginSample("Animators Manager: sending data to shaders");
            UnityEngine.Profiling.Profiler.BeginSample("setting bone mapped Ids");
            for (int i = 0; i < allBones.Count; i++)
            {
                bonesMappedIds[i] = getBoneGlobalId(allBones[i].objectNumber,allBones[i].boneLocalId);
            }
            UnityEngine.Profiling.Profiler.EndSample();
            UnityEngine.Profiling.Profiler.BeginSample("sending buffers data to GPU");
            KernelUtils.setBufferData(MAPPED_BONES_IDS_BUFFER_NAME, bonesMappedIds);
            UnityEngine.Profiling.Profiler.EndSample();
            UnityEngine.Profiling.Profiler.BeginSample("setting bone mapped Ids");
            for (int i = 0; i < allBones.Count; i++)
            {
                bonesMappedIds[i] = 0;
            }
            UnityEngine.Profiling.Profiler.EndSample();
            UnityEngine.Profiling.Profiler.BeginSample("sending buffers data to GPU");
            KernelUtils.setBufferData(BONE_STATES_BUFFER_NAME, gpuBoneStates);
            UnityEngine.Profiling.Profiler.EndSample();
            KernelUtils.dispatch(MATRIX_COMPUTER_KERNEL_NAME,1+bonesMappedIds.Length/MATRIX_COMPUTER_DISPATCH_SIZE);
            UnityEngine.Profiling.Profiler.EndSample();
        }

        public static void updateTranslation(int animatorId, int boneLocalId, Vector3 newTranslation)
        {
            //TODO move this into a separate compute shader
            gpuBoneStates[getBoneGlobalId(animatorId, boneLocalId)].translation = newTranslation;
        }

        public static void updateRotation(int animatorId, int boneLocalId, Vector3 newRotation)
        {
            //TODO move this into a separate compute shader
            gpuBoneStates[getBoneGlobalId(animatorId, boneLocalId)].rotation = newRotation;
        }

        public static Vector3 getBoneRotation(int animatorId, int boneLocalId, Vector3 objectRotation)
        {
            var currentBoneId = getBoneGlobalId(animatorId, boneLocalId);
            //TODO make this function work in general case
            return objectRotation + gpuBoneStates[currentBoneId].rotation;
        }

        private static void translate(ref Matrix4x4 matrix, Vector3 vec)
        {
            matrix = Matrix4x4.Translate(vec) * matrix;
        }

        private static void rotateAroundPoint(ref Matrix4x4 matrix, Vector3 rot, Vector3 center)
        {
            translate(ref matrix, -center);
            rot *= -360.0f;
            matrix = Matrix4x4.Rotate(Quaternion.Euler(rot)) * matrix;
            translate(ref matrix, center);
        }

        public static Vector3 getBonePosition(int animatorId, int boneLocalId, Vector3 objectPosition)
        {
            if (boneLocalId < 0)
            {
                throw new ArgumentException("boneLocalId cannot be negative number");
            }
            var currentBoneId = getBoneGlobalId(animatorId, boneLocalId);
            var boneCenter = gpuBoneStates[currentBoneId].boneCenter;
            var currentTransformation = Matrix4x4.identity;
            while (currentBoneId != -1)
            {
                rotateAroundPoint(ref currentTransformation, gpuBoneStates[currentBoneId].rotation, gpuBoneStates[currentBoneId].boneCenter);
                translate(ref currentTransformation, gpuBoneStates[currentBoneId].translation + gpuBoneStates[currentBoneId].boneBasicTranslation);
                currentBoneId = gpuBoneStates[currentBoneId].previousBoneId;
            }

            return objectPosition + currentTransformation.MultiplyPoint(boneCenter);
        }

        private static void addBoneInternal(int objectNumber,int boneLocalId)
        {
            var result = new BoneId(objectNumber, boneLocalId);
            allBones.Add(result);
            if (allBones.Count > bonesMappedIds.Length)
            {
                resizeMappedBoneIds(bonesMappedIds.Length * 2);
            }
        }

        private static bool loggingEnabled;

        public static void enableLogging()
        {
            loggingEnabled = true;
        }

        public static void disableLogging()
        {
            loggingEnabled = false;
        }

        public static void addBone(int animatorId, int boneLocalId, int previousBoneLocalId,
            Vector3 boneBasicTranslation, Vector3 boneCenter)
        {
            addBoneInternal(animatorId, boneLocalId);
            var boneGlobalId = getBoneGlobalId(animatorId,boneLocalId);
            var previousBoneGlobalId =
                previousBoneLocalId == -1 ? -1 : getBoneGlobalId(animatorId,previousBoneLocalId);
            gpuBoneStates[boneGlobalId] = new GpuBoneState(previousBoneGlobalId, boneCenter, boneBasicTranslation);
            if (loggingEnabled)
            {
                Debug.Log("animator ID:" + animatorId + " added new bone with ID: " + boneLocalId +
                          "  previous bone ID was: " + previousBoneLocalId + " bone global ID is " + boneGlobalId +
                          " bone offset is " + boneBasicTranslation);
            }
        }

        static AnimatorsManager()
        {
            initShaderVariables();
            var updateCaller = new GameObject("Animators Manager");
            updateCaller.hideFlags = HideFlags.HideInHierarchy;
            updateCaller.AddComponent<AnimatorsManager>();
        }
    }
}
