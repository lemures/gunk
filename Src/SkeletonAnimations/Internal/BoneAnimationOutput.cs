﻿using UnityEngine;

namespace Src.SkeletonAnimations.Internal
{
    public class BoneAnimationOutput
    {
        public readonly int boneId;
        public Vector3 translation;
        public Vector3 rotation;

        public BoneAnimationOutput(int boneId, Vector3 translation, Vector3 rotation)
        {
            this.boneId = boneId;
            this.translation = translation;
            this.rotation = rotation;
        }
    }
}