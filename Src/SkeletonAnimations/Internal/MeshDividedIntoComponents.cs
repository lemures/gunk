﻿using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.SkeletonAnimations;
using Src.SkeletonAnimations.BoneTypes;
using UnityEngine;

namespace Src.SkeletonAnimations.Internal
{
    /// <summary>
    /// divides mesh into components to Color with assigned bones based on Bones connections and positions 
    /// </summary>
    public class MeshDividedIntoComponents
    {

        private struct Triangle
        {
            public Vector3 p1;
            public Vector3 p2;
            public Vector3 p3;

            public Triangle(Vector3 p1, Vector3 p2, Vector3 p3)
            {
                this.p1 = p1;
                this.p2 = p2;
                this.p3 = p3;
            }
        }

        public Bone[] assignedBones { get; private set;}

        public MeshDividedIntoComponents(Mesh mesh, SkeletonData skeletonData)
        {
            assignedBones=assignBonesToVerticesBasedOnSkeleton(mesh, skeletonData);
        }

        /// <summary>
        /// Returns true if any new vertex was visited
        /// </summary>
        private bool dfs(int vertexId, int connectedComponentId, HashSet<int>[] edges, bool[] visitedTab, int[] connectedComponentsIds)
        {
            if (visitedTab[vertexId])
            {
                return false;
            }
            visitedTab[vertexId] = true;
            connectedComponentsIds[vertexId] = connectedComponentId;
            foreach (var neighbour in edges[vertexId])
            {
                dfs(neighbour, connectedComponentId, edges, visitedTab, connectedComponentsIds);
            }
            return true;
        }

        private float vector3ToUniqueFloat(Vector3 data)
        {
            return data.x + data.y * 10.0f + data.z * 100.0f;
        }

        /// <summary>
        /// calculates distance from triangle to point(i.e. minimal distance from a point that belongs to that triangle to the point)
        /// </summary>
        /// <param name="a1">first vertex of triangle</param>
        /// <param name="a2">second vertex of triangle</param>
        /// <param name="a3">third vertex of triangle</param>
        /// <param name="point">point to calculate distance to</param>
        /// <returns></returns>
        private static float calculateDistanceFromTriangle(Vector3 a1, Vector3 a2, Vector3 a3, Vector3 point)
        {
            Vector3 diff = point - a1;
            Vector3 edge0 = a2 - a1;
            Vector3 edge1 = a3 - a1;
            float a00 = Vector3.Dot(edge0, edge0);
            float a01 = Vector3.Dot(edge0, edge1);
            float a11 = Vector3.Dot(edge1, edge1);
            float b0 = -Vector3.Dot(diff, edge0);
            float b1 = -Vector3.Dot(diff, edge1);
            float det = a00 * a11 - a01 * a01;
            float t0 = a01 * b1 - a11 * b0;
            float t1 = a01 * b0 - a00 * b1;

            if (t0 + t1 <= det)
            {
                if (t0 < 0.0f)
                {
                    if (t1 < 0.0f)  // region 4
                    {
                        if (b0 < 0.0f)
                        {
                            t1 = 0.0f;
                            if (-b0 >= a00)  // V1
                            {
                                t0 = 1.0f;
                            }
                            else  // E01
                            {
                                t0 = -b0 / a00;
                            }
                        }
                        else
                        {
                            t0 = 0.0f;
                            if (b1 >= 0.0f)  // V0
                            {
                                t1 = 0.0f;
                            }
                            else if (-b1 >= a11)  // V2
                            {
                                t1 = 1.0f;
                            }
                            else  // E20
                            {
                                t1 = -b1 / a11;
                            }
                        }
                    }
                    else  // region 3
                    {
                        t0 = 0.0f;
                        if (b1 >= 0.0f)  // V0
                        {
                            t1 = 0.0f;
                        }
                        else if (-b1 >= a11)  // V2
                        {
                            t1 = 1.0f;
                        }
                        else  // E20
                        {
                            t1 = -b1 / a11;
                        }
                    }
                }
                else if (t1 < 0.0f)  // region 5
                {
                    t1 = 0.0f;
                    if (b0 >= 0.0f)  // V0
                    {
                        t0 = 0.0f;
                    }
                    else if (-b0 >= a00)  // V1
                    {
                        t0 = 1.0f;
                    }
                    else  // E01
                    {
                        t0 = -b0 / a00;
                    }
                }
                else  // region 0, interior
                {
                    float invDet = 1.0f / det;
                    t0 *= invDet;
                    t1 *= invDet;
                }
            }
            else
            {
                float tmp0, tmp1, numer, denom;

                if (t0 < 0.0f)  // region 2
                {
                    tmp0 = a01 + b0;
                    tmp1 = a11 + b1;
                    if (tmp1 > tmp0)
                    {
                        numer = tmp1 - tmp0;
                        denom = a00 - 2.0f * a01 + a11;
                        if (numer >= denom)  // V1
                        {
                            t0 = 1.0f;
                            t1 = 0.0f;
                        }
                        else  // E12
                        {
                            t0 = numer / denom;
                            t1 = 1.0f - t0;
                        }
                    }
                    else
                    {
                        t0 = 0.0f;
                        if (tmp1 <= 0.0f)  // V2
                        {
                            t1 = 1.0f;
                        }
                        else if (b1 >= 0.0f)  // V0
                        {
                            t1 = 0.0f;
                        }
                        else  // E20
                        {
                            t1 = -b1 / a11;
                        }
                    }
                }
                else if (t1 < 0.0f)  // region 6
                {
                    tmp0 = a01 + b1;
                    tmp1 = a00 + b0;
                    if (tmp1 > tmp0)
                    {
                        numer = tmp1 - tmp0;
                        denom = a00 - 2.0f * a01 + a11;
                        if (numer >= denom)  // V2
                        {
                            t1 = 1.0f;
                            t0 = 0.0f;
                        }
                        else  // E12
                        {
                            t1 = numer / denom;
                            t0 = 1.0f - t1;
                        }
                    }
                    else
                    {
                        t1 = 0.0f;
                        if (tmp1 <= 0.0f)  // V1
                        {
                            t0 = 1.0f;
                        }
                        else if (b0 >= 0.0f)  // V0
                        {
                            t0 = 0.0f;
                        }
                        else  // E01
                        {
                            t0 = -b0 / a00;
                        }
                    }
                }
                else  // region 1
                {
                    numer = a11 + b1 - a01 - b0;
                    if (numer <= 0.0f)  // V2
                    {
                        t0 = 0.0f;
                        t1 = 1.0f;
                    }
                    else
                    {
                        denom = a00 - 2.0f * a01 + a11;
                        if (numer >= denom)  // V1
                        {
                            t0 = 1.0f;
                            t1 = 0.0f;
                        }
                        else  // 12
                        {
                            t0 = numer / denom;
                            t1 = 1.0f - t0;
                        }
                    }
                }
            }
            return Vector3.Distance(point, a1 + t0 * edge0 + t1 * edge1);
        }

        private static float calculateBoneDistanceFromTriangles(List<Triangle> triangles, Bone bone)
        {
            return (from triangle in triangles select calculateDistanceFromTriangle(triangle.p1, triangle.p2, triangle.p3, bone.position)).Min();
        }

        private static float calculateAverageBoneDistanceFromTriangles(List<Triangle> triangles, List<Bone> bones)
        {
            return bones.Average(bone => calculateBoneDistanceFromTriangles(triangles, bone));
        }

        private static List<List<Triangle>> groupTrianglesByComponents(int[] triangles, Vector3[] vertices, int[] componentIds)
        {
            var result = new List<List<Triangle>>();
            result.Capacity = componentIds.Max() + 1;
            for (int i = 0; i < result.Capacity; i++)
            {
                result.Add(new List<Triangle>());
            }
            for (int i = 0; i < triangles.Length; i+=3)
            {
                result[componentIds[triangles[i]]].Add(new Triangle(vertices[triangles[i]],
                    vertices[triangles[i+1]],
                    vertices[triangles[i+2]]));
            }
            return result;
        }

        /// <summary>
        /// finds unssasigned component closest to certain bones and assignes that component to their representative
        /// </summary>
        private static void assignComponentToBones(List<List<Triangle>>trianglesByComponent, List<Bone> bones, Bone representative,  Bone[] assignedBones)
        {
            var avaibleComponents = new List<int>();
            for (int i = 0; i < assignedBones.Length; i++)
            {
                if (assignedBones[i] == null)
                {
                    avaibleComponents.Add(i);
                }
            }
            if (avaibleComponents.Count == 0)
            {
                return;
            }
            var closestComponent = avaibleComponents.OrderBy(index =>
                calculateAverageBoneDistanceFromTriangles(trianglesByComponent[index],bones)).First();
            assignedBones[closestComponent] = representative;
        }

        private Bone[] assignBonesToComponents(List<List<Triangle>>trianglesByComponents, SkeletonData skeletonData)
        {
            Bone[] currentAssignedBones = new Bone[trianglesByComponents.Count];

            var importantBones = new List<Bone> {};
            var currentLayer = new List<Bone> { skeletonData.getMainBodyBone() };

            //performing bfs so that bones that are higher in hierarchy will be first in importantBones
            while (currentLayer.Count != 0)
            {
                var nextLayer = new List<Bone>();
                currentLayer.ForEach(x => nextLayer.AddRange(skeletonData.getHigherBonesInLimb(x)));
                importantBones.AddRange(currentLayer.FindAll(x => x.needsAssignedComponent()));
                currentLayer.Clear();
                currentLayer.AddRange(nextLayer);
            }

            foreach (var bone in importantBones)
            {
                var groupOfBones = skeletonData.getHigherBonesInLimb(bone);
                groupOfBones.Add(bone);
                assignComponentToBones(trianglesByComponents,groupOfBones,bone,currentAssignedBones);
            }

            if (importantBones.Count > currentAssignedBones.Length)
            {
                Debug.Log("Too many bones to assign all components!!!");
            }

            //TODO better assigning of "loose" components, now all of them are assigned to main bone
            for (var i = 0; i < currentAssignedBones.Length; i++)
            {
                if (currentAssignedBones[i] == null)
                {
                    currentAssignedBones[i] = skeletonData.getMainBodyBone();
                }
            }

            return currentAssignedBones;
        }

        private Bone[] assignBonesToVerticesBasedOnSkeleton(Mesh meshToColor,SkeletonData skeletonData)
        {
            //getting Data from mesh
            int[] triangles = (int[])meshToColor.triangles.Clone();
            Vector3[] vertices = (Vector3[])meshToColor.vertices.Clone();
            int vertexCount = vertices.Length;

            //Creating graph
            HashSet<int>[] edges = new HashSet<int>[vertexCount];
            for (var i = 0; i < edges.Length; i++)
            {
                edges[i] = new HashSet<int>();
            }
            for (int i = 0; i < triangles.Length; i += 3)
            {
                int index1 = triangles[i];
                int index2 = triangles[i + 1];
                int index3 = triangles[i + 2];

                edges[index1].Add(index3);
                edges[index1].Add(index2);
                edges[index2].Add(index1);
                edges[index2].Add(index3);
                edges[index3].Add(index1);
                edges[index3].Add(index2);
            }

            //vertices occupying the same positions in space should be treated as the same vertices
            Dictionary<float, int> verticesRepresentatives = new Dictionary<float, int>();
            int[] verticesRepresentativesTab = new int[vertexCount];
            for (int i = 0; i < vertexCount; i++)
            {
                float myHash = vector3ToUniqueFloat(vertices[i]);
                int myId = 0;
                if (!verticesRepresentatives.TryGetValue(myHash, out myId))
                    verticesRepresentatives.Add(myHash, i);
                verticesRepresentativesTab[i] = verticesRepresentatives[myHash];
            }
            //adding "virtual" edges between vertices with the same position in space
            for (int i = 0; i < vertexCount; i++)
            {
                if (verticesRepresentativesTab[i] != i)
                {
                    edges[verticesRepresentativesTab[i]].Add(i);
                    edges[i].Add(verticesRepresentativesTab[i]);
                }
            }

            //Calculating connected component ID of each vertex
            int currentComponentId = 0;
            bool[] visitedTab = new bool[vertexCount];
            int[] componentIds = new int[vertexCount];
            for (int i = 0; i < vertexCount; i++)
            {
                if (dfs(i, currentComponentId, edges, visitedTab, componentIds))
                {
                    currentComponentId++;
                }
            }

            var groupedTriangles = groupTrianglesByComponents(triangles, vertices, componentIds);

            var bonesAssignedToComponents=assignBonesToComponents(groupedTriangles,skeletonData);

            var bonesAssignedToVertices = new Bone[vertexCount];

            for (int i = 0; i < bonesAssignedToVertices.Length; i++)
            {
                bonesAssignedToVertices[i] = bonesAssignedToComponents[componentIds[i]];
            }

            return bonesAssignedToVertices;
        }
    }
}
