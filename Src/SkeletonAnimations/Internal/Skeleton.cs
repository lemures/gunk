﻿using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.SkeletonAnimations;
using Src.SkeletonAnimations.BoneTypes;
using UnityEngine;

namespace Src.SkeletonAnimations.Internal
{
    [System.Serializable]
    public class Skeleton
    {
        public Skeleton(string skeletonName)
        {
            revertFromFile(skeletonName);
            if (skeletonData == null)
            {
                skeletonData = new SkeletonData();
            }
        }

        public Skeleton()
        {
            skeletonData = new SkeletonData();
        }

        public static Skeleton constructSkeletonWithSingleSocketBone()
        {
            var result = new Skeleton();
            result.skeletonData.addNextSocketBone(new Float4(Vector3.zero));
            return result;
        }

        public SkeletonData skeletonData { get; private set; }

        public bool importFromMesh(Mesh skeletonMesh)
        {
            skeletonData = null;

            if (skeletonMesh==null)
            { return false; }

            MeshFbxUtils.fixMeshRotation(skeletonMesh);

            skeletonData = new SkeletonData();

            var positions = new Vector3[32];
            var centres = new Vector3[32];
            var sockets = new Vector3[4];
            for (var i = 0; i < skeletonMesh.colors.Length; i++)
            {

                var current = skeletonMesh.colors[i];
                var limbNum = (int)(current.r * 10.1f);
                var segmNum = (int)(current.g * 4.1f);
                if (current.b > 0.45f && current.b < 0.55f)
                {
                    positions[limbNum * 4 + segmNum] += skeletonMesh.vertices[i];
                }
                if (current.b < 0.05f)
                {
                    centres[limbNum * 4 + segmNum] += skeletonMesh.vertices[i];
                }
                if (current.b > 0.95f)
                {
                    sockets[limbNum] += skeletonMesh.vertices[i];
                }
            }

            for (var i = 0; i < 32; i++)
            {
                if (positions[i] != new Vector3(0, 0, 0))
                {
                    positions[i] /= 8.0f;
                    skeletonData.addBasicBone(i % 4, i / 4, new Float4((positions[i])));
                }
            }
            for (var i = 0; i < 32; i++)
            {
                if (centres[i] != new Vector3(0, 0, 0))
                {
                    centres[i] /= 8.0f;
                    skeletonData.addBasicBoneOffset(i % 4, i / 4, new Float4((centres[i])));
                }
            }
            for (var i = 0; i < 4; i++)
            {
                sockets[i] /= 8.0f;
                if (sockets[i] == new Vector3(0, 0, 0))
                {
                    continue;
                }
                skeletonData.addNextSocketBone(new Float4((sockets[i])));
            }
            return true;
        }

        private static string getDatabaseName(string skeletonName)
        {
            return skeletonName+"_skeletonData";
        }

        /// <summary>
        /// saves internalData on disk, saved instance are identified by skeletonName
        /// </summary>
        /// <param name="skeletonName"></param>
        public void saveSkeletonDataToFile(string skeletonName)
        {
            DatabasesManager.saveObject(skeletonData, getDatabaseName(skeletonName), "skeletons");
        }

        public static void deleteSavedData(string skeletonName)
        {
            DatabasesManager.deleteAllVersions(getDatabaseName(skeletonName), "skeletons");
        }

        public static bool isDatabaseValid(string databaseName)
        {
            return DatabasesManager.getObjectLatestVersion(databaseName, "skeletons") is SkeletonData;
        }

        /// <summary>
        /// loads from disk last data that were saved under this name and returns true if was successfully imported
        /// </summary>
        public bool revertFromFile(string skeletonName)
        {
            skeletonData = DatabasesManager.getObjectLatestVersion(getDatabaseName(skeletonName), "skeletons")as SkeletonData;
            return skeletonData != null;
        }

        public MeshDividedIntoComponents getMeshDividedOnBaseOfSkeleton(Mesh meshToColor)
        {
            return new MeshDividedIntoComponents(meshToColor, skeletonData);
        }

        public List<SocketData> getAllSocketBones()
        {
            var result =
                from bone in skeletonData.getAllBones()
                where bone is SocketBone
                select new SocketData(bone.position, (bone as SocketBone).rotation);
            return result.ToList();
        }

        public void addBonesToAnimator(int animatorId)
        {
            skeletonData.addBonesToAnimatorsManager(animatorId);
        }

        public override string ToString() => skeletonData.ToString();
    }
}

