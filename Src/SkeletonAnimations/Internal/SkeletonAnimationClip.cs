using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.SkeletonAnimations;
using Src.SkeletonAnimations.CurvesTypes;
using UnityEngine;

namespace Src.SkeletonAnimations.Internal
{
    [System.Serializable]
    public class SkeletonAnimationClip
    {
        public enum BoneAnimationParameter
        {
            XRotation,
            YRotation,
            ZRotation,
            XTranslation,
            YTranslation,
            ZTranslation
        }

        public string name;

        [System.Serializable]
        private class BoneAnimationCurvesCollection
        {
            public readonly int boneId;

            private IAnimationCurve xRotation;
            private IAnimationCurve yRotation;
            private IAnimationCurve zRotation;

            private IAnimationCurve xTranslation;
            private IAnimationCurve yTranslation;
            private IAnimationCurve zTranslation;

            public BoneAnimationOutput evaluateAtPoint(float time, float argument)
            {
                var xRotationValue = xRotation?.evaluate(time, argument) ?? 0.0f;
                var yRotationValue = yRotation?.evaluate(time, argument) ?? 0.0f;
                var zRotationValue = zRotation?.evaluate(time, argument) ?? 0.0f;
                var rotation = new Vector3(xRotationValue, yRotationValue, zRotationValue);

                var xTranslationValue = xTranslation?.evaluate(time, argument) ?? 0.0f;
                var yTranslationValue = yTranslation?.evaluate(time, argument) ?? 0.0f;
                var zTranslationValue = zTranslation?.evaluate(time, argument) ?? 0.0f;
                var translation = new Vector3(xTranslationValue, yTranslationValue, zTranslationValue);

                return new BoneAnimationOutput(boneId, translation, rotation);
            }

            public void assignCurveToParameter(BoneAnimationParameter parameterToSet, IAnimationCurve curve)
            {
                switch (parameterToSet)
                {
                    case BoneAnimationParameter.XRotation:
                        xRotation = curve;
                        break;
                    case BoneAnimationParameter.YRotation:
                        yRotation = curve;
                        break;
                    case BoneAnimationParameter.ZRotation:
                        zRotation = curve;
                        break;
                    case BoneAnimationParameter.XTranslation:
                        xTranslation = curve;
                        break;
                    case BoneAnimationParameter.YTranslation:
                        yTranslation = curve;
                        break;
                    case BoneAnimationParameter.ZTranslation:
                        zTranslation = curve;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(parameterToSet), parameterToSet, null);
                }
            }

            public IAnimationCurve getAssignedCurve(BoneAnimationParameter parameter)
            {
                switch (parameter)
                {
                    case BoneAnimationParameter.XRotation:
                        return xRotation;
                    case BoneAnimationParameter.YRotation:
                        return yRotation;
                    case BoneAnimationParameter.ZRotation:
                        return zRotation;
                    case BoneAnimationParameter.XTranslation:
                        return xTranslation;
                    case BoneAnimationParameter.YTranslation:
                        return yTranslation;
                    case BoneAnimationParameter.ZTranslation:
                        return zTranslation;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(parameter), parameter, null);
                }
            }

            public void removeCurve(BoneAnimationParameter parameter)
            {
                assignCurveToParameter(parameter, null);
            }

            public List< KeyValuePair<BoneAnimationParameter,IAnimationCurve>> getAssignedCurves()
            {
                var values = Enum.GetValues(typeof(BoneAnimationParameter)).OfType<BoneAnimationParameter>().ToList();
                return (from parameter in values where getAssignedCurve(parameter) != null select
                    new KeyValuePair<BoneAnimationParameter, IAnimationCurve>(parameter,getAssignedCurve(parameter))).ToList();
            }

            public BoneAnimationCurvesCollection(int boneId)
            {
                this.boneId = boneId;
            }

            private BoneAnimationCurvesCollection()
            {}
        }

        private readonly List<BoneAnimationCurvesCollection> bonesCurvesCollections = new List<BoneAnimationCurvesCollection>();

        private BoneAnimationCurvesCollection findBoneCurvesCollection(int boneId)
            => bonesCurvesCollections.Find(x => x.boneId == boneId);

        private void addMissingCollection(int boneId)
        {
            if (findBoneCurvesCollection(boneId) == null)
            {
                bonesCurvesCollections.Add(new BoneAnimationCurvesCollection(boneId));
            }
        }

        public List<int> getAllBonesHavingCurves()
        {
            return (from collection in bonesCurvesCollections select collection.boneId).ToList();
        }

        public List<KeyValuePair<BoneAnimationParameter, IAnimationCurve>>getAllCurvesForBone(int boneId)
        {
            return findBoneCurvesCollection(boneId).getAssignedCurves();
        }

        public void addOrReplaceCurve(int boneId, BoneAnimationParameter parameterToSet, IAnimationCurve curveToSet)
        {
            addMissingCollection(boneId);
            findBoneCurvesCollection(boneId).assignCurveToParameter(parameterToSet, curveToSet);
        }

        public List<BoneAnimationOutput> evaluateAtPoint(float time, float argument = 0.0f)
        {
            var result = new List<BoneAnimationOutput>();
            foreach (var curvesCollection in bonesCurvesCollections)
            {
                result.Add(curvesCollection.evaluateAtPoint(time, argument));
            }

            return result;
        }

        public SkeletonAnimationClip(string name)
        {
            this.name = name;
        }
    }
}
