using System;
using System.Collections.Generic;

namespace Src.SkeletonAnimations.Internal
{
    /// <summary>
    /// data structure for simulating smooth transitions between animation clips
    /// keeps collection of clips where each clip gets assigned weight, sum of all weights is 1.0
    /// new added clip gets weight 0.0, when update is called, weights from previously added clips
    /// are transfered to last added clips
    /// </summary>
    public class WeightedClipsCollection
    {
        public class WeightedClip
        {
            public string name;
            public float weight;

            public WeightedClip(string name, float weight)
            {
                this.name = name;
                this.weight = weight;
            }
        }

        private List<WeightedClip>previousClips = new List<WeightedClip>();
        private WeightedClip currentClip;

        /// <summary>
        /// adds clip as "currently playing"
        /// </summary>
        public void addClip(string name)
        {
            previousClips.Add(currentClip);
            currentClip = new WeightedClip(name, 0.0f);
        }

        /// <summary>
        /// transfers weights from previous clips to last added clip
        /// </summary>
        public void update(float timeElapsed)
        {
            var weightToAdd = Math.Min(1.0f - currentClip.weight, timeElapsed);

            previousClips.ForEach(x => x.weight -= (weightToAdd / (1.0f - currentClip.weight)) * x.weight);
            previousClips.RemoveAll(x => x.weight < 0.0000001f);

            currentClip.weight += weightToAdd;
        }

        /// <summary>
        /// returns all added clips that have weights greater than zero
        /// </summary>
        public List<WeightedClip>getAllClips()
        {
            var result = new List<WeightedClip>();
            result.Add(currentClip);
            result.AddRange(previousClips);

            return result;
        }

        public WeightedClipsCollection(string startClipName)
        {
            currentClip = new WeightedClip(startClipName, 1.0f);
        }

        public float getCurrentClipWeight => currentClip.weight;
    }
}