﻿using System.Linq;
using Gunk_Scripts;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.SkeletonAnimations;
using Src.SkeletonAnimations.Internal;
using UnityEngine;

namespace Src.SkeletonAnimations
{
    public static class MeshColorInitializer
    {
        private static Color getColorById(int id)
        {
            //assumes that id is less than 1000
            var r = (id / 100)/10.0f;
            var g = ((id / 10)%10) / 10.0f;
            var b = (id %10) / 10.0f;
            return new Color(r, g, b, 1.0f);
        }

        private static void colorMesh(Mesh meshToColor,MeshDividedIntoComponents meshDividedIntoComponents)
        {
            var assignedBones = meshDividedIntoComponents.assignedBones;

            var colors = new Color[assignedBones.Length];
            for (int i = 0; i < assignedBones.Length; i++)
            {
                colors[i] =getColorById(SkeletonData.getBoneId(assignedBones[i]));
            }

            meshToColor.colors = colors;
        }

        public static void colorAllMeshesFromPrefab(IPrefabManager manager, string prefabName)
        {
            var prefab = manager.findPrefab(prefabName);
            if (prefab == null)
            {
                Debug.Log("unable to color meshes for: "+ prefabName+",  prefab does not exist!");
                return;
            }
            var meshes = from comp in prefab.go.GetComponentsInChildren<MeshFilter>() select comp.sharedMesh;
            MeshFbxUtils.fixMeshRotation(prefab.meshSkeleton);
            var skeleton = new Skeleton(prefabName);
            if (prefab.meshSkeleton != null)
            {
                skeleton.importFromMesh(prefab.meshSkeleton);
            }

            foreach (var mesh in meshes)
            {
                var divided=skeleton.getMeshDividedOnBaseOfSkeleton(mesh);
                colorMesh(mesh, divided);
            }
        }
    }
}

