using System;
using System.Collections.Generic;

namespace Src.SkeletonAnimations
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class NeedsClip : Attribute
    {
        public string clipName;

        public NeedsClip(string text)
        {
            clipName = text;
        }
    }
}