using Src.SkeletonAnimations.Internal;

namespace Src.SkeletonAnimations
{
    public class SkeletonAnimations
    {
        public static void forcedSendDataToShaders()
        {
            AnimatorsManager.sendAnimationDataToShaders();
        }

        public static void enableLogging()
        {
            AnimatorsManager.enableLogging();
        }

        public static void disableLogging()
        {
            AnimatorsManager.disableLogging();
        }
    }
}