using System;
using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts;
using Gunk_Scripts.SkeletonAnimations;
using Src.GunkComponents;
using Src.Libraries;
using Src.SkeletonAnimations.BoneTypes;
using Src.SkeletonAnimations.Events;
using Src.SkeletonAnimations.Internal;
using UnityEngine;

namespace Src.SkeletonAnimations
{
    public class SkeletonAnimator
    {
        private ActivePose currentPose;
        private float currentArgument = 0.0f;
        private float desiredArgument = 0.0f;
        private readonly int myId;

        public string currentClip;

        public bool isCurrentClipLoaded()
        {
            return currentPose.isCurrentClipLoaded() && Math.Abs(currentArgument - desiredArgument) < 0.1f ;
        }

        public void playClip(string name, float argument = 0)
        {
            var clip = AnimationClipDatabase.getClip(name);

            desiredArgument = argument;
            currentArgument = argument;
            currentClip = name;
            currentPose.playNewClip(clip, argument);
        }

        public void changeClipArgument(float value)
        {
            if (float.IsNaN(value))
            {
                throw new ArgumentOutOfRangeException(nameof(value), "clip argument name cannot be NaN");
            }
            desiredArgument = value;
        }

        public List<EventData> collectTriggeredEvents()
        {
            throw new System.NotImplementedException();
        }

        public List<SocketData> getAllSocketBones()
        {
            var result =
                from bone in skeletonData.getAllBones()
                where bone is SocketBone
                select new SocketData(bone.position, (bone as SocketBone).rotation);
            return result.ToList();
        }

        private SkeletonData skeletonData;

        public SkeletonAnimator(string skeletonDataName, IRenderersParametersManager objectManager,int objectId, string startClipName = "")
        {
            skeletonData = new Skeleton(skeletonDataName).skeletonData;
            myId = AnimatorsManager.addAnimator(objectManager, objectId);
            skeletonData.addBonesToAnimatorsManager(myId);
            var startClip = AnimationClipDatabase.getClip(startClipName);
            currentPose = new ActivePose(startClip);
            currentClip = startClipName;
        }

        public void updateAnimation(float timeElapsed)
        {
            var change = Math.Min(timeElapsed, Math.Abs(desiredArgument - currentArgument));
            currentArgument += (currentArgument < desiredArgument ? 1 : -1) * change;

            currentPose.updateArgument(currentArgument);
            currentPose.update(timeElapsed);

            var output = currentPose.evaluateCurrentPose();
            foreach (var animationOutput in output)
            {
                AnimatorsManager.updateRotation(myId, animationOutput.boneId, animationOutput.rotation);
                AnimatorsManager.updateTranslation(myId, animationOutput.boneId, animationOutput.translation);
            }
        }

        public void destroy()
        {
            AnimatorsManager.removeAnimator(myId);
        }

        public override string ToString()
        {
            return new VariablesReportBuilder(nameof(SkeletonAnimatorComponent))
                .addVariable(myId, nameof(myId))
                .addVariable(currentArgument, nameof(currentArgument))
                .addVariable(skeletonData, nameof(skeletonData))
                .print();
        }

        public Vector3 getBarrelBoneWorldPosition(int index, Vector3 objectPosition)
        {
            return AnimatorsManager.getBonePosition(myId, skeletonData.getBarrelBoneId(index), objectPosition);
        }
    }
}