using UnityEngine;

namespace Src.SkeletonAnimations
{
    public class SocketData
    {
        public Vector3 position;
        public float rotation;

        public SocketData(Vector3 position, float rotation)
        {
            this.position = position;
            this.rotation = rotation;
        }
    }
}