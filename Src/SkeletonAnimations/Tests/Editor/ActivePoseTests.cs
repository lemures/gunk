using FakeItEasy;
using Gunk_Scripts.SkeletonAnimations;
using NUnit.Framework;
using Src.SkeletonAnimations.CurvesTypes;
using Src.SkeletonAnimations.Internal;

namespace Src.SkeletonAnimations.Tests.Editor
{
    public class ActivePoseTests
    {
        [Test]
        public void noAdditionalClipsAddedProduceCorrectOutput()
        {
            var myClip = new SkeletonAnimationClip("fakeClip");
            myClip.addOrReplaceCurve(13, SkeletonAnimationClip.BoneAnimationParameter.XRotation, new ConstantValueCurve(5.0f));
            myClip.addOrReplaceCurve(17, SkeletonAnimationClip.BoneAnimationParameter.ZTranslation, new ConstantValueCurve(9.0f));

            var pose = new ActivePose(myClip);

            var poseOutput = pose.evaluateCurrentPose();
            Assert.AreEqual(5.0f, poseOutput.Find(x => x.boneId == 13).rotation.x);
            Assert.AreEqual(9.0f, poseOutput.Find(x => x.boneId == 17).translation.z);
        }

        [Test]
        public void secondClipAddedWithTimePassedProducesCorrectOutput()
        {
            var startClip = new SkeletonAnimationClip("fakeClip");
            startClip.addOrReplaceCurve(13, SkeletonAnimationClip.BoneAnimationParameter.XRotation, new ConstantValueCurve(5.0f));
            startClip.addOrReplaceCurve(17, SkeletonAnimationClip.BoneAnimationParameter.ZTranslation, new ConstantValueCurve(9.0f));

            var newClip = new SkeletonAnimationClip("fakeClip");
            newClip.addOrReplaceCurve(13, SkeletonAnimationClip.BoneAnimationParameter.XRotation, new ConstantValueCurve(1.0f));
            newClip.addOrReplaceCurve(17, SkeletonAnimationClip.BoneAnimationParameter.ZTranslation, new ConstantValueCurve(2.0f));

            var pose = new ActivePose(startClip);
            pose.playNewClip(newClip);
            pose.update(1.0f);

            var poseOutput = pose.evaluateCurrentPose();
            Assert.AreEqual(1.0f, poseOutput.Find(x => x.boneId == 13).rotation.x);
            Assert.AreEqual(2.0f, poseOutput.Find(x => x.boneId == 17).translation.z);
        }

        [Test]
        public void argumentCalculationsAreCorrect()
        {
            var startClip = new SkeletonAnimationClip("fakeClip");
            startClip.addOrReplaceCurve(13, SkeletonAnimationClip.BoneAnimationParameter.XRotation, new FirstArgumentDependentCurve());

            var newClip = new SkeletonAnimationClip("fakeClip");
            newClip.addOrReplaceCurve(13, SkeletonAnimationClip.BoneAnimationParameter.XRotation, new FirstArgumentDependentCurve());

            var pose = new ActivePose(startClip);
            pose.updateArgument(7.9f);
            pose.playNewClip(newClip, 3.5f);
            pose.update(0.5f);

            var poseOutput = pose.evaluateCurrentPose();
            Assert.AreEqual((3.5f + 7.9f) / 2.0f, poseOutput.Find(x => x.boneId == 13).rotation.x);
        }
    }
}