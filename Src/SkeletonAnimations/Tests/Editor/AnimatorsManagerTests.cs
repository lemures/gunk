using FakeItEasy;
using Gunk_Scripts;
using Gunk_Scripts.Asset_Importing;
using NUnit.Framework;
using Src.Libraries;
using Src.SkeletonAnimations.Internal;
using UnityEngine;

namespace Src.SkeletonAnimations.Tests.Editor
{
    public class AnimatorsManagerTests
    {
        [Test]
        public void getPositionWorksForOneBoneChain()
        {
            var fakeManager = A.Fake<IRenderersParametersManager>();
            var animatorId = AnimatorsManager.addAnimator(fakeManager, 0);
            var boneCenter = new Vector3(6, 8, 9);
            AnimatorsManager.addBone(animatorId, 0, -1, Vector3.zero, boneCenter);
            var worldPosition = new Vector3(3, 5, 7);
            var bonePosition = AnimatorsManager.getBonePosition(animatorId, 0, worldPosition);

            AnimatorsManager.removeAnimator(animatorId);
            Assert.AreEqual(0, Vector3.Distance(worldPosition + boneCenter, bonePosition));
        }

        [Test]
        public void getPositionCorrectlyCalculatesXRotationForTwoBoneChain()
        {
            var fakeManager = A.Fake<IRenderersParametersManager>();
            var animatorId = AnimatorsManager.addAnimator(fakeManager, 0);
            var boneCenter1 = new Vector3(1, 2, 3);
            var boneCenter2 = new Vector3(13, 15, 19);

            AnimatorsManager.addBone(animatorId, 0, -1, Vector3.zero, boneCenter1);
            AnimatorsManager.addBone(animatorId, 1, 0, Vector3.zero, boneCenter2 + boneCenter1);
            AnimatorsManager.updateRotation(animatorId, 0, new Vector3(0.5f, 0, 0));

            var worldPosition = Vector3.zero;
            var bonePosition = AnimatorsManager.getBonePosition(animatorId, 1, worldPosition);
            AnimatorsManager.removeAnimator(animatorId);

            Assert.AreEqual(boneCenter1.x + boneCenter2.x, bonePosition.x, 0.001f);
            Assert.AreEqual(boneCenter1.y - boneCenter2.y, bonePosition.y, 0.001f);
            Assert.AreEqual(boneCenter1.z - boneCenter2.z, bonePosition.z, 0.001f);
        }
    }
}