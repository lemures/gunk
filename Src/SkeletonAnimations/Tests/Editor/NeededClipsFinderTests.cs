using NUnit.Framework;
using Src.SkeletonAnimations.Internal;

namespace Src.SkeletonAnimations.Tests.Editor
{
    public class NeededClipsFinderTests
    {
        [NeedsClip("blah1")]
        [NeedsClip("blah2")]
        [NeedsClip("blah3")]
        private class TestClass
        {}

        [Test]
        public void newAddedClipIsFound()
        {
            Assert.Contains("blah1", NeededClipsFinder.getAllNeededScripts());
            Assert.Contains("blah2", NeededClipsFinder.getAllNeededScripts());
            Assert.Contains("blah3", NeededClipsFinder.getAllNeededScripts());
        }
    }
}