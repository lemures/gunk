using FakeItEasy;
using Gunk_Scripts.SkeletonAnimations;
using NUnit.Framework;
using Src.SkeletonAnimations.CurvesTypes;
using Src.SkeletonAnimations.Internal;

namespace Src.SkeletonAnimations.Tests.Editor
{
    public class SkeletonAnimationClipTests
    {
        [Test]
        public void getAllBonesHavingCurvesReturnsNewlyAddedBone()
        {
            var clip = new SkeletonAnimationClip("");
            var unimportantParameter = SkeletonAnimationClip.BoneAnimationParameter.XRotation;
            var boneId = 42;

            clip.addOrReplaceCurve(boneId, unimportantParameter, A.Fake<IAnimationCurve>());
            Assert.Contains(boneId,clip.getAllBonesHavingCurves());
        }

        [Test]
        public void getAllBonesHavingCurvesReturnsAllAddedBones()
        {
            var clip = new SkeletonAnimationClip("");
            var unimportantParameter = SkeletonAnimationClip.BoneAnimationParameter.XRotation;

            var boneId1 = 42;
            var boneId2 = 46;
            var boneId3 = 49;

            clip.addOrReplaceCurve(boneId1, unimportantParameter, A.Fake<IAnimationCurve>());
            clip.addOrReplaceCurve(boneId2, unimportantParameter, A.Fake<IAnimationCurve>());
            clip.addOrReplaceCurve(boneId3, unimportantParameter, A.Fake<IAnimationCurve>());

            Assert.Contains(boneId1,clip.getAllBonesHavingCurves());
            Assert.Contains(boneId2,clip.getAllBonesHavingCurves());
            Assert.Contains(boneId3,clip.getAllBonesHavingCurves());
        }

        [Test]
        public void getAllCurvesForBonesWorksCanReturnMultipleCurvesForDifferentParameters()
        {
            var clip = new SkeletonAnimationClip("");
            var unimportantParameter1 = SkeletonAnimationClip.BoneAnimationParameter.XRotation;
            var unimportantParameter2 = SkeletonAnimationClip.BoneAnimationParameter.YRotation;
            var boneId = 42;

            clip.addOrReplaceCurve(boneId, unimportantParameter1, A.Fake<IAnimationCurve>());
            clip.addOrReplaceCurve(boneId, unimportantParameter2, A.Fake<IAnimationCurve>());
            Assert.AreEqual(2, clip.getAllCurvesForBone(boneId).Count);
        }

        [Test]
        public void addingNewCurveWithSameParameterOverridesOldValue()
        {
            var clip = new SkeletonAnimationClip("");
            var unimportantParameter1 = SkeletonAnimationClip.BoneAnimationParameter.XRotation;
            var unimportantParameter2 = SkeletonAnimationClip.BoneAnimationParameter.YRotation;
            var boneId = 42;

            clip.addOrReplaceCurve(boneId, unimportantParameter1, A.Fake<IAnimationCurve>());
            clip.addOrReplaceCurve(boneId, unimportantParameter2, A.Fake<IAnimationCurve>());
            clip.addOrReplaceCurve(boneId, unimportantParameter2, A.Fake<IAnimationCurve>());
            Assert.AreEqual(2, clip.getAllCurvesForBone(boneId).Count);
        }

        [Test]
        public void evaluatingCurveValuesAtPointReturnsExpectedValuesForSingleCurve()
        {
            var clip = new SkeletonAnimationClip("");
            var unimportantParameter = SkeletonAnimationClip.BoneAnimationParameter.XRotation;
            var boneId = 42;
            var curve = new ConstantValueCurve(7.0f);

            clip.addOrReplaceCurve(boneId, unimportantParameter, curve);
            var result = clip.evaluateAtPoint(0.0f);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(boneId,result[0].boneId);
            Assert.AreEqual(7.0f,result[0].rotation.x);
        }

        [Test]
        public void evaluatingCurveSuccessfullyEvaluatesRotation()
        {
            var clip = new SkeletonAnimationClip("");
            var boneId = 42;
            var curve1 = new ConstantValueCurve(1.0f);
            var curve2 = new ConstantValueCurve(3.0f);
            var curve3 = new ConstantValueCurve(5.0f);

            clip.addOrReplaceCurve(boneId, SkeletonAnimationClip.BoneAnimationParameter.XRotation, curve1);
            clip.addOrReplaceCurve(boneId, SkeletonAnimationClip.BoneAnimationParameter.YRotation, curve2);
            clip.addOrReplaceCurve(boneId, SkeletonAnimationClip.BoneAnimationParameter.ZRotation, curve3);

            var result = clip.evaluateAtPoint(0.0f);
            Assert.AreEqual(1.0f,result[0].rotation.x);
            Assert.AreEqual(3.0f,result[0].rotation.y);
            Assert.AreEqual(5.0f,result[0].rotation.z);
        }
    }
}