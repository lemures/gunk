﻿using Gunk_Scripts.SkeletonAnimations;
using NUnit.Framework;
using Src.SkeletonAnimations.Internal;

namespace Src.SkeletonAnimations.Tests.Editor
{
    public class SkeletonTests
    {
        private readonly string testSkeletonName = "testSkeleton";

        [Test]
        public void saveToFileTest()
        {
            Skeleton skeleton = new Skeleton();
            skeleton.saveSkeletonDataToFile(testSkeletonName);
            Assert.IsTrue(skeleton.revertFromFile(testSkeletonName));
            Skeleton.deleteSavedData(testSkeletonName);
        }
    }
}
