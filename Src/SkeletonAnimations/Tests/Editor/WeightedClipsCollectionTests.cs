using NUnit.Framework;
using Src.SkeletonAnimations.Internal;

namespace Src.SkeletonAnimations.Tests.Editor
{
    public class WeightedClipsCollectionTests
    {
        [Test]
        public void exactlyOneClipIsPresentAtCreationWithWeightOneAndCorrectName()
        {
            var clipName = "myClip";
            var clipsCollection = new WeightedClipsCollection(clipName);

            var result = clipsCollection.getAllClips();

            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(clipName, result[0].name);
            Assert.AreEqual(1.0f, result[0].weight, 0.000001f);
        }

        [Test]
        public void secondAddedClipHasZeroWeight()
        {
            var clipName1 = "myClip1";
            var clipName2 = "myClip2";
            var clipsCollection = new WeightedClipsCollection(clipName1);

            clipsCollection.addClip(clipName2);
            var result = clipsCollection.getAllClips();

            var newClip = result.Find(x => x.name == clipName2);
            Assert.AreEqual(0.0f, newClip.weight, 0.000001f);
        }

        [Test]
        public void secondAddedClipHasWeightOneAfterOneTimeUnit()
        {
            var clipName1 = "myClip1";
            var clipName2 = "myClip2";
            var clipsCollection = new WeightedClipsCollection(clipName1);

            clipsCollection.addClip(clipName2);
            clipsCollection.update(1.0f);
            var result = clipsCollection.getAllClips();

            var newClip = result.Find(x => x.name == clipName2);
            Assert.AreEqual(1.0f, newClip.weight, 0.000001f);
        }

        [Test]
        public void weightsSumToOneWhenManyClipsAreAdded()
        {
            var clipName = "myClip";
            var clipsCollection = new WeightedClipsCollection(clipName);
            for (int i = 0; i < 10; i++)
            {
                clipsCollection.addClip(clipName);
                clipsCollection.update(0.05f);
            }

            var result = clipsCollection.getAllClips();

            float sum = 0.0f;
            result.ForEach(x => sum += x.weight);

            Assert.AreEqual(1.0f, sum, 0.00001f);
        }
    }
}