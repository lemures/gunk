﻿using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.Tests.DatabasesManagerTestsResources
{
    public class TestScriptableObject : ScriptableObject
    {
        public int a;
        public int b;
        public List<int> c;
    }
}
