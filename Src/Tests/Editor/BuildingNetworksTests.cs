using System;
using System.Linq;
using Gunk_Scripts.Data_Structures;
using NUnit.Framework;
using UnityEngine;
using Random = System.Random;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class BuildingNetworksTests
    {
        [Test]
        public void addObjectDoesNotResultInErrorTest()
        {
            var objectNetworks=new ObjectNetworks();
            for (int i = 0; i < 100; i++)
            {
                objectNetworks.addObject(i, new Vector3(0, 0, 0));
            }
        }

        [Test]
        public void addSameObjectTwiceProducesErrorTest()
        {
            var objectNetworks=new ObjectNetworks();
            objectNetworks.addObject(0, Vector3.zero);
            Assert.Catch(() => objectNetworks.addObject(0, Vector3.zero));
        }

        [Test]
        public void addSameConnectorTwiceProducesErrorTest()
        {
            var objectNetworks=new ObjectNetworks();
            objectNetworks.addConnector(0, Vector3.zero, 5.0f);
            Assert.Catch(() => objectNetworks.addConnector(0, Vector3.zero,5.0f));
        }

        [Test]
        public void removeNonExistentObjectProducesErrorTest()
        {
            var objectNetworks = new ObjectNetworks();
            Assert.Catch(() => objectNetworks.removeObject(0));
        }

        [Test]
        public void removingObjectReallyRemovesItTest()
        {
            var objectNetworks = new ObjectNetworks();
            objectNetworks.addObject(0, new Vector3(0, 0, 0));
            objectNetworks.removeObject(0);
            var addedEdges=objectNetworks.addConnector(1, new Vector3(1, 1, 1),10.0f).addedEdges;
            Assert.IsEmpty(addedEdges);
        }

        [Test]
        public void removingObjectRemovesEdgesTest()
        {
            var objectNetworks = new ObjectNetworks();
            objectNetworks.addObject(0, new Vector3(0, 0, 0));
            objectNetworks.addConnector(1, new Vector3(5, 0, 0),10.0f);
            var removedEdges=objectNetworks.removeObject(0).removedEdges;
            Assert.IsNotEmpty(removedEdges);
        }

        [Test]
        public void removeObjectDoesNotThrowExceptionsTest()
        {
            var objectNetworks = new ObjectNetworks();
            objectNetworks.addObject(0, new Vector3(0, 0, 0));
            objectNetworks.removeObject(0);
            objectNetworks.addObject(0, new Vector3(1, 1, 1));
        }

        [Test]
        public void connectorConnectsToBuildingInRangeTest()
        {
            var objectNetworks = new ObjectNetworks();
            objectNetworks.addObject(0, new Vector3(0, 0, 0));
            var edges=objectNetworks.addConnector(1, new Vector3(10, 0, 0), 20.0f);

            Assert.AreEqual(1,edges.addedEdges.Count);
            Assert.AreEqual(new Edge(0, 1), edges.addedEdges[0]);
        }

        [Test]
        public void connectorDoesNotConnectToBuildingOutsideRangeTest()
        {
            var objectNetworks = new ObjectNetworks();
            objectNetworks.addObject(0, new Vector3(0, 0, 0));
            var edges=objectNetworks.addConnector(1, new Vector3(10, 0, 0), 5.0f);

            Assert.AreEqual(0,edges.addedEdges.Count);
        }

        [Test]
        public void removingBuildingRemovesConnectionsTest()
        {
            var objectNetworks = new ObjectNetworks();
            objectNetworks.addObject(0, new Vector3(0, 0, 0));
            objectNetworks.addConnector(1, new Vector3(10, 0, 0), 20.0f);
            var removedEdges = objectNetworks.removeObject(0);

            Assert.AreEqual(1,removedEdges.removedEdges.Count);
            Assert.AreEqual(new Edge(0,1), removedEdges.removedEdges[0] );
        }

        [Test]
        public void networksRepairsItselfTest()
        {
            var objectNetworks = new ObjectNetworks();
            objectNetworks.addObject(0, new Vector3(0, 0, 0));
            objectNetworks.addConnector(1, new Vector3(10, 0, 0), 30.0f);
            objectNetworks.addConnector(2, new Vector3(20, 0, 0), 30.0f);
            var changes=objectNetworks.removeObject(1);
            Assert.AreEqual(1,changes.addedEdges.Count);
            Assert.AreEqual(new Edge(0,2),changes.addedEdges[0]);
        }

        [Test]
        public void transformIntoConnectorProducesAnyEdgesTest()
        {
            var objectNetworks=new ObjectNetworks();
            objectNetworks.addObject(0, new Vector3(0, 0, 0));
            objectNetworks.addObject(1, new Vector3(10, 0, 0));
            var addedEdges=objectNetworks.transformIntoConnector(1, 20).addedEdges;
            Assert.AreEqual(1,addedEdges.Count);
            Assert.AreEqual(new Edge(0,1),addedEdges[0]);
        }

        [Test]
        public void transformIntoConnectorTwiceOnSameObjectProducesError()
        {
            var objectNetworks=new ObjectNetworks();
            objectNetworks.addObject(0, new Vector3(0, 0, 0));
            objectNetworks.transformIntoConnector(0, 10);
            Assert.Catch(() => objectNetworks.transformIntoConnector(0, 10));
        }

        [Test]
        public void transformIntoConnectorOnNonExistentObjectProducesError()
        {
            var objectNetworks=new ObjectNetworks();
            Assert.Catch(() => objectNetworks.transformIntoConnector(0, 10));
        }

        [Test]
        public void getConnectedBuildingSetsProducesValidResultsTest()
        {
            var objectNetworks=new ObjectNetworks();

            //first set
            objectNetworks.addConnector(0, new Vector3(0, 0, 0), 20);
            objectNetworks.addConnector(1, new Vector3(0, 2, 0), 20);
            objectNetworks.addConnector(2, new Vector3(0, 4, 0), 20);
            objectNetworks.addConnector(3, new Vector3(0, 6, 0), 20);

            //second set
            objectNetworks.addConnector(4, new Vector3(0, 40, 0), 20);

            var sets = objectNetworks.getAllConnectedObjectsSets();
            sets.Sort((x,y)=>x.Count.CompareTo(y.Count));
            sets.ForEach(x=>x.Sort());

            Assert.Contains(4,sets[0]);

            Assert.Contains(0,sets[1]);
            Assert.Contains(1,sets[1]);
            Assert.Contains(2,sets[1]);
            Assert.Contains(3,sets[1]);
        }

        [Test]
        public void addingObjectAndThenConnectorWithSameIdProducesError()
        {
            var objectNetworks=new ObjectNetworks();

            objectNetworks.addObject(0, new Vector3(0, 0, 0));
            Assert.Catch(() => objectNetworks.addConnector(0, new Vector3(0, 0, 0), 20));
        }
    }
}