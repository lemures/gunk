﻿using System;
using System.Collections.Generic;
using Gunk_Scripts.Data_Structures;
using NUnit.Framework;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class ClosestVisibleObjectTargetTests
    {
        /// <summary>
        /// generates List of KeyValuePairs in format (ID,position), where each position is random from range [0,maxX] for x and [0,maxY]
        /// for y and IDs are next integral numbers starting from startId
        /// </summary>
        private List<KeyValuePair<int,Float2>> getRandomPositions(int startId, int maxX, int maxY, int count,int seed)
        {
            var result = new List<KeyValuePair<int, Float2>>();
            var generator = new Random(seed);
            for (int i = 0; i < count; i++)
            {
                result.Add(new KeyValuePair<int, Float2>(startId+i, new Float2((float) generator.NextDouble() % maxX, (float) generator.NextDouble() % maxY)));
            }
            return result;
        }

        /// <summary>
        /// generates simple hostile relationships array where only friendly relationships are relationships with the type itself
        /// </summary>
        /// <param name="typeCount">how big the generated array should be</param>
        /// <returns></returns>
        private ClosestVisibleObjectFinder.Visibility[,] getRelationshipsArray(int typeCount)
        {
            var relationships = new ClosestVisibleObjectFinder.Visibility[typeCount,typeCount];
            for (int i = 0; i < typeCount; i++)
            {
                for (int q = 0; q < typeCount; q++)
                {
                    relationships[i, q] =
                        i == q
                            ? ClosestVisibleObjectFinder.Visibility.Invisible
                            : ClosestVisibleObjectFinder.Visibility.Visible;
                }
            }
            return relationships;
        }

        private int findtype(List<List<KeyValuePair<int, Float2>>> objects, int idToFindClosestTo)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                for (int q = 0; q < objects[i].Count; q++)
                {
                    if (objects[i][q].Key == idToFindClosestTo)
                    {
                        return i;
                    }
                }
            }

            return -1;
        }

        private Float2 findPosition(List<List<KeyValuePair<int, Float2>>> objects, int idToFindClosestTo)
        {
            for (int i = 0; i < objects.Count; i++)
            {
                for (int q = 0; q < objects[i].Count; q++)
                {
                    if (objects[i][q].Key == idToFindClosestTo)
                    {
                        return objects[i][q].Value;
                    }
                }
            }

            return new Float2(0,0);
        }

        private int getBruteClosestTarget(List<List<KeyValuePair<int, Float2>>> objects, int idToFindClosestTo,
            ClosestVisibleObjectFinder.Visibility[,] relationships)
        {
            int type = findtype(objects, idToFindClosestTo);
            Float2 position = findPosition(objects, idToFindClosestTo);

            int closestId = int.MaxValue;
            float closestDistance = float.MaxValue;
            for (int i = 0; i < relationships.GetLength(0); i++)
            {
                if (relationships[type, i] == ClosestVisibleObjectFinder.Visibility.Visible)
                {
                    for (int q = 0; q < objects[i].Count; q++)
                    {
                        if (closestDistance > Float2.distance(position, objects[i][q].Value))
                        {
                            closestDistance = Float2.distance(position, objects[i][q].Value);
                            closestId = objects[i][q].Key;
                        }
                    }
                }
            }

            return closestId;
        }

        [Test]
        public void simpleTwoObjectTest()
        {
            ClosestVisibleObjectFinder finder = new ClosestVisibleObjectFinder(1000, 1000, 2, getRelationshipsArray(2));
            finder.addObject(0, 100, 100, 0);
            finder.addObject(2, 150, 150, 1);
            finder.removeObject(2);
            finder.addObject(1, 200, 200, 1);
            FoundObject foundObject = finder.getClosestTarget(0, 300);
            Console.WriteLine(foundObject.distance + "  " + foundObject.id);
            Assert.IsTrue(foundObject.id == 1, "simple two objects test failed");
        }

        [Test]
        public void simpleTwoObjectTestRectangle()
        {
            ClosestVisibleObjectFinder finder = new ClosestVisibleObjectFinder(1000, 5, 2, getRelationshipsArray(2));
            finder.addObject(0, 999.9f, 0, 0);
            finder.addObject(2, 0, 4.9f, 1);
            finder.removeObject(2);
            finder.addObject(1, 0, 4, 1);
            FoundObject foundObject = finder.getClosestTarget(0, 3000);
            Console.WriteLine(foundObject.distance + "  " + foundObject.id);
            Assert.IsTrue(foundObject.id == 1, "simple two objects test failed");
        }

        [Test]
        public void simpleSmallTest()
        {
            ClosestVisibleObjectFinder finder = new ClosestVisibleObjectFinder(1000, 1000, 10, getRelationshipsArray(10));

            var objects = new List<List<KeyValuePair<int, Float2>>>();
            for (int i = 0; i < 10; i++)
            {
                objects.Add(getRandomPositions(20*i, 1000, 1000, 20, i));
            }

            for (int i = 0; i < objects.Count; i++)
            {
                for (int q = 0; q < objects[i].Count; q++)
                {
                    finder.addObject(objects[i][q].Key, objects[i][q].Value.x, objects[i][q].Value.y, i);
                }
            }

            for (int i = 0; i < 200; i++)
            {
                Assert.IsTrue(
                    finder.getClosestTarget(i, 1000).id == getBruteClosestTarget(objects, i, getRelationshipsArray(10)),
                    "simple small test wrong target for object " + i);
            }
        }

        [Test]
        public void bigTest()
        {
            ClosestVisibleObjectFinder finder = new ClosestVisibleObjectFinder(1000, 1000, 2, getRelationshipsArray(2));

            var objects = new List<List<KeyValuePair<int, Float2>>>
            {
                getRandomPositions(0, 1000, 1000, 100, 13),
                getRandomPositions(1000, 1000, 1000, 100, 19)
            };
            for (int i = 0; i < objects.Count; i++)
            {
                for (int q = 0; q < objects[i].Count; q++)
                {
                    finder.addObject(objects[i][q].Key, objects[i][q].Value.x, objects[i][q].Value.y, i);
                }
            }

            for (int i = 0; i < 100; i++)
            {
                Assert.IsTrue(
                    finder.getClosestTarget(i, 1000).id == getBruteClosestTarget(objects, i, getRelationshipsArray(2)),
                    "simple small test wrong target for object " + i);
            }
        }

        [Test]
        public void rectangleDimensionsTest()
        {
            ClosestVisibleObjectFinder finder = new ClosestVisibleObjectFinder(10, 1000, 2, getRelationshipsArray(2));

            var objects = new List<List<KeyValuePair<int, Float2>>>
            {
                getRandomPositions(0, 10, 1000, 100, 13),
                getRandomPositions(100, 10, 1000, 100, 19)
            };
            for (int i = 0; i < objects.Count; i++)
            {
                for (int q = 0; q < objects[i].Count; q++)
                {
                    finder.addObject(objects[i][q].Key, objects[i][q].Value.x, objects[i][q].Value.y, i);
                }
            }

            for (int i = 0; i < 200; i++)
            {
                Assert.IsTrue(
                    finder.getClosestTarget(i, 1000).id == getBruteClosestTarget(objects, i, getRelationshipsArray(2)),
                    "simple small test wrong target for object " + i);
            }
        }

        [Test]
        public void serializationTest()
        {
            ClosestVisibleObjectFinder finder = new ClosestVisibleObjectFinder(1000, 1000, 2, getRelationshipsArray(2));

            var objects = new List<List<KeyValuePair<int, Float2>>>
            {
                getRandomPositions(0, 1000, 1000, 100, 13),
                getRandomPositions(100, 1000, 1000, 100, 19)
            };
            for (int i = 0; i < objects.Count; i++)
            {
                for (int q = 0; q < objects[i].Count; q++)
                {
                    finder.addObject(objects[i][q].Key, objects[i][q].Value.x, objects[i][q].Value.y, i);
                }
            }

            string filePath = "C:\\Users\\Tomek\\Desktop\\TargetFinder.bin";
            FileManager.binarySerializeToFile(filePath, finder);
            finder = (ClosestVisibleObjectFinder)FileManager.binaryDeserializeFromFile(filePath);
            FileManager.deleteOldVersion(filePath);


            for (int i = 0; i < 200; i++)
            {
                Assert.IsTrue(
                    finder.getClosestTarget(i, 1000).id == getBruteClosestTarget(objects, i, getRelationshipsArray(2)),
                    "simple small test wrong target for object " + i);
            }
        }
    }
}

