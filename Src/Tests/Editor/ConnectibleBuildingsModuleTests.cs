using System.Collections.Generic;
using FakeItEasy;
using Gunk_Scripts;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Renderers;
using Gunk_Scripts.SkeletonAnimations;
using NUnit.Framework;
using Src.SkeletonAnimations;
using UnityEngine;

namespace Src.Tests.Editor
{
    [TestFixture]
    public class ConnectibleBuildingsModuleTests
    {
        private List<SocketData> getSimpleSocketBoneList()
        {
            var bone=new SocketData(Vector3.zero, 0);
            return new List<SocketData>(){bone};
        }

        [Test]
        public void addingConnectorNearBuildingAddsEdgesToRenderer()
        {
            var renderer = A.Fake<ICableCollection>();

            var buildingsModule = new ConnectablesBuildingsModule(renderer);

            var building1 = new ConnectablesBuildingsModule.ConnectibleObjectData(0,
                Faction.MainPlayer,
                new Vector3(0, 0, 0),
                getSimpleSocketBoneList());

            var building2 = new ConnectablesBuildingsModule.ConnectibleObjectData(1,
                Faction.MainPlayer,
                new Vector3(10, 0, 0),
                getSimpleSocketBoneList());

            buildingsModule.addObject(building1);
            buildingsModule.addConnector(building2,20.0f);

            A.CallTo(() => renderer.addCable(A<Edge>.Ignored, A<Vector3>.Ignored, A<Vector3>.Ignored))
                .MustHaveHappenedOnceExactly();
        }

        [Test]
        public void addingSameBuildingTwiceProducesErrors()
        {
            var renderer = A.Fake<ICableCollection>();
            var buildingsModule = new ConnectablesBuildingsModule(renderer);

            var building1 = new ConnectablesBuildingsModule.ConnectibleObjectData(0,
                Faction.MainPlayer,
                new Vector3(0, 0, 0),
                getSimpleSocketBoneList());

            var building2 = new ConnectablesBuildingsModule.ConnectibleObjectData(0,
                Faction.MainPlayer,
                new Vector3(10, 0, 0),
                getSimpleSocketBoneList());

            buildingsModule.addObject(building1);
            Assert.Catch(() => buildingsModule.addConnector(building2,20.0f));
        }

        [Test]
        public void addingSameBuildingFromDifferentFactionsProducesErrors()
        {
            var renderer = A.Fake<ICableCollection>();
            var buildingsModule = new ConnectablesBuildingsModule(renderer);

            var building1 = new ConnectablesBuildingsModule.ConnectibleObjectData(0,
                Faction.Corruption,
                new Vector3(0, 0, 0),
                getSimpleSocketBoneList());

            var building2 = new ConnectablesBuildingsModule.ConnectibleObjectData(0,
                Faction.MainPlayer,
                new Vector3(10, 0, 0),
                getSimpleSocketBoneList());

            buildingsModule.addObject(building1);
            Assert.Catch(() => buildingsModule.addConnector(building2,20.0f));
        }

        [Test]
        public void addingTemporaryEdgeAddsCable()
        {
            var renderer = A.Fake<ICableCollection>();
            var buildingsModule = new ConnectablesBuildingsModule(renderer);

            var building = new ConnectablesBuildingsModule.ConnectibleObjectData(0,
                Faction.Corruption,
                new Vector3(0, 0, 0),
                getSimpleSocketBoneList());

            buildingsModule.addConnector(building,10.0f);
            buildingsModule.temporarilyConnectBuilding(getSimpleSocketBoneList(),new Vector3(0,5,0), Faction.Corruption );

            A.CallTo(() => renderer.addCable(A<Edge>.Ignored, A<Vector3>.Ignored, A<Vector3>.Ignored))
                .MustHaveHappenedOnceExactly();
        }

        [Test]
        public void clearingTemporaryCableErasesCableFromRenderer()
        {
            var renderer = A.Fake<ICableCollection>();
            var buildingsModule = new ConnectablesBuildingsModule(renderer);

            var building = new ConnectablesBuildingsModule.ConnectibleObjectData(0,
                Faction.Corruption,
                new Vector3(0, 0, 0),
                getSimpleSocketBoneList());

            buildingsModule.addConnector(building,10.0f);
            buildingsModule.temporarilyConnectBuilding(getSimpleSocketBoneList(),new Vector3(0,5,0), Faction.Corruption );
            buildingsModule.clearTemporaryConnections();

            A.CallTo(() => renderer.removeCable(A<Edge>.Ignored)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void canConnectBuildingReturnsTrueIfInRange()
        {
            var renderer = A.Fake<ICableCollection>();
            var buildingsModule = new ConnectablesBuildingsModule(renderer);

            var building = new ConnectablesBuildingsModule.ConnectibleObjectData(0,
                Faction.Corruption,
                new Vector3(0, 0, 0),
                getSimpleSocketBoneList());

            buildingsModule.addConnector(building,10.0f);
            Assert.IsTrue(buildingsModule.isConnectorNearby(new Vector3(7,0,0),Faction.Corruption ));
        }

        [Test]
        public void canConnectBuildingReturnsFalseIfNotInRange()
        {
            var renderer = A.Fake<ICableCollection>();
            var buildingsModule = new ConnectablesBuildingsModule(renderer);

            var building = new ConnectablesBuildingsModule.ConnectibleObjectData(0,
                Faction.Corruption,
                new Vector3(0, 0, 0),
                getSimpleSocketBoneList());

            buildingsModule.addConnector(building,10.0f);
            Assert.IsFalse(buildingsModule.isConnectorNearby(new Vector3(14,0,0),Faction.Corruption ));
        }

        [Test]
        public void clearingTemporaryConnectionsDoesNotProduceErrors()
        {
            var renderer = A.Fake<ICableCollection>();
            var buildingsModule = new ConnectablesBuildingsModule(renderer);
            Assert.DoesNotThrow(buildingsModule.clearTemporaryConnections);
        }

        [Test]
        public void removingObjectRemovesConnections()
        {
            var renderer = A.Fake<ICableCollection>();
            var buildingsModule = new ConnectablesBuildingsModule(renderer);

            var building1 = new ConnectablesBuildingsModule.ConnectibleObjectData(0,
                Faction.Corruption,
                new Vector3(0, 0, 0),
                getSimpleSocketBoneList());

            var building2 = new ConnectablesBuildingsModule.ConnectibleObjectData(1,
                Faction.Corruption,
                new Vector3(1, 0, 0),
                getSimpleSocketBoneList());

            var building3 = new ConnectablesBuildingsModule.ConnectibleObjectData(2,
                Faction.Corruption,
                new Vector3(2, 0, 0),
                getSimpleSocketBoneList());

            buildingsModule.addConnector(building1,10);
            buildingsModule.addConnector(building2,10);
            buildingsModule.addConnector(building3,10);

            //we are disconnecting object in the middle
            buildingsModule.removeObject(building2.id);
            //two connections should have been destroyed
            A.CallTo(()=>renderer.removeCable(A<Edge>.Ignored)).MustHaveHappenedTwiceExactly();
            //one new connection should have been created
            A.CallTo(() => renderer.addCable(A<Edge>.Ignored, A<Vector3>.Ignored, A<Vector3>.Ignored))
                .MustHaveHappenedANumberOfTimesMatching(x => x == 3);
        }
    }
}