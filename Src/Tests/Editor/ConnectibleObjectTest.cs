using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using NUnit.Framework;

namespace Gunk_Scripts.Tests.Editor
{
    public class ConnectibleObjectTest
    {
        [Test]
        public void objectCreationCreatesEdges()
        {
            var surroundings = new FakeComponentSurroundings(0);
            var component=new ConnectibleObject(surroundings.fakeObject);
            A.CallTo(() => surroundings.fakeManager.connectablesNetwork.addObject(A<ConnectablesBuildingsModule.ConnectibleObjectData>.Ignored)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void objectDestructionDestroysEdges()
        {
            var surroundings = new FakeComponentSurroundings(0);
            var component=new ConnectibleObject(surroundings.fakeObject);
            component.destroy();
            A.CallTo(() => surroundings.fakeManager.connectablesNetwork.removeObject(surroundings.fakeObject.id)).MustHaveHappenedOnceExactly();
        }
    }
}