using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using NUnit.Framework;

namespace Gunk_Scripts.Tests.Editor
{
    public class ConstructableConnectibleConnectorTests
    {
        [Test]
        public void objectCreationCreatesEdges()
        {
            var surroundings = new FakeComponentSurroundings(0);
            var component=new ConnectibleConnector(surroundings.fakeObject,10.1f);
            A.CallTo(() => surroundings.fakeManager.connectablesNetwork.addObject(A<ConnectablesBuildingsModule.ConnectibleObjectData>.Ignored)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void objectDestructionDestroysEdges()
        {
            var surroundings = new FakeComponentSurroundings(0);
            var component=new ConnectibleConnector(surroundings.fakeObject,10.1f);
            component.destroy();
            A.CallTo(() => surroundings.fakeManager.connectablesNetwork.removeObject(surroundings.fakeObject.id)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void objectCreationDoesNotTransformIntoConnector()
        {
            var surroundings = new FakeComponentSurroundings(0);
            var component=new ConnectibleConnector(surroundings.fakeObject,10.1f);
            A.CallTo(() => surroundings.fakeManager.connectablesNetwork.transformIntoConnector(surroundings.fakeObject.id,10.1f)).MustNotHaveHappened();
        }

        [Test]
        public void objectConstructionCompletionTransformsIntoConnector()
        {
            var surroundings = new FakeComponentSurroundings(0);
            var component=new ConnectibleConnector(surroundings.fakeObject,10.1f);
            component.finishConstruction();
            A.CallTo(() => surroundings.fakeManager.connectablesNetwork.transformIntoConnector(surroundings.fakeObject.id, 10.1f)).MustHaveHappenedOnceExactly();
        }
    }
}