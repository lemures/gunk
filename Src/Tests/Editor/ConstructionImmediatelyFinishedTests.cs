using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using NUnit.Framework;

namespace Gunk_Scripts.Tests.Editor
{
    public class ConstructionImmediatelyFinishedTests
    {
        [Test]
        public void constructionIsImmediatelyFinished()
        {
            var constructionState = new UnderConstructionImmediatelyFinished();
            Assert.IsTrue(constructionState.constructionFinished);
        }
    }
}