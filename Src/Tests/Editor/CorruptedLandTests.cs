using System.Linq;
using NUnit.Framework;
using Src.Libraries;
using UnityEngine;

namespace Src.Tests.Editor
{
    public class CorruptedLandTests
    {
        [TestCase(0, 0)]
        [TestCase(4, 7)]
        [TestCase(9, 0)]
        [TestCase(0, 9)]
        public void infectFieldWorks(int x, int y)
        {
            var corruptedLand = new CorruptedLand(10, 10);

            corruptedLand.infectField(new Vector2Int(x, y));

            Assert.IsTrue(corruptedLand.isCorrupted(new Vector2Int(x, y)));
        }

        [Test]
        public void neighbouringFieldsGetInfected()
        {
            var corruptedLand = new CorruptedLand(10, 10);

            corruptedLand.infectField(new Vector2Int(5, 5));
            corruptedLand.update(10.0f);

            Assert.IsTrue(corruptedLand.isCorrupted(new Vector2Int(4, 5)));
            Assert.IsTrue(corruptedLand.isCorrupted(new Vector2Int(6, 5)));
            Assert.IsTrue(corruptedLand.isCorrupted(new Vector2Int(5, 4)));
            Assert.IsTrue(corruptedLand.isCorrupted(new Vector2Int(5, 6)));
        }

        [Test]
        public void fullLandIsCorruptedAfterLongTime()
        {
            var corruptedLand = new CorruptedLand(10, 10);

            corruptedLand.infectField(new Vector2Int(0, 0));
            corruptedLand.update(20.0f);

            foreach (var x in Enumerable.Range(0, 10))
            foreach (var y in Enumerable.Range(0, 10))
                Assert.IsTrue(corruptedLand.isCorrupted(new Vector2Int(x, y)));
        }

        [Test]
        public void cleansingAFewFieldsDoesNotPreventSpreadingAcrossAllMap()
        {
            var corruptedLand = new CorruptedLand(10, 10);

            corruptedLand.infectField(new Vector2Int(0, 0));
            corruptedLand.update(20.0f);

            foreach (var x in Enumerable.Range(5, 3))
            foreach (var y in Enumerable.Range(5, 3))
                corruptedLand.damageField(new Vector2Int(x, y), 10.0f);
            corruptedLand.update(20.0f);

            foreach (var x in Enumerable.Range(0, 10))
            foreach (var y in Enumerable.Range(0, 10))
                Assert.IsTrue(corruptedLand.isCorrupted(new Vector2Int(x, y)));
        }

        [TestCase(0, 0)]
        [TestCase(2, 6)]
        [TestCase(5, 9)]
        [TestCase(9, 9)]
        public void markingFieldAsNonInfectableCleansesIt(int x, int y)
        {
            var corruptedLand = new CorruptedLand(10, 10);

            corruptedLand.infectField(new Vector2Int(5, 5));
            //so that whole land is corrupted
            corruptedLand.update(20.0f);
            corruptedLand.markFieldAsNonInfectable(new Vector2Int(x, y));

            Assert.IsFalse(corruptedLand.isCorrupted(new Vector2Int(x, y)));
        }

        [TestCase(0, 0)]
        [TestCase(2, 6)]
        [TestCase(5, 9)]
        [TestCase(9, 9)]
        public void nonInfectableFieldCannotBeCorrupted(int x, int y)
        {
            var corruptedLand = new CorruptedLand(10, 10);
            corruptedLand.infectField(new Vector2Int(5, 5));
            corruptedLand.markFieldAsNonInfectable(new Vector2Int(x, y));

            corruptedLand.update(20.0f);

            Assert.IsFalse(corruptedLand.isCorrupted(new Vector2Int(x, y)));
        }

        [TestCase(0, 0)]
        [TestCase(4, 7)]
        [TestCase(9, 0)]
        [TestCase(0, 9)]
        public void damagingFieldCanCleanseIt(int x, int y)
        {
            var corruptedLand = new CorruptedLand(10, 10);

            corruptedLand.infectField(new Vector2Int(x, y));
            corruptedLand.damageField(new Vector2Int(x, y), 10.0f);

            Assert.False(corruptedLand.isCorrupted(new Vector2Int(5, 5)));
        }

        [Test]
        public void findingClosestInfectedWorks()
        {
            var corruptedLand = new CorruptedLand(10, 10);

            corruptedLand.infectField(new Vector2Int(3, 4));
            corruptedLand.infectField(new Vector2Int(6, 8));
            corruptedLand.infectField(new Vector2Int(2, 7));
            var closest = corruptedLand.getClosestCorruptedField(new Vector2Int(0, 0));

            Assert.IsNotNull(closest);
            Assert.AreEqual(new Vector2Int(3, 4), closest.Value);
        }
    }
}