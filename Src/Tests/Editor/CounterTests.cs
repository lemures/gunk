using System.Security.Policy;
using Gunk_Scripts.GunkComponents;
using NUnit.Framework;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class CounterTests
    {
        [Test]
        public void chargingAddsValueToCounterValue()
        {
            var counter = new Counter(100, 200);
            counter.increaseValue(100);
            Assert.AreEqual(200.0f, counter.currentValue, 0.01f);
        }

        [Test]
        public void dischargingSubstractsValueFromCounterValue()
        {
            var counter = new Counter(100, 200);
            counter.decreaseValue(100);
            Assert.AreEqual(counter.currentValue, 0.0f, 0.01f);
        }

        [Test]
        public void dischargingStopsAtZero()
        {
            var counter = new Counter(100, 200);
            counter.decreaseValue(150);
            Assert.AreEqual(counter.currentValue, 0.0f, 0.01f);
        }

        [Test]
        public void chargingDoesNotExceedMaxValue()
        {
            var counter = new Counter(100, 200);
            counter.increaseValue(150);
            Assert.GreaterOrEqual(200.0f, counter.currentValue);
        }
    }
}