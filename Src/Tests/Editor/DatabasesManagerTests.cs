﻿using System.Collections.Generic;
using Gunk_Scripts.Shader_Constants;
using Gunk_Scripts.Tests.DatabasesManagerTestsResources;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class DatabasesManagerTests
    {
        [Test]
        public void saveListTest()
        {

#if UNITY_EDITOR
            string unusedName = "abcTest1";
            DatabasesManager.deleteAllVersions(unusedName);
            var helper = new List<int>() {1, 3, 4};
            DatabasesManager.saveObject(helper, unusedName);
            var helperCopy= (List<int>)DatabasesManager.getObjectLatestVersion(unusedName);
            Assert.IsTrue(helperCopy != null);
            Assert.IsTrue(helperCopy[0] == 1);
            Assert.IsTrue(helperCopy[1] == 3);
            Assert.IsTrue(helperCopy[2] == 4);
            DatabasesManager.deleteAllVersions(unusedName);
            Assert.IsTrue(DatabasesManager.getObjectLatestVersion(unusedName) == null,"deleted all versions but object is still available");
#endif
        }

        [Test]
        public void deleteLastVersionTest()
        {
#if UNITY_EDITOR
            string unusedName = "abcTest2";
            DatabasesManager.deleteAllVersions(unusedName);
            var helper1 = new List<int>() { 1, 3, 4 };
            var helper2 = new List<int>() { 6, 7, 8 };
            var helper3 = new List<int>() { 9, 10, 11 };
            var helper4 = new List<int>() { 79, 50, 51 };
            DatabasesManager.saveObject(helper1, unusedName);
            DatabasesManager.saveObject(helper2, unusedName);
            DatabasesManager.saveObject(helper3, unusedName);
            DatabasesManager.saveObject(helper4, unusedName);
            DatabasesManager.deleteLatestVersion(unusedName);
            DatabasesManager.deleteLatestVersion(unusedName);
            var helperCopy = (List<int>) DatabasesManager.getObjectLatestVersion(unusedName);
            Assert.AreEqual(helperCopy[0], 6);
            Assert.AreEqual(helperCopy[1], 7);
            Assert.AreEqual(helperCopy[2], 8);
            DatabasesManager.deleteAllVersions(unusedName);
            Assert.IsTrue(DatabasesManager.getObjectLatestVersion(unusedName) == null);
#endif
        }

        [Test]
        public void savedGameObjectRecoverableTest()
        {
            string unusedName = "abcTest";
            DatabasesManager.deleteAllVersions(unusedName);
            var GO = new GameObject("tester");
            DatabasesManager.saveObjectWithUnityReferences(GO, unusedName);
            Assert.NotNull(DatabasesManager.getObjectLatestVersion(unusedName));
            DatabasesManager.deleteAllVersions(unusedName);
        }

        [Test]
        public void savingGameObjectTwiceOverridesOldVersionTest()
        {
            string unusedName = "abcTest";
            DatabasesManager.deleteAllVersions(unusedName);
            var GO1 = new GameObject("testerOld");
            var GO2 = new GameObject("testerNew");
            GO2.AddComponent<MeshFilter>();
            DatabasesManager.saveObjectWithUnityReferences(GO1, unusedName);
            DatabasesManager.saveObjectWithUnityReferences(GO2, unusedName);
            var recovered=DatabasesManager.getObjectLatestVersion(unusedName) as GameObject;
            Assert.IsTrue(recovered.GetComponent<MeshFilter>() != null);
            DatabasesManager.deleteAllVersions(unusedName);
        }

        [Test]
        public void saveUnityObjectTest()
        {
#if UNITY_EDITOR
            string unusedName1 = "abcTest3_1";
            string unusedName2 = "abcTest3_2";
            DatabasesManager.deleteAllVersions(unusedName1);
            DatabasesManager.deleteAllVersions(unusedName2);
            var GO1 = new GameObject("tester1");
            var GO2 = new GameObject("tester2");
            var comp1 = GO1.AddComponent<TestMono>();
            DatabasesManager.saveObjectWithUnityReferences(GO2, unusedName2);
            Debug.Log(DatabasesManager.getObjectLatestVersion(unusedName2).GetType());
            var recoveredData2 = (GameObject)DatabasesManager.getObjectLatestVersion(unusedName2);
            comp1.reference = recoveredData2;
            comp1.version = 1;
            DatabasesManager.saveObjectWithUnityReferences(GO1, unusedName1);
            var recoveredData1 = (GameObject)DatabasesManager.getObjectLatestVersion(unusedName1);
            Assert.IsTrue(recoveredData1.GetComponent<TestMono>().reference.GetComponent<TestMono>()!=null);
            Assert.IsTrue(recoveredData1.GetComponent<TestMono>().version==1);
            DatabasesManager.deleteAllVersions(unusedName1);
            DatabasesManager.deleteAllVersions(unusedName2);
#endif
        }

        [Test]
        public void saveScriptableObjectTest()
        {
#if UNITY_EDITOR
            string unusedName1 = "abcTest4";
            DatabasesManager.deleteAllVersions(unusedName1);
            var instance = ScriptableObject.CreateInstance<TestScriptableObject>();
            instance.a = 10;
            instance.b = 20;
            instance.c = new List<int>() {30, 40, 50};
            DatabasesManager.saveScriptableObject(instance, unusedName1);
            instance.b = 60;
            DatabasesManager.saveScriptableObject(instance, unusedName1);
            var recovered = (TestScriptableObject)DatabasesManager.getObjectLatestVersion(unusedName1);
            Assert.IsTrue(recovered.a == 10);
            Assert.IsTrue(recovered.b == 60);
            Assert.IsTrue(recovered.c[1] == 40);
            //TODO make delete last version work
            /*
            DatabasesManager.deleteLatestVersion(unusedName1);
            recovered = (TestScriptableObject)DatabasesManager.getObjectLatestVersion(unusedName1);
            Assert.AreEqual(20,recovered.b);
            */
            DatabasesManager.deleteAllVersions(unusedName1);
            Assert.IsTrue(DatabasesManager.getObjectLatestVersion(unusedName1) == null, "deleted all versions but object is still avaible");
#endif
        }

        [Test]
        public void deleteAllVersionsWorksOnUnityObjects()
        {
            string unusedName = "abcTest";
            var instance = new Material(Shader.Find(ToonStandardConstants.shaderName));
            instance.color = Color.blue;
            DatabasesManager.saveMaterial(instance, unusedName);
            DatabasesManager.deleteAllVersions(unusedName);
            var recovered = (Material)DatabasesManager.getObjectLatestVersion(unusedName);
            Assert.IsNull(recovered);
        }

        [Test]
        public void saveMaterialIsRecoverableTest()
        {
            string unusedName = "abcTest";
            DatabasesManager.deleteAllVersions(unusedName);
            var instance = new Material(Shader.Find(ToonStandardConstants.shaderName));
            instance.color = Color.blue;

            DatabasesManager.saveMaterial(instance, unusedName);
            var recovered = (Material)DatabasesManager.getObjectLatestVersion(unusedName);

            Assert.IsNotNull(recovered);
            Assert.AreEqual(Color.blue,recovered.color);

            DatabasesManager.deleteAllVersions(unusedName);
        }

        [Test]
        public void savedMaterialCanBeOverwrittenTest()
        {
            string unusedName = "abcTest";
            DatabasesManager.deleteAllVersions(unusedName);
            var instance = new Material(Shader.Find(ToonStandardConstants.shaderName));
            instance.color = Color.blue;
            DatabasesManager.saveMaterial(instance, unusedName);
            instance.color = Color.red;
            DatabasesManager.saveMaterial(instance, unusedName);
            var recovered = (Material)DatabasesManager.getObjectLatestVersion(unusedName);
            Assert.AreEqual(Color.red,recovered.color);
            DatabasesManager.deleteAllVersions(unusedName);
        }

        [Test]
        public void saveMaterialTest()
        {
#if UNITY_EDITOR
            string unusedName1 = "abcTest5";
            DatabasesManager.deleteAllVersions(unusedName1);
            var instance = new Material(Shader.Find(ToonStandardConstants.shaderName));
            instance.color = Color.blue;
            DatabasesManager.saveMaterial(instance, unusedName1);
            instance.color = Color.red;
            DatabasesManager.saveMaterial(instance, unusedName1);
            var recovered = (Material)DatabasesManager.getObjectLatestVersion(unusedName1);
            Assert.IsTrue(recovered.color == Color.red);
            DatabasesManager.deleteLatestVersion(unusedName1);
            recovered = (Material)DatabasesManager.getObjectLatestVersion(unusedName1);
            Assert.IsNotNull(recovered);
            Assert.AreEqual(recovered.color, Color.blue);
            DatabasesManager.deleteAllVersions(unusedName1);
            Assert.IsTrue(DatabasesManager.getObjectLatestVersion(unusedName1) == null, "deleted all versions but object is still avaible");
#endif
        }

        [Test]
        public void creatingNewVersionDoesntBreakLastVersion()
        {
            string unusedName = "abcTest6";
            DatabasesManager.deleteAllVersions(unusedName);
            var instance = ScriptableObject.CreateInstance<TestScriptableObject>();
            instance.a = 10;
            instance.b = 20;
            instance.c = new List<int>() { 30, 40, 50 };
            DatabasesManager.saveScriptableObject(instance, unusedName);
            var recovered = (TestScriptableObject)DatabasesManager.getObjectLatestVersion(unusedName);
            instance.a = 0;
            DatabasesManager.saveScriptableObject(instance, unusedName);
            DatabasesManager.saveScriptableObject(recovered, unusedName);
            Assert.IsTrue(recovered.a == 10);
            DatabasesManager.deleteAllVersions(unusedName);
        }

        [Test]
        public void getingAllSavedObjectsInDirectoryWorks()
        {
            string categoryFolder = "TestFolder1";
            string name1 = "myIntegerTest1";
            string name2 = "myIntegerTest2";
            var test = new int?(42);
            DatabasesManager.saveObject(test, name1, categoryFolder);
            DatabasesManager.saveObject(test, name2, categoryFolder);

            var assets = DatabasesManager.getAllSavedObjectsInDirectory(categoryFolder);
            Assert.AreEqual(2, assets.Count);
            Assert.Contains(name1, assets);
            Assert.Contains(name2, assets);

            DatabasesManager.deleteAllVersions(name1, categoryFolder);
            DatabasesManager.deleteAllVersions(name2, categoryFolder);
        }

        [Test]
        public void deletingSavedObjectRemovesItFromSavedObjectsList()
        {
            string categoryFolder = "TestFolder2";
            string name = "myIntegerTest";
            var test = new int?(42);
            DatabasesManager.saveObject(test, name, categoryFolder);
            DatabasesManager.deleteAllVersions(name, categoryFolder);

            var assets = DatabasesManager.getAllSavedObjectsInDirectory(categoryFolder);
            Assert.AreEqual(0, assets.Count);
        }

        [Test]
        public void movingAssetsBetweenCategoriesWorks()
        {

            var categoryFolderSource = "TestFolderSource1";
            var categoryFolderDestination = "TestFolderDest1";

            var name = "myIntegerTest";

            DatabasesManager.deleteAllVersions(name, categoryFolderDestination);
            var test = new int?(42);
            DatabasesManager.saveObject(test, name, categoryFolderSource);
            DatabasesManager.moveObject(name, categoryFolderSource, categoryFolderDestination);
            Assert.IsNotNull(DatabasesManager.getObjectLatestVersion(name, categoryFolderDestination));
            Assert.IsNull(DatabasesManager.getObjectLatestVersion(name, categoryFolderSource));
            DatabasesManager.deleteAllVersions(name, categoryFolderDestination);
        }
    }
}

