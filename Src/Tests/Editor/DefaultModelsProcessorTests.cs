﻿using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.Asset_Importing;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class DefaultModelsProcessorTests
    {
        public PrefabAssets getEvilCube()
        {
            DefaultModelProcessor processor = new DefaultModelProcessor();
            var meshes = (from asset in AssetUtils.findAllAssetsOfType(typeof(Mesh), "Assets/Src/Tests/DefaultModelProcessorTestsResources") select (Mesh)asset).ToList();
            var result = processor.process(meshes, new List<Texture2D>());
            return result[0];
        }

        [Test]
        public void evilCubeImportTest()
        {
            var evilCube = getEvilCube();
            Assert.IsTrue(evilCube.go != null);
            var LOD = evilCube.go.GetComponent<LODGroup>().GetLODs();
            Assert.IsTrue(LOD.Length == 4);
            var meshRenderers = evilCube.go.GetComponentsInChildren<MeshRenderer>();
            Assert.IsTrue(meshRenderers.All(x => x.sharedMaterial != null));
        }
    }
}
