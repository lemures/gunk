using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine.XR.WSA.WebCam;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class EnergyNetworkTests
    {
        private static List<EnergyNetwork.EnergyObjectData> generateBuildingsData(int objectCount, float energyGeneratedPerObject, float energyNeedsPerObject)
        {
            var buildingsData = new List<EnergyNetwork.EnergyObjectData>();
            for (int i = 0; i < objectCount; i++)
            {
                buildingsData.Add( new EnergyNetwork.EnergyObjectData(i,energyGeneratedPerObject,energyNeedsPerObject));
            }

            return buildingsData;
        }

        private List<List<int>> generateSingleSet(int objectCount)
        {
            var result = new List<List<int>> {Enumerable.Range(0, objectCount).ToList()};
            return result;
        }

        private List<List<int>> generateSingletonSets(int objectCount)
        {
            var result = new List<List<int>>();
            Enumerable.Range(0, objectCount)
                .ToList()
                .ForEach(x => result.Add(new List<int>(){x}));
            return result;
        }

        private float getTotalEnergyGeneration(List<EnergyNetwork.EnergyObjectData> objectData)
        {
            var total = 0.0f;
            objectData.ForEach(x=>total+=x.energyProducedThisTick);
            return total;
        }

        [Test]
        public void simulationResultGeneratesDataForEveryObject()
        {
            var buildingsData = generateBuildingsData(3, 10.0f, 100.0f);
            var sets = generateSingleSet(3);
            var network=new EnergyNetwork();

            var result = network.simulateSingleTick(buildingsData, sets);
            Assert.AreEqual(3,result.Count());
        }

        [Test]
        public void amountOfEnergyProvidedAmountsToAmountOfGeneratedForSingleSet()
        {
            var buildingsData = generateBuildingsData(10, 10.0f, 100.0f);
            var sets = generateSingleSet(10);
            var network=new EnergyNetwork();

            var result = network.simulateSingleTick(buildingsData, sets);
            var totalAcquired = 0.0f;
            result.ForEach(x=>totalAcquired+=x.energyProvided);
            Assert.AreEqual(getTotalEnergyGeneration(buildingsData),totalAcquired);
        }

        [Test]
        public void providedEnergyIsNotMoreThanNeeds()
        {
            var buildingsData = generateBuildingsData(10, 200.0f, 100.0f);
            var sets = generateSingleSet(10);
            var network=new EnergyNetwork();

            var result = network.simulateSingleTick(buildingsData, sets);
            Assert.IsTrue(result.TrueForAll(x=>x.energyProvided<=100.0f));
        }

        [Test]
        public void negativeEnergyGenerationProducesError()
        {
            var buildingsData = generateBuildingsData(10, -200.0f, 100.0f);
            var sets = generateSingleSet(10);
            var network=new EnergyNetwork();

            Assert.Catch(() => network.simulateSingleTick(buildingsData, sets));
        }

        [Test]
        public void negativeEnergyNeedsProducesError()
        {
            var buildingsData = generateBuildingsData(10, 100.0f, -100.0f);
            var sets = generateSingleSet(10);
            var network=new EnergyNetwork();

            Assert.Catch(() => network.simulateSingleTick(buildingsData, sets));
        }

        private void setGeneratedEnergyBasedOnPositionInArray(IReadOnlyList<EnergyNetwork.EnergyObjectData>buildingsData,
            float extraNeedsForNextObjects)
        {
            for (var i = 0; i < buildingsData.Count; i++)
            {
                buildingsData[i].energyProducedThisTick = (i) * extraNeedsForNextObjects;
            }
        }

        [Test]
        public void amountOfEnergyProvidedAmountsToAmountOfGeneratedForSingletonSets()
        {
            var buildingsData = generateBuildingsData(10, 10.0f, 100.0f);
            setGeneratedEnergyBasedOnPositionInArray(buildingsData,10.0f);
            var sets = generateSingletonSets(10);
            var network=new EnergyNetwork();

            var result = network.simulateSingleTick(buildingsData, sets);
            result.Sort((x,y)=>x.objectId.CompareTo(y.objectId));
            for (int i = 0; i < buildingsData.Count; i++)
            {
                Assert.AreEqual(buildingsData[i].energyProducedThisTick,result[i].energyProvided, 0.001f);
            }
        }
    }
}