using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Renderers;
using NUnit.Framework;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class EnergyTests
    {
        [Test]
        public void objectCreationAddsCounter()
        {
            int objectId = 123;
            var surroundings = new FakeComponentSurroundings(objectId);
            var fakeCounters = surroundings.addFakeCountersCollection();
            var energyState = new Energy.EntryEnergyState(energy: 100, maxEnergy: 200, maxEnergyProvidedPerSecond: 50);
            var energy = new Energy(surroundings.fakeObject, energyState);
            A.CallTo(() => fakeCounters.addHiddenCounter(
                objectId,
                A<Counter>.That.IsEqualTo(new Counter(100, 200)),
                CountersRenderer.DrawStyle.Energy)).MustHaveHappened();
        }

        [Test]
        public void objectDestroyingMakesCounterInvisible()
        {
            var objectId = 123;
            var surroundings = new FakeComponentSurroundings(objectId);
            var fakeCounters=surroundings.addFakeCountersCollection();
            var energyState = new Energy.EntryEnergyState(energy: 100, maxEnergy: 200, maxEnergyProvidedPerSecond: 50);
            var energy = new Energy(surroundings.fakeObject, energyState);
            energy.destroy();
            A.CallTo(
                    () => fakeCounters.hideCounter(objectId, CountersRenderer.DrawStyle.Energy))
                .MustHaveHappenedOnceExactly();
        }

        [Test]
        public void defaultDrawingBehaviourLeavesCounterVisible()
        {
            var objectId = 123;
            var surroundings = new FakeComponentSurroundings(objectId);
            var fakeCounters = surroundings.addFakeCountersCollection();
            var energyState = new Energy.EntryEnergyState(energy: 100, maxEnergy: 200, maxEnergyProvidedPerSecond: 50);
            var energy = new Energy(surroundings.fakeObject, energyState);
            A.CallTo(() => fakeCounters.hideCounter(objectId, CountersRenderer.DrawStyle.Energy))
                .MustNotHaveHappened();
        }

        [Test]
        public void chargingEnergyChangesRenderedCounterValue()
        {
            var objectId = 123;
            var surroundings = new FakeComponentSurroundings(objectId);
            var fakeCounters = surroundings.addFakeCountersCollection();
            var energyState = new Energy.EntryEnergyState(energy: 100, maxEnergy: 200, maxEnergyProvidedPerSecond: 50);
            var energy = new Energy(surroundings.fakeObject, energyState);
            energy.chargeEnergy(100.0f);
            A.CallTo(() => fakeCounters.setCounterValue(objectId, CountersRenderer.DrawStyle.Energy, 200.0f))
                .MustHaveHappened();
        }

        [Test]
        public void dischargingEnergyChangesRenderedCounterValue()
        {
            var objectId = 123;
            var surroundings = new FakeComponentSurroundings(objectId);
            var fakeCounters = surroundings.addFakeCountersCollection();
            var energyState = new Energy.EntryEnergyState(energy: 100, maxEnergy: 200, maxEnergyProvidedPerSecond: 50);
            var energy = new Energy(surroundings.fakeObject, energyState);
            energy.useEnergy(100.0f);
            A.CallTo(() => fakeCounters.setCounterValue(objectId, CountersRenderer.DrawStyle.Energy, 0.0f))
                .MustHaveHappened();
        }

        [Test]
        public void areEnergyNeedsEqualToMaxProvidedEnergyWhenCounterIsAlmostEmpty()
        {
            var objectId = 123;
            var surroundings = new FakeComponentSurroundings(objectId);
            var energyState = new Energy.EntryEnergyState(energy: 100, maxEnergy: 200, maxEnergyProvidedPerSecond: 50);
            var energy = new Energy(surroundings.fakeObject, energyState);
            var energyNeeded = energy.calculateEnergyNeeds();
            Assert.AreEqual(energyNeeded,50);
        }

        [Test]
        public void areEnergyNeedsLimitedByEnergyToFull()
        {
            var objectId = 123;
            var surroundings = new FakeComponentSurroundings(objectId);
            var energyState = new Energy.EntryEnergyState(energy: 190, maxEnergy: 200, maxEnergyProvidedPerSecond: 50);
            var energy = new Energy(surroundings.fakeObject, energyState);
            var energyNeeded = energy.calculateEnergyNeeds();
            Assert.AreEqual(energyNeeded,10);
        }
    }
}