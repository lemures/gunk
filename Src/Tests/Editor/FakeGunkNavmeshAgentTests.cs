using Gunk_Scripts.GunkComponents;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class FakeGunkNavmeshAgentTests
    {
        [Test]
        public void destinationIsReachedAfterFiniteTime()
        {
            var fakeAgent = new FakeNavigationAgent(Vector3.zero);
            fakeAgent.destination=(new Vector3(0,10,0));
            for (var i = 0; i < 1000; i++)
            {
                fakeAgent.update(0.1f);
            }

            Assert.AreEqual(fakeAgent.remainingDistance, 0);
        }

        [Test]
        public void destinationIsNotReachedImmediately()
        {
            var fakeAgent = new FakeNavigationAgent(Vector3.zero);
            fakeAgent.destination=(new Vector3(0,10,0));
            for (var i = 0; i < 1; i++)
            {
                fakeAgent.update(0.001f);
            }

            Assert.AreNotEqual(fakeAgent.remainingDistance, 0);
        }

        private static float simulateTimeNeededToReachDestination(Vector3 startPos, Vector3 endPos, float maxSpeed)
        {
            var agent = new FakeNavigationAgent(startPos);
            agent.destination=(endPos);
            agent.setMaxSpeed(maxSpeed);

            float timeNeeded = 0;
            while (agent.remainingDistance > 0)
            {
                agent.update(0.01f);
                timeNeeded += 0.01f;
            }

            return timeNeeded;
        }

        [Test]
        public void doublingSpeedProportionallyDecreasesTime()
        {
            var time1 = simulateTimeNeededToReachDestination(Vector3.zero, Vector3.one, 1.0f);
            var time2 = simulateTimeNeededToReachDestination(Vector3.zero, Vector3.one, 0.1f);

            Assert.AreEqual(time1/time2,0.1f,0.01f);
        }
    }
}
