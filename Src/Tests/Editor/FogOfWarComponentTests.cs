using System.Security.Policy;
using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Renderers;
using NUnit.Framework;
using Src.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class FogOfWarComponentTests
    {
        [Test]
        public void changingFactionToPlayerEnablesFogOfWar()
        {
            var id = 42;
            var position = new Vector3(1, 3, 5);
            var fakeSurroundings = new FakeComponentSurroundings(id);
            var fogOfWarCollection = A.Fake<IFogOfWarObjectsCollection>();
            PerManagerObject<IFogOfWarObjectsCollection>.forceSetCreatedObject(fogOfWarCollection);

            var component = new FogOfWarComponent(fakeSurroundings.fakeObject);
            fakeSurroundings.addFakeTransformPosition(position);

            component.changeFaction(Faction.MainPlayer);
            A.CallTo(() => fogOfWarCollection.addObject(id, position)).MustHaveHappenedOnceExactly();
            A.CallTo(() => fogOfWarCollection.setRange(id, 15.0f)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void addingFogOfWarComponentWithDefaultFactionDoesNotAddObjectToCollection()
        {
            var id = 42;
            var position = new Vector3(1, 3, 5);
            var fakeSurroundings = new FakeComponentSurroundings(id);
            var fogOfWarCollection = A.Fake<IFogOfWarObjectsCollection>();
            PerManagerObject<IFogOfWarObjectsCollection>.forceSetCreatedObject(fogOfWarCollection);

            var component = new FogOfWarComponent(fakeSurroundings.fakeObject);

            A.CallTo(() => fogOfWarCollection.addObject(id, position)).MustNotHaveHappened();
        }

        [TearDown]
        public void tearDown()
        {
            PerManagerObject<IFogOfWarObjectsCollection>.removeForcedObject();
        }
    }
}