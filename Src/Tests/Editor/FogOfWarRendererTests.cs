﻿using Gunk_Scripts.Renderers;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class FogOfWarRendererTests
    {

        [Test]
        public void functionalityTest()
        {
            FogOfWarRenderer renderer = new FogOfWarRenderer(1000, 80,80);
            var renderTexture1 = renderer.getActivelyVisibleTerrainRenderTexture();
            var renderTexture2 = renderer.getDiscoveredTerrainRenderTexture();
            renderer.addObject(0, Vector3.zero);
            renderer.setRange(0,40.0f);
            renderer.render();
            var pixels1=TextureCapturer.getTexturePixels(renderTexture1);
            var pixels2 = TextureCapturer.getTexturePixels(renderTexture1);
            var texHeight1 = renderTexture1.height;
            var texWidth1 = renderTexture1.width;
            Assert.Greater(pixels1[0].r,FogOfWarRenderer.getVisiblePixelThreshold());
            Assert.Greater(pixels2[0].r, FogOfWarRenderer.getVisiblePixelThreshold());
            Assert.Greater(pixels2[texWidth1 / 2 - 1].r, FogOfWarRenderer.getVisiblePixelThreshold());
            Assert.Greater(-pixels2[texWidth1 / 2].r, -FogOfWarRenderer.getVisiblePixelThreshold());
            renderer.removeObject(0);
            renderer.render();
            pixels1 = TextureCapturer.getTexturePixels(renderTexture1);
            pixels2 = TextureCapturer.getTexturePixels(renderTexture2);
            //this should be close to zero
            Assert.Greater(1-pixels1[0].r, 0.5f);
            //this should remain colored
            Assert.Greater(pixels2[0].r, 0.5f);
        }

    }
}
