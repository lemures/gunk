﻿using System;
using Gunk_Scripts.GUI;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class GuiManagerTests
    {
        public static void resourceExistTest(GuiManager.GuiPrimitives.PrimitiveType type)
        {
            Assert.IsNotNull(GuiManager.GuiPrimitives.getPrimitive(type));
        }


        [Test]
        public void guiPrimitivesResourcesExist()
        {
            foreach (var type in Enum.GetValues(typeof(GuiManager.GuiPrimitives.PrimitiveType)))
            {
                resourceExistTest((GuiManager.GuiPrimitives.PrimitiveType) type);
            }
        }

        [Test]
        public void anchorInRightPlaceInResources()
        {
            foreach (var type in Enum.GetValues(typeof(GuiManager.GuiPrimitives.PrimitiveType)))
            {
                var prefab = GuiManager.GuiPrimitives.getPrimitive((GuiManager.GuiPrimitives.PrimitiveType)type);
                var rectTransform = prefab.GetComponent<RectTransform>();
                Assert.AreEqual(rectTransform.pivot, new Vector2(0, 1));
                Assert.AreEqual(rectTransform.anchorMin, new Vector2(0, 1));
                Assert.AreEqual(rectTransform.anchorMax, new Vector2(0, 1));
            }
        }
    }
}
