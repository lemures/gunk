using System;
using FakeItEasy;
using FakeItEasy.Configuration;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using Gunk_Scripts.Renderers;
using Src.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class FakeComponentSurroundings
    {
        public IGunkObjectManagerInternalData fakeManager;
        public IGunkObject fakeObject;

        public FakeComponentSurroundings(int objectId)
        {
            fakeManager = A.Fake<IGunkObjectManagerInternalData>();
            fakeObject = A.Fake<IGunkObject>();

            A.CallTo(() => fakeObject.objectManager).Returns(fakeManager);
            A.CallTo(() => fakeObject.id).ReturnsLazily(() => objectId);
            A.CallTo(() => fakeObject.animator.getAllSocketBones()).Returns(null);

        }

        public ICountersCollection addFakeCountersCollection()
        {
            var fakeCounters = A.Fake<ICountersCollection>();
            A.CallTo(() => fakeManager.countersCollection).Returns(fakeCounters);
            return fakeCounters;
        }

        public void addFakeTransformPosition(Vector3 position)
        {
            A.CallTo(() => fakeObject.transform.position).Returns(position);
        }

        private IGunkObject getFakeResourceObject(int id, Vector3 position, IResourcesContainer container)
        {
            var resourceObject = A.Fake<IGunkObject>();
            A.CallTo(() => resourceObject.resourcesContainer).Returns(container);
            A.CallTo(() => resourceObject.transform.position).Returns(position);
            return resourceObject;
        }

        public IClosestResourceFinder createFakeResourceFinder()
        {
            var resourceFinder = A.Fake<IClosestResourceFinder>();
            PerManagerObject<IClosestResourceFinder>.forceSetCreatedObject(resourceFinder);
            return resourceFinder;
        }

        private ClosestResourceFinder closestResourceFinder = new ClosestResourceFinder(1000,1000);

        public IGunkObject addFakeResourceObjectToManager(int resourceId, Vector3 position,
            IResourcesContainer resourcesContainer)
        {
            var resource = getFakeResourceObject(resourceId, position, resourcesContainer);
            A.CallTo(() => fakeManager.getObjectWithId(resourceId)).Returns(resourcesContainer.isDepleted()?null:resource);
            closestResourceFinder.addObject(resourceId, new Float2(position.x,position.z),resourcesContainer.type);
            return resource;
        }

        public void addFakeAgent()
        {
            A.CallTo(() => fakeObject.agent).Returns(A.Fake<INavigationAgent>());
        }

        public void addStubAgent(Vector3 startPosition)
        {
            var agent = new FakeNavigationAgent(startPosition);
            A.CallTo(() => fakeObject.transform.position).ReturnsLazily(() => agent.currentPosition);
            A.CallTo(() => fakeObject.agent).Returns(agent);
        }
    }
}