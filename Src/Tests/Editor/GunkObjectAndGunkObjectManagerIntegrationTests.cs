using System;
using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.GUI;
using Gunk_Scripts.SkeletonAnimations;
using NUnit.Framework;
using Src.GameRenderer;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class GunkObjectAndGunkObjectManagerIntegrationTests
    {
        private IPrefabManager constructFakePrefabManager(List<string> prefabNames)
        {
            var fakeManager = A.Fake<IPrefabManager>();
            foreach (var name in prefabNames)
            {
                var prefab=new Prefab(new GameObject(name));
                A.CallTo(() => fakeManager.findPrefab(name)).Returns(prefab);
            }
            return fakeManager;
        }

        private Camera constructEmptyCamera()
        {
            GameObject cameraObject=new GameObject();
            var camera=cameraObject.AddComponent<Camera>();
            return camera;
        }

        [TearDown]
        public void resetGuiManager()
        {
            GuiManager.reset();
        }

        [Test]
        public void buildingNetworksGenerateCorrectly()
        {
            var fakeCamera = constructEmptyCamera();
            var fakePrefabManager = constructFakePrefabManager(new List<string> {"blah"});
            var gunkObjectManager = new GunkObjectManager(100, 100, fakeCamera, fakePrefabManager, new GunkExtraObjectsRenderer(fakeCamera));

            var object1=new GunkObject(gunkObjectManager, "blah", new Vector3(0,0,0));
            object1.addAnimator("");
            object1.addConnectible(new ConnectibleObject(object1));

            var object2=new GunkObject(gunkObjectManager, "blah", new Vector3(0,0,5));
            object2.addAnimator("");
            object2.addConnectible(new ConnectibleConnector(object2,6.0f));

            var object3=new GunkObject(gunkObjectManager, "blah", new Vector3(0,0,10));
            object3.addAnimator("");
            object3.addConnectible(new ConnectibleObject(object3));

            //so that cable connections are really created
            gunkObjectManager.update(0);

            var networks = gunkObjectManager.getAllConnectibleNetworks();

            Assert.AreEqual(1,networks.Count);
            Assert.Contains(object1.id, networks[0]);
            Assert.Contains(object2.id, networks[0]);
            Assert.Contains(object3.id, networks[0]);
        }

        [Test]
        public void objectUnderConstructionConstructs()
        {
            var fakeCamera = constructEmptyCamera();
            var fakePrefabManager = constructFakePrefabManager(new List<string> {"blah"});
            var gunkObjectManager = new GunkObjectManager(100, 100, fakeCamera, fakePrefabManager, new GunkExtraObjectsRenderer(fakeCamera));

            var generator=new GunkObject(gunkObjectManager, "blah", new Vector3(0,0,0));
            generator.addAnimator("");
            generator.addConnectible(new ConnectibleConnector(generator,6.0f));
            generator.addEnergyGenerator(new ConstantValueEnergyGenerator(100.0f));

            var constructedObject=new GunkObject(gunkObjectManager, "blah", new Vector3(0,0,5));
            constructedObject.addAnimator("");
            constructedObject.addConnectible(new ConnectibleConnector(constructedObject,6.0f));
            constructedObject.addEnergyNeededForConstruction(100.0f);
            constructedObject.finishAddingComponents();

            //so that cable connections are really created
            gunkObjectManager.update(0);
            for (int i = 0; i < 101; i++)
            {
                gunkObjectManager.update(0.01f);
            }

            Assert.IsTrue(constructedObject.isConstructed());
        }
    }
}