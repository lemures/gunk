using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class GunkObjectTests
    {
        [Test]
        public void constructionFinishedTurnsOnEnergy()
        {
            var surroundings=new GunkObjectsFakeSurroundingsCreator(1);
            var myObject = surroundings.gunkObjects[0];
            myObject.addEnergy(100,200,300);
            myObject.reportConstructionFinished();
            Assert.AreEqual(100.0f, myObject.getEnergyNeeds());
        }

        [Test]
        public void defaultConstructionStateIsFinishedWhenFinishAddingComponentsIsCalled()
        {
            var surroundings=new GunkObjectsFakeSurroundingsCreator(1);
            var fakeComponent = A.Fake<IConstructionFinishedCallbackReceiver>(builder => builder.Implements<IGunkComponent>());
            var myObject = surroundings.gunkObjects[0];
            myObject.addComponent(fakeComponent as IGunkComponent);
            myObject.finishAddingComponents();
            A.CallTo(() => fakeComponent.finishConstruction()).MustHaveHappened();
        }

        [Test]
        public void callbackFunctionsAreCalledWhenConstructionIsFinished()
        {
            var surroundings=new GunkObjectsFakeSurroundingsCreator(1);
            var fakeComponent = A.Fake<IConstructionFinishedCallbackReceiver>(builder => builder.Implements<IGunkComponent>());
            var myObject = surroundings.gunkObjects[0];
            myObject.addComponent(fakeComponent as IGunkComponent);
            myObject.reportConstructionFinished();
            A.CallTo(() => fakeComponent.finishConstruction()).MustHaveHappened();
        }

        [Test]
        public void callbackFunctionsAreCalledWhenPositionIsChanged()
        {
            var surroundings=new GunkObjectsFakeSurroundingsCreator(1);
            var fakeComponent = A.Fake<IPositionChangedCallbackReceiver>(builder => builder.Implements<IGunkComponent>());
            var myObject = surroundings.gunkObjects[0];

            myObject.addComponent(fakeComponent as IGunkComponent);
            myObject.reportPositionChanged(new Vector3(1,2,3));

            A.CallTo(() => fakeComponent.changePosition(new Vector3(1,2,3))).MustHaveHappened();
        }

        [Test]
        public void callbackFunctionsAreCalledWhenFactionIsChanged()
        {
            var faction = Faction.Player3;
            var surroundings=new GunkObjectsFakeSurroundingsCreator(1);
            var fakeComponent = A.Fake<IFactionChangedCallbackReceiver>(builder => builder.Implements<IGunkComponent>());
            var myObject = surroundings.gunkObjects[0];

            myObject.addComponent(fakeComponent as IGunkComponent);
            myObject.setFaction(faction);

            A.CallTo(() => fakeComponent.changeFaction(faction)).MustHaveHappened();
        }

        [Test]
        public void connectingEnergyGeneratorMakesObjectReturnRightEnergyGenerationLevel()
        {
            var surroundings=new GunkObjectsFakeSurroundingsCreator(1);
            var myObject = surroundings.gunkObjects[0];
            var fakeGenerator = A.Fake<IEnergyGenerator>();
            A.CallTo(() => fakeGenerator.getEnergyGenerationLevel()).Returns(100.0f);
            myObject.addEnergyGenerator(fakeGenerator);
            Assert.AreEqual(100.0f,myObject.getEnergyGenerationLevel());
        }
    }
}