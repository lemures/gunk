using System.Collections.Generic;
using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    
    class GunkObjectsFakeSurroundingsCreator
    {
        public readonly IGunkObjectManagerInternalData fakeManager;
        public readonly List<GunkObject> gunkObjects= new List<GunkObject>();

        public GunkObjectsFakeSurroundingsCreator(int objectCount)
        {
            var fakeManagerFacade = A.Fake<IGunkObjectManager>();
            fakeManager = A.Fake<IGunkObjectManagerInternalData>();
            A.CallTo(() => fakeManagerFacade.getInternalData(A<IGunkObject>.Ignored)).Returns(fakeManager);

            for (int i = 0; i < objectCount; i++)
            {
                var fakeTransform = A.Fake<IGunkTransform>();
                A.CallTo(() =>
                    fakeManager.instantiatePrefab(A<GunkObject>.Ignored, "blah", new Vector3(0, 0, 0),
                        Quaternion.identity)).Returns(fakeTransform);
                gunkObjects.Add(new GunkObject(fakeManagerFacade, "blah", new Vector3(0, 0, 0)));
            }
        }
    }
}