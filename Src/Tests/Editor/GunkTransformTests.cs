using System.Collections.Generic;
using System.ComponentModel;
using FakeItEasy;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.GunkComponents;
using NUnit.Framework;
using Src.Libraries;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class GunkTransformTests
    {
        private class InitializedData
        {
            public IGunkObjectManagerInternalData fakeManager;
            public IGunkObject fakeObject;
            public GunkTransform transform;
            public GameObjectManager gameObjectManager;

            public InitializedData(IGunkObjectManagerInternalData fakeManager, IGunkObject fakeObject, GunkTransform transform, GameObjectManager gameObjectManager)
            {
                this.fakeManager = fakeManager;
                this.fakeObject = fakeObject;
                this.transform = transform;
                this.gameObjectManager = gameObjectManager;
            }
        }

        private InitializedData createTransformWithFakeSurroundings(int gunkObjectId, Vector3 objectPosition)
        {
            var testObject = new GameObject("testObject");
            var fakePrefabManager = A.Fake<IPrefabManager>();
            A.CallTo(() => fakePrefabManager.findPrefab("testObject")).Returns(new Prefab(testObject));
            GameObjectManager objectManager=new GameObjectManager(fakePrefabManager);

            var myId=objectManager.instantiatePrefab(testObject.name, objectPosition, Quaternion.identity);
            Object.DestroyImmediate(testObject);

            var fakeManager = A.Fake<IGunkObjectManagerInternalData>();
            var fakeObject = A.Fake<IGunkObject>();

            A.CallTo(() => fakeObject.objectManager).Returns(fakeManager);
            A.CallTo(() => fakeObject.id).ReturnsLazily(() => gunkObjectId);

            var transform = new GunkTransform(fakeObject, objectManager, myId);

            return new InitializedData(fakeManager,fakeObject,transform,objectManager);
        }

        [Test]
        public void settingPositionUpdatesPositionInObjectManager()
        {
            var initializedData = createTransformWithFakeSurroundings(5, new Vector3(0,0,0));

            initializedData.transform.position=new Vector3(5,7,0);
            Assert.AreEqual(new Vector3(5,7,0), initializedData.gameObjectManager.getPosition(0));
        }

        [Test]
        public void settingRotationUpdatesPositionInObjectManager()
        {
            var initializedData = createTransformWithFakeSurroundings(5, new Vector3(0,0,0));

            initializedData.transform.rotation=Quaternion.Euler(10,20,30);
            Assert.AreEqual(Quaternion.Euler(10,20,30).x, initializedData.gameObjectManager.getRotation(0).x,0.01d);
            Assert.AreEqual(Quaternion.Euler(10,20,30).y, initializedData.gameObjectManager.getRotation(0).y,0.01d);
            Assert.AreEqual(Quaternion.Euler(10,20,30).z, initializedData.gameObjectManager.getRotation(0).z,0.01d);
        }

        [Test]
        public void settingPositionReportsPositionChangedInParent()
        {
            var initializedData = createTransformWithFakeSurroundings(5, new Vector3(0,1,2));
            A.CallTo(() => initializedData.fakeObject.reportPositionChanged(new Vector3(0,1,2))).MustHaveHappened();
        }
    }
}