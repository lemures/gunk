﻿using System;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class KernelUtilsTests
    {
        private static string testKernelName = "KernelUtilsTest";
        private static string intUniformName = "testInt";
        private static string floatUniformName = "testFloat";
        private static string vectorUniformName = "testVector";
        private static string matrixUniformName = "testMatrix";
        private static string bufferOutName = "kernelUtilsOut";
        private static string bufferInName = "kernelUtilsIn";

        [Test]
        public void uniformsTest()
        {
            KernelUtils.reset();
            KernelUtils.initializeEmptyBuffer(bufferInName, 10, 4, true);
            KernelUtils.initializeEmptyBuffer(bufferOutName, 40, 4, true);
            Array bufferInData = new float[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            Array bufferOutData = new float[40];
            KernelUtils.setBufferData(bufferInName, bufferInData);
            KernelUtils.setInt(intUniformName, 37);
            KernelUtils.setFloat(floatUniformName, 5.67f);
            KernelUtils.setVector(vectorUniformName, new Vector4(1, 2, 3, 4));
            KernelUtils.setMatrix(matrixUniformName, new Matrix4x4(Vector4.one, Vector4.one, Vector4.one, Vector4.one));
            KernelUtils.dispatch(testKernelName);
            KernelUtils.getDataFromBuffer(bufferOutName, bufferOutData);

            Assert.IsTrue(((float)bufferOutData.GetValue(0) - 37.0f) < 0.001f);
            Assert.IsTrue(((float)bufferOutData.GetValue(1) - 5.67f) < 0.001f);
            Assert.IsTrue(((float)bufferOutData.GetValue(2) - 1.00f) < 0.001f);
            Assert.IsTrue(((float)bufferOutData.GetValue(3) - 2.00f) < 0.001f);
            Assert.IsTrue(((float)bufferOutData.GetValue(4) - 3.00f) < 0.001f);
            Assert.IsTrue(((float)bufferOutData.GetValue(5) - 4.00f) < 0.001f);
            for (int i = 0; i < 16; i++)
            {
                Assert.IsTrue(((float)bufferOutData.GetValue(6+i) - 1.00f) < 0.001f);
            }
            for (int i = 0; i < 10; i++)
            {
                Assert.IsTrue(((float)bufferOutData.GetValue(22 + i) - i) < 0.001f);
            }
        }

        [Test]
        public void serializationTest()
        {
            KernelUtils.reset();
            KernelUtils.initializeEmptyBuffer(bufferInName, 10, 4, true,ComputeBufferType.Counter);
            KernelUtils.initializeEmptyBuffer(bufferOutName, 40, 4, true);
            Array bufferInData = new float[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            Array bufferOutData = new float[40];
            KernelUtils.setBufferData(bufferInName, bufferInData);
            KernelUtils.setCounterValue(bufferInName, 13);
            KernelUtils.setInt(intUniformName, 37);
            KernelUtils.setFloat(floatUniformName, 5.67f);
            KernelUtils.setVector(vectorUniformName, new Vector4(1, 2, 3, 4));
            KernelUtils.setMatrix(matrixUniformName, new Matrix4x4(Vector4.one, Vector4.one, Vector4.one, Vector4.one));
            KernelUtils.dispatch(testKernelName);
            KernelUtils.getDataFromBuffer(bufferOutName, bufferOutData);

            var rawData = KernelUtils.getRuntimeResourcesCopy();
            KernelUtils.reset();
            KernelUtils.initializeEmptyBuffer(bufferInName, 10, 4, true);
            KernelUtils.setBufferData(bufferInName, bufferInData);
            KernelUtils.setInt(intUniformName, 87);
            KernelUtils.setFloat(floatUniformName, 8.67f);
            KernelUtils.setVector(vectorUniformName, new Vector4(5, 6, 7, 8));
            KernelUtils.setMatrix(matrixUniformName, new Matrix4x4(Vector4.one, Vector4.one, Vector4.zero, Vector4.zero));
            KernelUtils.initializeEmptyBuffer(bufferInName, 10, 4, true);
            KernelUtils.initializeEmptyBuffer(bufferOutName, 40, 4, true);
            bufferInData = new float[] { 0, 1, 2, 3, 44, 44, 44, 7, 8, 9, 10 };
            KernelUtils.setBufferData(bufferInName, bufferInData);

            KernelUtils.loadRuntimeResources(rawData);

            Assert.IsTrue(((float)bufferOutData.GetValue(0) - 37.0f) < 0.001f);
            Assert.IsTrue(((float)bufferOutData.GetValue(1) - 5.67f) < 0.001f);
            Assert.IsTrue(((float)bufferOutData.GetValue(2) - 1.00f) < 0.001f);
            Assert.IsTrue(((float)bufferOutData.GetValue(3) - 2.00f) < 0.001f);
            Assert.IsTrue(((float)bufferOutData.GetValue(4) - 3.00f) < 0.001f);
            Assert.IsTrue(((float)bufferOutData.GetValue(5) - 4.00f) < 0.001f);
            Assert.AreEqual(13,KernelUtils.getCounterValue(bufferInName));
            for (int i = 0; i < 16; i++)
            {
                Assert.IsTrue(((float)bufferOutData.GetValue(6 + i) - 1.00f) < 0.001f);
            }
            for (int i = 0; i < 10; i++)
            {
                Assert.IsTrue(((float)bufferOutData.GetValue(22 + i) - i) < 0.001f);
            }
        }

        [Test]
        public void resizeBufferTest()
        {
            string bufferName = "resizeBufferTestBuffer";
            KernelUtils.reset();
            KernelUtils.initializeEmptyBuffer(bufferName, 1, 4, false);
            int[] ints = new int[1000];
            for (int i = 0; i < 1000; i++)
            {
                ints[i] = 2;
            }
            KernelUtils.setBufferData(bufferName, ints);
            ints = new int[20000];
            for (int i = 0; i < 20000; i++)
            {
                ints[i] = 8;
            }
            KernelUtils.setBufferData(bufferName, ints);
            var retrievenInts = new int[20000];
            KernelUtils.getDataFromBuffer(bufferName, retrievenInts);
            for (int i = 0; i < 20000; i++)
            {
                Assert.AreEqual(retrievenInts[i], 8);
            }

        }
    }
}
