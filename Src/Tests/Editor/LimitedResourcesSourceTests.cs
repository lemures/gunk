using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using NUnit.Framework;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class LimitedResourcesSourceTests
    {
        [TestCase(0.0f)]
        [TestCase(1.0f)]
        [TestCase(1000.0f)]
        [TestCase(1000000.0f)]
        public void gatheredResourcesTotalEqualToStartResources(float startAmount)
        {
            LimitedResourcesSource container = new LimitedResourcesSource(ResourceType.BlueCrystals, startAmount, startAmount/500.0f);

            var totalGathered = 0.0f;

            for (int i = 0; i < 1000; i++)
            {
                totalGathered+=container.gatherResources(1.0f).amount;
            }
            Assert.AreEqual(totalGathered,startAmount,0.01f);
        }

        [Test]
        public void resourcesAreGatheredWithCorrectSpeed()
        {
            LimitedResourcesSource container = new LimitedResourcesSource(ResourceType.BlueCrystals, 100.0f, 100.0f);
            Assert.AreEqual(37.0f,container.gatherResources(0.37f).amount,0.01f);
        }

        [Test]
        public void isDepletedReturnsTrueOnEmptyContainer()
        {
            LimitedResourcesSource container = new LimitedResourcesSource(ResourceType.BlueCrystals, 0.0f, 100.0f);
            Assert.IsTrue(container.isDepleted());
        }
    }
}