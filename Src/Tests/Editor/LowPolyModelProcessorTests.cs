using System.Collections.Generic;
using Gunk_Scripts.Asset_Importing;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class LowPolyModelProcessorTests
    {

        [Ignore("too slow too run")]
        [Test]
        public void prefabsGenerateCorrectlyTest()
        {
            var meshes=LowPolyTreesResourcesFinder.findAllMeshes();
            var textures = LowPolyTreesResourcesFinder.findAllTextures();
            var processor=new LowPolyModelProcessor();
            processor.process(meshes, textures);
        }
    }
}