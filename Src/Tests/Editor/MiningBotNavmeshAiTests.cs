using System;
using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class MiningBotNavmeshAiTests
    {
        static readonly int id = 42;
        static readonly int resourceId = 134;

        [Test]
        public void givingOrderToMineMakesAgentMove()
        {
            var fakeSurroundings = new FakeComponentSurroundings(id);
            fakeSurroundings.addFakeAgent();
            fakeSurroundings.addFakeResourceObjectToManager(resourceId, Vector3.one * 100.0f,
                new LimitedResourcesSource(ResourceType.PinkCrystals, 100, 10) );

            var miningBotAi = new MiningBotNavmeshAi(fakeSurroundings.fakeObject);
            miningBotAi.finishConstruction();
            miningBotAi.goMineSpecificResource(resourceId);
            Assert.AreEqual(new Vector3(100, 100, 100), fakeSurroundings.fakeObject.agent.destination);
        }

        [Test]
        public void resourceIsDepletedAfterFiniteTime()
        {
            var fakeSurroundings = new FakeComponentSurroundings(id);
            fakeSurroundings.addStubAgent(Vector3.zero);
            var resource = fakeSurroundings.addFakeResourceObjectToManager(resourceId, Vector3.one * 100.0f,
                new LimitedResourcesSource(ResourceType.PinkCrystals, 100, 10) );

            var miningBotAi = new MiningBotNavmeshAi(fakeSurroundings.fakeObject);
            miningBotAi.finishConstruction();
            miningBotAi.goMineSpecificResource(resourceId);
            for (var i = 0; i < 1000; i++)
            {
                miningBotAi.update(0.1f);
                ((FakeNavigationAgent)fakeSurroundings.fakeObject.agent).update(0.1f);
            }
            Assert.IsTrue(resource.resourcesContainer.isDepleted());
        }
    }
}