using Gunk_Scripts.Maps;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class ObjectPatternTests
    {
        private static bool haveSameFieldsSet(ObjectPattern a, ObjectPattern b)
        {
            return a.fieldsLocations.TrueForAll(x => b.fieldsLocations.Contains(x))
                && b.fieldsLocations.TrueForAll(x => a.fieldsLocations.Contains(x));
        }

        [TestCase(1,5)]
        [TestCase(15,5)]
        [TestCase(13,53)]
        public void rectangleConstructorGeneratesCorrectObjectForEvenSides(int shapeWidth,int shapeHeight)
        {
            var pattern=new ObjectPattern(shapeWidth,shapeHeight);
            Assert.AreEqual(expected: shapeHeight*shapeWidth,actual: pattern.fieldsLocations.Count);
            for (var i = -shapeWidth/2; i <= shapeWidth/2; i++)
            {
                for (var q = -shapeHeight/2; q <= shapeHeight/2; q++)
                {
                    Assert.IsTrue(pattern.fieldsLocations.Exists(field => field.x == i && field.y == q),
                        "field ("+i+","+q+") is not in the created pattern but it should be");
                }
            }
        }

        [TestCase(1,5)]
        [TestCase(15,5)]
        [TestCase(13,53)]
        public void rotation90DegreesWorksCorrectly(int shapeWidth,int shapeHeight)
        {
            var pattern=new ObjectPattern(shapeWidth,shapeHeight);
            var patternRotatedCorrectly=new ObjectPattern(shapeHeight,shapeWidth);
            var patternRotated = pattern.rotate(90.0f);

            Assert.IsTrue(haveSameFieldsSet(patternRotated,patternRotatedCorrectly));
        }

        [TestCase(1,1)]
        [TestCase(5,7)]
        [TestCase(21,15)]
        [TestCase(61,15)]
        public void objectPatternBasedOnBoundsWithIntegerCoordinatesIsGeneratedCorrectly(int height, int width)
        {
            var bounds=new Bounds(new Vector3(0.5f,0,0.5f), new Vector3(width,0,height));

            var objectToTest=new ObjectPattern(bounds);
            var equivalent = new ObjectPattern(width,height);
            Assert.IsTrue(haveSameFieldsSet(objectToTest,equivalent), "expected:\n" + equivalent + " got:\n"+objectToTest);
        }
    }
}