using NUnit.Framework;
using UnityEngine;
using UnityEngine.Networking;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class ObjectReferenceCollectorTests
    {
        [Test]
        public void objectsWithDifferentNamesGetDifferentIds()
        {
            var go1 = new GameObject("test1");
            var go2 = new GameObject("test2");

            ObjectReferenceCollector referenceCollector=new ObjectReferenceCollector();
            referenceCollector.addResource(go1);
            referenceCollector.addResource(go2);

            var id1 = referenceCollector.getResourceId(go1);
            var id2 = referenceCollector.getResourceId(go2);

            Assert.AreNotEqual(id1,id2);
        }

        [Test]
        public void objectCanBeRetrievenById()
        {
            var go = new GameObject("test");

            ObjectReferenceCollector referenceCollector=new ObjectReferenceCollector();
            referenceCollector.addResource(go);
            var id = referenceCollector.getResourceId(go);
            var retrievedGo = referenceCollector.getResourceById(id);

            Assert.AreSame(go,retrievedGo);
        }

        [Test]
        public void findingNonExistentIdProducesError()
        {
            ObjectReferenceCollector referenceCollector=new ObjectReferenceCollector();
            Assert.Catch(() => referenceCollector.getResourceById(0));
        }

        [Test]
        public void gettingIdOfNonExistentObjectProducesError()
        {
            ObjectReferenceCollector referenceCollector=new ObjectReferenceCollector();
            Assert.Catch(() => referenceCollector.getResourceId(new GameObject("test")));
        }

        [Test]
        public void addingSameObjectTwiceDoesNotProduceErrors()
        {
            var go = new GameObject("test");

            ObjectReferenceCollector referenceCollector=new ObjectReferenceCollector();
            referenceCollector.addResource(go);
            Assert.DoesNotThrow(()=>referenceCollector.addResource(go));
        }

        [Test]
        public void addingSameObjectTwiceUpdatesSavedReference()
        {
            var go1 = new GameObject("test1");
            var go2 = new GameObject("test1");

            ObjectReferenceCollector referenceCollector=new ObjectReferenceCollector();
            referenceCollector.addResource(go1);
            var id = referenceCollector.getResourceId(go1);

            referenceCollector.addResource(go2);
            Assert.AreSame(referenceCollector.getResourceById(id),go2);
        }
    }
}