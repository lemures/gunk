using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using NUnit.Framework;
using Src.Logic;
using UnityEngine;

namespace Src.Tests.Editor
{
    public class OwnedResourcesTests
    {
        [Test]
        public void addedResourcesSumUpToGetResourceCount()
        {
            var owned = new OwnedResources();

            owned.addResources(new GatheredResources(ResourceType.PinkCrystals, 20));
            owned.addResources(new GatheredResources(ResourceType.PinkCrystals, 6));

            owned.addResources(new GatheredResources(ResourceType.BlueCrystals, 30));
            owned.addResources(new GatheredResources(ResourceType.BlueCrystals, 8));

            Assert.AreEqual(owned.getResourcesCount(ResourceType.PinkCrystals), 26);
            Assert.AreEqual(owned.getResourcesCount(ResourceType.BlueCrystals), 38);
        }
    }
}