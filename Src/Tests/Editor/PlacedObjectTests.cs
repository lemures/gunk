using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class PlacedObjectTests
    {
        [Test]
        public void creatingObjectLocksSpace()
        {
            var shape = new ObjectPattern(3, 5);
            var surroundings = new FakeComponentSurroundings(1);
            A.CallTo(() => surroundings.fakeObject.transform.position).Returns(new Vector3(1, 2, 3));
            var placedObject = new PlacedObject(surroundings.fakeObject, shape);
            A.CallTo(() => surroundings.fakeManager.lockSpaceUnderObject(surroundings.fakeObject, shape))
                .MustHaveHappenedOnceExactly();
        }
        
        [Test]
        public void destroyingObjectUnlocksSpace()
        {
            var shape = new ObjectPattern(3, 5);
            var surroundings = new FakeComponentSurroundings(1);
            A.CallTo(() => surroundings.fakeObject.transform.position).Returns(new Vector3(1, 2, 3));
            var placedObject = new PlacedObject(surroundings.fakeObject, shape);
            placedObject.destroy();
            A.CallTo(() => surroundings.fakeManager.unlockSpaceUnderObject(surroundings.fakeObject, shape))
                .MustHaveHappenedOnceExactly();
        }
    }
}