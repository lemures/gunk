using Gunk_Scripts.Maps;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Diagnostics;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class PlacedObjectsManagerTests
    {
        private static void setFieldTypesMatchingShape(Vector2Int center, ObjectPattern pattern, PlacedObjectsManager placedObjectsManager)
        {
            foreach (var field in pattern.fieldsLocations)
            {
                var location = center + new Vector2Int(field.x, field.y);
                placedObjectsManager.changeFieldType(location,field.type);
            }
        }

        [Test]
        public void objectWithMixedFieldTypesCanBePlaced()
        {
            var placedManager = new PlacedObjectsManager(100, 100);
            var shape=new ObjectPattern(3,3);

            var center=new Vector2Int(50,50);

            var offset1=new Vector2Int(-1,-1);
            var offset2=new Vector2Int(-1,1);
            var offset3=new Vector2Int(1,-1);
            var offset4=new Vector2Int(1,1);

            shape.setFieldType(offset1,TerrainFieldType.Slope);
            shape.setFieldType(offset2,TerrainFieldType.Water);
            shape.setFieldType(offset3,TerrainFieldType.CrystalMine);
            shape.setFieldType(offset4,TerrainFieldType.Slope);

            setFieldTypesMatchingShape(center, shape, placedManager);
            Assert.IsTrue(placedManager.canObjectBeAdded(center,shape));
        }

        [Test]
        public void singleFieldObjectAndWrongFieldTypeCannotBePlaced()
        {
            var placedManager = new PlacedObjectsManager(100, 100);
            var shape=new ObjectPattern(1,1);
            var center=new Vector2Int(50,50);
            shape.setFieldType(Vector2Int.zero, TerrainFieldType.Slope);
            Assert.IsFalse(placedManager.canObjectBeAdded(center,shape));
        }

        [Test]
        public void singleFieldObjectAndRightFieldTypeCanBePlaced()
        {
            var placedManager = new PlacedObjectsManager(100, 100);
            placedManager.changeFieldType(new Vector2Int(50,50), TerrainFieldType.Slope);
            var shape=new ObjectPattern(1,1);
            var center=new Vector2Int(50,50);
            shape.setFieldType(Vector2Int.zero, TerrainFieldType.Slope);
            Assert.IsTrue(placedManager.canObjectBeAdded(center,shape));
        }

        [Test]
        public void calculateClosestAvailablePositionFindsMatch()
        {
            var placedManager = new PlacedObjectsManager(100, 100);
            placedManager.changeFieldType(new Vector2Int(50,50), TerrainFieldType.Slope);

            var shape=new ObjectPattern(1,1);
            var center=new Vector2Int(47,54);
            shape.setFieldType(Vector2Int.zero, TerrainFieldType.Slope);

            var match = placedManager.calculateClosestAvailablePosition(center, shape);

            Assert.NotNull(match);
            Assert.AreEqual(match.Value, new Vector2Int(50,50));
        }

        [Test]
        public void calculateClosestAvailablePositionReturnsNullIfNoMatchesExist()
        {
            var placedManager = new PlacedObjectsManager(100, 100);

            var shape=new ObjectPattern(1,1);
            var center=new Vector2Int(47,54);
            shape.setFieldType(Vector2Int.zero, TerrainFieldType.Slope);

            var match = placedManager.calculateClosestAvailablePosition(center, shape);

            Assert.IsNull(match);
        }


        [TestCase(1,1)]
        [TestCase(31,5)]
        [TestCase(5,101)]
        [TestCase(101,101)]
        public void addingObjectLocksSpaceSuccessfully(int objectHeight,int objectWidth)
        {
            var placedManager = new PlacedObjectsManager(101, 101);
            var shape=new ObjectPattern(objectHeight,objectWidth);
            var center=new Vector2Int(50,50);
            placedManager.addObject(center,shape);
            Assert.IsFalse(placedManager.canObjectBeAdded(center,shape));
        }

        [TestCase(1,1)]
        [TestCase(31,5)]
        [TestCase(5,101)]
        [TestCase(101,101)]
        public void removingObjectUnlocksSpaceSuccessfully(int objectHeight,int objectWidth)
        {
            var placedManager = new PlacedObjectsManager(101, 101);
            var shape=new ObjectPattern(1,1);
            var center=new Vector2Int(50,50);
            placedManager.addObject(center,shape);
            placedManager.removeObject(center,shape);
            Assert.IsTrue(placedManager.canObjectBeAdded(center,shape));
        }
    }
}