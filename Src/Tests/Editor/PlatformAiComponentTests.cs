using System.Deployment.Internal;
using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.GunkObjects;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class PlatformAiComponentTests
    {
        private IGunkObject createAndAddFakeUnitUnderConstruction(float energyToConstruct, int id,
            FakeComponentSurroundings surroundings)
        {
            var fakeObject = A.Fake<IGunkObject>();
            A.CallTo(() => surroundings.fakeManager.getObjectWithId(id)).Returns(fakeObject);

            var counter = new Counter(0, energyToConstruct);

            A.CallTo(() => fakeObject.id).Returns(id);
            A.CallTo(() => fakeObject.getEnergyNeeds()).ReturnsLazily(() => counter.valueToFull);
            A.CallTo(() => fakeObject.provideWithEnergy(A<float>._))
                .Invokes((float energy) => counter.increaseValue(energy));
            A.CallTo(() => fakeObject.isConstructed()).ReturnsLazily(() => counter.isFull());
            return fakeObject;
        }

        [Test]
        public void objectIsSpawnedAfterItIsAddedToQueue()
        {
            var surroundings = new FakeComponentSurroundings(0);
            surroundings.addFakeTransformPosition(Vector3.zero);
            var component = new PlatformAiComponent(surroundings.fakeObject);
            var underConstruction = createAndAddFakeUnitUnderConstruction(100.0f, 13, surroundings);
            GunkObjectFactory.setNextSpawnedObject(underConstruction);
            component.addObjectToQueue(GunkObjectType.WeldingBot);
            component.update(1.0f);
            Assert.IsFalse(GunkObjectFactory.hasNextObjectToSpawn());
        }

        [Test]
        public void spawnedObjectIsChargedWhenComponentIsCharged()
        {
            var surroundings = new FakeComponentSurroundings(0);
            surroundings.addFakeTransformPosition(Vector3.zero);
            var component = new PlatformAiComponent(surroundings.fakeObject);
            var underConstruction = createAndAddFakeUnitUnderConstruction(100.0f, 13, surroundings);
            GunkObjectFactory.setNextSpawnedObject(underConstruction);
            component.addObjectToQueue(GunkObjectType.WeldingBot);
            component.update(1.0f);
            for (int i = 0; i < 50; i++)
            {
                component.chargeConstructedObject(1.0f);
            }
            Assert.AreEqual(underConstruction.getEnergyNeeds(),50,0.1f);
        }

        [Test]
        public void objectIsNotSpawnedImmediately()
        {
            var surroundings = new FakeComponentSurroundings(0);
            surroundings.addFakeTransformPosition(Vector3.zero);
            var component = new PlatformAiComponent(surroundings.fakeObject);
            var underConstruction = createAndAddFakeUnitUnderConstruction(100.0f, 13, surroundings);
            GunkObjectFactory.setNextSpawnedObject(underConstruction);
            component.addObjectToQueue(GunkObjectType.WeldingBot);
            component.update(0.0f);
            Assert.IsTrue(GunkObjectFactory.hasNextObjectToSpawn());
        }

        [Test]
        public void updateDoesNotThrowErrorsWhenConstructedObjectIsDestroyed()
        {
            var surroundings = new FakeComponentSurroundings(0);
            surroundings.addFakeTransformPosition(Vector3.zero);
            var component = new PlatformAiComponent(surroundings.fakeObject);
            var underConstruction = createAndAddFakeUnitUnderConstruction(100.0f, 13, surroundings);
            GunkObjectFactory.setNextSpawnedObject(underConstruction);
            component.addObjectToQueue(GunkObjectType.WeldingBot);
            component.update(1.0f);
            A.CallTo(() => surroundings.fakeManager.getObjectWithId(13)).Returns(null);
            Assert.DoesNotThrow(() => component.update(1.0f));
        }

        [TearDown]
        public void teardownMethod()
        {
            GunkObjectFactory.resetNextSpawnedObject();
        }
    }
}