﻿using System.Collections.Generic;
using System.Linq;
using Gunk_Scripts.Shader_Constants;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class PrefabManagerTests
    {
        [Test]
        public void addingPrefabWorks()
        {
            var go=new GameObject("blah");
            Gunk_Scripts.GlobalPrefabsManager.getInstance().addPrefab(go);
            Assert.DoesNotThrow(() => Gunk_Scripts.GlobalPrefabsManager.getInstance().findPrefab(go.name));
        }

        [Test]
        public void findPrefabWorks()
        {
            var go=new GameObject();
            Gunk_Scripts.GlobalPrefabsManager.getInstance().addPrefab(go);
            var prefab=Gunk_Scripts.GlobalPrefabsManager.getInstance().findPrefab(go.name);
            Assert.AreSame(prefab.go,go);
        }

        [Test]
        public void addingObjectWithSameNameOverwritesObject()
        {
            var go1 = new GameObject("testObject");
            var go2 = new GameObject("testObject");
            Gunk_Scripts.GlobalPrefabsManager.getInstance().addPrefab(go1);
            Gunk_Scripts.GlobalPrefabsManager.getInstance().addPrefab(go2);
            var prefab=Gunk_Scripts.GlobalPrefabsManager.getInstance().findPrefab(go2.name);
            Assert.AreSame(prefab.go,go2);
        }

        [Test]
        public void meshReferencesAreCollectedCorrectlyTest()
        {
            var go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            var mesh1 = go.GetComponent<MeshFilter>().sharedMesh;

            var prefabManager=Gunk_Scripts.GlobalPrefabsManager.getInstance();
            prefabManager.addPrefab(go);

            var references = Gunk_Scripts.GlobalPrefabsManager.getInstance().getAllPrefabsUnityReferences();
            var referencesList = new List<Object>(references);

            Assert.Contains(mesh1, referencesList);
        }

        [Test]
        public void materialReferencesAreCollectedCorrectlyTest()
        {
            var go = GameObject.CreatePrimitive(PrimitiveType.Cube);

            var mat1 = new Material(Shader.Find(ToonStandardConstants.shaderName)) {name = "mat1"};
            var mat2 = new Material(Shader.Find(ToonStandardConstants.shaderName)) {name = "mat2"};

            var myRenderer=go.GetComponent<MeshRenderer>();
            myRenderer.sharedMaterials = new[]{mat1,mat2};

            Gunk_Scripts.GlobalPrefabsManager.getInstance().addPrefab(go);
            var references = Gunk_Scripts.GlobalPrefabsManager.getInstance().getAllPrefabsUnityReferences();
            var referencesList = new List<Object>(references);

            Assert.Contains(mat1, referencesList);
            Assert.Contains(mat2, referencesList);
        }
    }
}
