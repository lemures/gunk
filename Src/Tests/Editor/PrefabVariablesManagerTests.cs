﻿using System.Collections.Generic;
using Gunk_Scripts.Asset_Importing;
using NUnit.Framework;

namespace Gunk_Scripts.Tests.Editor
{
    public class PrefabVariablesManagerTests
    {
        [Test]
        public void basicFunctionalityTest()
        {
            string prefabName = "testPrefabName1234";
            string prefabVariable1 = "blah1";
            string prefabVariable2 = "blah2";
            string prefabVariable3 = "blah3";

            PrefabVariablesManager.saveVariable(prefabName, prefabVariable1, 1);
            PrefabVariablesManager.saveVariable(prefabName, prefabVariable2, 3.5f);
            PrefabVariablesManager.saveVariable(prefabName, prefabVariable3, new List<int>{1,2,3});

            Assert.IsNotNull((PrefabVariablesManager.getVariable(prefabName, prefabVariable3) as List<int>));
            Assert.AreEqual(1, (PrefabVariablesManager.getVariable(prefabName, prefabVariable3) as List<int>)[0]);
            Assert.AreEqual(2, (PrefabVariablesManager.getVariable(prefabName, prefabVariable3) as List<int>)[1]);
            Assert.AreEqual(3, (PrefabVariablesManager.getVariable(prefabName, prefabVariable3) as List<int>)[2]);
            Assert.AreEqual(1, (int)PrefabVariablesManager.getVariable(prefabName, prefabVariable1));
            Assert.AreEqual(3.5f, (float)PrefabVariablesManager.getVariable(prefabName, prefabVariable2));

        }

    }
}
