using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class RectangleFormationTests
    {
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        [TestCase(20)]
        public void averagePositionInFormationIsZeroForSquaresError(int squareSide)
        {
            var numbers = Enumerable.Range(1, squareSide * squareSide).ToList();
            var formation = new RectangleFormation(numbers, Vector3.zero, 1.0f);
            var sumOfPositions = Vector3.zero;
            numbers.ForEach(x => sumOfPositions += formation.getPosition(x));
            Assert.AreEqual(Vector3.Distance(sumOfPositions, Vector3.zero), 0.0f, 0.01f);
        }

        [TestCase(0.5f, 0.8f)]
        [TestCase(0.15f, 20.8f)]
        [TestCase(5.15f, 24.8f)]
        public void distanceScalesWithTargetDistanceBetweenObjectsVariable(float targetDistance1, float targetDistance2)
        {
            const int squareSide = 5;
            var numbers = Enumerable.Range(1, squareSide * squareSide).ToList();
            var formation1 = new RectangleFormation(numbers, Vector3.zero, targetDistance1);
            var formation2 = new RectangleFormation(numbers, Vector3.zero, targetDistance2);

            foreach (var number in numbers)
            {
                var distance1 = formation1.getPosition(number).magnitude;
                var distance2 = formation2.getPosition(number).magnitude;
                //to avoid division by zero
                if (distance1 < 0.01f) continue;

                Assert.AreEqual(distance1 / distance2, targetDistance1 / targetDistance2, 0.01f);
            }
        }

        [Test]
        public void nonExistentObjectPositionCheckingProducesError()
        {
            var formation = new RectangleFormation(new List<int> {93}, Vector3.zero, 1.0f);
            Assert.Catch(() => formation.getPosition(10));
        }
    }
}