﻿using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class RenderTextureCapturerTests
    {
        [Test]
        public void smallTextureCaptureTest()
        {
            Texture2D textureToCapture = new Texture2D(2,2,TextureFormat.ARGB32,false,true);
            textureToCapture.SetPixel(0, 0, Color.yellow);
            textureToCapture.SetPixel(0, 1, Color.red);
            textureToCapture.SetPixel(1, 0, Color.cyan);
            textureToCapture.SetPixel(1, 1, Color.green);
            textureToCapture.Apply();
            var pixels=TextureCapturer.getTexturePixels(textureToCapture);
            Assert.AreEqual(pixels[0], Color.yellow);
            Assert.AreEqual(pixels[1], Color.cyan);
            Assert.AreEqual(pixels[2], Color.red);
            Assert.AreEqual(pixels[3], Color.green);
        }

        [Test]
        public void hugeTextureCaptureTest()
        {
            Texture2D textureToCapture = new Texture2D(1000,1000,TextureFormat.ARGB32,false,true);
            textureToCapture.SetPixel(500, 500, Color.yellow);
            textureToCapture.Apply();
            var pixels = TextureCapturer.getTexturePixels(textureToCapture);
            Assert.AreEqual(Color.yellow,pixels[500*1000+500]);
        }
    }
}
