using FakeItEasy;
using FakeItEasy.Core;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class ResourceComponentTests
    {
        [Test]
        public void creatingComponentAddResourceInObjectManager()
        {
            //creating fake surroundings
            var fakeSurroundings = new FakeComponentSurroundings(0);
            fakeSurroundings.addFakeTransformPosition(new Vector3(1,2,3));
            var fakeResourceFinder = fakeSurroundings.createFakeResourceFinder();
            var resourceContainer = A.Fake<IResourcesContainer>();
            A.CallTo(() => resourceContainer.type).Returns(ResourceType.PinkCrystals);

            //creating component
            var resourceComponent = new ResourceComponent(fakeSurroundings.fakeObject, resourceContainer, true, false);

            //assertion
            A.CallTo(() => fakeResourceFinder.addObject(0, new Float2(1, 3), ResourceType.PinkCrystals)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void objectIsDestroyedWhenResourcesContainerBecomesDepleted()
        {
            //creating fake surroundings
            var fakeSurroundings = new FakeComponentSurroundings(0);
            fakeSurroundings.addFakeTransformPosition(new Vector3(1,2,3));
            var fakeResourceFinder = fakeSurroundings.createFakeResourceFinder();
            var resourceContainer = A.Fake<IResourcesContainer>();
            A.CallTo(() => resourceContainer.isDepleted()).Returns(true);

            //actions around component
            var resourceComponent = new ResourceComponent(fakeSurroundings.fakeObject, resourceContainer, true, false);
            resourceComponent.gatherResources(1.0f);

            //assertion
            A.CallTo(() => fakeSurroundings.fakeObject.destroy()).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void destroyingComponentRemovesResource()
        {
            //creating fake surroundings
            var fakeSurroundings = new FakeComponentSurroundings(0);
            var fakeResourceFinder = fakeSurroundings.createFakeResourceFinder();
            fakeSurroundings.addFakeTransformPosition(new Vector3(1,2,3));
            var resourceContainer = A.Fake<IResourcesContainer>();

            //actions around component
            var resourceComponent = new ResourceComponent(fakeSurroundings.fakeObject, resourceContainer, true, false);
            resourceComponent.destroy();

            //assertion
            A.CallTo(() => fakeResourceFinder.removeObject(0)).MustHaveHappenedOnceExactly();
        }
    }
}