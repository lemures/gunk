﻿using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class ScriptFileCacherTests
    {
        [Test]
        public void functionalityTest()
        {
            string testFilePath = Application.dataPath+"/"+"test.txt";
            string messageFirstLine = "blah blah blahblah";
            string messageSecondLine="blahblahblah";
            FileManager.putStringToFile(testFilePath, messageFirstLine+"\n"+messageSecondLine);
            var lines=ScriptFileCacher.getFileLines(testFilePath);
            Assert.IsTrue(lines[1][0] == messageSecondLine);
            ScriptFileCacher.forgetFile(testFilePath);
            FileManager.deleteFile(testFilePath);
        }

    }
}
