using System.Linq;
using FakeItEasy;
using Gunk_Scripts.Objects_Selection;
using Gunk_Scripts.SerializationHelpers;
using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class SelectableObjectsCollectionTests
    {
        private static ICollider createFakeColliderThatIsAlwaysSelected()
        {
            var result = A.Fake<ICollider>();
            A.CallTo(() => result.center).Returns(new Vector3(0, 0, 0));
            A.CallTo(() => result.isUnderPoint(A<IScreenPositionCalculator>.Ignored, A<Vector2>.Ignored)).Returns(true);
            A.CallTo(() => result.isInScreenRectangle(
                A<Vector2>.Ignored,
                A<Vector2>.Ignored,
                A<IScreenPositionCalculator>.Ignored)).Returns(true);
            return result;
        }

        private static ICollider createFakeColliderThatIsNeverSelected()
        {
            var result = A.Fake<ICollider>();
            A.CallTo(() => result.center).Returns(new Vector3(0, 0, 0));
            A.CallTo(() => result.isUnderPoint(A<IScreenPositionCalculator>.Ignored, A<Vector2>.Ignored)).Returns(false);
            A.CallTo(() => result.isInScreenRectangle(
                A<Vector2>.Ignored,
                A<Vector2>.Ignored,
                A<IScreenPositionCalculator>.Ignored)).Returns(false);
            return result;
        }

        [Test]
        public void checkingUnderPointerObjectWhenNoneAreAddedDoesNotThrowErrors()
        {
            var fakeCalculator = A.Fake<IScreenPositionCalculator>();
            var collection=new SelectableColliderCollection(fakeCalculator);
            Assert.DoesNotThrow(()=>collection.getObjectUnderPointer(Vector2.zero));
        }

        [Test]
        public void checkingCollidersInRectangleWhenNoneAreAddedDoesNotThrowErrors()
        {
            var fakeCalculator = A.Fake<IScreenPositionCalculator>();
            var collection=new SelectableColliderCollection(fakeCalculator);
            Assert.DoesNotThrow(() => collection.getObjectsInRectangle(new DragRectangle(Vector3.zero, Vector3.zero)));
        }

        [Test]
        public void highestPriorityObjectUnderPointerIsReturned()
        {
            var fakeCalculator = A.Fake<IScreenPositionCalculator>();
            var collection=new SelectableColliderCollection(fakeCalculator);

            collection.addCollider(5,createFakeColliderThatIsAlwaysSelected(),SelectionPriority.High);
            collection.addCollider(1,createFakeColliderThatIsAlwaysSelected(),SelectionPriority.VeryHigh);
            collection.addCollider(9,createFakeColliderThatIsAlwaysSelected(),SelectionPriority.Low);

            Assert.AreEqual(1,collection.getObjectUnderPointer(Vector2.zero));
        }

        [TestCase(1,1)]
        [TestCase(10,1)]
        [TestCase(1,10)]
        [TestCase(20,20)]
        public void highestPriorityObjectsInRectangleAreReturned(int lowPriorityCount, int mediumPriorityCount)
        {
            var fakeCalculator = A.Fake<IScreenPositionCalculator>();
            var collection=new SelectableColliderCollection(fakeCalculator);

            var lowIds = Enumerable.Range(0, lowPriorityCount).ToList();
            var mediumIds = Enumerable.Range(lowPriorityCount, mediumPriorityCount).ToList();

            var collider = createFakeColliderThatIsAlwaysSelected();

            lowIds.ForEach(x=>collection.addCollider(x,collider,SelectionPriority.Low));
            mediumIds.ForEach(x=>collection.addCollider(x,collider,SelectionPriority.Medium));

            var foundIds = collection.getObjectsInRectangle(new DragRectangle(Vector2.zero, Vector2.zero));
            Assert.AreEqual(mediumPriorityCount,foundIds.Count);
            Assert.IsTrue(foundIds.TrueForAll(foundIds.Contains));
        }

        [Test]
        public void removingNonExistentColliderThrowsError()
        {
            var fakeCalculator = A.Fake<IScreenPositionCalculator>();
            var collection=new SelectableColliderCollection(fakeCalculator);
            Assert.Catch(() => collection.removeCollider(0));
        }

        [Test]
        public void removingObjectReallyRemovesIt()
        {
            var fakeCalculator = A.Fake<IScreenPositionCalculator>();
            var collection=new SelectableColliderCollection(fakeCalculator);
            collection.addCollider(0,A.Fake<ICollider>(),SelectionPriority.High);
            collection.removeCollider(0);
            Assert.IsFalse(collection.hasCollider(0));
        }

        [Test]
        public void addingSameObjectTwiceProducesError()
        {
            var fakeCalculator = A.Fake<IScreenPositionCalculator>();
            var collection=new SelectableColliderCollection(fakeCalculator);
            collection.addCollider(0,A.Fake<ICollider>(),SelectionPriority.High);

            Assert.Catch(() => collection.addCollider(0,A.Fake<ICollider>(),SelectionPriority.High));
        }

        [Test]
        public void doesNotReturnColliderThatIsNotUnderPointer()
        {
                var fakeCalculator = A.Fake<IScreenPositionCalculator>();
                var collection=new SelectableColliderCollection(fakeCalculator);

                collection.addCollider(1,createFakeColliderThatIsNeverSelected(),SelectionPriority.VeryHigh);

                Assert.AreEqual(null,collection.getObjectUnderPointer(Vector2.zero));
        }

        [Test]
        public void doesNotReturnColliderThatIsNotInRectangle()
        {
            var fakeCalculator = A.Fake<IScreenPositionCalculator>();
            var collection=new SelectableColliderCollection(fakeCalculator);

            collection.addCollider(1,createFakeColliderThatIsNeverSelected(),SelectionPriority.VeryHigh);

            Assert.AreEqual(0,collection.getObjectsInRectangle(new DragRectangle(Vector3.zero, Vector3.zero)).Count);
        }
    }
}