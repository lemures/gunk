﻿using Gunk_Scripts.Renderers;
using NUnit.Framework;

namespace Gunk_Scripts.Tests.Editor
{
    public class SelectedRendererTests {

        private void rendererRendersCircleWithCorrectRadius()
        {
            SelectedObjectsRenderer renderer = new SelectedObjectsRenderer(1000, 80, 80);
            var renderTexture = renderer.getSelectedObjectsRenderTexture();
            renderer.addObject(0, 0, 0, 0.0f);
            renderer.setSelectedCircleRadius(0, 40.0f);
            renderer.render();
            var pixels = TextureCapturer.getTexturePixels(renderTexture);
            var texHeight = renderTexture.height;
            var texWidth = renderTexture.width;
            Assert.Greater(pixels[0].r, renderer.getSelectedObjectColorThreshold());
            Assert.Greater(pixels[texWidth / 2 - 1].r, renderer.getSelectedObjectColorThreshold());
            Assert.Greater(-pixels[texWidth / 2].r, -renderer.getSelectedObjectColorThreshold());
            renderer.removeObject(0);
            renderer.render();
            pixels = TextureCapturer.getTexturePixels(renderTexture);
            Assert.Greater(-pixels[0].r, -renderer.getSelectedObjectColorThreshold());
        }

        [Test]
        public void functionalityTest()
        {
            for (int i = 0; i < 5; i++)
            {
                rendererRendersCircleWithCorrectRadius();
            }

        }

    }
}
