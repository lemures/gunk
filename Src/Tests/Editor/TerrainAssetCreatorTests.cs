﻿using Gunk_Scripts.Maps;
using Gunk_Scripts.Shader_Constants;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.XR.WSA.WebCam;

namespace Gunk_Scripts.Tests.Editor
{
    [TestFixture]
    public class TerrainAssetCreatorTests
    {
        private readonly string testMapName = "test map";

        [Test]
        public void createdMapIsNotNull()
        {
            var map=TerrainAssetCreator.importMapAssets(testMapName);
            Assert.IsTrue(map != null);
        }

        [Test]
        public void terrainDataIsNotNull()
        {
            var map=TerrainAssetCreator.importMapAssets(testMapName);
            Assert.IsTrue(map.prefab.GetComponent<Terrain>().terrainData != null);
        }

        [Test]
        public void terrainDataIsSameAsMapVariableTerrainData()
        {
            var map=TerrainAssetCreator.importMapAssets(testMapName);
            Assert.IsTrue(map.prefab.GetComponent<Terrain>().terrainData == map.terrainData);
        }

        [Test]
        public void splatMapIsSuccessfullyFound()
        {
            var map=TerrainAssetCreator.importMapAssets(testMapName);
            Assert.IsTrue(map.splatMap != null);
        }

        [Test]
        public void normalMapIsSuccessfullyFound()
        {
            var map=TerrainAssetCreator.importMapAssets(testMapName);
        }

        [Test]
        public void mapNameIsCorrectlyAssigned()
        {
            var map=TerrainAssetCreator.importMapAssets(testMapName);
            Assert.IsTrue(map.mapName == testMapName);
        }

        [Test]
        public void materialIsCreated()
        {
            var map=TerrainAssetCreator.importMapAssets(testMapName);
            Assert.IsTrue(map.mat != null);
        }

        [Test]
        public void materialHasRightShader()
        {
            var map=TerrainAssetCreator.importMapAssets(testMapName);
            Assert.IsTrue(map.mat.shader == Shader.Find(ToonTerrainShaderConstants.shaderName));
        }

        [Test]
        public void deletingMapAssetsTwiceDoesNotProduceErrors()
        {
            TerrainAssetCreator.deleteCreatedMapAssets(testMapName);
            Assert.DoesNotThrow(() => TerrainAssetCreator.deleteCreatedMapAssets(testMapName));
        }

        [SetUp]
        [TearDown]
        public void deleteExistingAssets()
        {
            TerrainAssetCreator.deleteCreatedMapAssets(testMapName);
        }

    }
}
