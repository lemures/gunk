using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using NUnit.Framework;

namespace Gunk_Scripts.Tests.Editor
{
    public class UnderConstructionNeedingEnergyTests
    {
        [Test]
        public void zeroValuedEnergyNeededConstructsImmediately()
        {
            var fakeSurroundings = new FakeComponentSurroundings(0);
            var constructionState = new UnderConstructionNeedingEnergy(0);
            Assert.IsTrue(constructionState.constructionFinished);
        }
    }
}