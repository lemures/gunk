﻿using NUnit.Framework;
using UnityEngine;

namespace Gunk_Scripts.Tests.Editor
{
    public class UpdateCallerTests
    {
        [Test]
        public void updateCallerFunctionalityTest()
        {
            int akt = 0;
            UpdateCaller<GameObject> updateCaller = new UpdateCaller<GameObject>(x=>x!=null);
            var GO1 = new GameObject();
            var GO2 = new GameObject();
            updateCaller.addFunction(GO1, () => akt += 1);
            updateCaller.addFunction(GO1, () => akt += 10);
            updateCaller.addFunction(GO1, () => akt += 100);
            updateCaller.addFunction(GO2, () => akt += 1000);
            updateCaller.addFunction(GO2, () => akt += 10000);
            updateCaller.addFunction(GO2, () => akt += 100000);
            updateCaller.invokeAll();
            Assert.AreEqual(111111, akt);
            akt = 0;
            updateCaller.deleteAllObjectFunctions(GO1);
            updateCaller.invokeAll();
            Assert.AreEqual(111000, akt);
            Object.DestroyImmediate(GO2);
            akt = 0;
            updateCaller.invokeAll();
            Assert.AreEqual(0, akt);
        }
    }
}
