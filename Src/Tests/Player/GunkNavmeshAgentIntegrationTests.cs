﻿using System.Collections;
using FakeItEasy;
using Gunk_Scripts.GunkComponents;
using Gunk_Scripts.Maps;
using Gunk_Scripts.SerializationHelpers;
using NUnit.Framework;
using Src.GameRenderer;
using UnityEngine;
using UnityEngine.TestTools;

namespace Gunk_Scripts.Tests.Player
{
    public class GunkNavmeshAgentIntegrationTests
    {
        private bool isSetupDone;
        private Vector3 destination = new Vector3(350, 200, 380);
        private float timePassed;
        private GunkObject gunkObject;
        private GunkObjectManager objectManager;

        private void setup()
        {
            var map = DatabasesManager.getObjectLatestVersion(Map.getDatabaseName("test map with navmesh")) as Map;
            MainCamera.getInstance().spawnObject();
            var camera = MainCamera.getInstance().getCamera();
            Assert.NotNull(map);
            NewMapManager.MapLoader.loadMap(map);
            objectManager=new GunkObjectManager(
                map.getMapHeight(),
                map.getMapWidth(),
                camera,
                GlobalPrefabsManager.getInstance(),
                new GunkExtraObjectsRenderer(camera));

            gunkObject=new GunkObject(objectManager,"worker",new Vector3(350, Terrain.activeTerrain.SampleHeight(new Vector3(350, 200, 350)),350));
            gunkObject.addNavmeshAgent();
            gunkObject.agent.destination=destination;
            Time.timeScale = 10.0f;

            isSetupDone = true;
        }

        [UnityTest]
        public IEnumerator spawnedGunkNavmeshAgentTravelsToDestinationAfterItIsSet()
        {
            if (!isSetupDone)
            {
               setup();
            }

            if (timePassed > 30.0f)
            {
                var destinationSampled=new Vector3(destination.x, Terrain.activeTerrain.SampleHeight(this.destination),destination.z);
                Assert.IsTrue(Vector3.Distance(gunkObject.transform.position, destinationSampled) < 0.1f);
            }

            timePassed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}