using Gunk_Scripts.GunkComponents;
using Src.Libraries;
using UnityEngine;

namespace Src.Turrets
{
    public class ClosestCorruptedFieldLocator : ITurretTargetLocator
    {
        private CorruptedLand land;
        private float maxRange;
        private IGunkObject gunkObject;

        public ClosestCorruptedFieldLocator(IGunkObject gunkObject, float maxRange = 15.0f)
        {
            this.land = gunkObject.objectManager.corruptedLand;
        }

        public Target getTarget()
        {
            var currentPosition = gunkObject.transform.position;
            var found = land.getClosestCorruptedField(new Vector2Int((int)currentPosition.x, (int)currentPosition.z), (int)maxRange);
            if (found == null)
            {
                return null;
            }
            var targetPosition = new Vector3(found.Value.x + 0.5f, 0, found.Value.x + 0.5f);
            targetPosition.y = Terrain.activeTerrain.SampleHeight(targetPosition);
            return new ConstantPositionTarget(targetPosition);
        }
    }
}