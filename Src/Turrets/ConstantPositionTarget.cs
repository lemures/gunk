using UnityEngine;

namespace Src.Turrets
{
    public class ConstantPositionTarget : Target
    {
        public Vector3 position;

        public ConstantPositionTarget(Vector3 position)
        {
            this.position = position;
        }
    }
}