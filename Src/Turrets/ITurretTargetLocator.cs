using JetBrains.Annotations;

namespace Src.Turrets
{
    public interface ITurretTargetLocator
    {
        Target getTarget();
    }
}