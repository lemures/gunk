namespace Src.Turrets
{
    public interface IWeapon
    {
        void fire(Target target);

        bool isFiring { get; }

        void reload();

        bool isReloading { get; }
    }
}