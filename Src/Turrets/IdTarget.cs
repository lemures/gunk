namespace Src.Turrets
{
    public class IdTarget : Target
    {
        private int id;

        public IdTarget(int id)
        {
            this.id = id;
        }
    }
}