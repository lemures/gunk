using System;
using Gunk_Scripts.Data_Structures;
using Gunk_Scripts.GunkComponents;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.BoneTypes;
using Src.SkeletonAnimations.CurvesTypes;
using Src.SkeletonAnimations.Internal;
using UnityEngine;

namespace Src.Turrets
{
    [NeedsClip(idleClipName)]
    [NeedsClip(movingTowardsTargetClipName)]
    public class TurretRotator : GunkComponent
    {
        private float calculateManufactureClipArgument(Vector3 targetPosition)
        {
            var myPosition = parent.transform.position;
            var difference = targetPosition - myPosition;
            if (Vector3.Distance(targetPosition, myPosition) < 0.001f)
            {
                return 0;
            }
            var sin = -difference.x / Vector3.Distance(targetPosition, myPosition);
            return -0.5f + (difference.z < 0 ? 0 : 0.5f) + Math.Sign(difference.z)*(float)(Math.Asin(sin) / (2 * Math.PI));
        }

        private const string idleClipName = "SingleNozzleTower Idle";
        private const string movingTowardsTargetClipName = "SingleNozzleTower moving towards target";

        private static void makeIdleClip()
        {
            var clip = AnimationClipDatabase.getClip(idleClipName);
            var turretBone = new BasicBone(0,0,new Float4(0,0,0,0), null);
            clip.addOrReplaceCurve(SkeletonData.getBoneId(turretBone),SkeletonAnimationClip.BoneAnimationParameter.XRotation,new ConstantValueCurve(0));
            AnimationClipDatabase.saveClipChangesToDisk(idleClipName);
        }

        private static void makeManufactureClip()
        {
            var clip = AnimationClipDatabase.getClip(movingTowardsTargetClipName);
            var turretBone = new BasicBone(0,0,new Float4(0,0,0,0), null);
            clip.addOrReplaceCurve(SkeletonData.getBoneId(turretBone),SkeletonAnimationClip.BoneAnimationParameter.YRotation,new FirstArgumentDependentCurve());
            AnimationClipDatabase.saveClipChangesToDisk(movingTowardsTargetClipName);
        }

        static TurretRotator()
        {
            makeIdleClip();
            makeManufactureClip();
        }

        public TurretRotator(IGunkObject parent) : base(parent)
        {
            parent.animator.playClip(idleClipName);
        }

        public override void destroy()
        {}

        public void rotateTowardsTarget(Vector3 targetPosition)
        {
            if (parent.animator.currentClip == idleClipName)
            {
                parent.animator.playClip(movingTowardsTargetClipName, calculateManufactureClipArgument(targetPosition));
            }
            else
            {
                parent.animator.changeClipArgument(calculateManufactureClipArgument(targetPosition));
            }
        }

        public void cancelTarget()
        {
            parent.animator.playClip(idleClipName);
        }

        public bool facesTarget()
        {
            return parent.animator.isCurrentClipLoaded() && parent.animator.currentClip == movingTowardsTargetClipName;
        }
    }
}