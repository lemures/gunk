﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gunk_Scripts.Data_Structures
{
    public class ClosestObjectFinder
    {
        private int areaWidth;
        private int areaHeight;

        /// <summary>
        /// contains position and id of a single object
        /// </summary>
        [Serializable]
        private struct ObjectData
        {
            public readonly int id;
            public readonly float x;
            public readonly float y;
            //keeps cached value of x^2+y^2, enables faster calculations of distance
            public readonly float x2PlusY2;

            public ObjectData(int id, float x, float y)
            {
                this.id = id;
                this.x = x;
                this.y = y;
                x2PlusY2 = x * x + y * y;
            }
        }

        /// <summary>
        /// cartesian plane divided into squares with list of objects in each square
        /// </summary>
        private class Layer
        {
            private readonly List<ObjectData>[,] fields;
            private readonly SortedDictionary<int, ObjectData> positions=new SortedDictionary<int, ObjectData>();
            private readonly int squareDimensions;
            public Layer(int squareDimensions,int totalDimensionX,int totalDimensionY)
            {
                fields=new List<ObjectData>[3+totalDimensionX/squareDimensions,3+totalDimensionY/squareDimensions];
                for (var i = 0; i < 3 + totalDimensionX / squareDimensions; i++)
                {
                    for (var q = 0; q < 3 + totalDimensionY / squareDimensions; q++)
                    {
                        fields[i, q] = new List<ObjectData>();
                    }
                }
                this.squareDimensions = squareDimensions;
            }

            private Vector2Int getObjectLocationInArray(float x, float y)
            {
                return new Vector2Int(1 + (int)(x / squareDimensions), 1 + (int)(y / squareDimensions));
            }

            public void addObject(int id, float x,float y)
            {
                var myLocation = getObjectLocationInArray(x, y);
                var position = new ObjectData(id, x, y);
                fields[myLocation.x, myLocation.y].Add(position);
                positions[id] = position;
            }

            public void removeObject(int id)
            {
                var position = positions[id];
                var location = getObjectLocationInArray(position.x, position.y);
                positions.Remove(id);
                fields[location.x, location.y].Remove(position);
            }

            public void updatePosition(int id, float x,float y)
            {
                var position = positions[id];
                if ( Math.Abs(position.x - x) < 0.5f
                     && Math.Abs(position.y - y) < 0.5f)
                { return; }
                removeObject(id);
                addObject(id, x, y);
            }

            public FoundObject getClosestObject(Float2 position)
            {
                var result = int.MaxValue;
                var closest = float.MaxValue;

                var myField = getObjectLocationInArray(position.x, position.y);
                var helper = position.x * position.x + position.y * position.y;
                var doublePositionX = 2 * position.x;
                var doublePositionY = 2 * position.y;

                for (var i = -1; i <= 1; i++)
                {
                    for (var q = -1; q <= 1; q++)
                    {
                        foreach(var otherPosition in fields[myField.x+i,myField.y+q])
                        {
                            var distance = helper + otherPosition.x2PlusY2 - doublePositionX * otherPosition.x -
                                           doublePositionY * otherPosition.y;
                            if (distance<closest)
                            {
                                closest = distance;
                                result = otherPosition.id;
                            }
                        }
                    }
                }

                // ReSharper disable once CompareOfFloatsByEqualityOperator
                return result == int.MaxValue ? null : new FoundObject((float)Math.Sqrt(closest) , result);
            }
        }

        private readonly List<Layer> layers = new List<Layer>();
        private readonly SortedSet<int>existingObjects = new SortedSet<int>();

        /// <summary>
        /// utility for finding closest objects
        /// </summary>
        /// <param name="areaWidth">max possible x coordinate of object that can be added</param>
        /// <param name="areaHeight">max possible x coordinate of object that can be added</param>
        public ClosestObjectFinder(int areaWidth, int areaHeight)
        {
            this.areaHeight = areaHeight;
            this.areaWidth = areaWidth;
            //if it was always 1, initialization time would be horribly long because of memory footprint
            float power = Math.Min(areaHeight,areaWidth) <= 100 ? 1 : 16;
            while (power/2 <= areaHeight || power/2 <= areaWidth)
            {
                layers.Add(new Layer((int)power, areaWidth, areaHeight));
                power *= 2;
            }
        }

        private bool checkBoundaries(Float2 position)
        {
            return position.x >= 0 &&
                   position.y >= 0 &&
                   position.x < areaWidth &&
                   position.y < areaHeight;
        }

        private void throwErrorIfOutsideBoundaries(Float2 position)
        {
            if (!checkBoundaries(position))
            {
                throw new ArgumentOutOfRangeException("position "+position+ " is outside rectangle where distances can be calculated" );
            }
        }

        private void throwErrorIfObjectDoesNotExist(int id)
        {
            if (!existingObjects.Contains(id))
            {
                throw new ArgumentException("this id " + id + "doesn't exist");
            }
        }

        private void throwErrorIfObjectExists(int id)
        {
            if (existingObjects.Contains(id))
            {
                throw new ArgumentException("this id " + id + "already exists");
            }
        }

        /// <summary>
        /// adds object with certain id and position
        /// </summary>
        /// <exception cref="ArgumentException">if object already exists</exception>
        /// <exception cref="ArgumentOutOfRangeException">if position is not in rectangle for which object was created</exception>
        public void addObject(int id, Float2 position)
        {
            throwErrorIfOutsideBoundaries(position);
            throwErrorIfObjectExists(id);

            existingObjects.Add(id);
            foreach (var layer in layers)
            {
                layer.addObject(id, position.x, position.y);
            }
        }

        /// <summary>
        /// removes object with certain id and position
        /// </summary>
        /// <exception cref="ArgumentException">if object does not exist</exception>
        public void removeObject(int id)
        {
            throwErrorIfObjectDoesNotExist(id);

            existingObjects.Remove(id);
            foreach (var layer in layers)
            {
                layer.removeObject(id);
            }
        }

        /// <summary>
        /// updates object's position
        /// </summary>
        /// <exception cref="ArgumentException">if object does not exist</exception>
        /// <exception cref="ArgumentOutOfRangeException">if position is not in rectangle for which object was created</exception>
        public void updatePosition(int id,Float2 newPosition)
        {
            throwErrorIfOutsideBoundaries(newPosition);
            throwErrorIfObjectDoesNotExist(id);

            foreach (var layer in layers)
            {
                layer.updatePosition(id,newPosition.x,newPosition.y);
            }
        }

        /// <summary>
        /// finds closest added object that has distance smaller than maxDistance or null if there are none
        /// </summary>
        public FoundObject findObject(Float2 position, float maxDistance)
        {
            float powerOfTwo = 1;
            foreach (var layer in layers)
            {
                if (2*powerOfTwo>maxDistance+1)
                {
                    return null;
                }
                var closest=layer.getClosestObject(position);
                if (closest != null)
                {
                    closest.distance = closest.distance;
                    return closest.distance>maxDistance? null : closest;
                }

                powerOfTwo *= 2;
            }
            return null;
        }
    }
}