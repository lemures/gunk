﻿using System;
using UnityEngine;

namespace Gunk_Scripts.Data_Structures
{
    [Serializable]
    public struct Command
    {
        public int commandType;
        public int data12;
        public int data13;
        public int data14;
        public int data21;
        public int data22;
        public int data23;
        public int data24;
        public Float4 data3;
        public Float4 data4;
        public Command(Vector4 data3, Vector4 data4, int commandType = 0, int data12 = 0, int data13 = 0, int data14 = 0, int data21 = 0, int data22 = 0, int data23 = 0, int data24 = 0)
        {
            this.commandType = commandType;
            this.data12 = data12;
            this.data13 = data13;
            this.data14 = data14;
            this.data21 = data21;
            this.data22 = data22;
            this.data23 = data23;
            this.data24 = data24;
            this.data3 = new Float4(data3);
            this.data4 = new Float4(data4);
        }
        public Command(int commandType = 0, int data12 = 0, int data13 = 0, int data14 = 0, int data21 = 0, int data22 = 0, int data23 = 0, int data24 = 0)
        {
            this.commandType = commandType;
            this.data12 = data12;
            this.data13 = data13;
            this.data14 = data14;
            this.data21 = data21;
            this.data22 = data22;
            this.data23 = data23;
            this.data24 = data24;
            data3 = new Float4(new Vector4(0, 0, 0, 0));
            data4 = new Float4(new Vector4(0, 0, 0, 0));
        }
    }
}
