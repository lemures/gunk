﻿namespace Gunk_Scripts.Data_Structures
{
    [System.Serializable]
    public struct Edge
    {
        public readonly int x;
        public readonly int y;

        public Edge(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override int GetHashCode() => (x * 10000 + y);

        public override bool Equals(object obj)
        {
            if (obj?.GetType() != typeof(Edge)) return base.Equals(obj);

            var edge = (Edge)obj;
            return ((x == edge.x && y == edge.y) || (x == edge.y && y == edge.x));
        }
    }
}

