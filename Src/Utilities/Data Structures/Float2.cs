﻿using System;
using UnityEngine;

namespace Gunk_Scripts.Data_Structures
{
    ///<summary>
    ///Helper class for serialization of basic unity built-in types
    ///</summary>
    [Serializable]
    public struct Float2
    {
        public float x;
        public float y;

        public Float2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }


        public static Float2 operator + (Float2 a, Float2 b)
        {
            return new Float2(a.x + b.x, a.y + b.y);
        }

        public static Float2 operator -(Float2 a, Float2 b)
        {
            return new Float2(a.x - b.x, a.y - b.y);
        }

        public static Float2 operator -(Float2 a)
        {
            return new Float2(-a.x, -a.y);
        }

        public static Float2 operator *(Float2 a, float b)
        {
            return new Float2(a.x * b, a.y * b);
        }

        public static Float2 operator *(float a, Float2 b)
        {
            return new Float2(b.x * a, b.y * a);
        }

        public static implicit operator Float2(Vector2 val)
        {
            return new Float2(val.x,val.y);
        }

        public static implicit operator Vector2(Float2 val)
        {
            return new Vector2(val.x, val.y);
        }

        public static float distance(Float2 a, Float2 b)
        {
            return (float)Math.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
        }

        public override string ToString()
        {
            return "{" + PrettyPrinter.reformatFloat(x) + " "+ PrettyPrinter.reformatFloat(y) + "}";
        }

        public float distance(Float2 other)
        {
            return (float)Math.Sqrt((x - other.x)*(x - other.x) + (y - other.y)*(y - other.y));
        }
    }
}