﻿using System;
using UnityEngine;

namespace Gunk_Scripts.Data_Structures
{
    ///<summary>
    ///Helper class for serialization of basic unity built-in types
    ///</summary>
    [Serializable]
    public struct Float4
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public Float4(Vector4 data)
        {
            x = data.x;
            y = data.y;
            z = data.z;
            w = data.w;
        }

        public Float4(Vector3 data)
        {
            x = data.x;
            y = data.y;
            z = data.z;
            w = 0;
        }
        public Float4(Quaternion data)
        {
            x = data.x;
            y = data.y;
            z = data.z;
            w = data.w;
        }
        public Float4(Color data)
        {
            x = data.r;
            y = data.g;
            z = data.b;
            w = data.a;
        }

        public Float4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public static implicit operator Float4(Vector4 val) => new Float4(val);

        public static implicit operator Vector4(Float4 val) => new Vector4(val.x, val.y,val.z,val.w);

        public static implicit operator Float4(Quaternion val) => new Float4(val);

        public static implicit operator Quaternion(Float4 val) => new Quaternion(val.x, val.y,val.z,val.w);

        public static implicit operator Float4(Color val) => new Float4(val);

        public static implicit operator Color(Float4 val) => new Color(val.x, val.y,val.z,val.w);

        public static implicit operator Float4(Vector3 val) => new Float4(val);

        public static implicit operator Vector3(Float4 val) => new Vector3(val.x, val.y,val.z);

        public override string ToString()
        {
            return "{" + x + " " + y + " " + z + " " + w + " }";
        }
    }
}