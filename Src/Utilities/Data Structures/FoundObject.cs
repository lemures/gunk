﻿namespace Gunk_Scripts
{
    public class FoundObject
    {
        public float distance;
        public readonly int id;

        public FoundObject(float distance, int id)
        {
            this.distance = distance;
            this.id = id;
        }

        public override string ToString()
        {
            return "id: " + id + " distance: " + distance;
        }
    }
}