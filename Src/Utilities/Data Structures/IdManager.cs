﻿using System.Collections.Generic;

namespace Gunk_Scripts.Data_Structures
{
    /// <summary>
    /// Class for managing unique IDs
    /// </summary>
    [System.Serializable]
    public class IdManager
    {
        private readonly SortedSet<int> freeIDs=new SortedSet<int>();
        private readonly SortedSet<int> takenIDs=new SortedSet<int>();
        private int lastIdTaken = 0;

        /// <summary>
        /// finds first free non-negative not used id and marks it as used
        /// </summary>
        /// <returns></returns>
        public int getFreeId()
        {
            if (freeIDs.Count == 0)
            {
                lastIdTaken++;
                takenIDs.Add(lastIdTaken - 1);
                return lastIdTaken - 1;
            }
            var firstFree = freeIDs.Min;
            freeIDs.Remove(firstFree);
            takenIDs.Add(firstFree);
            return firstFree;
        }

        /// <summary>
        /// marks certain id as not used
        /// </summary>
        /// <param name="id"></param>
        public void deregisterId(int id)
        {
            if (!takenIDs.Contains(id))
            {
                throw new System.InvalidOperationException("number " + id + " was never registered");
            }
            takenIDs.Remove(id);
            freeIDs.Add(id);
        }

        public void deregisterAllIds()
        {
            freeIDs.Clear();
            takenIDs.Clear();
            lastIdTaken = 0;
        }

        /// <summary>
        /// returns max taken id or -1 if none were taken
        /// </summary>
        /// <returns></returns>
        public int getMaxTakenId() => takenIDs.Count!=0?takenIDs.Max:-1;

        public List<int> getAllUsedIDs()=>new List<int>(takenIDs);
    }
}