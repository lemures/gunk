using System;

namespace Src.Utilities.Data_Structures
{
    [Serializable]
    public class IntObject
    {
        public int value;

        public IntObject(int value)
        {
            this.value = value;
        }
    }
}