﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Gunk_Scripts.Data_Structures
{
    ///<summary>
    ///Fast data structure for inserting and erasing keys with an operation of getting min. key, it can store integer keys from range [0,max_key]
    ///</summary>
    [Serializable]
    public class IntervalTree : ISerializable
    {
        ///<summary>
        ///Array of values in interval tree nodes
        ///</summary>
        private readonly int[] _minTab;
        private bool _changed = true;
        private readonly int _size;
        private readonly int _maxKey;

        private readonly int[] _allElements;
        private int _lastElement;

        ///<summary>
        ///Too large or negative key was tried to be inserted in tree
        ///</summary>
        [Serializable]
        private class InvalidArgumentException : Exception
        {
        }

        public IntervalTree(int maxKey)
        {
            _size = 1;
            while (_size <= maxKey)
            {
                _size *= 2;
            }
            _minTab = new int[_size * 2 + 2];
            for (var i = 0; i < _minTab.Length; i++)
            { _minTab[i] = maxKey + 1; }
            _allElements = new int[maxKey + 1];
            _maxKey = maxKey;
        }

        ///<summary>
        ///Returns pair of number of keys and array of keys
        ///</summary>
        public KeyValuePair<int, int[]> getAllElements()
        {
            if (_changed)
            {
                _lastElement = 0;
                findInChildren(1);
            }
            _changed = false;
            return new KeyValuePair<int, int[]>(_lastElement, _allElements);
        }

        ///<summary>
        ///Adds all keys from its' child nodes, goes down only if needed
        ///</summary>
        private void findInChildren(int num)
        {
            if (num * 2 >= _minTab.Length)
            {
                if (_minTab[num] == _maxKey + 1) return;

                _allElements[_lastElement] = _minTab[num];
                _lastElement++;
            }
            else
            {
                if (_minTab[num * 2] != _maxKey + 1)
                {
                    findInChildren(num * 2);
                }
                if (_minTab[num * 2 + 1] != _maxKey + 1)
                {
                    findInChildren(num * 2 + 1);
                }
            }
        }

        ///<summary>
        ///Inserts key to container
        ///</summary>
        public void insert(int num)
        {

            if (num > _maxKey || num < 0)
            {
                throw new InvalidArgumentException();
            }
            _minTab[_size + num] = num;
            updateTree(num);
        }

        ///<summary>
        ///Erases certain key from container
        ///</summary>
        public void erase(int num)
        {
            if (num > _maxKey || num < 0)
            {
                throw new InvalidArgumentException();
            }
            _minTab[_size + num] = _maxKey + 1;
            updateTree(num);
        }

        private void updateTree(int changedIndex)
        {
            _changed = true;
            var num = (_size + changedIndex);
            while (num / 2 != 0)
            {
                _minTab[num / 2] = _minTab[(num / 2) * 2 + 1] < _minTab[(num / 2) * 2] ? _minTab[(num / 2) * 2 + 1] : _minTab[(num / 2) * 2];
                num /= 2;
            }
        }

        ///<summary>
        ///Returns smallest key or undefinied value if there are no keys inserted
        ///</summary>
        public int findMin()
        {
            return _minTab[1];
        }

        protected IntervalTree(SerializationInfo info, StreamingContext context)
        {
            _size = info.GetInt32("size");
            _maxKey = info.GetInt32("max_key");
            _minTab = new int[_size * 2 + 2];
            for (var i = 0; i < _minTab.Length; i++)
            { _minTab[i] = _maxKey + 1; }
            _allElements = new int[_maxKey + 1];
            var elements = (List<int>)info.GetValue("elements", typeof(List<int>));
            foreach (var val in elements)
            {
                insert(val);
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("max_key", _maxKey);
            info.AddValue("size", _size);
            info.AddValue("elements", getAllElements().Value.ToList().GetRange(0, _lastElement));
        }
    }
}
