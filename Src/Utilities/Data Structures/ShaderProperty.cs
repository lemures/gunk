﻿using System;
using System.Runtime.Serialization;
using UnityEngine;

namespace Gunk_Scripts.Data_Structures
{
    [Serializable]
    public class ShaderProperty:ISerializable
    {
        [Serializable]
        public enum FieldType
        {
            IntType,
            FloatType,
            Vector4Type,
            Matrix4X4Type
        };
        public readonly string name;
        public Matrix4x4 val;
        public readonly FieldType type;
        public ShaderProperty(int myVal, string myName)
        {
            type = FieldType.IntType;
            val.m00 = myVal + 0.1f;
            name = myName;
        }
        public ShaderProperty(float myVal, string myName)
        {
            type = FieldType.FloatType;
            val.m00 = myVal;
            name = myName;
        }
        public ShaderProperty(Vector4 myVal, string myName)
        {
            type = FieldType.Vector4Type;
            val.SetRow(0, myVal);
            name = myName;
        }
        public ShaderProperty(Matrix4x4 myVal, string myName)
        {
            type = FieldType.Matrix4X4Type;
            val = myVal;
            name = myName;
        }

        public override string ToString()
        {
            switch (type)
            {
                case FieldType.IntType:
                    return val.m00.ToString();
                case FieldType.FloatType:
                    return val.m00.ToString();
                case FieldType.Vector4Type:
                    return val.GetRow(0).ToString();
                case FieldType.Matrix4X4Type:
                    return val.ToString();
            }

            return "";
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("values0", new Float4(val.GetColumn(0)));
            info.AddValue("values1", new Float4(val.GetColumn(1)));
            info.AddValue("values2", new Float4(val.GetColumn(2)));
            info.AddValue("values3", new Float4(val.GetColumn(3)));
            info.AddValue("fieldType", type);
            info.AddValue("name", name);
        }

        public ShaderProperty(SerializationInfo info, StreamingContext context)
        {
            name = info.GetString("name");
            type = (FieldType)info.GetValue("fieldType",typeof(FieldType));
            val = new Matrix4x4(
               ((Float4)info.GetValue("values0", typeof(Float4))),
               ((Float4)info.GetValue("values1", typeof(Float4))),
               ((Float4)info.GetValue("values2", typeof(Float4))),
               ((Float4)info.GetValue("values3", typeof(Float4)))
            );
        }
    }
}