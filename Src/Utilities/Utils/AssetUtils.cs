﻿#if UNITY_EDITOR
#endif
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;
using Object = UnityEngine.Object;

namespace Gunk_Scripts
{
    [System.Serializable]
    public class AssetUtils
    {
        private static List<string> getModelFileExtensions()
        {
            return new List<string>() {".fbx", ".obj", ".3ds", ".dxf", ".dae", "skp"};
        }

        private static List<string> getTexureFileExtensions()
        {
            return new List<string>() {".jpg", ".jpeg", ".png", ".bmp", ".tga",".tiff",".psd"};
        }

        private static bool isModelExtension(string assetPath)
        {
            return getModelFileExtensions().Contains(FileManager.getFileExtension(assetPath));
        }


        private static List<KeyValuePair<string,Object>> findAssetsWithExtensions(string filePath,List<string>allowedExtensions)
        {
#if UNITY_EDITOR
            var res = (from file in FileManager.getFilesInDir(filePath)
                where allowedExtensions.Contains(FileManager.getFileExtension(file))
                select new KeyValuePair<string,Object>(file, AssetDatabase.LoadAssetAtPath<ComputeShader>(file))).ToList();

            foreach (var directory in FileManager.getFoldersInDir(filePath))
            {
                res.AddRange(findAssetsWithExtensions(directory,allowedExtensions));
            }
            return res;
#else
            return new List<KeyValuePair<string,Object>>();
#endif
        }

        /// <summary>
        /// Returns list of pairs of file paths and assets from that path, only works in Editor, path needs to be relative to the project folder
        /// </summary>
        public static List<KeyValuePair<string,Object>> findAllAssetsOfTypeWithPaths(System.Type T,string filePath="Assets")
        {
            var time = System.DateTime.Now;
#if UNITY_EDITOR
            Profiler.BeginSample("Finding all assets of type " + T + " at path " + filePath);
            string filter = "";
            if (T == typeof(Texture2D))
            {
                filter = "t:Texture2D";
            }

            if (T == typeof(ComputeShader))
            {
                double timeElapsedMs2 = (System.DateTime.Now - time).TotalMilliseconds;
                Profiler.EndSample();
                return findAssetsWithExtensions(filePath,new List<string>(){".compute"});
            }
            if (T == typeof(Mesh))
            {
                filter = "t:Mesh";
            }
            var myAssetsGuiDs = AssetDatabase.FindAssets(filter,new[] {filePath});
            var myAssetsPaths = new List<string>();
            foreach (var assetGuid in myAssetsGuiDs)
            {
                myAssetsPaths.Add(AssetDatabase.GUIDToAssetPath(assetGuid));
            }
            var res = new List<KeyValuePair<string,Object>>();
            var processedPaths = new Dictionary<string,bool>();
            foreach (var assetPath in myAssetsPaths)
            {
                if (!processedPaths.ContainsKey(assetPath))
                {
                    processedPaths[assetPath] = true;
                    if (T == typeof(Mesh) && isModelExtension(assetPath))
                    {
                        var allAssets = AssetDatabase.LoadAllAssetsAtPath(assetPath);

                        foreach (var myAsset in allAssets)
                        {
                            if (myAsset is MeshFilter)
                            {
                                res.Add(new KeyValuePair<string, Object>(assetPath, ((MeshFilter)myAsset).sharedMesh));
                            }
                        }
                    }
                    else
                    {
                        //BUG unity sometimes doesn't like calling that function below for empty files
                        var assetType = AssetDatabase.GetMainAssetTypeAtPath(assetPath);
                        if (assetType == null || assetType.ToString() != T.ToString()) continue;

                        var myObject = AssetDatabase.LoadAssetAtPath(assetPath, T);
                        res.Add(new KeyValuePair<string, Object>(assetPath, myObject));
                    }
                }
            }

            double timeElapsedMs = (System.DateTime.Now - time).TotalMilliseconds;
            Debug.Log(timeElapsedMs + "   " +  T.ToString());
            Profiler.EndSample();
            return res;
#else
        return new List<KeyValuePair<string,Object>>();
#endif
        }

        public static List<Object> findAllAssetsOfType(System.Type T, string filePath = "Assets")
        {
            return (from asset in findAllAssetsOfTypeWithPaths(T, filePath) select asset.Value).ToList();
        }
    }
}
