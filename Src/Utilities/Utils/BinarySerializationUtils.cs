﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Gunk_Scripts
{
    public class BinarySerializationUtils
    {
        public static byte[] serializeObject(object objectToSerialize)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream();
            formatter.Serialize(memoryStream, objectToSerialize);
            return memoryStream.GetBuffer();
        }

        public static object deserializeObject(byte[] data)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream(data);
            return formatter.Deserialize(memoryStream);
        }

        public static T cloneObject<T>(T objectToClone) where T:class
        {
            var bytes=serializeObject(objectToClone);
            return deserializeObject(bytes) as T;
        }
    }
}
