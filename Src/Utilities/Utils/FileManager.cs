﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Gunk_Scripts
{
    ///<summary>
    ///Helper Class for reading and saving data from files
    ///</summary>
    [Serializable]
    public static class FileManager
    {
        public static void moveFile(string source, string destination)
        {
            File.Move(source, destination);
        }

        public static void moveDirectory(string source, string destination)
        {
            Directory.Move(source, destination);
        }

        /// <summary>
        /// deletes folder at specified path and all files in it
        /// </summary>
        public static void deleteFolder(string path, bool recursive = true)
        {
            try
            {
                Directory.Delete(path, recursive);
            }
            catch (Exception)
            {
                //probably folder just doesn't exist
            }
        }

        /// <summary>
        /// replaces all "\\" with "/" in path and adds missing "/" at end of the path
        /// </summary>
        public static string repairPath(string path)
        {
            string res = "";
            for (int i = 0; i < path.Length; i++)
            {
                if (path[i] == '\\')
                {
                    res=res+'/';
                }

                if (path[i] != '\\')
                {
                    res = res + path[i];
                }
            }
            if (res.Length > 0 && res[res.Length - 1] != '/')
            {
                res = res + '/';
            }
            return res;
        }

        /// <summary>
        /// returns true if the paths point to the same location
        /// </summary>
        public static bool comparePaths(string path1, string path2) => repairPath(path1) == repairPath(path2);

        /// <summary>
        /// Checks if path, no matter folder of file is somewhere in folder
        /// </summary>
        public static bool isPathInDirectory(string folder, string path)
        {
            while (path!="")
            {
                if (comparePaths(path,folder))
                { return true; }
                path = getDirectoryName(path);
            }
            return false;
        }

        public static void createFolder(string path)
        {
            if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
        }

        private static void createMissingContainingFolder(string path)
        {
            var folderPath = getDirectoryName(path);
            Directory.CreateDirectory(folderPath);
        }

        /// <summary>
        /// makes sure that there is no file at path deleting the old file if needed
        /// </summary>
        public static void deleteOldVersion(string path)
        {
            createMissingContainingFolder(path);
            if (File.Exists(path)) File.Delete(path);
        }

        ///<summary>
        ///Opens certain file, puts certain string into it, then closes the file
        ///</summary>
        public static void putStringToFile(string path, string data)
        {
            var myCount = data.Length;
            var dataConverted = new byte[myCount];
            for (var i = 0; i < myCount; i++)
            {
                dataConverted[i] = (byte)data[i];
            }

            putBytesToFile(path, dataConverted);
        }

        ///<summary>
        ///Opens certain file, puts the content of a byte array into it, then closes the file
        ///</summary>
        public static void putBytesToFile(string path, byte[] data)
        {
            createMissingContainingFolder(path);
            deleteOldVersion(path);
            var outputFile = new FileStream(path, FileMode.Create, FileAccess.Write);
            var myCount = data.Length;
            outputFile.Write(data, 0, myCount);
            outputFile.Flush();
            outputFile.Close();
        }

        ///<summary>
        ///Opens certain file and returns all bytes read from it
        ///</summary>
        public static byte[] getBytesFromFile(string path)
        {
            var outputFile = new FileStream(path, FileMode.Open, FileAccess.Read);
            var data = new byte[outputFile.Length];
            outputFile.Read(data, 0, (int)outputFile.Length);
            outputFile.Close();
            return data;
        }

        ///<summary>
        ///Opens certain file and reads all bytes from it and converts them into string
        ///</summary>
        public static string getStringFromFile(string path)
        {
            var rawData = getBytesFromFile(path);
            var data = "";
            if (rawData.Length == 0) return data;

            var rawCharData = new char[rawData.Length];
            for (var i = 0; i < rawData.Length; i++) rawCharData[i] = (char)rawData[i];
            data = new string(rawCharData);
            return data;
        }

        /// <summary>
        /// for a given full file path returns folder path that contains the file (or directory)
        /// </summary>
        public static string getDirectoryName(string path) => Path.GetDirectoryName(path);

        ///<summary>
        ///for a given full file path returns file name with it's extension
        ///</summary>
        public static string getFileName(string path) => Path.GetFileName(path);

        ///<summary>
        ///for a given full file path returns file name
        ///</summary>
        public static string getFileNameWithoutExtension(string path) => Path.GetFileNameWithoutExtension(path);

        /// <summary>
        /// returns file extension
        /// </summary>
        public static string getFileExtension(string path) => Path.GetExtension(path);

        ///<summary>
        ///returns List of full paths of all folders found in directory
        ///</summary>
        public static List<string> getFoldersInDir(string path)
        {
            return Directory.Exists(path) == false ? new List<string>() : Directory.GetDirectories(path).ToList();
        }

        /// <summary>
        /// returns List of full paths of all files found in directory (without .meta files)
        /// </summary>
        public static List<string> getFilesInDir(string path)
        {
            if (Directory.Exists(path)==false) return new List<string>();

            //Unity creates meta files for all their assets and we do not wan't them
            var directoriesIncludingMetaFiles = Directory.GetFiles(path).ToList();

            var result = new List<string>();
            foreach(var filePath in directoriesIncludingMetaFiles)
                if (getFileExtension(filePath)!=".meta")
                    result.Add(filePath);
            return result;
        }

        /// <summary>
        /// serializes object and saves it in certain directory
        /// </summary>
        public static void binarySerializeToFile(string path, object objectToSerialize)
        {
            IFormatter formatter = new BinaryFormatter();
            createMissingContainingFolder(path);
            deleteOldVersion(path);
            Stream stream = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
            try
            {
                formatter.Serialize(stream, objectToSerialize);
            }
            catch (Exception)
            {
                stream.Close();
                throw;
            }
            stream.Close();
        }

        /// <summary>
        /// deserializes object from certain file and returns it
        /// </summary>
        public static object binaryDeserializeFromFile(string path)
        {
            IFormatter formatter = new BinaryFormatter();
            try
            {
                Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);
                try
                {
                    var res = formatter.Deserialize(stream);
                    stream.Close();
                    return res;
                }
                catch (Exception e)
                {
                    stream.Close();
                    return null;
                }
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        public static void deleteFile(string path)
        {
            File.Delete(path);
        }
    }
}