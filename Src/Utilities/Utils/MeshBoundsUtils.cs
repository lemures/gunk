using System.Linq;
using Gunk_Scripts.Asset_Importing;
using UnityEngine;

namespace Gunk_Scripts
{
    public static class MeshBoundsUtils
    {
           /// <summary>
        ///finds radius of a smallest sphere with certain center that fits the whole mesh
        /// </summary>
        /// <returns></returns>
        public static float getMeshRadius(Mesh mesh, Vector3 center)
        {
            var myBounds = getMeshDimensions(mesh);
            float maxRadius = 0;
            foreach (var vertex in mesh.vertices)
            {
                //there are vertices that are deliberately not included in mesh dimensions calculations
                if (myBounds.Contains(vertex))
                maxRadius = Mathf.Max(maxRadius, Vector3.Distance(vertex, center));
            }

            return maxRadius;
        }

        /// <summary>
        /// finds smallest cuboid with faces parallel to main XYZ surfaces that fits the whole mesh
        /// and does not include "base" of a mesh that looks like a building
        /// </summary>
        /// <returns></returns>
        public static Bounds getMeshDimensions(Mesh mesh)
        {
            var firstResult = new Bounds();
            foreach (var vertex in mesh.vertices)
            {
                firstResult.Encapsulate(vertex);
            }

            var finalResult = new Bounds();
            foreach (var vertex in mesh.vertices)
            {
                //Vertex close to base, probably base of a building
                if (Mathf.Abs(vertex.y - (firstResult.center.y - firstResult.extents.y)) > 0.3f)
                {
                    finalResult.Encapsulate(vertex);
                }
            }

            //if it changed too little there is a high probability that it was not a building with such "high base" in the first place
            return firstResult.extents.y - finalResult.extents.y > 1.5f * 0.3f ? finalResult : firstResult;
        }

        public static Bounds getUncorrectedMeshDimensions(Mesh mesh)
        {
            var result = new Bounds();
            foreach (var vertex in mesh.vertices)
            {
                result.Encapsulate(vertex);
            }

            return result;
        }

        /// <summary>
        /// does similar thing as getMeshDimensions but for all meshes in prefab
        /// </summary>
        public static Bounds getPrefabDimensions(Prefab prefab)
        {
            var meshFilters = prefab.go.GetComponentsInChildren<MeshFilter>().ToList();
            var bounds = new Bounds();
            meshFilters.ForEach(filter=>bounds.Encapsulate(getMeshDimensions(filter.sharedMesh)));
            return bounds;
        }
    }
}