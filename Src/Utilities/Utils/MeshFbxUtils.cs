﻿using System.Linq;
using UnityEngine;

namespace Gunk_Scripts
{
    public class MeshFbxUtils
    {
        public static Vector3 rotate90Degrees(Vector3 pos)
        {
            var temp = pos.y;
            pos.y = pos.z;
            pos.z = -temp;
            return pos;
        }

        private static bool isMeshFixed(Mesh mesh) => mesh.name.Contains(suffix);

        //suffix that is added to each "fixed" mesh

        public static string suffix = "_fbxFixed";

        public static void fixMeshRotation(Mesh mesh)
        {
            //nothing to do here
            if (mesh == null || isMeshFixed(mesh))
            {
                return;
            }

            Debug.Log("fixed rotation for mesh named: " + mesh.name);
            mesh.name += suffix;

            mesh.vertices = (from vertex in mesh.vertices.Clone() as Vector3[] select rotate90Degrees(vertex)).ToArray();
        }
    }
}
