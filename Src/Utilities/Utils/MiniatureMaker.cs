﻿using System;
using System.Collections.Generic;
using System.IO;
using Gunk_Scripts.Asset_Importing;
using Gunk_Scripts.Shader_Constants;
using Gunk_Scripts.SkeletonAnimations;
using Src.Libraries;
using Src.SkeletonAnimations;
using Src.SkeletonAnimations.Internal;
using UnityEditor;
using UnityEngine;
using static Gunk_Scripts.MeshBoundsUtils;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
#endif

namespace Gunk_Scripts
{
    [System.Serializable]
    public class MiniatureMaker
    {
        //TODO Different camera angle
        //TODO Support for floating units
        //TODO Take into consideration Limb nodes

        private static readonly Vector3 PhotographingPlaceCenter = new Vector3(-20000, 0, -20000);
        private static readonly Quaternion ModelRotation = Quaternion.Euler(new Vector3(0, 200, 0));

        private static readonly int standardMiniatureSize = 600;

        private static string getMiniaturePath()
        {
            return "Assets/Miniatures/";
        }

        private static string getMiniatureName(string prefabName)
        {
            return prefabName + "_miniature";
        }

        /// <summary>
        /// basically produces a fancy .png miniature of screenshot of a certain object and returns Texture2D copy of created object
        /// </summary>
        public static Texture2D produceMiniature(Prefab prefabToPhotograph)
        {
            if (prefabToPhotograph.go == null)
            {
                throw new NullReferenceException("prefab GameObject is null!");
            }
            var prefabMesh = prefabToPhotograph.go.GetComponentInChildren<MeshFilter>().sharedMesh;
            var meshBounds = getMeshDimensions(prefabMesh);
            var radius = getMeshRadius(prefabMesh, meshBounds.center);

            var prefabName = prefabToPhotograph.name;
            var objectManager=new GameObjectManager(GlobalPrefabsManager.getInstance());
            var extraTranslationY = getMeshDimensions(prefabMesh).extents.y - getUncorrectedMeshDimensions(prefabMesh).extents.y;
            var spawnedId=objectManager.instantiatePrefab(prefabName, PhotographingPlaceCenter - meshBounds.center - new Vector3(0, extraTranslationY, 0), ModelRotation);

            var animator = new SkeletonAnimator(prefabName, objectManager, spawnedId, "");
            AnimatorsManager.sendAnimationDataToShaders();
            animator.destroy();

            var floorMaterial =
                PrefabImporterUtils.createMaterial("walls", ToonStandardConstants.shaderName, new List<string>());
            floorMaterial.color = Color.white;

            var floor = GameObject.CreatePrimitive(PrimitiveType.Cube);
            floor.GetComponent<MeshRenderer>().material = floorMaterial;
            floor.transform.localScale = new Vector3(100.0f, 0.1f, 100.0f);
            floor.transform.position = PhotographingPlaceCenter-meshBounds.extents;

            var wallsMaterial = floorMaterial;

            var backWall = GameObject.CreatePrimitive(PrimitiveType.Cube);
            backWall.GetComponent<MeshRenderer>().material = wallsMaterial;
            backWall.transform.localScale = new Vector3(100.0f, 100.0f, 0.1f);
            backWall.transform.position = PhotographingPlaceCenter + new Vector3(0,0, Mathf.Max(10.0f,meshBounds.extents.z * 5.0f));

            KernelUtils.setInt("FOW_enabled", 0);

            var settings = new ScreenshotMaker.ScreenshotSettings
            {
                position = PhotographingPlaceCenter - new Vector3(0, 0, 6) * radius,
                rotation = new Vector3(0, 0, 0),
                resolutionX = standardMiniatureSize,
                resolutionY = standardMiniatureSize,
                filePath = getMiniaturePath(),
                name = getMiniatureName(prefabName),
                needsCompression = true,
                forcedFov = 20
            };

            ScreenshotMaker.saveScreenshotToFile(settings);

            //a fix so that if we try to make a next Miniature the same frame, only the new one will be visible.
            //This is due to the fact that objects are not destroyed at once
            objectManager.setPosition(spawnedId, PhotographingPlaceCenter * 2);
            objectManager.destroyObject(spawnedId);
            floor.transform.position =new Vector3(0,-100000,0);

            Object.DestroyImmediate(floor);
            backWall.transform.position = new Vector3(0, -100000, 0);
            Object.DestroyImmediate(backWall);

            KernelUtils.setInt("FOW_enabled", GraphicsSettingsManager.currentSettings.forOfWarEnabled?1:0);
            Debug.Log("made miniature for "+prefabName);
            return AssetDatabase.LoadAssetAtPath<Texture2D>(Path.Combine(getMiniaturePath(), getMiniatureName(prefabName)+".png"));
        }
    }
}
