﻿using Gunk_Scripts.Objects_Selection;
using UnityEngine;

namespace Gunk_Scripts
{
    public interface IPointer
    {
        bool isPointerDragging();
        DragRectangle getDragRectangle();
        bool hasJustStoppedDragging();
        bool wasClicked();
        bool wasRightClicked();
        bool wasDoubleClicked();
        Vector2 getPointerPosition();
    }

    public class MouseUtils:MonoBehaviour, IPointer
    {
        private static float secondLastMouseUp = float.MinValue;
        private static float secondLastMouseDown = float.MinValue;

        private static float lastMouseUp = float.MinValue/2.0f;
        private static float lastMouseDown = float.MinValue/2.0f;

        private static float lastRightMouseUp = float.MinValue / 2.0f;
        private static float lastRightMouseDown = float.MinValue / 2.0f;

        private static TerrainCollider myTerrainCollider;

        private static RaycastHit hit;
        private static Ray ray;

        private static bool isDragging = false;
        private static Vector2 dragStartPosition;

        private static Vector3 terrainMousePos;

        private void calculateTerrainMousePosition()
        {
            if (Terrain.activeTerrain == null)
            {
                return;
            }

            if (myTerrainCollider == null)
            {
                myTerrainCollider = Terrain.activeTerrain.GetComponent<TerrainCollider>();
            }

            //TODO break this dependency
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            myTerrainCollider.Raycast(ray, out hit, 10000.0f);
            terrainMousePos = hit.point;
        }

        private static bool isLeftButtonDown;
        private static bool isLeftButtonUp;
        private static bool isLeftButtonPressed;

        private static bool isRightButtonDown;
        private static bool isRightButtonUp;
        private static bool isRightButtonPressed;

        private void updateMousePressesVariables()
        {
            if (Input.GetMouseButtonUp(0))
            {
                secondLastMouseUp = lastMouseUp;
                lastMouseUp = Time.time;
            }

            if (Input.GetMouseButtonDown(0))
            {
                secondLastMouseDown = lastMouseDown;
                lastMouseDown = Time.time;
            }

            if (Input.GetMouseButtonDown(1))
            {
                lastRightMouseDown = Time.time;
            }

            if (Input.GetMouseButtonUp(1))
            {
                lastRightMouseUp = Time.time;
            }

            if (isLeftButtonPressed && isDragging == false)
            {
                isDragging = true;
                dragStartPosition = Input.mousePosition;
            }

            if (hasJustStoppedDragging())
            {
                isDragging = false;
            }

            isLeftButtonDown = Input.GetMouseButtonDown(0);
            isLeftButtonUp = Input.GetMouseButtonUp(0);
            isLeftButtonPressed = Input.GetMouseButton(0);

            isRightButtonDown = Input.GetMouseButtonDown(1);
            isRightButtonUp = Input.GetMouseButtonUp(1);
            isRightButtonPressed = Input.GetMouseButton(1);
        }

        public Vector3 getTerrainMousePos() => terrainMousePos;

        public bool isPointerDragging()=> isDragging;

        public DragRectangle getDragRectangle() => 
            (isDragging || hasJustStoppedDragging()) ? new DragRectangle(dragStartPosition, Input.mousePosition) : null;

        public bool hasJustStoppedDragging() => isLeftButtonUp;

        public bool wasClicked() => isLeftButtonUp && (lastMouseUp - lastMouseDown < 0.3f);

        public bool wasRightClicked() => isRightButtonUp && (lastRightMouseUp - lastRightMouseDown < 0.3f);

        public bool wasDoubleClicked() => (isLeftButtonUp && !isLeftButtonDown) && (lastMouseUp - secondLastMouseDown < 0.7f);

        public Vector2 getPointerPosition() => Input.mousePosition;

        public static MouseUtils instance;

        public void Update()
        {
            calculateTerrainMousePosition();
            updateMousePressesVariables();
        }

        private static GameObject mouseHelper;

        public static void install()
        {
            mouseHelper = new GameObject("MouseUtilsObject");
            instance = mouseHelper.AddComponent<MouseUtils>();
        }

        private MouseUtils()
        {}

    }
}
