﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Gunk_Scripts
{
    public static class ScriptFileCacher
    {
        [Serializable]
        private class CachedProjectFile
        {
            public readonly string path;
            [HideInInspector] public List<string> lines;
            public CachedProjectFile(string path, List<string> lines)
            {
                this.path = path;
                this.lines = lines;
            }
        }

        private static List<CachedProjectFile> _cachedFiles;
        private static string myName="Script File Cacher";

        private static List<CachedProjectFile> cachedFiles
        {
            get
            {
                if (_cachedFiles == null)
                {
                    _cachedFiles = (List<CachedProjectFile>)DatabasesManager.getObjectLatestVersion(myName);
                    if (_cachedFiles == null)
                    {
                        _cachedFiles = new List<CachedProjectFile>();
                        DatabasesManager.saveObject(_cachedFiles, myName);
                    }
                }
                return _cachedFiles;
            }
        }

        private static void updateCachedFileList(string path, List<string> lines)
        {
            foreach (var cachedFile in cachedFiles)
                if (cachedFile.path == path)
                {
                    cachedFile.lines = lines;
                    return;
                }

            cachedFiles.Add(new CachedProjectFile(path, lines));
        }

        private static List<string> findCachedFile(string path)
        {
            foreach (var cachedFile in cachedFiles)
                if (cachedFile.path == path)
                {
                    return cachedFile.lines;
                }

            Debug.Log("Warning! there is no cached file: " + path + "Total number of cached files is: " + cachedFiles.Count);
            return new List<string>();
        }


        /// <summary>
        /// returns a list of lines from a file at path. For a file to be accessed, it must be first called from Editor.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<string[]> getFileLines(string path)
        {
            var rawLines = new List<string>();
            var lines = new List<string[]>();
#if UNITY_EDITOR

            var reader = new StreamReader(path);
            var pom = reader.ReadLine();
            while (pom != null)
            {
                rawLines.Add(pom);
                pom = reader.ReadLine();
            }
            reader.Close();
            updateCachedFileList(path, rawLines);
#endif
            rawLines = findCachedFile(path);
            foreach (var aktLine in rawLines)
            {
                string[] separators = { " ", "	" };
                var pomSplit = aktLine.Split(separators, 10000, StringSplitOptions.RemoveEmptyEntries);
                lines.Add(pomSplit);
            }
            return lines;
        }

        public static void saveDatabase()
        {
            DatabasesManager.saveObject(cachedFiles, myName);
        }

        public static void forgetFile(string path)
        {
#if UNITY_EDITOR
            foreach (var cachedFile in cachedFiles)
                if (cachedFile.path == path)
                {
                    cachedFiles.Remove(cachedFile);
                    return;
                }
#endif
        }
    }
}
