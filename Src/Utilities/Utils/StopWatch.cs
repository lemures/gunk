﻿using System;

namespace Gunk_Scripts
{
    public class StopWatch
    {
        DateTime _lastStartDate;
        DateTime _totalElapsed;
        private bool _isStopped;

        public StopWatch(bool isStopped)
        {
            _lastStartDate = System.DateTime.Now;
            this._isStopped = isStopped;
        }

        public double getMsElapsed()
        {
            return _totalElapsed.Millisecond + (_isStopped ? 0 : (System.DateTime.Now- _lastStartDate ).Milliseconds);
        }

        public double getSecElapsed()
        {
            return _totalElapsed.Second + (_isStopped ? 0 : (System.DateTime.Now- _lastStartDate  ).Seconds);
        }

        /// <summary>
        /// Stops timer ticking
        /// </summary>
        public void stop()
        {
            if (_isStopped==false)
            _totalElapsed += (System.DateTime.Now - _lastStartDate);
            _isStopped = true;
        }

        /// <summary>
        /// Resumes ticking
        /// </summary>
        public void resume()
        {
            _lastStartDate = System.DateTime.Now;
            _isStopped = false;
        }
    }
}