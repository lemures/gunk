﻿namespace Gunk_Scripts
{
    public static class StringHasher
    {
        private static long p1=1000*1000*1000+9;
        private static long alphabetBase=253;

        public static int calculateHash(string str)
        {
            long res = 0;
            foreach (var letter in str)
            {
                res *= alphabetBase;
                res += letter;
                res %= p1;
            }
            return (int)res;
        }
    }
}
