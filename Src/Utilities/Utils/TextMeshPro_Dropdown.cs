﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Gunk_Scripts
{
    internal class ObjectPool2<T> where T : new()
    {
        private readonly Stack<T> _mStack = new Stack<T>();
        private readonly UnityAction<T> _mActionOnGet;
        private readonly UnityAction<T> _mActionOnRelease;

        public int countAll { get; private set; }
        public int countActive { get { return countAll - countInactive; } }
        public int countInactive { get { return _mStack.Count; } }

        public ObjectPool2(UnityAction<T> actionOnGet, UnityAction<T> actionOnRelease)
        {
            _mActionOnGet = actionOnGet;
            _mActionOnRelease = actionOnRelease;
        }

        public T get()
        {
            T element;
            if (_mStack.Count == 0)
            {
                element = new T();
                countAll++;
            }
            else
            {
                element = _mStack.Pop();
            }
            if (_mActionOnGet != null)
                _mActionOnGet(element);
            return element;
        }

        public void release(T element)
        {
            if (_mStack.Count > 0 && ReferenceEquals(_mStack.Peek(), element))
                Debug.LogError("Internal error. Trying to destroy object that is already released to pool.");
            if (_mActionOnRelease != null)
                _mActionOnRelease(element);
            _mStack.Push(element);
        }
    }

    internal static class ListPool2<T>
    {
        // Object pool to avoid allocations.
        private static readonly ObjectPool2<List<T>> SListPool = new ObjectPool2<List<T>>(null, l => l.Clear());

        public static List<T> get()
        {
            return SListPool.get();
        }

        public static void release(List<T> toRelease)
        {
            SListPool.release(toRelease);
        }
    }

    // Base interface for tweeners,
    // using an interface instead of
    // an abstract class as we want the
    // tweens to be structs.
    internal interface ITweenValue
        {
            void tweenValue(float floatPercentage);
            bool ignoreTimeScale { get; }
            float duration { get; }
            bool validTarget();
        }

    // Float tween class, receives the
    // TweenValue callback and then sets
    // the value on the target.
    internal struct FloatTween2 : ITweenValue
    {
        public class FloatTweenCallback : UnityEvent<float> { }

        private FloatTweenCallback _mTarget;
        private float _mStartValue;
        private float _mTargetValue;

        private float _mDuration;
        private bool _mIgnoreTimeScale;

        public float startValue
        {
            get { return _mStartValue; }
            set { _mStartValue = value; }
        }

        public float targetValue
        {
            get { return _mTargetValue; }
            set { _mTargetValue = value; }
        }

        public float duration
        {
            get { return _mDuration; }
            set { _mDuration = value; }
        }

        public bool ignoreTimeScale
        {
            get { return _mIgnoreTimeScale; }
            set { _mIgnoreTimeScale = value; }
        }

        public void tweenValue(float floatPercentage)
        {
            if (!validTarget())
                return;

            var newValue = Mathf.Lerp(_mStartValue, _mTargetValue, floatPercentage);
            _mTarget.Invoke(newValue);
        }

        public void addOnChangedCallback(UnityAction<float> callback)
        {
            if (_mTarget == null)
                _mTarget = new FloatTweenCallback();

            _mTarget.AddListener(callback);
        }

        public bool getIgnoreTimescale()
        {
            return _mIgnoreTimeScale;
        }

        public float getDuration()
        {
            return _mDuration;
        }

        public bool validTarget()
        {
            return _mTarget != null;
        }
    }

    // Tween runner, executes the given tween.
    // The coroutine will live within the given
    // behaviour container.
    internal class TweenRunner2<T> where T : struct, ITweenValue
    {
        protected MonoBehaviour mCoroutineContainer;
        protected IEnumerator mTween;

        // utility function for starting the tween
        private static IEnumerator start(T tweenInfo)
        {
            if (!tweenInfo.validTarget())
                yield break;

            var elapsedTime = 0.0f;
            while (elapsedTime < tweenInfo.duration)
            {
                elapsedTime += tweenInfo.ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
                var percentage = Mathf.Clamp01(elapsedTime / tweenInfo.duration);
                tweenInfo.tweenValue(percentage);
                yield return null;
            }
            tweenInfo.tweenValue(1.0f);
        }

        public void init(MonoBehaviour coroutineContainer)
        {
            mCoroutineContainer = coroutineContainer;
        }

        public void startTween(T info)
        {
            if (mCoroutineContainer == null)
            {
                Debug.LogWarning("Coroutine container not configured... did you forget to call Init?");
                return;
            }

            stopTween();

            if (!mCoroutineContainer.gameObject.activeInHierarchy)
            {
                info.tweenValue(1.0f);
                return;
            }

            mTween = start(info);
            mCoroutineContainer.StartCoroutine(mTween);
        }

        public void stopTween()
        {
            if (mTween != null)
            {
                mCoroutineContainer.StopCoroutine(mTween);
                mTween = null;
            }
        }
    }


    [AddComponentMenu("UI/Dropdown-TextMeshPro", 35)]
    [RequireComponent(typeof(RectTransform))]
    public class TextMeshProDropdown : Selectable, IPointerClickHandler, ISubmitHandler, ICancelHandler
    {
        protected internal class DropdownItem : MonoBehaviour, IPointerEnterHandler, ICancelHandler
        {
            [SerializeField]
            private TextMeshProUGUI _mText;
            [SerializeField]
            private Image _mImage;
            [SerializeField]
            private RectTransform _mRectTransform;
            [SerializeField]
            private Toggle _mToggle;

            public TextMeshProUGUI text { get { return _mText; } set { _mText = value; } }
            public Image image { get { return _mImage; } set { _mImage = value; } }
            public RectTransform rectTransform { get { return _mRectTransform; } set { _mRectTransform = value; } }
            public Toggle toggle { get { return _mToggle; } set { _mToggle = value; } }

            public virtual void OnPointerEnter(PointerEventData eventData)
            {
                EventSystem.current.SetSelectedGameObject(gameObject);
            }

            public virtual void OnCancel(BaseEventData eventData)
            {
                var dropdown = GetComponentInParent<Dropdown>();
                if (dropdown)
                    dropdown.Hide();
            }
        }

        [Serializable]
        public class OptionData
        {
            [SerializeField]
            private string _mText;
            [SerializeField]
            private Sprite _mImage;

            public string text { get { return _mText; } set { _mText = value; } }
            public Sprite image { get { return _mImage; } set { _mImage = value; } }

            public OptionData()
            {
            }

            public OptionData(string text)
            {
                this.text = text;
            }

            public OptionData(Sprite image)
            {
                this.image = image;
            }

            public OptionData(string text, Sprite image)
            {
                this.text = text;
                this.image = image;
            }
        }

        [Serializable]
        public class OptionDataList
        {
            [SerializeField]
            private List<OptionData> _mOptions;
            public List<OptionData> options { get { return _mOptions; } set { _mOptions = value; } }


            public OptionDataList()
            {
                options = new List<OptionData>();
            }
        }

        [Serializable]
        public class DropdownEvent : UnityEvent<int> { }

        // Template used to create the dropdown.
        [SerializeField]
        private RectTransform _mTemplate;
        public RectTransform template { get { return _mTemplate; } set { _mTemplate = value; refresh(); } }

        // Text to be used as a caption for the current value. It's not required, but it's kept here for convenience.
        [SerializeField]
        private TextMeshProUGUI _mCaptionText;
        public TextMeshProUGUI captionText { get { return _mCaptionText; } set { _mCaptionText = value; refresh(); } }

        [SerializeField]
        private Image _mCaptionImage;
        public Image captionImage { get { return _mCaptionImage; } set { _mCaptionImage = value; refresh(); } }

        [Space]

        [SerializeField]
        private TextMeshProUGUI _mItemText;
        public TextMeshProUGUI itemText { get { return _mItemText; } set { _mItemText = value; refresh(); } }

        [SerializeField]
        private Image _mItemImage;
        public Image itemImage { get { return _mItemImage; } set { _mItemImage = value; refresh(); } }

        [Space]

        [SerializeField]
        private int _mValue;

        [Space]

        // Items that will be visible when the dropdown is shown.
        // We box this into its own class so we can use a Property Drawer for it.
        [SerializeField]
        private readonly OptionDataList _mOptions = new OptionDataList();
        public List<OptionData> options
        {
            get { return _mOptions.options; }
            set { _mOptions.options = value; refresh(); }
        }

        [Space]

        // Notification triggered when the dropdown changes.
        [SerializeField]
        private DropdownEvent _mOnValueChanged = new DropdownEvent();
        public DropdownEvent onValueChanged { get { return _mOnValueChanged; } set { _mOnValueChanged = value; } }

        private GameObject _mDropdown;
        private GameObject _mBlocker;
        private readonly List<DropdownItem> _mItems = new List<DropdownItem>();
        private TweenRunner2<FloatTween2> _mAlphaTweenRunner;
        private bool _validTemplate = false;

        // Current value.
        public int value
        {
            get
            {
                return _mValue;
            }
            set
            {
                if (Application.isPlaying && (value == _mValue || options.Count == 0))
                    return;

                _mValue = Mathf.Clamp(value, 0, options.Count - 1);
                refresh();

                // Notify all listeners
                _mOnValueChanged.Invoke(_mValue);
            }
        }

        protected TextMeshProDropdown()
        { }

        protected override void Awake()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                return;
#endif

            _mAlphaTweenRunner = new TweenRunner2<FloatTween2>();
            _mAlphaTweenRunner.init(this);

            if (_mCaptionImage)
                _mCaptionImage.enabled = (_mCaptionImage.sprite != null);

            if (_mTemplate)
                _mTemplate.gameObject.SetActive(false);
        }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            base.OnValidate();

            if (!IsActive())
                return;

            refresh();
        }

#endif

        void refresh()
        {
            if (options.Count == 0)
                return;

            var data = options[Mathf.Clamp(_mValue, 0, options.Count - 1)];

            if (_mCaptionText)
            {
                if (data != null && data.text != null)
                    _mCaptionText.text = data.text;
                else
                    _mCaptionText.text = "";
            }

            if (_mCaptionImage)
            {
                if (data != null)
                    _mCaptionImage.sprite = data.image;
                else
                    _mCaptionImage.sprite = null;
                _mCaptionImage.enabled = (_mCaptionImage.sprite != null);
            }
        }

        private void setupTemplate()
        {
            _validTemplate = false;

            if (!_mTemplate)
            {
                Debug.LogError("The dropdown template is not assigned. The template needs to be assigned and must have a child GameObject with a Toggle component serving as the item.", this);
                return;
            }

            var templateGo = _mTemplate.gameObject;
            templateGo.SetActive(true);
            var itemToggle = _mTemplate.GetComponentInChildren<Toggle>();

            _validTemplate = true;
            if (!itemToggle || itemToggle.transform == template)
            {
                _validTemplate = false;
                Debug.LogError("The dropdown template is not valid. The template must have a child GameObject with a Toggle component serving as the item.", template);
            }
            else if (!(itemToggle.transform.parent is RectTransform))
            {
                _validTemplate = false;
                Debug.LogError("The dropdown template is not valid. The child GameObject with a Toggle component (the item) must have a RectTransform on its parent.", template);
            }
            else if (itemText != null && !itemText.transform.IsChildOf(itemToggle.transform))
            {
                _validTemplate = false;
                Debug.LogError("The dropdown template is not valid. The Item Text must be on the item GameObject or children of it.", template);
            }
            else if (itemImage != null && !itemImage.transform.IsChildOf(itemToggle.transform))
            {
                _validTemplate = false;
                Debug.LogError("The dropdown template is not valid. The Item Image must be on the item GameObject or children of it.", template);
            }

            if (!_validTemplate)
            {
                templateGo.SetActive(false);
                return;
            }

            var item = itemToggle.gameObject.AddComponent<DropdownItem>();
            item.text = _mItemText;
            item.image = _mItemImage;
            item.toggle = itemToggle;
            item.rectTransform = (RectTransform)itemToggle.transform;

            var popupCanvas = getOrAddComponent<Canvas>(templateGo);
            popupCanvas.overrideSorting = true;
            popupCanvas.sortingOrder = 30000;

            getOrAddComponent<GraphicRaycaster>(templateGo);
            getOrAddComponent<CanvasGroup>(templateGo);
            templateGo.SetActive(false);

            _validTemplate = true;
        }

        private static T getOrAddComponent<T>(GameObject go) where T : Component
        {
            var comp = go.GetComponent<T>();
            if (!comp)
                comp = go.AddComponent<T>();
            return comp;
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            show();
        }

        public virtual void OnSubmit(BaseEventData eventData)
        {
            show();
        }

        public virtual void OnCancel(BaseEventData eventData)
        {
            hide();
        }

        // Show the dropdown.
        //
        // Plan for dropdown scrolling to ensure dropdown is contained within screen.
        //
        // We assume the Canvas is the screen that the dropdown must be kept inside.
        // This is always valid for screen space canvas modes.
        // For world space canvases we don't know how it's used, but it could be e.g. for an in-game monitor.
        // We consider it a fair constraint that the canvas must be big enough to contains dropdowns.
        public void show()
        {
            if (!IsActive() || !IsInteractable() || _mDropdown != null)
                return;

            if (!_validTemplate)
            {
                setupTemplate();
                if (!_validTemplate)
                    return;
            }

            // Get root Canvas.
            var list = ListPool2<Canvas>.get();
            gameObject.GetComponentsInParent(false, list);
            if (list.Count == 0)
                return;
            var rootCanvas = list[0];
            ListPool2<Canvas>.release(list);

            _mTemplate.gameObject.SetActive(true);

            // Instantiate the drop-down template
            _mDropdown = createDropdownList(_mTemplate.gameObject);
            _mDropdown.name = "Dropdown List";
            _mDropdown.SetActive(true);

            // Make drop-down RectTransform have same values as original.
            var dropdownRectTransform = _mDropdown.transform as RectTransform;
            dropdownRectTransform.SetParent(_mTemplate.transform.parent, false);

            // Instantiate the drop-down list items

            // Find the dropdown item and disable it.
            var itemTemplate = _mDropdown.GetComponentInChildren<DropdownItem>();

            var content = itemTemplate.rectTransform.parent.gameObject;
            var contentRectTransform = content.transform as RectTransform;
            itemTemplate.rectTransform.gameObject.SetActive(true);

            // Get the rects of the dropdown and item
            var dropdownContentRect = contentRectTransform.rect;
            var itemTemplateRect = itemTemplate.rectTransform.rect;

            // Calculate the visual offset between the item's edges and the background's edges
            var offsetMin = itemTemplateRect.min - dropdownContentRect.min + (Vector2)itemTemplate.rectTransform.localPosition;
            var offsetMax = itemTemplateRect.max - dropdownContentRect.max + (Vector2)itemTemplate.rectTransform.localPosition;
            var itemSize = itemTemplateRect.size;

            _mItems.Clear();

            Toggle prev = null;
            for (var i = 0; i < options.Count; ++i)
            {
                var data = options[i];
                var item = addItem(data, value == i, itemTemplate, _mItems);
                if (item == null)
                    continue;

                // Automatically set up a Toggle state change listener
                item.toggle.isOn = value == i;
                item.toggle.onValueChanged.AddListener(x => onSelectItem(item.toggle));

                // Select current option
                if (item.toggle.isOn)
                    item.toggle.Select();

                // Automatically set up explicit navigation
                if (prev != null)
                {
                    var prevNav = prev.navigation;
                    var toggleNav = item.toggle.navigation;
                    prevNav.mode = Navigation.Mode.Explicit;
                    toggleNav.mode = Navigation.Mode.Explicit;

                    prevNav.selectOnDown = item.toggle;
                    prevNav.selectOnRight = item.toggle;
                    toggleNav.selectOnLeft = prev;
                    toggleNav.selectOnUp = prev;

                    prev.navigation = prevNav;
                    item.toggle.navigation = toggleNav;
                }
                prev = item.toggle;
            }

            // Reposition all items now that all of them have been added
            var sizeDelta = contentRectTransform.sizeDelta;
            sizeDelta.y = itemSize.y * _mItems.Count + offsetMin.y - offsetMax.y;
            contentRectTransform.sizeDelta = sizeDelta;

            var extraSpace = dropdownRectTransform.rect.height - contentRectTransform.rect.height;
            if (extraSpace > 0)
                dropdownRectTransform.sizeDelta = new Vector2(dropdownRectTransform.sizeDelta.x, dropdownRectTransform.sizeDelta.y - extraSpace);

            // Invert anchoring and position if dropdown is partially or fully outside of canvas rect.
            // Typically this will have the effect of placing the dropdown above the Button instead of below,
            // but it works as inversion regardless of initial setup.
            var corners = new Vector3[4];
            dropdownRectTransform.GetWorldCorners(corners);
            var outside = false;
            var rootCanvasRectTransform = rootCanvas.transform as RectTransform;
            for (var i = 0; i < 4; i++)
            {
                var corner = rootCanvasRectTransform.InverseTransformPoint(corners[i]);
                if (!rootCanvasRectTransform.rect.Contains(corner))
                {
                    outside = true;
                    break;
                }
            }
            if (outside)
            {
                RectTransformUtility.FlipLayoutOnAxis(dropdownRectTransform, 0, false, false);
                RectTransformUtility.FlipLayoutOnAxis(dropdownRectTransform, 1, false, false);
            }

            for (var i = 0; i < _mItems.Count; i++)
            {
                var itemRect = _mItems[i].rectTransform;
                itemRect.anchorMin = new Vector2(itemRect.anchorMin.x, 0);
                itemRect.anchorMax = new Vector2(itemRect.anchorMax.x, 0);
                itemRect.anchoredPosition = new Vector2(itemRect.anchoredPosition.x, offsetMin.y + itemSize.y * (_mItems.Count - 1 - i) + itemSize.y * itemRect.pivot.y);
                itemRect.sizeDelta = new Vector2(itemRect.sizeDelta.x, itemSize.y);
            }

            // Fade in the popup
            alphaFadeList(0.15f, 0f, 1f);

            // Make drop-down template and item template inactive
            _mTemplate.gameObject.SetActive(false);
            itemTemplate.gameObject.SetActive(false);

            _mBlocker = createBlocker(rootCanvas);
        }

        protected virtual GameObject createBlocker(Canvas rootCanvas)
        {
            // Create blocker GameObject.
            var blocker = new GameObject("Blocker");

            // Setup blocker RectTransform to cover entire root canvas area.
            var blockerRect = blocker.AddComponent<RectTransform>();
            blockerRect.SetParent(rootCanvas.transform, false);
            blockerRect.anchorMin = Vector3.zero;
            blockerRect.anchorMax = Vector3.one;
            blockerRect.sizeDelta = Vector2.zero;

            // Make blocker be in separate canvas in same layer as dropdown and in layer just below it.
            var blockerCanvas = blocker.AddComponent<Canvas>();
            blockerCanvas.overrideSorting = true;
            var dropdownCanvas = _mDropdown.GetComponent<Canvas>();
            blockerCanvas.sortingLayerID = dropdownCanvas.sortingLayerID;
            blockerCanvas.sortingOrder = dropdownCanvas.sortingOrder - 1;

            // Add raycaster since it's needed to block.
            blocker.AddComponent<GraphicRaycaster>();

            // Add image since it's needed to block, but make it clear.
            var blockerImage = blocker.AddComponent<Image>();
            blockerImage.color = Color.clear;

            // Add Button since it's needed to block, and to close the dropdown when blocking area is clicked.
            var blockerButton = blocker.AddComponent<Button>();
            blockerButton.onClick.AddListener(hide);

            return blocker;
        }

        protected virtual void destroyBlocker(GameObject blocker)
        {
            Destroy(blocker);
        }

        protected virtual GameObject createDropdownList(GameObject template)
        {
            return (GameObject)Instantiate(template);
        }

        protected virtual void destroyDropdownList(GameObject dropdownList)
        {
            Destroy(dropdownList);
        }

        protected virtual DropdownItem createItem(DropdownItem itemTemplate)
        {
            return (DropdownItem)Instantiate(itemTemplate);
        }

        protected virtual void destroyItem(DropdownItem item)
        {
            // No action needed since destroying the dropdown list destroys all contained items as well.
        }

        // Add a new drop-down list item with the specified values.
        private DropdownItem addItem(OptionData data, bool selected, DropdownItem itemTemplate, List<DropdownItem> items)
        {
            // Add a new item to the dropdown.
            var item = createItem(itemTemplate);
            item.rectTransform.SetParent(itemTemplate.rectTransform.parent, false);

            item.gameObject.SetActive(true);
            item.gameObject.name = "Item " + items.Count + (data.text != null ? ": " + data.text : "");

            if (item.toggle != null)
            {
                item.toggle.isOn = false;
            }

            // Set the item's data
            if (item.text)
                item.text.text = data.text;
            if (item.image)
            {
                item.image.sprite = data.image;
                item.image.enabled = (item.image.sprite != null);
            }

            items.Add(item);
            return item;
        }

        private void alphaFadeList(float duration, float alpha)
        {
            var group = _mDropdown.GetComponent<CanvasGroup>();
            alphaFadeList(duration, group.alpha, alpha);
        }

        private void alphaFadeList(float duration, float start, float end)
        {
            if (end.Equals(start))
                return;

            var tween = new FloatTween2 { duration = duration, startValue = start, targetValue = end };
            tween.addOnChangedCallback(setAlpha);
            tween.ignoreTimeScale = true;
            _mAlphaTweenRunner.startTween(tween);
        }

        private void setAlpha(float alpha)
        {
            if (!_mDropdown)
                return;
            var group = _mDropdown.GetComponent<CanvasGroup>();
            group.alpha = alpha;
        }

        // Hide the dropdown.
        public void hide()
        {
            if (_mDropdown != null)
            {
                alphaFadeList(0.15f, 0f);
                StartCoroutine(delayedDestroyDropdownList(0.15f));
            }
            if (_mBlocker != null)
                destroyBlocker(_mBlocker);
            _mBlocker = null;
            Select();
        }

        private IEnumerator delayedDestroyDropdownList(float delay)
        {
            yield return new WaitForSecondsRealtime(delay);
            for (var i = 0; i < _mItems.Count; i++)
            {
                if (_mItems[i] != null)
                    destroyItem(_mItems[i]);
                _mItems.Clear();
            }
            if (_mDropdown != null)
                destroyDropdownList(_mDropdown);
            _mDropdown = null;
        }

        // Change the value and hide the dropdown.
        private void onSelectItem(Toggle toggle)
        {
            if (!toggle.isOn)
                toggle.isOn = true;

            var selectedIndex = -1;
            var tr = toggle.transform;
            var parent = tr.parent;
            for (var i = 0; i < parent.childCount; i++)
            {
                if (parent.GetChild(i) == tr)
                {
                    // Subtract one to account for template child.
                    selectedIndex = i - 1;
                    break;
                }
            }

            if (selectedIndex < 0)
                return;

            value = selectedIndex;
            hide();
        }
    }
}