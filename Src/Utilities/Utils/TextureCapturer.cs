﻿using UnityEngine;

namespace Gunk_Scripts
{
    public static class TextureCapturer
    {
        /// <summary>
        /// returns array of pixels retrieved from a given texture. WARNING! remember to set ColorSpace to linear!
        /// </summary>
        /// <param name="textureToCapture"></param>
        /// <returns></returns>
        public static Color[] getTexturePixels(Texture textureToCapture)
        {
            if (textureToCapture == null)
            {
                throw new System.NullReferenceException("textureToCapture can not be null");
            }
            int pixelsCount = textureToCapture.width * textureToCapture.height;

            if (!KernelUtils.hasBuffer("output"))
            {
                KernelUtils.initializeEmptyBuffer("output", pixelsCount, 4 * 4, false);
            }

            KernelUtils.setInt("textureToCaptureX", textureToCapture.width);
            KernelUtils.setInt("textureToCaptureY", textureToCapture.height);

            KernelUtils.setGlobalTexture("textureToCapture", textureToCapture);
            KernelUtils.dispatch("TextureCapturer", textureToCapture.width, textureToCapture.height);

            var colorsArray = new Color[pixelsCount];
            KernelUtils.getBuffer("output").GetData(colorsArray);

            return colorsArray;
        }
    }
}
