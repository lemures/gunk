﻿using System;
using System.Collections.Generic;
using UnityEngine.Profiling;

namespace Gunk_Scripts
{
    public class UpdateCaller<T>
    {
        private readonly Func<T,bool> isObjectActiveFunction;

        private readonly List<KeyValuePair<T, Action>> _updateFunctionsWithOwners =
            new List<KeyValuePair<T, Action>>();

        public void addFunction(T target, Action function)
        {
            _updateFunctionsWithOwners.Add(new KeyValuePair<T, Action>(target, function));
        }

        public void invokeAll()
        {
            Profiler.BeginSample("Calling update functions in updateCaller "+this.GetHashCode());
            for (int i = 0; i < _updateFunctionsWithOwners.Count; i++)
            {
                var elem = _updateFunctionsWithOwners[i];
                if (elem.Key != null && isObjectActiveFunction(elem.Key))
                {

                    elem.Value();

                }
            }
            Profiler.EndSample();
        }

        public UpdateCaller(Func<T,bool> isObjectActiveFunction)
        {
            this.isObjectActiveFunction = isObjectActiveFunction;
        }

        public void deleteAllObjectFunctions(object target)
        {
            _updateFunctionsWithOwners.RemoveAll(x => x.Key as object == target);
        }
    }
}