﻿using System.Collections;

namespace Gunk_Scripts
{
    public class VariablesReportBuilder
    {
        private readonly string className;
        private readonly string variables;

        private static string printVariableWithName(object variableToPrint, string variableName)
        {
            if (variableToPrint is ICollection)
            {
                return printCollection(variableToPrint, variableName);
            }

            var variableString = variableToPrint?.ToString()??"null";
            if (!variableString.Contains("\n"))
            {
                return variableName + ": " + variableString + "\n";
            }

            return variableName + ":\n " + addTabToEachLine(variableString);
        }

        private static string printCollection(object variableToPrint, string variableName)
        {
            var resultFirstLine = variableName + ": [\n";
            var resultLastLine = "]\n";
            var resultMiddle = "";
            foreach (var element in (IEnumerable) variableToPrint)
            {
                resultMiddle += addTabToEachLine(element?.ToString()??"null")+",\n";
            }

            //removing last unnecessary coma
            if (resultMiddle.Length>1)
            resultMiddle = resultMiddle.Substring(0, resultMiddle.Length - 2) + "\n";

            return resultFirstLine + addTabToEachLine(resultMiddle) + resultLastLine;
        }

        private static string addTabToEachLine(string str)
        {
            var result = "    " + str;
            result = result.Replace("\n", "\n    ");
            //removing last tab
            if (result.Length>=5 && result.Substring(result.Length - 5, 5) == "\n    ")
            {
                result = result.Substring(0, result.Length - 4);
            }
            return result;
        }

        public VariablesReportBuilder(string className)
        {
            this.className = className;
        }

        public VariablesReportBuilder(object variablesOwner)
        {
            this.className = variablesOwner.GetType().Name;
        }

        private VariablesReportBuilder(string className, string variables)
        {
            this.className = className;
            this.variables = variables;
        }

        public VariablesReportBuilder addVariable(object variableToPrint, string variableName)
        {
            return new VariablesReportBuilder(className, variables+ printVariableWithName(variableToPrint, variableName));
        }

        public string print()
        {
            return className + "\n" + addTabToEachLine(variables);
        }
    }
}